﻿using System;

namespace Generic_appWeb.Models
{
    public class Punto
    {
        public int Pu_id { get; set; }
        public string Pu_codigo_empresa { get; set; }
        public string Pu_codigo_alterno { get; set; }
        public string Pu_descripcion_larga { get; set; }
        public string Pu_descripcion_med { get; set; }
        public string Pu_descripcion_corta { get; set; }
        public string Pu_abreviacion { get; set; }
        public string Pu_observacion { get; set; }
        public DateTime Pu_fecha_creacion { get; set; }
        public DateTime Pu_fecha_modificacion { get; set; }
        public Usuario Pu_usuario_creacion { get; set; }
        public Usuario Pu_usuario_modificacion { get; set; }
        public char Pu_estado { get; set; }
        public int Pu_secuencia { get; set; }
        public double Pu_radio { get; set; }
        public double Pu_longitud { get; set; }
        public double Pu_latitud { get; set; }
        public double Pu_altitud { get; set; }
        public double Pu_coordenadas_utmx { get; set; }
        public double Pu_coordenadas_utmy { get; set; }
        public Grupo_punto Grupo_punto { get; set; }
        public Categoria_punto Categoria_punto { get; set; }
        public Tipo_punto Tipo_punto { get; set; }
        public  Sitio Sitio { get; set; }
        public Hacienda Hacienda { get; set; }
    }
}
