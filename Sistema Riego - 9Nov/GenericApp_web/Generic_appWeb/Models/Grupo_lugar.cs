﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_lugar
    {
      
        public int GL_id { get; set; }
        public string GL_codigo_empresa { get; set; }
        public string GL_codigo_alterno { get; set; }
        public string GL_descripcion_larga { get; set; }
        public string GL_descripcion_med { get; set; }
        public string GL_descripcion_corta { get; set; }
        public string GL_abreviacion { get; set; }
        public string GL_observacion { get; set; }
        public DateTime GL_fecha_creacion { get; set; }
        public DateTime GL_fecha_modificacion { get; set; }
        public Usuario GL_usuario_creacion { get; set; }
        public Usuario GL_usuario_modificacion { get; set; }
        public char GL_estado { get; set; }
        public int GL_secuencia { get; set; }
        public Hacienda Hacienda { get; set; }

    }
}
