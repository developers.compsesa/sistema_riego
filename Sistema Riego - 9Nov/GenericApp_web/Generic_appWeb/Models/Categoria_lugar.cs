﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_lugar
    {
    
        public int CL_id { get; set; }
        public string CL_codigo_empresa { get; set; }
        public string CL_codigo_alterno { get; set; }
        public string CL_descripcion_larga { get; set; }
        public string CL_descripcion_med { get; set; }
        public string CL_descripcion_corta { get; set; }
        public string CL_abreviacion { get; set; }
        public string CL_observacion { get; set; }
        public DateTime CL_fecha_creacion { get; set; }
        public DateTime CL_fecha_modificacion { get; set; }
        public Usuario CL_usuario_creacion { get; set; }
        public Usuario CL_usuario_modificacion { get; set; }
        public char CL_estado { get; set; }
        public int CL_secuencia { get; set; }
        public Grupo_lugar Grupo_lugar { get; set; }
        public Hacienda Hacienda { get; set; }

    }
}
