﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_programa_diario
    {
        
        public int TPD_id { get; set; }
        public string TPD_codigo_empresa { get; set; }
        public string TPD_codigo_alterno { get; set; }
        public string TPD_descripcion_larga { get; set; }
        public string TPD_descripcion_med { get; set; }
        public string TPD_descripcion_corta { get; set; }
        public string TPD_abreviacion { get; set; }
        public string TPD_observacion { get; set; }
        public DateTime TPD_fecha_creacion { get; set; }
        public DateTime TPD_fecha_modificacion { get; set; }
        public Usuario TPD_usuario_creacion { get; set; }
        public Usuario TPD_usuario_modificacion { get; set; }
        public char TPD_estado { get; set; }
        public int TPD_secuencia { get; set; }
    }
}
