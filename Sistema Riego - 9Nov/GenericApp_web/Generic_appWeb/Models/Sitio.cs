﻿using System;

namespace Generic_appWeb.Models
{
    public class Sitio
    {
        
        public int Si_id { get; set; }
        public string Si_codigo_empresa { get; set; }
        public string Si_codigo_alterno { get; set; }
        public string Si_descripcion_larga { get; set; }
        public string Si_descripcion_med { get; set; }
        public string Si_descripcion_corta { get; set; }
        public string Si_abreviacion { get; set; }
        public string Si_observacion { get; set; }
        public DateTime Si_fecha_creacion { get; set; }
        public DateTime Si_fecha_modificacion { get; set; }
        public Usuario Si_usuario_creacion { get; set; }
        public Usuario Si_usuario_modificacion { get; set; }
        public char Si_estado { get; set; }
        public int Si_secuencia { get; set; }
        public Grupo_sitio Grupo_sitio { get; set; }
        public Categoria_sitio Categoria_sitio { get; set; }
        public Tipo_sitio Tipo_sitio { get; set; }


        public Area Area { get; set; }
        public Hacienda Hacienda { get; set; }
    }
}
