﻿using System;

namespace Generic_appWeb.Models
{
    public class Descripcion_equipo
    {
        
        public int DEq_id { get; set; }
        public string DEq_codigo_empresa { get; set; }
        public string DEq_codigo_alterno { get; set; }
        public string DEq_descripcion_larga { get; set; }
        public string DEq_descripcion_med { get; set; }
        public string DEq_descripcion_corta { get; set; }
        public string DEq_abreviacion { get; set; }
        public string DEq_observacion { get; set; }
        public DateTime DEq_fecha_creacion { get; set; }
        public DateTime DEq_fecha_modificacion { get; set; }
        public Usuario DEq_usuario_creacion { get; set; }
        public Usuario DEq_usuario_modificacion { get; set; }
        public char DEq_estado { get; set; }
        public int DEq_secuencia { get; set; }
        public Grupo_equipo Grupo_equipo { get; set; }
        public Categoria_equipo Categoria_equipo { get; set; }
        public Tipo_equipo Tipo_equipo { get; set; }
        public Entidad Entidad { get; set; }

    }
}
