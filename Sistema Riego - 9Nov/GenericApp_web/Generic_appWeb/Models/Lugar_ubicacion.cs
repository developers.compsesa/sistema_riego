﻿using System;

namespace Generic_appWeb.Models
{
    public class Lugar_ubicacion
    {
        public int Luu_id { get; set; }
        public string Luu_codigo_empresa { get; set; }
        public string Luu_codigo_alterno { get; set; }
        public string Luu_descripcion_larga { get; set; }
        public string Luu_descripcion_med { get; set; }
        public string Luu_descripcion_corta { get; set; }
        public string Luu_abreviacion { get; set; }
        public string Luu_observacion { get; set; }
        public DateTime Luu_fecha_creacion { get; set; }
        public DateTime Luu_fecha_modificacion { get; set; }
        public Usuario Luu_usuario_creacion { get; set; }
        public Usuario Luu_usuario_modificacion { get; set; }
        public char Luu_estado { get; set; }
        public int Luu_secuencia { get; set; }
        public  Lugar Lugar { get; set; }
        public  Sitio Sitio { get; set; }
        public  Area Area { get; set; }
        public Punto Punto { get; set; }
        public Hacienda Hacienda { get; set; }
    }
}
