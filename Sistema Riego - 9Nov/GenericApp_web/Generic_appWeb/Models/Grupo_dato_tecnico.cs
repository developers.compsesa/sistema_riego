﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_dato_tecnico
    {
        
        public int GDTec_id { get; set; }
        public string GDTec_codigo_empresa { get; set; }
        public string GDTec_codigo_alterno { get; set; }
        public string GDTec_descripcion_larga { get; set; }
        public string GDTec_descripcion_med { get; set; }
        public string GDTec_descripcion_corta { get; set; }
        public string GDTec_abreviacion { get; set; }
        public string GDTec_observacion { get; set; }

        public DateTime GDTec_fecha_creacion { get; set; }
        public DateTime GDTec_fecha_modificacion { get; set; }
        public Usuario GDTec_usuario_creacion { get; set; }
        public Usuario GDTec_usuario_modificacion { get; set; }
        public char GDTec_estado { get; set; }
        public int GDTec_secuencia { get; set; } 

    }
}
