﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_sitio
    {
        
        public int CSit_id { get; set; }
        public string CSit_codigo_empresa { get; set; }
        public string CSit_codigo_alterno { get; set; }
        public string CSit_descripcion_larga { get; set; }
        public string CSit_descripcion_med { get; set; }
        public string CSit_descripcion_corta { get; set; }
        public string CSit_abreviacion { get; set; }
        public string CSit_observacion { get; set; }
        public DateTime CSit_fecha_creacion { get; set; }
        public DateTime CSit_fecha_modificacion { get; set; }
        public Usuario CSit_usuario_creacion { get; set; }
        public Usuario CSit_usuario_modificacion { get; set; }
        public char CSit_estado { get; set; }
        public int CSit_secuencia { get; set; }
        public Grupo_sitio Grupo_sitio { get; set; }
        public Hacienda Hacienda { get; set; }
    }
}
