﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_alerta
    {
        
        public int GAl_id { get; set; }
        public string GAl_codigo_empresa { get; set; }
        public string GAl_codigo_alterno { get; set; }
        public string GAl_descripcion_larga { get; set; }
        public string GAl_descripcion_med { get; set; }
        public string GAl_descripcion_corta { get; set; }
        public string GAl_abreviacion { get; set; }
        public string GAl_observacion { get; set; }

        public DateTime GAl_fecha_creacion { get; set; }
        public DateTime GAl_fecha_modificacion { get; set; }
        public Usuario GAl_usuario_creacion { get; set; }
        public Usuario GAl_usuario_modificacion { get; set; }
        public char GAl_estado { get; set; }
        public int GAl_secuencia { get; set; }        

    }
}
