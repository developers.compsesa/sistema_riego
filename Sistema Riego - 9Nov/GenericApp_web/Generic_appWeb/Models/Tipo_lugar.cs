﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_lugar 
    {
        
        public int TL_id { get; set; }
        public string TL_codigo_empresa { get; set; }
        public string TL_codigo_alterno { get; set; }
        public string TL_descripcion_larga { get; set; }
        public string TL_descripcion_med { get; set; }
        public string TL_descripcion_corta { get; set; }
        public string TL_abreviacion { get; set; }
        public string TL_observacion { get; set; }
        public DateTime TL_fecha_creacion { get; set; }
        public DateTime TL_fecha_modificacion { get; set; }
        public Usuario TL_usuario_creacion { get; set; }
        public Usuario TL_usuario_modificacion { get; set; }
        public int TL_estado { get; set; }
        public int TL_secuencia { get; set; }
        public Categoria_lugar Categoria_lugar { get; set; }
        public Hacienda Hacienda { get; set; }

    }
}
