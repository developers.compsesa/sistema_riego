﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_valor
    {
        
        public int CVal_id { get; set; }
        public string CVal_codigo_empresa { get; set; }
        public string CVal_codigo_alterno { get; set; }
        public string CVal_descripcion_larga { get; set; }
        public string CVal_descripcion_med { get; set; }
        public string CVal_descripcion_corta { get; set; }
        public string CVal_abreviacion { get; set; }
        public string CVal_observacion { get; set; }
        public DateTime CVal_fecha_creacion { get; set; }
        public DateTime CVal_fecha_modificacion { get; set; }
        public Usuario CVal_usuario_creacion { get; set; }
        public Usuario CVal_usuario_modificacion { get; set; }
        public char CVal_estado { get; set; }
        public int CVal_secuencia { get; set; }
        public Grupo_valor Grupo_valor { get; set; }

    }
}
