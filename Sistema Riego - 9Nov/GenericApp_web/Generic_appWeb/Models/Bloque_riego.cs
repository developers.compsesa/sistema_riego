﻿using NpgsqlTypes;
using System;

namespace Generic_appWeb.Models
{
    public class Bloque_riego 
    {
      
        public int BRi_id { get; set; }
        public string BRi_codigo_empresa { get; set; }
        public string BRi_codigo_alterno { get; set; }
        public string BRi_descripcion_larga { get; set; }
        public string BRi_descripcion_med { get; set; }
        public string BRi_descripcion_corta { get; set; }
        public string BRi_abreviacion { get; set; }
        public string BRi_observacion { get; set; }
        public DateTime BRi_fecha_creacion { get; set; }
        public DateTime BRi_fecha_modificacion { get; set; }
        public Usuario BRi_usuario_creacion { get; set; }
        public Usuario BRi_usuario_modificacion { get; set; }
        public char BRi_estado { get; set; }
        public int BRi_secuencia { get; set; }
        public Double BRi_supercifie_hm2 { get; set; }
        public string BRi_densidad_cultivo { get; set; }
        public int BRi_profundidad_raiz { get; set; }
        public string BRi_forma_bloque { get; set; }
        public Double BRi_radio { get; set; }
        public NpgsqlPolygon BRi_puntos_multipoligonos { get; set; }
        public Entidad Entidad { get; set; }
        public Ubicacion_geografica Ubicacion_geografica { get; set; }
        public Lugar_ubicacion Lugar_ubicacion { get; set; }
        public Pendiente Pendiente { get; set; }
        public Tipo_suelo Tipo_suelo { get; set; }
        public Cultivo Cultivo { get; set; }
        public Etapa_cultivo Etapa_cultivo { get; set; }



    }
}
