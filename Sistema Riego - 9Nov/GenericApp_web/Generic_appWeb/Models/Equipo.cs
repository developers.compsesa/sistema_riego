﻿using System;

namespace Generic_appWeb.Models
{
    public class Equipo
    {
        
        public int Eq_id { get; set; }
        public string Eq_codigo_empresa { get; set; } // codigo de la etiqueta
        public string Eq_codigo_alterno { get; set; }
        public string Eq_descripcion_larga { get; set; }
        public string Eq_descripcion_med { get; set; }
        public string Eq_descripcion_corta { get; set; }
        public string Eq_abreviacion { get; set; }
        public string Eq_observacion { get; set; }
        public DateTime Eq_fecha_creacion { get; set; }
        public DateTime Eq_fecha_modificacion { get; set; }
        public Usuario Eq_usuario_creacion { get; set; }
        public Usuario Eq_usuario_modificacion { get; set; }
        public char Eq_estado { get; set; }
        public int Eq_secuencia { get; set; }
        public string Eq_serial { get; set; }
        public string Eq_codigo_EUI { get; set; }
        public  Entidad Entidad { get; set; }
        public Status Status { get; set; }
        public  Descripcion_equipo Descripcion_equipo { get; set; }
        public  Marca_Modelo Marca_Modelo { get; set; }
        public Conjunto_dato_tecnico Conjunto_dato_tecnico { get; set; }
        public Conjunto_parametro Conjunto_parametro { get; set; }
        public  Ubicacion_geografica Ubicacion_geografica { get; set; }
        public  Lugar_ubicacion Lugar_ubicacion { get; set; }



    }
}
