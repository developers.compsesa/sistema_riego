﻿using System;

namespace Generic_appWeb.Models
{
    public partial class Entidad
    {

      
        public int En_id { get; set; }
        public string En_codigo_empresa { get; set; }
        public string En_codigo_alterno { get; set; }
        public string En_descripcion_larga { get; set; }
        public string En_descripcion_med { get; set; }
        public string En_descripcion_corta { get; set; }
        public string En_abreviacion { get; set; }
        public string En_observacion { get; set; }
        public string En_nombre { get; set; }
        public string En_apellido { get; set; }
        public string En_razon_Social{ get; set; }
        public string En_identificacion{ get; set; }
        public DateTime En_fecha_creacion { get; set; }
        public DateTime En_fecha_modificacion { get; set; }
        public Usuario En_usuario_creacion { get; set; }
        public Usuario En_usuario_modificacion { get; set; }
        public char En_estado { get; set; }
        public int En_secuencia { get; set; }
        public Grupo_entidad Grupo_entidad { get; set; }
        public Categoria_entidad Categoria_entidad { get; set; }
        public Tipo_entidad Tipo_entidad { get; set; }

  


    }
}
