﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_area
    {
        
        public int CAre_id { get; set; }
        public string CAre_codigo_empresa { get; set; }
        public string CAre_codigo_alterno { get; set; }
        public string CAre_descripcion_larga { get; set; }
        public string CAre_descripcion_med { get; set; }
        public string CAre_descripcion_corta { get; set; }
        public string CAre_abreviacion { get; set; }
        public string CAre_observacion { get; set; }
        public DateTime CAre_fecha_creacion { get; set; }
        public DateTime CAre_fecha_modificacion { get; set; }
        public Usuario CAre_usuario_creacion { get; set; }
        public Usuario CAre_usuario_modificacion { get; set; }
        public char CAre_estado { get; set; }
        public int CAre_secuencia { get; set; }
        public Grupo_area Grupo_area { get; set; }        
        public Hacienda Hacienda { get; set; }

    }
}
