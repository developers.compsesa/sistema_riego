﻿using System;

namespace Generic_appWeb.Models
{
    public class Parametro
    {

        public int Par_id { get; set; }
        public string Par_codigo_empresa { get; set; }
        public string Par_codigo_alterno { get; set; }
        public string Par_descripcion_larga { get; set; }
        public string Par_descripcion_med { get; set; }
        public string Par_descripcion_corta { get; set; }
        public string Par_abreviacion { get; set; }
        public string Par_observacion { get; set; }
        public DateTime Par_fecha_creacion { get; set; }
        public DateTime Par_fecha_modificacion { get; set; }
        public Usuario Par_usuario_creacion { get; set; }
        public Usuario Par_usuario_modificacion { get; set; }
        public char Par_estado { get; set; }
        public int Par_secuencia { get; set; }
        public Descripcion_parametro Descripcion_parametro { get; set; }
        public Descripcion_unidad Descripcion_unidad { get; set; }
        public Descripcion_valor Descripcion_valor { get; set; }
        public Entidad Entidad { get; set; }

    }
}
