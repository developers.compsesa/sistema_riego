﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_cultivo
    {
        
        public int GCul_id { get; set; }
        public string GCul_codigo_empresa { get; set; }
        public string GCul_codigo_alterno { get; set; }
        public string GCul_descripcion_larga { get; set; }
        public string GCul_descripcion_med { get; set; }
        public string GCul_descripcion_corta { get; set; }
        public string GCul_abreviacion { get; set; }
        public string GCul_observacion { get; set; }

        public DateTime GCul_fecha_creacion { get; set; }
        public DateTime GCul_fecha_modificacion { get; set; }
        public Usuario GCul_usuario_creacion { get; set; }
        public Usuario GCul_usuario_modificacion { get; set; }
        public char GCul_estado { get; set; }
        public int GCul_secuencia { get; set; }
 

    }
}
