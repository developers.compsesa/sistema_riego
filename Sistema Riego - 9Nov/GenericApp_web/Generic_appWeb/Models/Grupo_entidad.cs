﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_entidad
    {
        
        public int GEn_id { get; set; }
        public string GEn_codigo_empresa { get; set; }
        public string GEn_codigo_alterno { get; set; }
        public string GEn_descripcion_larga { get; set; }
        public string GEn_descripcion_med { get; set; }
        public string GEn_descripcion_corta { get; set; }
        public string GEn_abreviacion { get; set; }
        public string GEn_observacion { get; set; }

        public DateTime GEn_fecha_creacion { get; set; }
        public DateTime GEn_fecha_modificacion { get; set; }
        public Usuario GEn_usuario_creacion { get; set; }
        public Usuario GEn_usuario_modificacion { get; set; }
        public char GEn_estado { get; set; }
        public int GEn_secuencia { get; set; }

    }
}
