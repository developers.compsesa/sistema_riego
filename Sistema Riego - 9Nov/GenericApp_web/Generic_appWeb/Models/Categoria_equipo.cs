﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_equipo
    {
    
        public int CE_id { get; set; }
        public string CE_codigo_empresa { get; set; }
        public string CE_codigo_alterno { get; set; }
        public string CE_descripcion_larga { get; set; }
        public string CE_descripcion_med { get; set; }
        public string CE_descripcion_corta { get; set; }
        public string CE_abreviacion { get; set; }
        public string CE_observacion { get; set; }
        public DateTime CE_fecha_creacion { get; set; }
        public DateTime CE_fecha_modificacion { get; set; }
        public Usuario CE_usuario_creacion { get; set; }
        public Usuario CE_usuario_modificacion { get; set; }
        public char CE_estado { get; set; }
        public int CE_secuencia { get; set; }
        public Grupo_equipo Grupo_equipo { get; set; }
        
    }
}
