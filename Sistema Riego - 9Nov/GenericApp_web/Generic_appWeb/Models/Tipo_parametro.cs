﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_parametro
    {
        
        public int TPar_id { get; set; }
        public string TPar_codigo_empresa { get; set; }
        public string TPar_codigo_alterno { get; set; }
        public string TPar_descripcion_larga { get; set; }
        public string TPar_descripcion_med { get; set; }
        public string TPar_descripcion_corta { get; set; }
        public string TPar_abreviacion { get; set; }
        public string TPar_observacion { get; set; }
        public DateTime TPar_fecha_creacion { get; set; }
        public DateTime TPar_fecha_modificacion { get; set; }
        public Usuario TPar_usuario_creacion { get; set; }
        public Usuario TPar_usuario_modificacion { get; set; }
        public int TPar_estado { get; set; }
        public int TPar_secuencia { get; set; }
        public Categoria_parametro Categoria_parametro { get; set; }


    }
}
