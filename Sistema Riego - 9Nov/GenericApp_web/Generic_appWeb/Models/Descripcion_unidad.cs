﻿using System;

namespace Generic_appWeb.Models
{
    public class Descripcion_unidad
    {
        
        public int Un_id { get; set; }
        public string Un_codigo_empresa { get; set; }
        public string Un_codigo_alterno { get; set; }
        public string Un_descripcion_larga { get; set; }
        public string Un_descripcion_med { get; set; }
        public string Un_descripcion_corta { get; set; }
        public string Un_abreviacion { get; set; }
        public string Un_observacion { get; set; }
        public DateTime Un_fecha_creacion { get; set; }
        public DateTime Un_fecha_modificacion { get; set; }
        public Usuario Un_usuario_creacion { get; set; }
        public Usuario Un_usuario_modificacion { get; set; }
        public char Un_estado { get; set; }
        public int Un_secuencia { get; set; }
        public Grupo_unidad Grupo_unidad { get; set; }
        public Categoria_unidad Categoria_unidad { get; set; }
        public Tipo_unidad Tipo_unidad { get; set; }
        public Hacienda Hacienda { get; set; }   
    }
}
