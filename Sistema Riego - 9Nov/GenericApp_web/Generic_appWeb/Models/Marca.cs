﻿using System;

namespace Generic_appWeb.Models
{
    public class Marca
    {
       
        public int Ma_id { get; set; }
        public string Ma_codigo_empresa { get; set; }
        public string Ma_codigo_alterno { get; set; }
        public string Ma_descripcion_larga { get; set; }
        public string Ma_descripcion_med { get; set; }
        public string Ma_descripcion_corta { get; set; }
        public string Ma_abreviacion { get; set; }
        public string Ma_observacion { get; set; }
        public DateTime Ma_fecha_creacion { get; set; }
        public DateTime Ma_fecha_modificacion { get; set; }
        public Usuario Ma_usuario_creacion { get; set; }
        public Usuario Ma_usuario_modificacion { get; set; }
        public char Ma_estado { get; set; }
        public int Ma_secuencia { get; set; }
        public Grupo_marca Grupo_marca { get; set; }
        public Categoria_marca Categoria_marca { get; set; }
        public Tipo_marca Tipo_marca { get; set; }

    }
}
