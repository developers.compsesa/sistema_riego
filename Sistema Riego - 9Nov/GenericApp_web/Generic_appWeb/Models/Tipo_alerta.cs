﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_alerta
    {
        
        public int TAl_id { get; set; }
        public string TAl_codigo_empresa { get; set; }
        public string TAl_codigo_alterno { get; set; }
        public string TAl_descripcion_larga { get; set; }
        public string TAl_descripcion_med { get; set; }
        public string TAl_descripcion_corta { get; set; }
        public string TAl_abreviacion { get; set; }
        public string TAl_observacion { get; set; }
        public DateTime TAl_fecha_creacion { get; set; }
        public DateTime TAl_fecha_modificacion { get; set; }
        public Usuario TAl_usuario_creacion { get; set; }
        public Usuario TAl_usuario_modificacion { get; set; }
        public char TAl_estado { get; set; }
        public int TAl_secuencia { get; set; }
        public Color Color { get; set; }
        public Icono Icono { get; set; }

        public Categoria_alerta Categoria_alerta { get; set; }

    }
}
