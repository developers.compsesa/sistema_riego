﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_usuario
    {
    
        public int TUs_id { get; set; }
        public string TUs_codigo_empresa { get; set; }
        public string TUs_codigo_alterno { get; set; }
        public string TUs_descripcion_larga { get; set; }
        public string TUs_descripcion_med { get; set; }
        public string TUs_descripcion_corta { get; set; }
        public string TUs_abreviacion { get; set; }
        public string TUs_observacion { get; set; }
        public DateTime TUs_fecha_creacion { get; set; }
        public DateTime TUs_fecha_modificacion { get; set; }
        public Usuario TUs_usuario_creacion { get; set; }
        public Usuario TUs_usuario_modificacion { get; set; }
        public char TUs_estado { get; set; }
        public int TUs_secuencia { get; set; }
        public Categoria_usuario Categoria_Usuario { get; set; }
    }
}
