﻿using System;

namespace Generic_appWeb.Models
{
    public class Dato_tecnico
    {
        
        public int DTec_id { get; set; }
        public string DTec_codigo_empresa { get; set; }
        public string DTec_codigo_alterno { get; set; }
        public string DTec_descripcion_larga { get; set; }
        public string DTec_descripcion_med { get; set; }
        public string DTec_descripcion_corta { get; set; }
        public string DTec_abreviacion { get; set; }
        public string DTec_observacion { get; set; }
        public DateTime DTec_fecha_creacion { get; set; }
        public DateTime DTec_fecha_modificacion { get; set; }
        public Usuario DTec_usuario_creacion { get; set; }
        public Usuario DTec_usuario_modificacion { get; set; }
        public char DTec_estado { get; set; }
        public int DTec_secuencia { get; set; }
        public Descripcion_dato_tecnico Descripcion_dato_tecnico { get; set; }
        public Descripcion_unidad Descripcion_unidad { get; set; }    
        public Descripcion_valor Descripcion_valor { get; set; }
        public Entidad Entidad { get; set; }


    }
}
