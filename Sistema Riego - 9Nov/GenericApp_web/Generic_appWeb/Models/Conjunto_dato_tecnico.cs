﻿using System;

namespace Generic_appWeb.Models
{
    public class Conjunto_dato_tecnico
    {
        
        public int CDTec_id { get; set; }
        public string CDTec_codigo_empresa { get; set; }
        public string CDTec_codigo_alterno { get; set; }
        public string CDTec_descripcion_larga { get; set; }
        public string CDTec_descripcion_med { get; set; }
        public string CDTec_descripcion_corta { get; set; }
        public string CDTec_abreviacion { get; set; }
        public string CDTec_observacion { get; set; }

        public DateTime CDTec_fecha_creacion { get; set; }
        public DateTime CDTec_fecha_modificacion { get; set; }
        public Usuario CDTec_usuario_creacion { get; set; }
        public Usuario CDTec_usuario_modificacion { get; set; }
        public char CDTec_estado { get; set; }
        public int CDTec_secuencia { get; set; }
        public Dato_tecnico Dato_tecnico { get; set; }
        public Entidad Entidad { get; set; }

        public int descripcion_conjunto_dato_tecnico_id { get; set; }
        public string valor { get; set; }
    }
}
