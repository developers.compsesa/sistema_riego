﻿using System;

namespace Generic_appWeb.Models
{
    public class Descripcion_valor
    {
        
        public int Val_id { get; set; }
        public string Val_codigo_empresa { get; set; }
        public string Val_codigo_alterno { get; set; }
        public string Val_descripcion_larga { get; set; }
        public string Val_descripcion_med { get; set; }
        public string Val_descripcion_corta { get; set; }
        public string Val_abreviacion { get; set; }
        public string Val_observacion { get; set; }
        public DateTime Val_fecha_creacion { get; set; }
        public DateTime Val_fecha_modificacion { get; set; }
        public Usuario Val_usuario_creacion { get; set; }
        public Usuario Val_usuario_modificacion { get; set; }
        public char Val_estado { get; set; }
        public int Val_secuencia { get; set; }
        public Grupo_valor Grupo_valor { get; set; }
        public Categoria_valor Categoria_valor { get; set; }
        public Tipo_valor Tipo_valor { get; set; }
        public Hacienda Hacienda { get; set; }

    }
}
