﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_riego
    {
        
        public int Gr_id { get; set; }
        public string Gr_codigo_empresa { get; set; }
        public string Gr_codigo_alterno { get; set; }
        public string Gr_descripcion_larga { get; set; }
        public string Gr_descripcion_med { get; set; }
        public string Gr_descripcion_corta { get; set; }
        public string Gr_abreviacion { get; set; }
        public string Gr_observacion { get; set; }

        public DateTime Gr_fecha_creacion { get; set; }
        public DateTime Gr_fecha_modificacion { get; set; }
        public Usuario Gr_usuario_creacion { get; set; }
        public Usuario Gr_usuario_modificacion { get; set; }
        public char Gr_estado { get; set; }
        public int Gr_secuencia { get; set; }
        public Hacienda Hacienda { get; set; }
    }
}
