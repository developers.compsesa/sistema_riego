﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Generic_appWeb.Models
{
    public class Marca_Modelo
    {

        
        public int MM_id { get; set; }
        public string MM_codigo_empresa { get; set; }
        public string MM_codigo_alterno { get; set; }
        public string MM_descripcion_larga { get; set; }
        public string MM_descripcion_med { get; set; }
        public string MM_descripcion_corta { get; set; }
        public string MM_abreviacion { get; set; }
        public string MM_observacion { get; set; }
        public DateTime MM_fecha_creacion { get; set; }
        public DateTime MM_fecha_modificacion { get; set; }
        public Usuario MM_usuario_creacion { get; set; }
        public Usuario MM_usuario_modificacion { get; set; }
        public char MM_estado { get; set; }
        public int MM_secuencia { get; set; }
        public Marca Marca { get; set; }
        public Modelo Modelo { get; set; }
        public Entidad Entidad { get; set; }


    }
}
