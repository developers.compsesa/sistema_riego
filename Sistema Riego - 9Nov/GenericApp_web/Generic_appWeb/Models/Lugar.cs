﻿using System;
using System.Collections.Generic;

namespace Generic_appWeb.Models
{
    public class Lugar
    {
        public int Lu_id { get; set; }
        public string Lu_codigo_empresa { get; set; }
        public string Lu_codigo_alterno { get; set; }
        public string Lu_descripcion_larga { get; set; }
        public string Lu_descripcion_med { get; set; }
        public string Lu_descripcion_corta { get; set; }
        public string Lu_abreviacion { get; set; }
        public string Lu_observacion { get; set; }
        public DateTime Lu_fecha_creacion { get; set; }
        public DateTime Lu_fecha_modificacion { get; set; }
        public Usuario Lu_usuario_creacion { get; set; }
        public Usuario Lu_usuario_modificacion { get; set; }
        public char Lu_estado { get; set; }
        public int Lu_secuencia { get; set; }
        public Grupo_lugar Grupo_lugar { get; set; }
        public Categoria_lugar Categoria_lugar { get; set; }
        public Tipo_lugar Tipo_lugar { get; set; }
        public Hacienda Hacienda { get; set; }
    }
}
