﻿using System;

namespace Generic_appWeb.Models
{
    public class Descripcion_dato_tecnico
    {
        
        public int DDTec_id { get; set; }
        public string DDTec_codigo_empresa { get; set; }
        public string DDTec_codigo_alterno { get; set; }
        public string DDTec_descripcion_larga { get; set; }
        public string DDTec_descripcion_med { get; set; }
        public string DDTec_descripcion_corta { get; set; }
        public string DDTec_abreviacion { get; set; }
        public string DDTec_observacion { get; set; }
        public DateTime DDTec_fecha_creacion { get; set; }
        public DateTime DDTec_fecha_modificacion { get; set; }
        public Usuario DDTec_usuario_creacion { get; set; }
        public Usuario DDTec_usuario_modificacion { get; set; }
        public char DDTec_estado { get; set; }
        public int DDTec_secuencia { get; set; }
        public Grupo_dato_tecnico Grupo_dato_tecnico { get; set; }
        public Categoria_dato_tecnico Categoria_dato_tecnico { get; set; }
        public Tipo_dato_tecnico Tipo_dato_tecnico { get; set; }
        public Entidad Entidad { get; set; }

    }
}
