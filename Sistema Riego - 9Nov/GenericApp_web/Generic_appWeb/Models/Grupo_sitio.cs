﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_sitio
    {
        
        public int GSit_id { get; set; }
        public string GSit_codigo_empresa { get; set; }
        public string GSit_codigo_alterno { get; set; }
        public string GSit_descripcion_larga { get; set; }
        public string GSit_descripcion_med { get; set; }
        public string GSit_descripcion_corta { get; set; }
        public string GSit_abreviacion { get; set; }
        public string GSit_observacion { get; set; }

        public DateTime GSit_fecha_creacion { get; set; }
        public DateTime GSit_fecha_modificacion { get; set; }
        public Usuario GSit_usuario_creacion { get; set; }
        public Usuario GSit_usuario_modificacion { get; set; }
        public char GSit_estado { get; set; }
        public int GSit_secuencia { get; set; }
        public Hacienda Hacienda { get; set; }


    }
}
