﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_programa_diario
    {
        
        public int CPD_id { get; set; }
        public string CPD_codigo_empresa { get; set; }
        public string CPD_codigo_alterno { get; set; }
        public string CPD_descripcion_larga { get; set; }
        public string CPD_descripcion_med { get; set; }
        public string CPD_descripcion_corta { get; set; }
        public string CPD_abreviacion { get; set; }
        public string CPD_observacion { get; set; }
        public DateTime CPD_fecha_creacion { get; set; }
        public DateTime CPD_fecha_modificacion { get; set; }
        public Usuario CPD_usuario_creacion { get; set; }
        public Usuario CPD_usuario_modificacion { get; set; }
        public char CPD_estado { get; set; }
        public int CPD_secuencia { get; set; }
    }
}
