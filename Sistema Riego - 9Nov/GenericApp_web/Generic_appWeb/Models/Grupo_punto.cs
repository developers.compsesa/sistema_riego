﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_punto    {
        
        public int GPu_id { get; set; }
        public string GPu_codigo_empresa { get; set; }
        public string GPu_codigo_alterno { get; set; }
        public string GPu_descripcion_larga { get; set; }
        public string GPu_descripcion_med { get; set; }
        public string GPu_descripcion_corta { get; set; }
        public string GPu_abreviacion { get; set; }
        public string GPu_observacion { get; set; }

        public DateTime GPu_fecha_creacion { get; set; }
        public DateTime GPu_fecha_modificacion { get; set; }
        public Usuario GPu_usuario_creacion { get; set; }
        public Usuario GPu_usuario_modificacion { get; set; }
        public char GPu_estado { get; set; }
        public int GPu_secuencia { get; set; }

        public Hacienda Hacienda { get; set; }



    }
}
