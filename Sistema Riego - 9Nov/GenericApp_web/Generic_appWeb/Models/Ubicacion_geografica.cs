﻿using System;

namespace Generic_appWeb.Models
{
    public class Ubicacion_geografica
    {
        
        public int Ug_id { get; set; }
        public string Ug_Codigo_empresa { get; set; }
        public string Ug_Codigo_alterno { get; set; }
        public string Ug_Descripcion_larga { get; set; }
        public string Ug_Descripcion_med { get; set; }
        public string Ug_Descripcion_corta { get; set; }
        public string Ug_Abreviacion { get; set; }
        public string Ug_Observacion { get; set; }
        public DateTime Ug_fecha_creacion { get; set; }
        public DateTime Ug_fecha_modificacion { get; set; }
        public Usuario Ug_usuario_creacion { get; set; }
        public Usuario Ug_usuario_modificacion { get; set; }
        public int Ug_estado { get; set; }
        public int Ug_secuencia { get; set; }
        public Pais Pais { get; set; }
        public Provincia Provincia { get; set; }
        public Ciudad Ciudad { get; set; }
    }
}
