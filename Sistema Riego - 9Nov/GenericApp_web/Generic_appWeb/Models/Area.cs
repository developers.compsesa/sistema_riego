﻿using System;

namespace Generic_appWeb.Models
{
    public class Area
    {
        
        public int Are_id { get; set; }
        public string Are_codigo_empresa { get; set; }
        public string Are_codigo_alterno { get; set; }
        public string Are_descripcion_larga { get; set; }
        public string Are_descripcion_med { get; set; }
        public string Are_descripcion_corta { get; set; }
        public string Are_abreviacion { get; set; }
        public string Are_observacion { get; set; }
        public DateTime Are_fecha_creacion { get; set; }
        public DateTime Are_fecha_modificacion { get; set; }
        public Usuario Are_usuario_creacion { get; set; }
        public Usuario Are_usuario_modificacion { get; set; }
        public char Are_estado { get; set; }
        public int Are_secuencia { get; set; }
        public Grupo_area Grupo_area { get; set; }
        public Categoria_area Categoria_area { get; set; }
        public Tipo_area Tipo_area { get; set; }
        public Hacienda Hacienda { get; set; }
        public Lugar Lugar { get; set; }

    }
}
