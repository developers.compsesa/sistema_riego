﻿using System;

namespace Generic_appWeb.Models
{
    public class Ciudad
    {
        
        public int Ci_id { get; set; }
        public string Ci_codigo_empresa { get; set; }
        public string Ci_codigo_alterno { get; set; }
        public string Ci_descripcion_larga { get; set; }
        public string Ci_descripcion_med { get; set; }
        public string Ci_descripcion_corta { get; set; }
        public string Ci_abreviacion { get; set; }
        public string Ci_observacion { get; set; }
        public DateTime Ci_fecha_creacion { get; set; }
        public DateTime Ci_fecha_modificacion { get; set; }
        public Usuario Ci_usuario_creacion { get; set; }
        public Usuario Ci_usuario_modificacion { get; set; }
        public char Ci_estado { get; set; }
        public int Ci_secuencia { get; set; }
        public Provincia Provincia { get; set; }

    }
}
