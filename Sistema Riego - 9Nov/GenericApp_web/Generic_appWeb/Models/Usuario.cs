﻿using System;

namespace Generic_appWeb.Models
{
    public class Usuario
    {
        
        public int Users_id { get; set; }
        public string Users_codigo_empresa { get; set; }
        public string Users_codigo_alterno { get; set; }
        public string Users_descripcion_larga { get; set; }
        public string Users_descripcion_med { get; set; }
        public string Users_descripcion_corta { get; set; }
        public string Users_abreviacion { get; set; }
        public string Users_observacion { get; set; }      
        public DateTime Users_fecha_creacion { get; set; }
        public DateTime Users_fecha_modificacion { get; set; }
        public Usuario Users_usuario_creacion { get; set; }
        public Usuario Users_usuario_modificacion { get; set; }
        public char Users_Estado { get; set; }
        public int Users_secuencia { get; set; }
        public string Users_clave { get; set; }
        public string Users_correo { get; set; }
        public Categoria_usuario Categoria_usuario { get; set; }
        public Tipo_usuario Tipo_usuario { get; set; }
    }
}
