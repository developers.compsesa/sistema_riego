﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_marca
    {
    
        public int CMa_id { get; set; }
        public string CMa_codigo_empresa { get; set; }
        public string CMa_codigo_alterno { get; set; }
        public string CMa_descripcion_larga { get; set; }
        public string CMa_descripcion_med { get; set; }
        public string CMa_descripcion_corta { get; set; }
        public string CMa_abreviacion { get; set; }
        public string CMa_observacion { get; set; }
        public DateTime CMa_fecha_creacion { get; set; }
        public DateTime CMa_fecha_modificacion { get; set; }
        public Usuario CMa_usuario_creacion { get; set; }
        public Usuario CMa_usuario_modificacion { get; set; }
        public char CMa_estado { get; set; }
        public int CMa_secuencia { get; set; }
        public Grupo_marca Grupo_marca { get; set; }

    }
}
