﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_alerta
    {
        
        public int CAl_id { get; set; }
        public string CAl_codigo_empresa { get; set; }
        public string CAl_codigo_alterno { get; set; }
        public string CAl_descripcion_larga { get; set; }
        public string CAl_descripcion_med { get; set; }
        public string CAl_descripcion_corta { get; set; }
        public string CAl_abreviacion { get; set; }
        public string CAl_observacion { get; set; }
        public DateTime CAl_fecha_creacion { get; set; }
        public DateTime CAl_fecha_modificacion { get; set; }
        public Usuario CAl_usuario_creacion { get; set; }
        public Usuario CAl_usuario_modificacion { get; set; }
        public string CAl_estado { get; set; }
        public int CAl_secuencia { get; set; }
        public Grupo_alerta Grupo_alerta { get; set; }        
        

    }
}
