﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_cultivo
    {
        
        public int TCul_id { get; set; }
        public string TCul_codigo_empresa { get; set; }
        public string TCul_codigo_alterno { get; set; }
        public string TCul_descripcion_larga { get; set; }
        public string TCul_descripcion_med { get; set; }
        public string TCul_descripcion_corta { get; set; }
        public string TCul_abreviacion { get; set; }
        public string TCul_observacion { get; set; }
        public DateTime TCul_fecha_creacion { get; set; }
        public DateTime TCul_fecha_modificacion { get; set; }
        public Usuario TCul_usuario_creacion { get; set; }
        public Usuario TCul_usuario_modificacion { get; set; }
        public char TCul_estado { get; set; }
        public int TCul_secuencia { get; set; }
        public Categoria_cultivo Categoria_cultivo { get; set; }
   

    }
}
