﻿using System;

namespace Generic_appWeb.Models
{
    public class Etapa_cultivo
    {
        
        public int ECul_id { get; set; }
        public string ECul_codigo_empresa { get; set; }
        public string ECul_codigo_alterno { get; set; }
        public string ECul_descripcion_larga { get; set; }
        public string ECul_descripcion_med { get; set; }
        public string ECul_descripcion_corta { get; set; }
        public string ECul_abreviacion { get; set; }
        public string ECul_observacion { get; set; }

        public DateTime ECul_fecha_creacion { get; set; }
        public DateTime ECul_fecha_modificacion { get; set; }
        public Usuario ECul_usuario_creacion { get; set; }
        public Usuario ECul_usuario_modificacion { get; set; }     
        public char ECul_estado { get; set; }
        public int ECul_secuencia { get; set; }
        

    }
}
