﻿using System;

namespace Generic_appWeb.Models
{
    public class Descripcion_conjunto_dato_tecnico
    {
        
        public int DCDTec_id { get; set; }
        public string DCDTec_valor { get; set; }
        public Conjunto_dato_tecnico Conjunto_dato_tecnico { get; set; }
        public Dato_tecnico Dato_tecnico { get; set; }
       


    }
}
