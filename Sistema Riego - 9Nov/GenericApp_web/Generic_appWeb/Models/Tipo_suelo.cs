﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_suelo
    {
        
        public int TSu_id { get; set; }
        public string TSu_codigo_empresa { get; set; }
        public string TSu_codigo_alterno { get; set; }
        public string TSu_descripcion_larga { get; set; }
        public string TSu_descripcion_med { get; set; }
        public string TSu_descripcion_corta { get; set; }
        public string TSu_abreviacion { get; set; }
        public string TSu_observacion { get; set; }

        public DateTime TSu_fecha_creacion { get; set; }
        public DateTime TSu_fecha_modificacion { get; set; }
        public Usuario TSu_usuario_creacion { get; set; }
        public Usuario TSu_usuario_modificacion { get; set; }
        public int TSu_estado { get; set; }
        public int TSu_secuencia { get; set; }

        public Hacienda Hacienda { get; set; }

    }
}
