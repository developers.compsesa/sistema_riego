﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_punto
    {
        
        public int TPu_id { get; set; }
        public string TPu_codigo_empresa { get; set; }
        public string TPu_codigo_alterno { get; set; }
        public string TPu_descripcion_larga { get; set; }
        public string TPu_descripcion_med { get; set; }
        public string TPu_descripcion_corta { get; set; }
        public string TPu_abreviacion { get; set; }
        public string TPu_observacion { get; set; }
        public DateTime TPu_fecha_creacion { get; set; }
        public DateTime TPu_fecha_modificacion { get; set; }
        public Usuario TPu_usuario_creacion { get; set; }
        public Usuario TPu_usuario_modificacion { get; set; }
        public char TPu_estado { get; set; }
        public int TPu_secuencia { get; set; }
        public Categoria_punto Categoria_punto { get; set; }
        public Hacienda Hacienda { get; set; }

    }
}
