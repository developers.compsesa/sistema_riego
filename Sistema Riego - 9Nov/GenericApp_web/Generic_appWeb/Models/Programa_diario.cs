﻿using System;

namespace Generic_appWeb.Models
{
    public class Programa_diario
    {
        
        public int PDi_id { get; set; }
        public string PDi_codigo_empresa { get; set; }
        public string PDi_codigo_alterno { get; set; }
        public string PDi_descripcion_larga { get; set; }
        public string PDi_descripcion_med { get; set; }
        public string PDi_descripcion_corta { get; set; }
        public string PDi_abreviacion { get; set; }
        public string PDi_observacion { get; set; }

        public DateTime PDi_fecha_creacion { get; set; }
        public DateTime PDi_fecha_modificacion { get; set; }
        public Usuario PDi_usuario_creacion { get; set; }
        public Usuario PDi_usuario_modificacion { get; set; }
        public char PDi_estado { get; set; }
        public int PDi_secuencia { get; set; } 
        public Double PDi_programa_standard { get; set; }
        public Categoria_programa_diario Categoria_programa_diario { get; set; }
        public Tipo_programa_diario Tipo_programa_diario { get; set; }
        public Equipo Equipo { get; set; }


    }
}
