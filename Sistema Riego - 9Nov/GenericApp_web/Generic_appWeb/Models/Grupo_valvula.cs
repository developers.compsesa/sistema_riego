﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_valvula
    {
        
        public int Gv_id { get; set; }
        public string Gv_codigo_empresa { get; set; }
        public string Gv_codigo_alterno { get; set; }
        public string Gv_descripcion_larga { get; set; }
        public string Gv_descripcion_med { get; set; }
        public string Gv_descripcion_corta { get; set; }
        public string Gv_abreviacion { get; set; }
        public string Gv_observacion { get; set; }

        public DateTime Gv_fecha_creacion { get; set; }
        public DateTime Gv_fecha_modificacion { get; set; }
        public Usuario Gv_usuario_creacion { get; set; }
        public Usuario Gv_usuario_modificacion { get; set; }
        public char Gv_estado { get; set; }
        public int Gv_secuencia { get; set; }
        public Hacienda Hacienda { get; set; }

    }
}
