﻿using NpgsqlTypes;
using System;

namespace Generic_appWeb.Models
{
    public class Hacienda 
    {
      
        public int Hac_id { get; set; }
        public string Hac_codigo_empresa { get; set; }
        public string Hac_codigo_alterno { get; set; }
        public string Hac_descripcion_larga { get; set; }
        public string Hac_descripcion_med { get; set; }
        public string Hac_descripcion_corta { get; set; }
        public string Hac_abreviacion { get; set; }
        public string Hac_observacion { get; set; }
        public DateTime Hac_fecha_creacion { get; set; }
        public DateTime Hac_fecha_modificacion { get; set; }
        public Usuario Hac_usuario_creacion { get; set; }
        public Usuario Hac_usuario_modificacion { get; set; }
        public char Hac_estado { get; set; }
        public int Hac_secuencia { get; set; }
        public int Hac_superficie_hectareas { get; set; }
        public NpgsqlPolygon Hac_puntos_multipoligonos { get; set; }
        public double Hac_radio { get; set; }
        public string Hac_forma { get; set; }
        public Entidad Entidad { get; set; }
        public Ubicacion_geografica Ubicacion_geografica { get; set; }



    }
}
