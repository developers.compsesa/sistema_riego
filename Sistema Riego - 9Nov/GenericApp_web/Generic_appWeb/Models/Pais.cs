﻿using System;

namespace Generic_appWeb.Models
{
    public class Pais
    {
        
        public int Pa_id { get; set; }
        public string Pa_codigo_empresa { get; set; }
        public string Pa_codigo_alterno { get; set; }
        public string Pa_descripcion_larga { get; set; }
        public string Pa_descripcion_med { get; set; }
        public string Pa_descripcion_corta { get; set; }
        public string Pa_abreviacion { get; set; }
        public string Pa_observacion { get; set; }

        public DateTime Pa_fecha_creacion { get; set; }
        public DateTime Pa_fecha_modificacion { get; set; }
        public Usuario Pa_usuario_creacion { get; set; }
        public Usuario Pa_usuario_modificacion { get; set; }
        public char Pa_estado { get; set; }
        public int Pa_secuencia { get; set; }

    }
}
