﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_valor
    {
        
        public int GVal_id { get; set; }
        public string GVal_codigo_empresa { get; set; }
        public string GVal_codigo_alterno { get; set; }
        public string GVal_descripcion_larga { get; set; }
        public string GVal_descripcion_med { get; set; }
        public string GVal_descripcion_corta { get; set; }
        public string GVal_abreviacion { get; set; }
        public string GVal_observacion { get; set; }

        public DateTime GVal_fecha_creacion { get; set; }
        public DateTime GVal_fecha_modificacion { get; set; }
        public Usuario GVal_usuario_creacion { get; set; }
        public Usuario GVal_usuario_modificacion { get; set; }
        public char GVal_estado { get; set; }
        public int GVal_secuencia { get; set; }

    }
}
