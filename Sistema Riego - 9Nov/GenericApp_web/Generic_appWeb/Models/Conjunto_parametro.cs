﻿using System;

namespace Generic_appWeb.Models
{
    public class Conjunto_parametro
    {
        
        public int CParm_id { get; set; }
        public string CParm_codigo_empresa { get; set; }
        public string CParm_codigo_alterno { get; set; }
        public string CParm_descripcion_larga { get; set; }
        public string CParm_descripcion_med { get; set; }
        public string CParm_descripcion_corta { get; set; }
        public string CParm_abreviacion { get; set; }
        public string CParm_observacion { get; set; }

        public DateTime CParm_fecha_creacion { get; set; }
        public DateTime CParm_fecha_modificacion { get; set; }
        public Usuario CParm_usuario_creacion { get; set; }
        public Usuario CParm_usuario_modificacion { get; set; }
        public char CParm_estado { get; set; }
        public int CParm_secuencia { get; set; }
        public Entidad Entidad { get; set; }
        public Parametro Parametro { get; set; }
        public int descripcion_conjunto_parametro_id { get; set; }
        public string valor { get; set; }
    }
}
