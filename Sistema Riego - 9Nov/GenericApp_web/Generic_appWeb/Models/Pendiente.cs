﻿using System;

namespace Generic_appWeb.Models
{
    public class Pendiente
    {
        
        public int Pen_id { get; set; }
        public string Pen_codigo_empresa { get; set; }
        public string Pen_codigo_alterno { get; set; }
        public string Pen_descripcion_larga { get; set; }
        public string Pen_descripcion_med { get; set; }
        public string Pen_descripcion_corta { get; set; }
        public string Pen_abreviacion { get; set; }
        public string Pen_observacion { get; set; }

        public DateTime Pen_fecha_creacion { get; set; }
        public DateTime Pen_fecha_modificacion { get; set; }
        public Usuario Pen_usuario_creacion { get; set; }
        public Usuario Pen_usuario_modificacion { get; set; }
        public char Pen_estado { get; set; }
        public int Pen_secuencia { get; set; } 

    }
}
