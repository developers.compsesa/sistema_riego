﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_parametro
    {
        
        public int GPar_id { get; set; }
        public string GPar_codigo_empresa { get; set; }
        public string GPar_codigo_alterno { get; set; }
        public string GPar_descripcion_larga { get; set; }
        public string GPar_descripcion_med { get; set; }
        public string GPar_descripcion_corta { get; set; }
        public string GPar_abreviacion { get; set; }
        public string GPar_observacion { get; set; }

        public DateTime GPar_fecha_creacion { get; set; }
        public DateTime GPar_fecha_modificacion { get; set; }
        public Usuario GPar_usuario_creacion { get; set; }
        public Usuario GPar_usuario_modificacion { get; set; }
        public char GPar_estado { get; set; }
        public int GPar_secuencia { get; set; }

    }
}
