﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_marca
    {
      
        public int GMa_id { get; set; }
        public string GMa_codigo_empresa { get; set; }
        public string GMa_codigo_alterno { get; set; }
        public string GMa_descripcion_larga { get; set; }
        public string GMa_descripcion_med { get; set; }
        public string GMa_descripcion_corta { get; set; }
        public string GMa_abreviacion { get; set; }
        public string GMa_observacion { get; set; }
        public DateTime GMa_fecha_creacion { get; set; }
        public DateTime GMa_fecha_modificacion { get; set; }
        public Usuario GMa_usuario_creacion { get; set; }
        public Usuario GMa_usuario_modificacion { get; set; }
        public char GMa_estado { get; set; }
        public int GMa_secuencia { get; set; }


    }
}
