﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_unidad
    {
        
        public int CUn_id { get; set; }
        public string CUn_codigo_empresa { get; set; }
        public string CUn_codigo_alterno { get; set; }
        public string CUn_descripcion_larga { get; set; }
        public string CUn_descripcion_med { get; set; }
        public string CUn_descripcion_corta { get; set; }
        public string CUn_abreviacion { get; set; }
        public string CUn_observacion { get; set; }
        public DateTime CUn_fecha_creacion { get; set; }
        public DateTime CUn_fecha_modificacion { get; set; }
        public Usuario CUn_usuario_creacion { get; set; }
        public Usuario CUn_usuario_modificacion { get; set; }
        public char CUn_estado { get; set; }
        public int CUn_secuencia { get; set; }
        public Grupo_unidad Grupo_unidad { get; set; }

    }
}
