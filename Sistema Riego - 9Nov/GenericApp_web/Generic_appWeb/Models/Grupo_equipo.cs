﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_equipo 
    {
      
        public int GE_id { get; set; }
        public string GE_codigo_empresa { get; set; }
        public string GE_codigo_alterno { get; set; }
        public string GE_descripcion_larga { get; set; }
        public string GE_descripcion_med { get; set; }
        public string GE_descripcion_corta { get; set; }
        public string GE_abreviacion { get; set; }
        public string GE_observacion { get; set; }
        public DateTime GE_fecha_creacion { get; set; }
        public DateTime GE_fecha_modificacion { get; set; }
        public Usuario GE_usuario_creacion { get; set; }
        public Usuario GE_usuario_modificacion { get; set; }
        public char GE_estado { get; set; }
        public int GE_secuencia { get; set; }


    }
}
