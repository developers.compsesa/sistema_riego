﻿using System;

namespace Generic_appWeb.Models
{
    public class Parametrizacion_equipo
    {
        
        public int PEq_id { get; set; }
        public string PEq_codigo_empresa { get; set; }
        public string PEq_codigo_alterno { get; set; }
        public string PEq_descripcion_larga { get; set; }
        public string PEq_descripcion_med { get; set; }
        public string PEq_descripcion_corta { get; set; }
        public string PEq_abreviacion { get; set; }
        public string PEq_observacion { get; set; }
        public DateTime PEq_fecha_creacion { get; set; }
        public DateTime PEq_fecha_modificacion { get; set; }
        public Usuario PEq_usuario_creacion { get; set; }
        public Usuario PEq_usuario_modificacion { get; set; }
        public char PEq_estado { get; set; }
        public int PEq_secuencia { get; set; }
        public Double PEq_factor_riego { get; set; }        
        public int PEq_cantidad_puntos_riego { get; set; }
        public Double PEq_m2_aspersor { get; set; }
        public Double PEq_caudal_l_h_x_m2 { get; set; }
        public int PEq_padre { get; set; }
        public Equipo Equipo  { get; set; }
        public string eq_hexadecimal { get; set; }
        public string eq_ruta { get; set; }

        public Bloque_riego Bloque_riego { get; set; }
        public Grupo_riego Grupo_riego { get; set; }
        public Grupo_valvula Grupo_Valvula { get; set; }
        public Nivel_riego Nivel_riego { get; set; }
           









    }
}
