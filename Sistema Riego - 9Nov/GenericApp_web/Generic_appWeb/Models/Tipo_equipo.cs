﻿using Microsoft.AspNetCore.Http;
using System;

namespace Generic_appWeb.Models
{
    public class Tipo_equipo
    {
        
        public int TE_id { get; set; }
        public string TE_codigo_empresa { get; set; }
        public string TE_codigo_alterno { get; set; }
        public string TE_descripcion_larga { get; set; }
        public string TE_descripcion_med { get; set; }
        public string TE_descripcion_corta { get; set; }
        public string TE_abreviacion { get; set; }
        public string TE_observacion { get; set; }
        public DateTime TE_fecha_creacion { get; set; }
        public DateTime TE_fecha_modificacion { get; set; }
        public Usuario TE_usuario_creacion { get; set; }
        public Usuario TE_usuario_modificacion { get; set; }
        public char TE_estado { get; set; }
        public int TE_secuencia { get; set; }

        public Categoria_equipo Categoria_equipo { get; set; }
        public Icono Icono { get; set; }
        public Color Color { get; set; }

    }
}
