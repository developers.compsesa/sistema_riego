﻿using System;

namespace Generic_appWeb.Models
{
    public partial class Tipo_entidad
    {

      
        public int TEn_id { get; set; }
        public string TEn_codigo_empresa { get; set; }
        public string TEn_codigo_alterno { get; set; }
        public string TEn_descripcion_larga { get; set; }
        public string TEn_descripcion_med { get; set; }
        public string TEn_descripcion_corta { get; set; }
        public string TEn_abreviacion { get; set; }
        public string TEn_observacion { get; set; }
        public DateTime TEn_fecha_creacion { get; set; }
        public DateTime TEn_fecha_modificacion { get; set; }
        public Usuario TEn_usuario_creacion { get; set; }
        public Usuario TEn_usuario_modificacion { get; set; }
        public int TEn_estado { get; set; }
        public int TEn_secuencia { get; set; }
        public Categoria_entidad Categoria_entidad { get; set; }

    }
}
