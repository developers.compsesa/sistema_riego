﻿using System;

namespace Generic_appWeb.Models
{
    public class Descripcion_conjunto_parametro
    {
        
        public int DCPar_id { get; set; }
        public string DCPar_valor { get; set; }
        public Conjunto_parametro Conjunto_parametro{ get; set; }
        public Parametro Parametro { get; set; }
       


    }
}
