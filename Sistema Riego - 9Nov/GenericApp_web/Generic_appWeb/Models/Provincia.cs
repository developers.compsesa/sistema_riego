﻿using System;

namespace Generic_appWeb.Models
{
    public class Provincia
    {
        
        public int Pr_id { get; set; }
        public string Pr_codigo_empresa { get; set; }
        public string Pr_codigo_alterno { get; set; }
        public string Pr_descripcion_larga { get; set; }
        public string Pr_descripcion_med { get; set; }
        public string Pr_descripcion_corta { get; set; }
        public string Pr_abreviacion { get; set; }
        public string Pr_observacion { get; set; }
        public DateTime Pr_fecha_creacion { get; set; }
        public DateTime Pr_fecha_modificacion { get; set; }
        public Usuario Pr_usuario_creacion { get; set; }
        public Usuario Pr_usuario_modificacion { get; set; }
        public char Pr_estado { get; set; }
        public int Pr_secuencia { get; set; }
        public Pais Pais { get; set; }

    }
}
