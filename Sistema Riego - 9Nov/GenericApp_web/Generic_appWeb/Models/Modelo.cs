﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Generic_appWeb.Models
{
    public class Modelo
    {

        
        public int Mo_id { get; set; }
        public string Mo_codigo_empresa { get; set; }
        public string Mo_codigo_alterno { get; set; }
        public string Mo_descripcion_larga { get; set; }
        public string Mo_descripcion_med { get; set; }
        public string Mo_descripcion_corta { get; set; }
        public string Mo_abreviacion { get; set; }
        public string Mo_observacion { get; set; }
        public DateTime Mo_fecha_creacion { get; set; }
        public DateTime Mo_fecha_modificacion { get; set; }
        public Usuario Mo_usuario_creacion { get; set; }
        public Usuario Mo_usuario_modificacion { get; set; }
        public char Mo_estado { get; set; }
        public int Mo_secuencia { get; set; }
    }
}
