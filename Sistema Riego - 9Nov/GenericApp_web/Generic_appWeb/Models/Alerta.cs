﻿using System;

namespace Generic_appWeb.Models
{
    public class Alerta
    {
        
        public int Al_id { get; set; }
        public string Al_codigo_empresa { get; set; }
        public string Al_codigo_alterno { get; set; }
        public string Al_descripcion_larga { get; set; }
        public string Al_descripcion_med { get; set; }
        public string Al_descripcion_corta { get; set; }
        public string Al_abreviacion { get; set; }
        public string Al_observacion { get; set; }

        public DateTime Al_fecha_creacion { get; set; }
        public DateTime Al_fecha_modificacion { get; set; }
        public Usuario Al_usuario_creacion { get; set; }
        public Usuario Al_usuario_modificacion { get; set; }
        public char Al_estado { get; set; }
        public int Al_secuencia { get; set; }
        public string Al_mensaje { get; set; }

        public Grupo_alerta Grupo_alerta { get; set; }
        public Categoria_alerta Categoria_alerta { get; set; }
        public Tipo_alerta Tipo_alerta { get; set; }
        public Hacienda Hacienda { get; set; }
    }
}
