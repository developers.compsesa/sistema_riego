﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_icono 
    {
      
        public int TIco_id { get; set; }
        public string TIco_codigo_empresa { get; set; }
        public string TIco_codigo_alterno { get; set; }
        public string TIco_descripcion_larga { get; set; }
        public string TIco_descripcion_med { get; set; }
        public string TIco_descripcion_corta { get; set; }
        public string TIco_abreviacion { get; set; }
        public string TIco_observacion { get; set; }
        public DateTime TIco_fecha_creacion { get; set; }
        public DateTime TIco_fecha_modificacion { get; set; }
        public Usuario TIco_usuario_creacion { get; set; }
        public Usuario TIco_usuario_modificacion { get; set; }
        public char TIco_estado { get; set; }
        public int TIco_secuencia { get; set; }


    }
}
