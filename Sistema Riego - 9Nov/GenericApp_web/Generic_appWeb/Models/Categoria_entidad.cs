﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_entidad
    {
        
        public int CEn_id { get; set; }
        public string CEn_codigo_empresa { get; set; }
        public string CEn_codigo_alterno { get; set; }
        public string CEn_descripcion_larga { get; set; }
        public string CEn_descripcion_med { get; set; }
        public string CEn_descripcion_corta { get; set; }
        public string CEn_abreviacion { get; set; }
        public string CEn_observacion { get; set; }
        public DateTime CEn_fecha_creacion { get; set; }
        public DateTime CEn_fecha_modificacion { get; set; }
        public Usuario CEn_usuario_creacion { get; set; }
        public Usuario CEn_usuario_modificacion { get; set; }
        public char CEn_estado { get; set; }
        public int CEn_secuencia { get; set; }
        public Grupo_entidad Grupo_entidad { get; set; }

    }
}
