﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_dato_tecnico
    {
        
        public int TDTec_id { get; set; }
        public string TDTec_codigo_empresa { get; set; }
        public string TDTec_codigo_alterno { get; set; }
        public string TDTec_descripcion_larga { get; set; }
        public string TDTec_descripcion_med { get; set; }
        public string TDTec_descripcion_corta { get; set; }
        public string TDTec_abreviacion { get; set; }
        public string TDTec_observacion { get; set; }
        public DateTime TDTec_fecha_creacion { get; set; }
        public DateTime TDTec_fecha_modificacion { get; set; }
        public Usuario TDTec_usuario_creacion { get; set; }
        public Usuario TDTec_usuario_modificacion { get; set; }
        public int TDTec_estado { get; set; }
        public int TDTec_secuencia { get; set; }
        public Categoria_dato_tecnico Categoria_dato_tecnico { get; set; }


    }
}
