﻿using System;

namespace Generic_appWeb.Models
{
    public class Icono 
    {
      
        public int Ico_id { get; set; }
        public string Ico_codigo_empresa { get; set; }
        public string Ico_codigo_alterno { get; set; }
        public string Ico_descripcion_larga { get; set; }
        public string Ico_descripcion_med { get; set; }
        public string Ico_descripcion_corta { get; set; }
        public string Ico_abreviacion { get; set; }
        public string Ico_observacion { get; set; }
        public DateTime Ico_fecha_creacion { get; set; }
        public DateTime Ico_fecha_modificacion { get; set; }
        public Usuario Ico_usuario_creacion { get; set; }
        public Usuario Ico_usuario_modificacion { get; set; }
        public char Ico_estado { get; set; }
        public int Ico_secuencia { get; set; }
        public string Ico_ruta { get; set; }
        public Tipo_icono Tipo_icono { get; set; }


    }
}
