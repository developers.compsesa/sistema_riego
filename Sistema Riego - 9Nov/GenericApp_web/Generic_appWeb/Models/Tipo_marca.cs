﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_marca
    {
        
        public int TMa_id { get; set; }
        public string TMa_codigo_empresa { get; set; }
        public string TMa_codigo_alterno { get; set; }
        public string TMa_descripcion_larga { get; set; }
        public string TMa_descripcion_med { get; set; }
        public string TMa_descripcion_corta { get; set; }
        public string TMa_abreviacion { get; set; }
        public string TMa_observacion { get; set; }
        public DateTime TMa_fecha_creacion { get; set; }
        public DateTime TMa_fecha_modificacion { get; set; }
        public Usuario TMa_usuario_creacion { get; set; }
        public Usuario TMa_usuario_modificacion { get; set; }
        public int TMa_estado { get; set; }
        public int TMa_secuencia { get; set; }
        public Categoria_marca Categoria_marca { get; set; }
        
        
    }
}
