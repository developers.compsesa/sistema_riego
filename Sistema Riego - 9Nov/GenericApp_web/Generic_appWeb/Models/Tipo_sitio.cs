﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_sitio
    {
        
        public int TSit_id { get; set; }
        public string TSit_codigo_empresa { get; set; }
        public string TSit_codigo_alterno { get; set; }
        public string TSit_descripcion_larga { get; set; }
        public string TSit_descripcion_med { get; set; }
        public string TSit_descripcion_corta { get; set; }
        public string TSit_abreviacion { get; set; }
        public string TSit_observacion { get; set; }
        public DateTime TSit_fecha_creacion { get; set; }
        public DateTime TSit_fecha_modificacion { get; set; }
        public Usuario TSit_usuario_creacion { get; set; }
        public Usuario TSit_usuario_modificacion { get; set; }
        public int TSit_estado { get; set; }
        public int TSit_secuencia { get; set; }
        public Categoria_sitio Categoria_sitio { get; set; }
        public Hacienda Hacienda { get; set; }

    }
}
