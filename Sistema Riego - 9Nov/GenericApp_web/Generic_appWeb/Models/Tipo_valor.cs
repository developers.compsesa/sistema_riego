﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_valor
    {
        
        public int TVal_id { get; set; }
        public string TVal_codigo_empresa { get; set; }
        public string TVal_codigo_alterno { get; set; }
        public string TVal_descripcion_larga { get; set; }
        public string TVal_descripcion_med { get; set; }
        public string TVal_descripcion_corta { get; set; }
        public string TVal_abreviacion { get; set; }
        public string TVal_observacion { get; set; }
        public DateTime TVal_fecha_creacion { get; set; }
        public DateTime TVal_fecha_modificacion { get; set; }
        public Usuario TVal_usuario_creacion { get; set; }
        public Usuario TVal_usuario_modificacion { get; set; }
        public int TVal_estado { get; set; }
        public int TVal_secuencia { get; set; }
        public Categoria_valor Categoria_valor { get; set; }


    }
}
