﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_parametro
    {
        
        public int CPar_id { get; set; }
        public string CPar_codigo_empresa { get; set; }
        public string CPar_codigo_alterno { get; set; }
        public string CPar_descripcion_larga { get; set; }
        public string CPar_descripcion_med { get; set; }
        public string CPar_descripcion_corta { get; set; }
        public string CPar_abreviacion { get; set; }
        public string CPar_observacion { get; set; }
        public DateTime CPar_fecha_creacion { get; set; }
        public DateTime CPar_fecha_modificacion { get; set; }
        public Usuario CPar_usuario_creacion { get; set; }
        public Usuario CPar_usuario_modificacion { get; set; }
        public char CPar_estado { get; set; }
        public int CPar_secuencia { get; set; }
        public Grupo_parametro Grupo_parametro { get; set; }
    

    }
}
