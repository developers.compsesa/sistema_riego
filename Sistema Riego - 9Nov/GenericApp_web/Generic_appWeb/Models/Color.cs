﻿using System;

namespace Generic_appWeb.Models
{
    public class Color 
    {
      
        public int Co_id { get; set; }
        public string Co_codigo_empresa { get; set; }
        public string Co_codigo_alterno { get; set; }
        public string Co_descripcion_larga { get; set; }
        public string Co_descripcion_med { get; set; }
        public string Co_descripcion_corta { get; set; }
        public string Co_abreviacion { get; set; }
        public string Co_observacion { get; set; }
        public DateTime Co_fecha_creacion { get; set; }
        public DateTime Co_fecha_modificacion { get; set; }
        public Usuario Co_usuario_creacion { get; set; }
        public Usuario Co_usuario_modificacion { get; set; }
        public char Co_estado { get; set; }
        public int Co_secuencia { get; set; }
        public string Co_hexadecimal { get; set; }
        public int Co_tr { get; set; }
        public int Co_tg{ get; set; }
        public int Co_tb { get; set; }


    }
}
