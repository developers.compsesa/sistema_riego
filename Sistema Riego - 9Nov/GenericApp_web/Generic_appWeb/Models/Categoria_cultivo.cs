﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_cultivo
    {
        
        public int CCul_id { get; set; }
        public string CCul_codigo_empresa { get; set; }
        public string CCul_codigo_alterno { get; set; }
        public string CCul_descripcion_larga { get; set; }
        public string CCul_descripcion_med { get; set; }
        public string CCul_descripcion_corta { get; set; }
        public string CCul_abreviacion { get; set; }
        public string CCul_observacion { get; set; }
        public DateTime CCul_fecha_creacion { get; set; }
        public DateTime CCul_fecha_modificacion { get; set; }
        public Usuario CCul_usuario_creacion { get; set; }
        public Usuario CCul_usuario_modificacion { get; set; }
        public char CCul_estado { get; set; }
        public int CCul_secuencia { get; set; }
        public Grupo_cultivo Grupo_cultivo { get; set; }        

    }
}
