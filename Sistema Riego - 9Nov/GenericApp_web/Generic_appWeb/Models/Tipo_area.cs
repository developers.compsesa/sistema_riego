﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_area
    {
        
        public int TAre_id { get; set; }
        public string TAre_codigo_empresa { get; set; }
        public string TAre_codigo_alterno { get; set; }
        public string TAre_descripcion_larga { get; set; }
        public string TAre_descripcion_med { get; set; }
        public string TAre_descripcion_corta { get; set; }
        public string TAre_abreviacion { get; set; }
        public string TAre_observacion { get; set; }
        public DateTime TAre_fecha_creacion { get; set; }
        public DateTime TAre_fecha_modificacion { get; set; }
        public Usuario TAre_usuario_creacion { get; set; }
        public Usuario TAre_usuario_modificacion { get; set; }
        public char TAre_estado { get; set; }
        public int TAre_secuencia { get; set; }
        public Categoria_area Categoria_area { get; set; }
        public Hacienda Hacienda { get; set; }

    }
}
