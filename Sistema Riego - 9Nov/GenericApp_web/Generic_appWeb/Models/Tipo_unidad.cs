﻿using System;

namespace Generic_appWeb.Models
{
    public class Tipo_unidad
    {
        
        public int TUn_id { get; set; }
        public string TUn_codigo_empresa { get; set; }
        public string TUn_codigo_alterno { get; set; }
        public string TUn_descripcion_larga { get; set; }
        public string TUn_descripcion_med { get; set; }
        public string TUn_descripcion_corta { get; set; }
        public string TUn_abreviacion { get; set; }
        public string TUn_observacion { get; set; }
        public DateTime TUn_fecha_creacion { get; set; }
        public DateTime TUn_fecha_modificacion { get; set; }
        public Usuario TUn_usuario_creacion { get; set; }
        public Usuario TUn_usuario_modificacion { get; set; }
        public char TUn_estado { get; set; }
        public int TUn_secuencia { get; set; }
        public Categoria_unidad Categoria_unidad { get; set; }


    }
}
