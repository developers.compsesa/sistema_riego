﻿using System;

namespace Generic_appWeb.Models
{
    public class Cultivo
    {
        
        public int Cult_id { get; set; }
        public string Cult_codigo_empresa { get; set; }
        public string Cult_codigo_alterno { get; set; }
        public string Cult_descripcion_larga { get; set; }
        public string Cult_descripcion_med { get; set; }
        public string Cult_descripcion_corta { get; set; }
        public string Cult_abreviacion { get; set; }
        public string Cult_observacion { get; set; }

        public DateTime Cult_fecha_creacion { get; set; }
        public DateTime Cult_fecha_modificacion { get; set; }
        public Usuario Cult_usuario_creacion { get; set; }
        public Usuario Cult_usuario_modificacion { get; set; }
        public char Cult_estado { get; set; }
        public int Cult_secuencia { get; set; }

        public Grupo_cultivo Grupo_cultivo { get; set; }
        public Categoria_cultivo Categoria_cultivo { get; set; }
        public Tipo_cultivo Tipo_cultivo { get; set; }
        public Hacienda Hacienda { get; set; }
    }
}
