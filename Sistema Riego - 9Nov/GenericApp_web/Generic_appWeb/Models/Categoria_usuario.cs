﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_usuario
    {
    
        public int CUs_id { get; set; }
        public string CUs_codigo_empresa { get; set; }
        public string CUs_codigo_alterno { get; set; }
        public string CUs_descripcion_larga { get; set; }
        public string CUs_descripcion_med { get; set; }
        public string CUs_descripcion_corta { get; set; }
        public string CUs_abreviacion { get; set; }
        public string CUs_observacion { get; set; }
        public DateTime CUs_fecha_creacion { get; set; }
        public DateTime CUs_fecha_modificacion { get; set; }
        public Usuario CUs_usuario_creacion { get; set; }
        public Usuario CUs_usuario_modificacion { get; set; }
        public char CUs_estado { get; set; }
        public int CUs_secuencia { get; set; }
        public Tipo_usuario Tipo_usuario { get; set; }

    }
}
