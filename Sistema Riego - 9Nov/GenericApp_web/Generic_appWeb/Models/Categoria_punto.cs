﻿using System;

namespace Generic_appWeb.Models
{
    public class Categoria_punto
    {
        
        public int CPu_id { get; set; }
        public string CPu_codigo_empresa { get; set; }
        public string CPu_codigo_alterno { get; set; }
        public string CPu_descripcion_larga { get; set; }
        public string CPu_descripcion_med { get; set; }
        public string CPu_descripcion_corta { get; set; }
        public string CPu_abreviacion { get; set; }
        public string CPu_observacion { get; set; }
        public DateTime CPu_fecha_creacion { get; set; }
        public DateTime CPu_fecha_modificacion { get; set; }
        public Usuario CPu_usuario_creacion { get; set; }
        public Usuario CPu_usuario_modificacion { get; set; }
        public char CPu_estado { get; set; }
        public int CPu_secuencia { get; set; }
        public Grupo_punto Grupo_punto { get; set; }
        public Hacienda Hacienda { get; set; }

    }
}
