﻿using System;

namespace Generic_appWeb.Models
{
    public class Status
    {
        
        public int St_id { get; set; }
        public string St_codigo_empresa { get; set; }
        public string St_codigo_alterno { get; set; }
        public string St_descripcion_larga { get; set; }
        public string St_descripcion_med { get; set; }
        public string St_descripcion_corta { get; set; }
        public string St_abreviacion { get; set; }
        public string St_observacion { get; set; }

        public DateTime St_fecha_creacion { get; set; }
        public DateTime St_fecha_modificacion { get; set; }
        public Usuario St_usuario_creacion { get; set; }
        public Usuario St_usuario_modificacion { get; set; }
        public char St_estado { get; set; }
        public int St_secuencia { get; set; }
        public Icono Icono { get; set; }

    }
}
