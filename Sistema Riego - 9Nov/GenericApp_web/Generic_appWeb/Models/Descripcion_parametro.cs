﻿using System;

namespace Generic_appWeb.Models
{
    public class Descripcion_parametro
    {
        
        public int DPar_id { get; set; }
        public string DPar_codigo_empresa { get; set; }
        public string DPar_codigo_alterno { get; set; }
        public string DPar_descripcion_larga { get; set; }
        public string DPar_descripcion_med { get; set; }
        public string DPar_descripcion_corta { get; set; }
        public string DPar_abreviacion { get; set; }
        public string DPar_observacion { get; set; }
        public DateTime DPar_fecha_creacion { get; set; }
        public DateTime DPar_fecha_modificacion { get; set; }
        public Usuario DPar_usuario_creacion { get; set; }
        public Usuario DPar_usuario_modificacion { get; set; }
        public char DPar_estado { get; set; }
        public int DPar_secuencia { get; set; }
        public Grupo_parametro Grupo_parametro { get; set; }
        public Categoria_parametro Categoria_parametro { get; set; }
        public Tipo_parametro Tipo_parametro { get; set; }
        public Entidad Entidad { get; set; }

    }
}
