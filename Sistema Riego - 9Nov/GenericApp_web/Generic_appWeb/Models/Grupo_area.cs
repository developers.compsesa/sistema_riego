﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_area
    {
        
        public int GAre_id { get; set; }
        public string GAre_codigo_empresa { get; set; }
        public string GAre_codigo_alterno { get; set; }
        public string GAre_descripcion_larga { get; set; }
        public string GAre_descripcion_med { get; set; }
        public string GAre_descripcion_corta { get; set; }
        public string GAre_abreviacion { get; set; }
        public string GAre_observacion { get; set; }

        public DateTime GAre_fecha_creacion { get; set; }
        public DateTime GAre_fecha_modificacion { get; set; }
        public Usuario GAre_usuario_creacion { get; set; }
        public Usuario GAre_usuario_modificacion { get; set; }
        public char GAre_estado { get; set; }
        public int GAre_secuencia { get; set; }
        public Hacienda Hacienda { get; set; }

        

    }
}
