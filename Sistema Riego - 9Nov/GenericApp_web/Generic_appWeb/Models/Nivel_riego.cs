﻿using System;

namespace Generic_appWeb.Models
{
    public class Nivel_riego
    {
        
        public int Nr_id { get; set; }
        public string Nr_codigo_empresa { get; set; }
        public string Nr_codigo_alterno { get; set; }
        public string Nr_descripcion_larga { get; set; }
        public string Nr_descripcion_med { get; set; }
        public string Nr_descripcion_corta { get; set; }
        public string Nr_abreviacion { get; set; }
        public string Nr_observacion { get; set; }

        public DateTime Nr_fecha_creacion { get; set; }
        public DateTime Nr_fecha_modificacion { get; set; }
        public Usuario Nr_usuario_creacion { get; set; }
        public Usuario Nr_usuario_modificacion { get; set; }
        public char Nr_estado { get; set; }
        public int Nr_secuencia { get; set; }
        public Hacienda Hacienda { get; set; }

    }
}
