﻿using System;

namespace Generic_appWeb.Models
{
    public class Grupo_unidad
    {
        
        public int GUn_id { get; set; }
        public string GUn_codigo_empresa { get; set; }
        public string GUn_codigo_alterno { get; set; }
        public string GUn_descripcion_larga { get; set; }
        public string GUn_descripcion_med { get; set; }
        public string GUn_descripcion_corta { get; set; }
        public string GUn_abreviacion { get; set; }
        public string GUn_observacion { get; set; }

        public DateTime GUn_fecha_creacion { get; set; }
        public DateTime GUn_fecha_modificacion { get; set; }
        public Usuario GUn_usuario_creacion { get; set; }
        public Usuario GUn_usuario_modificacion { get; set; }
        public char GUn_estado { get; set; }
        public int GUn_secuencia { get; set; }

    }
}
