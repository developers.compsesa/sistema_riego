﻿using Generic_appWeb.Models;
using System.Collections.Generic;

namespace Generic_appWeb.Repository
{
    public interface IRepository<T>
    {
        void Add(T item);
        void Remove(int id);
        void Update(T item);
        T FindByID(int id);
        IEnumerable<T> FindAll();
    }
}
