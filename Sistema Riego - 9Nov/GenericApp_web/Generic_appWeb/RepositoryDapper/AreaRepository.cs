﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class AreaRepository : IRepository<Area>
    {
        private string connectionString;

        public AreaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Area item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Are_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Are_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Are_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Are_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Are_descripcion_corta);
                parameters.Add("@_abreviacion", item.Are_abreviacion);
                parameters.Add("@_observacion", item.Are_observacion);
                parameters.Add("@_fecha_creacion", item.Are_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Are_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Are_secuencia);
                parameters.Add("@_tipo_area_id", item.Tipo_area.TAre_id);
                parameters.Add("@_lugar_id", item.Lugar.Lu_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);



                dbConnection.Execute("app_riego.create_area", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Area item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", item.Are_id);
                parameters.Add("@_codigo_empresa", item.Are_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Are_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Are_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Are_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Are_descripcion_corta);
                parameters.Add("@_abreviacion", item.Are_abreviacion);
                parameters.Add("@_observacion", item.Are_observacion);
                parameters.Add("@_fecha_creacion", item.Are_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Are_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Are_secuencia);
                parameters.Add("@_tipo_area_id", item.Tipo_area.TAre_id);
                parameters.Add("@_lugar_id", item.Lugar.Lu_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_area", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Area> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query("app_riego.getallarea", new[]
                {
                    typeof(Area),
                    typeof(Grupo_area),
                    typeof(Categoria_area),
                    typeof(Tipo_area),
                   
                    typeof(Hacienda),
                    typeof(Lugar),
                    typeof(Usuario)

                }
                , obj =>
                {
                    Area Area = obj[0] as Area;
                    Grupo_area Grupo_area = obj[1] as Grupo_area;
                    Categoria_area Categoria_area = obj[2] as Categoria_area;
                    Tipo_area Tipo_area = obj[3] as Tipo_area;
                    
                    Hacienda Hacienda = obj[4] as Hacienda;
                    Lugar Lugar = obj[5] as Lugar;
                    Usuario Usuario_c = obj[6] as Usuario;
                    Usuario Usuario_m = obj[7] as Usuario;


                    Area.Grupo_area = Grupo_area;
                    Area.Categoria_area = Categoria_area;
                    Area.Tipo_area = Tipo_area;
                   
                    Area.Hacienda = Hacienda;
                    Area.Lugar = Lugar;
                    Area.Are_usuario_creacion = Usuario_c;
                    Area.Are_usuario_modificacion = Usuario_m;

                    return Area;
                }, parameters, commandType: CommandType.StoredProcedure,
                             splitOn: "GAre_id, CAre_id, TAre_id, Hac_id, Lu_id, Users_id");

                return result;
            }
        }


        public Area FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query("app_riego.getallarea", new[]
                 {
                    typeof(Area),
                    typeof(Grupo_area),
                    typeof(Categoria_area),
                    typeof(Tipo_area),

                    typeof(Hacienda),
                    typeof(Lugar),
                    typeof(Usuario)

                }
                , obj =>
                {
                    Area Area = obj[0] as Area;
                    Grupo_area Grupo_area = obj[1] as Grupo_area;
                    Categoria_area Categoria_area = obj[2] as Categoria_area;
                    Tipo_area Tipo_area = obj[3] as Tipo_area;

                    Hacienda Hacienda = obj[4] as Hacienda;
                    Lugar Lugar = obj[5] as Lugar;
                    Usuario Usuario_c = obj[6] as Usuario;
                    Usuario Usuario_m = obj[7] as Usuario;


                    Area.Grupo_area = Grupo_area;
                    Area.Categoria_area = Categoria_area;
                    Area.Tipo_area = Tipo_area;

                    Area.Hacienda = Hacienda;
                    Area.Lugar = Lugar;
                    Area.Are_usuario_creacion = Usuario_c;
                    Area.Are_usuario_modificacion = Usuario_m;

                    return Area;
                }, parameters, commandType: CommandType.StoredProcedure,
                             splitOn: "GAre_id, CAre_id, TAre_id, Hac_id, Lu_id, Users_id");

                return result.FirstOrDefault();
            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Areas WHERE Id=@Id", new { Id = id });
            }
        }


       

        public IEnumerable<Area> GetallAreaByLugar(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Area>("app_riego.getallareabylugar", parameters, commandType: CommandType.StoredProcedure);
                return result;

            }
        }




        public IEnumerable<Area> GetallAreasBytipos_areas(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Area>("app_riego.GetallAreasBytipos_areas", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }




        public Area GetAreaByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Area>("app_riego.getallareas_codigo_empresa", parameters, commandType: CommandType.StoredProcedure);


                return result.FirstOrDefault();

                //return dbConnection.Query<Lugares>("Select descripcion_corta from app_riego.lugares where codigo_empresa = @codigo_empresa ", new { codigo_empresa }).SingleOrDefault();

            }

        }
    }
}
