﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_programa_diarioRepository
    {
        private string connectionString;

        public Categoria_programa_diarioRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_programa_diario item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CPD_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CPD_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CPD_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CPD_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CPD_descripcion_corta);
                parameters.Add("@_abreviacion", item.CPD_abreviacion);
                parameters.Add("@_observacion", item.CPD_observacion);               
                parameters.Add("@_fecha_creacion", item.CPD_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CPD_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CPD_secuencia);

                dbConnection.Execute("app_riego.create_categoria_programa_diario", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_programa_diario item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.CPD_id);
                parameters.Add("@_codigo_empresa", item.CPD_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CPD_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CPD_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CPD_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CPD_descripcion_corta);
                parameters.Add("@_abreviacion", item.CPD_abreviacion);
                parameters.Add("@_observacion", item.CPD_observacion);
                parameters.Add("@_fecha_creacion", item.CPD_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CPD_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CPD_secuencia);

                dbConnection.Execute("app_riego.create_categoria_programa_diario", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_programa_diario> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_programa_diario, Usuario, Categoria_programa_diario>("app_riego.getall_categoria_programa_diario", (Categoria_programa_diario, Usuario) =>
                {

                    Categoria_programa_diario.CPD_usuario_creacion = Usuario;
                    Categoria_programa_diario.CPD_usuario_modificacion = Usuario;

                    return Categoria_programa_diario;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id");


                return result;

            }
        }

        public Categoria_programa_diario FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Categoria_programa_diario, Usuario, Categoria_programa_diario>("app_riego.getall_categoria_programa_diario", (Categoria_programa_diario, Usuario) =>
                {                   
                    Categoria_programa_diario.CPD_usuario_creacion = Usuario;
                    Categoria_programa_diario.CPD_usuario_modificacion = Usuario;

                    return Categoria_programa_diario;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Categoria_programa_diario WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Categoria_programa_diario> GetallCategoria_programa_diarioBygrupo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_programa_diario>("app_riego.getallcategoriasvalorbygrupo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



    }
}
