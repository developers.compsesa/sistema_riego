﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_parametroRepository
    {
        private string connectionString;

        public Grupo_parametroRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_parametro item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GPar_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GPar_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GPar_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GPar_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GPar_descripcion_corta);
                parameters.Add("@_abreviacion", item.GPar_abreviacion);
                parameters.Add("@_observacion", item.GPar_observacion);               
                parameters.Add("@_fecha_creacion", item.GPar_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GPar_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GPar_secuencia);

                dbConnection.Execute("app_riego.create_grupo_parametro", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_parametro item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.GPar_id);
                parameters.Add("@_codigo_empresa", item.GPar_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GPar_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GPar_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GPar_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GPar_descripcion_corta);
                parameters.Add("@_abreviacion", item.GPar_abreviacion);
                parameters.Add("@_observacion", item.GPar_observacion);
                parameters.Add("@_fecha_creacion", item.GPar_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GPar_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GPar_secuencia);

                dbConnection.Execute("app_riego.create_grupo_parametro", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_parametro> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_parametro, Usuario, Grupo_parametro>("app_riego.getall_grupo_parametro", (Grupo_parametro, Usuario) =>
                {
                    Grupo_parametro.GPar_usuario_creacion = Usuario;
                    Grupo_parametro.GPar_usuario_modificacion = Usuario;

                    return Grupo_parametro;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Grupo_parametro FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Grupo_parametro, Usuario, Grupo_parametro>("app_riego.getall_grupo_parametro", (Grupo_parametro, Usuario) =>
                {
                    Grupo_parametro.GPar_usuario_creacion = Usuario;
                    Grupo_parametro.GPar_usuario_modificacion = Usuario;

                    return Grupo_parametro;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_parametro WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
