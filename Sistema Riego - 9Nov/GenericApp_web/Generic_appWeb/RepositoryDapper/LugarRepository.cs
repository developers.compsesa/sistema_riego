﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class LugarRepository : IRepository<Lugar>
    {
        private string connectionString;

        public LugarRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Lugar item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                try
                {
                    dbConnection.Open();

                    DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("@_id", 0);
                    parameters.Add("@_codigo_empresa", item.Lu_codigo_empresa);
                    parameters.Add("@_codigo_alterno", item.Lu_codigo_alterno);
                    parameters.Add("@_descripcion_larga", item.Lu_descripcion_larga);
                    parameters.Add("@_descripcion_med", item.Lu_descripcion_med);
                    parameters.Add("@_descripcion_corta", item.Lu_descripcion_corta);
                    parameters.Add("@_abreviacion", item.Lu_abreviacion);
                    parameters.Add("@_observacion", item.Lu_observacion);
                    parameters.Add("@_fecha_creacion", item.Lu_fecha_creacion);
                    parameters.Add("@_fecha_modificacion", item.Lu_fecha_modificacion);
                    parameters.Add("@_usuario_creacion", 1);
                    parameters.Add("@_usuario_modificacion", 1);
                    parameters.Add("@_estado", "A");
                    parameters.Add("@_secuencia", item.Lu_secuencia);
                    parameters.Add("@_grupo_lugar_id", item.Grupo_lugar.GL_id);
                    parameters.Add("@_categoria_lugar_id", item.Categoria_lugar.CL_id);
                    parameters.Add("@_tipo_lugar_id", item.Tipo_lugar.TL_id);
                    parameters.Add("@_entidad_id", item.Hacienda.Hac_id);

                    dbConnection.Execute("app_riego.create_lugar", parameters, commandType: CommandType.StoredProcedure);
                }
                catch (Exception e)
                {


                }

            }
        }
        public void Update(Lugar item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Lu_id);
                parameters.Add("@_codigo_empresa", item.Lu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Lu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Lu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Lu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Lu_descripcion_corta);
                parameters.Add("@_abreviacion", item.Lu_abreviacion);
                parameters.Add("@_observacion", item.Lu_observacion);
                parameters.Add("@_fecha_creacion", item.Lu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Lu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Lu_secuencia);
                parameters.Add("@_grupo_lugar_id", item.Grupo_lugar.GL_id);
                parameters.Add("@_categoria_lugar_id", item.Categoria_lugar.CL_id);
                parameters.Add("@_tipo_lugar_id", item.Tipo_lugar.TL_id);
                parameters.Add("@_entidad_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_lugar", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public IEnumerable<Lugar> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query("app_riego.getallLugar", new[]
                {
                    typeof(Lugar),
                    typeof(Grupo_lugar),
                    typeof(Categoria_lugar),
                    typeof(Tipo_lugar),                   
                    typeof(Hacienda),                 
                    typeof(Usuario)

                }
                , obj =>
                {
                    Lugar Lugar = obj[0] as Lugar;
                    Grupo_lugar Grupo_lugar = obj[1] as Grupo_lugar;
                    Categoria_lugar Categoria_lugar = obj[2] as Categoria_lugar;
                    Tipo_lugar Tipo_lugar = obj[3] as Tipo_lugar;
                    Hacienda Hacienda = obj[4] as Hacienda;                                   
                    Usuario Usuario = obj[5] as Usuario;


                    Lugar.Grupo_lugar = Grupo_lugar;
                    Lugar.Categoria_lugar = Categoria_lugar;
                    Lugar.Tipo_lugar = Tipo_lugar;
                    Lugar.Hacienda = Hacienda;                                    
                    Lugar.Lu_usuario_creacion = Usuario;
                    Lugar.Lu_usuario_modificacion = Usuario;

                    return Lugar;
                }, parameters, commandType: CommandType.StoredProcedure,
                             splitOn: "GL_id, CL_id, TL_id, Hac_id , Users_id");

                return result;
            }
        }

        public Lugar FindByID(int Id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", Id);

                var result = dbConnection.Query("app_riego.getallLugar", new[]
                 {
                    typeof(Lugar),
                    typeof(Grupo_lugar),
                    typeof(Categoria_lugar),
                    typeof(Tipo_lugar),
                    typeof(Hacienda),
                    typeof(Usuario)

                }
                 , obj =>
                 {
                     Lugar Lugar = obj[0] as Lugar;
                     Grupo_lugar Grupo_lugar = obj[1] as Grupo_lugar;
                     Categoria_lugar Categoria_lugar = obj[2] as Categoria_lugar;
                     Tipo_lugar Tipo_lugar = obj[3] as Tipo_lugar;
                     Hacienda Hacienda = obj[4] as Hacienda;
                     Usuario Usuario = obj[5] as Usuario;


                     Lugar.Grupo_lugar = Grupo_lugar;
                     Lugar.Categoria_lugar = Categoria_lugar;
                     Lugar.Tipo_lugar = Tipo_lugar;
                     Lugar.Hacienda = Hacienda;                    
                     Lugar.Lu_usuario_creacion = Usuario;
                     Lugar.Lu_usuario_modificacion = Usuario;

                     return Lugar;
                 }, parameters, commandType: CommandType.StoredProcedure,
                              splitOn: "GL_id, CL_id, TL_id, Hac_id , Users_id");


                return result.FirstOrDefault();
            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Lugar WHERE Id=@Id", new { Id = id });
            }
        }




        public IEnumerable<Lugar> FindAllLugarFiltered(int grupo_lugar_id, int categoria_lugar_id, int tipo_lugar_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (grupo_lugar_id != 0)
                {
                    if (categoria_lugar_id != 0)
                    {
                        if (tipo_lugar_id != 0)
                        {
                            flag = 3;

                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);

                parameters.Add("@_grupo_lugar_id", grupo_lugar_id);
                parameters.Add("@_categoria_lugar_id", categoria_lugar_id);
                parameters.Add("@_tipo_lugar_id", tipo_lugar_id);

                var result = dbConnection.Query("app_riego.getallLugar_filtered", new[]
                 {
                    typeof(Lugar),
                    typeof(Grupo_lugar),
                    typeof(Categoria_lugar),
                    typeof(Tipo_lugar),
                    typeof(Hacienda),
                    typeof(Usuario)

                }
                 , obj =>
                 {
                     Lugar Lugar = obj[0] as Lugar;
                     Grupo_lugar Grupo_lugar = obj[1] as Grupo_lugar;
                     Categoria_lugar Categoria_lugar = obj[2] as Categoria_lugar;
                     Tipo_lugar Tipo_lugar = obj[3] as Tipo_lugar;
                     Hacienda Hacienda = obj[4] as Hacienda;
                     Usuario Usuario = obj[5] as Usuario;


                     Lugar.Grupo_lugar = Grupo_lugar;
                     Lugar.Categoria_lugar = Categoria_lugar;
                     Lugar.Tipo_lugar = Tipo_lugar;
                     Lugar.Hacienda = Hacienda;
                     Lugar.Lu_usuario_creacion = Usuario;
                     Lugar.Lu_usuario_modificacion = Usuario;

                     return Lugar;
                 }, parameters, commandType: CommandType.StoredProcedure,
                              splitOn: "GL_id, CL_id, TL_id, Hac_id , Users_id");

                return result;
            }
        }




        public IEnumerable<Lugar> GetallLugarByCiudades(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Lugar>("app_riego.getalllugaresbyciudades", parameters, commandType: CommandType.StoredProcedure);
                return result;

            }
        }

        public IEnumerable<Lugar> GetallLugarBytipos_lugares(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Lugar>("app_riego.getalllugaresbytipo_lugares", parameters, commandType: CommandType.StoredProcedure);
                return result;

            }
        }


        public Lugar GetLugarByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Lugar>("app_riego.getalllugares_codigo_empresa",parameters, commandType: CommandType.StoredProcedure);


                return result.FirstOrDefault();

                //return dbConnection.Query<Lugar>("Select descripcion_corta from app_riego.lugares where codigo_empresa = @codigo_empresa ", new { codigo_empresa }).SingleOrDefault();

            }

        }



    }
}
