﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Descripcion_valorRepository
    {
        private string connectionString;

        public Descripcion_valorRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Descripcion_valor item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Val_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Val_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Val_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Val_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Val_descripcion_corta);
                parameters.Add("@_abreviacion", item.Val_abreviacion);
                parameters.Add("@_observacion", item.Val_observacion);
                parameters.Add("@_fecha_creacion", item.Val_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Val_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Val_secuencia);
                parameters.Add("@_grupo_valor_id", item.Grupo_valor.GVal_id);
                parameters.Add("@_categoria_valor_id", item.Categoria_valor.CVal_id);
                parameters.Add("@_tipo_valor_id", item.Tipo_valor.TVal_id);

                dbConnection.Execute("app_riego.create_descripcion_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Descripcion_valor item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Val_id);
                parameters.Add("@_codigo_empresa", item.Val_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Val_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Val_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Val_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Val_descripcion_corta);
                parameters.Add("@_abreviacion", item.Val_abreviacion);
                parameters.Add("@_observacion", item.Val_observacion);
                parameters.Add("@_fecha_creacion", item.Val_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Val_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Val_secuencia);
                parameters.Add("@_grupo_valor_id", item.Grupo_valor.GVal_id);
                parameters.Add("@_categoria_valor_id", item.Categoria_valor.CVal_id);
                parameters.Add("@_tipo_valor_id", item.Tipo_valor.TVal_id);

                dbConnection.Execute("app_riego.create_descripcion_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Descripcion_valor> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Descripcion_valor, Grupo_valor, Categoria_valor, Tipo_valor, Usuario, Descripcion_valor>("app_riego.getall_descripcion_valor", (Descripcion_valor, Grupo_valor, Categoria_valor, Tipo_valor, Usuario) =>
                {
                    Descripcion_valor.Grupo_valor = Grupo_valor;
                    Descripcion_valor.Categoria_valor = Categoria_valor;
                    Descripcion_valor.Tipo_valor = Tipo_valor;
                    Descripcion_valor.Val_usuario_creacion = Usuario;
                    Descripcion_valor.Val_usuario_modificacion = Usuario;


                    return Descripcion_valor;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GVal_id,CVal_id,TVal_id, Users_id");


                return result;

            }
        }

      

        public Descripcion_valor FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Descripcion_valor, Grupo_valor, Categoria_valor, Tipo_valor, Usuario, Descripcion_valor>("app_riego.getall_descripcion_valor", (Descripcion_valor, Grupo_valor, Categoria_valor, Tipo_valor, Usuario) =>
                {
                    Descripcion_valor.Grupo_valor = Grupo_valor;
                    Descripcion_valor.Categoria_valor = Categoria_valor;
                    Descripcion_valor.Tipo_valor = Tipo_valor;
                    Descripcion_valor.Val_usuario_creacion = Usuario;
                    Descripcion_valor.Val_usuario_modificacion = Usuario;


                    return Descripcion_valor;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "GVal_id,CVal_id,TVal_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.inventario_valor WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Descripcion_valor> FindAllFiltered(int? grupo_valor_id, int? categoria_valor_id, int? tipo_valor_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (grupo_valor_id != 0)
                {
                    if (categoria_valor_id != 0)
                    {
                        if (tipo_valor_id != 0)
                        {
                            flag = 3;
                            //if (subtipo_activo_id != 0)
                            //{
                            //    flag = 4;
                            //}
                            //else
                            //{
                            //    flag = 3;
                            //}
                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);

                parameters.Add("@_grupo_valor_id", grupo_valor_id);
                parameters.Add("@_categoria_valor_id", categoria_valor_id);
                parameters.Add("@_tipo_valor_id", tipo_valor_id);
                //parameters.Add("@_subtipo_activo_id", subtipo_activo_id);

                var result = dbConnection.Query<Descripcion_valor, Grupo_valor, Categoria_valor, Tipo_valor, Usuario, Descripcion_valor>("app_riego.getall_descripcion_valor", (Descripcion_valor, Grupo_valor, Categoria_valor, Tipo_valor, Usuario) =>
                {
                    Descripcion_valor.Grupo_valor = Grupo_valor;
                    Descripcion_valor.Categoria_valor = Categoria_valor;
                    Descripcion_valor.Tipo_valor = Tipo_valor;
                    Descripcion_valor.Val_usuario_creacion = Usuario;
                    Descripcion_valor.Val_usuario_modificacion = Usuario;


                    return Descripcion_valor;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "GVal_id,CVal_id,TVal_id, Users_id");


                return result;
            }
        }


        public IEnumerable<Descripcion_valor> GetallValoresByTipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Descripcion_valor>("app_riego.getalltiposbycategoriavalor", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public Descripcion_valor GetValorByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
           
                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Descripcion_valor, Grupo_valor, Categoria_valor, Tipo_valor, Usuario, Descripcion_valor>("app_riego.getall_descripcion_valor", (Descripcion_valor, Grupo_valor, Categoria_valor, Tipo_valor, Usuario) =>
                {
                    Descripcion_valor.Grupo_valor = Grupo_valor;
                    Descripcion_valor.Categoria_valor = Categoria_valor;
                    Descripcion_valor.Tipo_valor = Tipo_valor;
                    Descripcion_valor.Val_usuario_creacion = Usuario;
                    Descripcion_valor.Val_usuario_modificacion = Usuario;


                    return Descripcion_valor;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GVal_id,CVal_id,TVal_id, Users_id");


                return result.FirstOrDefault();

            }

        }


        public IEnumerable<Descripcion_valor> FindAllUnidades()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();


                var result = dbConnection.Query<Descripcion_valor>("app_riego.getallunidades2", commandType: CommandType.StoredProcedure);
                return result;

            }
        }


        public IEnumerable<Descripcion_valor> FindAllinv_valor()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();


                var result = dbConnection.Query<Descripcion_valor>("app_riego.getallinv_valor2", commandType: CommandType.StoredProcedure);
                return result;

            }
        }





    }
}
