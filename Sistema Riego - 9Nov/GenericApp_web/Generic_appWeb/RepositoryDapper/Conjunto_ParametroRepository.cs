﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Conjunto_ParametroRepository
    {
        private string connectionString;

        public Conjunto_ParametroRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Conjunto_parametro item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("@_id", 0);
                    parameters.Add("@_codigo_empresa", item.CParm_codigo_empresa);
                    parameters.Add("@_codigo_alterno", item.CParm_codigo_alterno);
                    parameters.Add("@_descripcion_larga", item.CParm_descripcion_larga);
                    parameters.Add("@_descripcion_med", item.CParm_descripcion_med);
                    parameters.Add("@_descripcion_corta", item.CParm_descripcion_corta);
                    parameters.Add("@_abreviacion", item.CParm_abreviacion);
                    parameters.Add("@_observacion", item.CParm_observacion);
                    parameters.Add("@_fecha_creacion", item.CParm_fecha_creacion);
                    parameters.Add("@_fecha_modificacion", item.CParm_fecha_creacion);
                    parameters.Add("@_usuario_creacion", 1);
                    parameters.Add("@_usuario_modificacion", 1);
                    parameters.Add("@_estado", "A");
                    parameters.Add("@_secuencia", item.CParm_secuencia);
                    parameters.Add("@_descripcion_conjunto_parametro_id", item.descripcion_conjunto_parametro_id);
                    parameters.Add("@_parametro_id", item.Parametro.Par_id);
                    parameters.Add("@_valor", item.valor);

                dbConnection.Execute("app_riego.create_conjunto_parametro", parameters, commandType: CommandType.StoredProcedure);
                
            }
        }
       
                                          
          public void Update(Conjunto_parametro item)
          {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.CParm_id);
                parameters.Add("@_codigo_empresa", item.CParm_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CParm_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CParm_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CParm_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CParm_descripcion_corta);
                parameters.Add("@_abreviacion", item.CParm_abreviacion);
                parameters.Add("@_observacion", item.CParm_observacion);
                parameters.Add("@_fecha_creacion", item.CParm_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CParm_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CParm_secuencia);
                parameters.Add("@_descripcion_conjunto_parametro_id", item.descripcion_conjunto_parametro_id);
                parameters.Add("@_parametro_id", item.Parametro.Par_id);
                parameters.Add("@_valor", item.valor);

                dbConnection.Execute("app_riego.create_conjunto_parametro", parameters, commandType: CommandType.StoredProcedure);
            }
          }

        public IEnumerable<Conjunto_parametro> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Conjunto_parametro, Parametro, Usuario, Conjunto_parametro>("app_riego.getall_conjunto_dato_tecnico", (Conjunto_parametro, Parametro , Usuario) =>
                {
                    Conjunto_parametro.Parametro = Parametro;
                    Conjunto_parametro.CParm_usuario_creacion = Usuario;
                    Conjunto_parametro.CParm_usuario_modificacion = Usuario;

                    return Conjunto_parametro;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Par_id , Users_id");


                return result;

            }
        }

        public Conjunto_parametro FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Conjunto_parametro, Parametro, Usuario, Conjunto_parametro>("app_riego.getall_conjunto_dato_tecnico", (Conjunto_parametro, Parametro, Usuario) =>
                {
                    Conjunto_parametro.Parametro = Parametro;
                    Conjunto_parametro.CParm_usuario_creacion = Usuario;
                    Conjunto_parametro.CParm_usuario_modificacion = Usuario;

                    return Conjunto_parametro;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "Par_id , Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Conjunto_datos_tecnicos WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
