﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class CiudadRepository : IRepository<Ciudad>
    {
        private string connectionString;

        public CiudadRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Ciudad item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Ci_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Ci_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Ci_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Ci_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Ci_descripcion_corta);
                parameters.Add("@_abreviacion", item.Ci_abreviacion);
                parameters.Add("@_observacion", item.Ci_observacion);
                parameters.Add("@_fecha_creacion", item.Ci_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Ci_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Ci_secuencia);
                parameters.Add("@_provincia_id", item.Provincia.Pr_id);

                
                dbConnection.Execute("app_riego.create_ciudad", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Ciudad item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", item.Ci_id);
                parameters.Add("@_codigo_empresa", item.Ci_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Ci_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Ci_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Ci_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Ci_descripcion_corta);
                parameters.Add("@_abreviacion", item.Ci_abreviacion);
                parameters.Add("@_observacion", item.Ci_observacion);
                parameters.Add("@_fecha_creacion", item.Ci_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Ci_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Ci_secuencia);
                parameters.Add("@_provincia_id", item.Provincia.Pr_id);
                dbConnection.Execute("app_riego.create_ciudad", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Ciudad> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Ciudad, Provincia,  Usuario, Ciudad>("app_riego.getall_ciudad", (Ciudad, Provincia, Usuario) =>
                {
                    Ciudad.Ci_usuario_creacion = Usuario;
                    Ciudad.Ci_usuario_modificacion = Usuario;
                    Ciudad.Provincia = Provincia;

                    return Ciudad;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Ci_id, Pr_id, Users_id");


                return result;

            }
        }

       



        public Ciudad FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Ciudad, Provincia, Usuario, Ciudad>("app_riego.getall_ciudad", (Ciudad, Provincia, Usuario) =>
                {
                    Ciudad.Ci_usuario_creacion = Usuario;
                    Ciudad.Ci_usuario_modificacion = Usuario;
                    Ciudad.Provincia = Provincia;

                    return Ciudad;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "Ci_id, Pr_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Ciudad WHERE Id=@Id", new { Id = id });
            }
        }

      




        


   public IEnumerable<Ciudad> GetallCiudadbyProvincias(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Ciudad>("app_riego.getallciudadesbyprovincias", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }










    }
}
