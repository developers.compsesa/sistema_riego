﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_cultivoRepository
    {
        private string connectionString;

        public Categoria_cultivoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_cultivo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CCul_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CCul_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CCul_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CCul_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CCul_descripcion_corta);
                parameters.Add("@_abreviacion", item.CCul_abreviacion);
                parameters.Add("@_observacion", item.CCul_observacion);

                parameters.Add("@_fecha_creacion", item.CCul_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CCul_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CCul_secuencia);
                parameters.Add("@_grupo_cultivo_id", item.Grupo_cultivo.GCul_id);
                dbConnection.Execute("app_riego.create_categoria_cultivo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_cultivo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", item.CCul_id);
                parameters.Add("@_codigo_empresa", item.CCul_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CCul_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CCul_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CCul_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CCul_descripcion_corta);
                parameters.Add("@_abreviacion", item.CCul_abreviacion);
                parameters.Add("@_observacion", item.CCul_observacion);
                parameters.Add("@_fecha_creacion", item.CCul_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CCul_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CCul_secuencia);
                parameters.Add("@_grupo_cultivo_id", item.Grupo_cultivo.GCul_id);
                dbConnection.Execute("app_riego.create_categoria_cultivo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_cultivo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_cultivo,  Usuario, Grupo_cultivo, Categoria_cultivo>("app_riego.getall_categoria_cultivo", (Categoria_cultivo, Usuario, Grupo_cultivo) =>
                {
                    
                    Categoria_cultivo.CCul_usuario_creacion = Usuario;
                    Categoria_cultivo.CCul_usuario_modificacion = Usuario;
                    Categoria_cultivo.Grupo_cultivo = Grupo_cultivo;
                    return Categoria_cultivo;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id,GCul_id");
                return result;
            }
        }




        public Categoria_cultivo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_cultivo, Usuario, Grupo_cultivo,  Categoria_cultivo>("app_riego.getall_categoria_cultivo", (Categoria_cultivo, Usuario, Grupo_cultivo) =>
                {
                    
                    Categoria_cultivo.CCul_usuario_creacion = Usuario;
                    Categoria_cultivo.CCul_usuario_modificacion = Usuario;
                    Categoria_cultivo.Grupo_cultivo = Grupo_cultivo;

                    return Categoria_cultivo;
                }, parameters, commandType: CommandType.StoredProcedure,
                   splitOn: "Users_id,GCul_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("UPDATE app_riego.categoria_cultivo SET estado = 'I' WHERE Id=@Id", new { Id = id });
            }
        }




        public IEnumerable<Categoria_cultivo> Getcmb_categoriabygrupo_cultivo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_cultivo>("app_riego.getcmb_categoriabygrupo_cultivo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }
    }
}
