﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Parametrizacion_equipoRepository
    { 
        private string connectionString;

        public Parametrizacion_equipoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Parametrizacion_equipo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();                

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.PEq_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.PEq_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.PEq_descripcion_larga);
                parameters.Add("@_descripcion_med", item.PEq_descripcion_med);
                parameters.Add("@_descripcion_corta", item.PEq_descripcion_corta);
                parameters.Add("@_abreviacion", item.PEq_abreviacion);
                parameters.Add("@_observacion", item.PEq_observacion);
                parameters.Add("@_fecha_creacion", item.PEq_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.PEq_fecha_modificacion);
                parameters.Add("@_usuario_creacion", "admin");
                parameters.Add("@_usuario_modificacion", "admin");
                parameters.Add("@_secuencia", item.PEq_secuencia);

                parameters.Add("@_factor_riego", item.PEq_factor_riego);
                parameters.Add("@_cantidad_puntos_riego", item.PEq_cantidad_puntos_riego);
                parameters.Add("@_m2_aspersor", item.PEq_m2_aspersor);
                parameters.Add("@_caudal_l_h_x_m2", item.PEq_caudal_l_h_x_m2);
                parameters.Add("@_padre", item.PEq_padre);


                parameters.Add("@_equipo_id", item.Equipo.Eq_id);
                
                parameters.Add("@_grupo_riego_id", item.Grupo_riego.Gr_id);

                parameters.Add("@_grupo_valvula_id", item.Grupo_Valvula.Gv_id);

                parameters.Add("@_nivel_riego_id", item.Nivel_riego.Nr_id);
               
                dbConnection.Execute("app_riego.create_parametrizacion_equipo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Parametrizacion_equipo item)
        {

            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.PEq_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.PEq_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.PEq_descripcion_larga);
                parameters.Add("@_descripcion_med", item.PEq_descripcion_med);
                parameters.Add("@_descripcion_corta", item.PEq_descripcion_corta);
                parameters.Add("@_abreviacion", item.PEq_abreviacion);
                parameters.Add("@_observacion", item.PEq_observacion);
                parameters.Add("@_fecha_creacion", item.PEq_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.PEq_fecha_modificacion);
                parameters.Add("@_usuario_creacion", "admin");
                parameters.Add("@_usuario_modificacion", "admin");
                parameters.Add("@_secuencia", item.PEq_secuencia);

                parameters.Add("@_factor_riego", item.PEq_factor_riego);
                parameters.Add("@_cantidad_puntos_riego", item.PEq_cantidad_puntos_riego);
                parameters.Add("@_m2_aspersor", item.PEq_m2_aspersor);
                parameters.Add("@_caudal_l_h_x_m2", item.PEq_caudal_l_h_x_m2);
                parameters.Add("@_padre", item.PEq_padre);


                parameters.Add("@_equipo_id", item.Equipo.Eq_id);

                parameters.Add("@_grupo_riego_id", item.Grupo_riego.Gr_id);

                parameters.Add("@_grupo_valvula_id", item.Grupo_Valvula.Gv_id);

                parameters.Add("@_nivel_riego_id", item.Nivel_riego.Nr_id);


                dbConnection.Execute("app_riego.create_parametrizacion_equipo", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        public IEnumerable<Parametrizacion_equipo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);


                var result = dbConnection.Query<Parametrizacion_equipo,Usuario, Equipo, Bloque_riego, Grupo_riego, Grupo_valvula, Nivel_riego, Parametrizacion_equipo>("app_riego.getall_parametrizacion_equipo", (Parametrizacion_equipo,Usuario, Equipo, Bloque_riego, Grupo_riego, Grupo_valvula, Nivel_riego) =>
                {
                    Parametrizacion_equipo.PEq_usuario_creacion = Usuario;
                    Parametrizacion_equipo.PEq_usuario_modificacion = Usuario;
                   
                    Parametrizacion_equipo.Equipo = Equipo;
                    
                    Parametrizacion_equipo.Bloque_riego = Bloque_riego;
                    Parametrizacion_equipo.Grupo_riego = Grupo_riego;
                    Parametrizacion_equipo.Grupo_Valvula = Grupo_valvula;
                    Parametrizacion_equipo.Nivel_riego = Nivel_riego;

                    return Parametrizacion_equipo;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id,Eq_id, Bri_id,Gr_id,Gv_id,Nr_id");
                return result;
            }
        }


        public Parametrizacion_equipo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Parametrizacion_equipo, Usuario, Equipo, Bloque_riego, Grupo_riego, Grupo_valvula, Nivel_riego, Parametrizacion_equipo>("app_riego.getall_parametrizacion_equipo", (Parametrizacion_equipo, Usuario, Equipo, Bloque_riego, Grupo_riego, Grupo_valvula, Nivel_riego) =>
                {
                    Parametrizacion_equipo.PEq_usuario_creacion = Usuario;
                    Parametrizacion_equipo.PEq_usuario_modificacion = Usuario;
                    Parametrizacion_equipo.Equipo = Equipo;

                    Parametrizacion_equipo.Bloque_riego = Bloque_riego;
                    Parametrizacion_equipo.Grupo_riego = Grupo_riego;
                    Parametrizacion_equipo.Grupo_Valvula = Grupo_valvula;
                    Parametrizacion_equipo.Nivel_riego = Nivel_riego;

                    return Parametrizacion_equipo;

                }, parameters, commandType: CommandType.StoredProcedure,
                   splitOn: "Users_id,Eq_id, Bri_id,Gr_id,Gv_id,Nr_id");


                return result.FirstOrDefault();

            }
        }







        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("UPDATE app_riego.parametrizacion_equipo SET estado = 'I' WHERE Id=@Id", new { Id = id });
            }
        }






    }
}
