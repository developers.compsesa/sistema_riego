﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_valorRepository
    {
        private string connectionString;

        public Grupo_valorRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_valor item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GVal_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GVal_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GVal_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GVal_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GVal_descripcion_corta);
                parameters.Add("@_abreviacion", item.GVal_abreviacion);
                parameters.Add("@_observacion", item.GVal_observacion);
               
                parameters.Add("@_fecha_creacion", item.GVal_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GVal_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GVal_secuencia);

                dbConnection.Execute("app_riego.create_grupo_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_valor item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.GVal_id);
                parameters.Add("@_codigo_empresa", item.GVal_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GVal_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GVal_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GVal_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GVal_descripcion_corta);
                parameters.Add("@_abreviacion", item.GVal_abreviacion);
                parameters.Add("@_observacion", item.GVal_observacion);

                parameters.Add("@_fecha_creacion", item.GVal_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GVal_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GVal_secuencia);

                dbConnection.Execute("app_riego.create_grupo_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_valor> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_valor, Usuario, Grupo_valor>("app_riego.getall_grupo_valor", (Grupo_valor, Usuario) =>
                {
                    Grupo_valor.GVal_usuario_creacion = Usuario;
                    Grupo_valor.GVal_usuario_modificacion = Usuario;

                    return Grupo_valor;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Grupo_valor FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Grupo_valor, Usuario, Grupo_valor>("app_riego.getall_grupo_valor", (Grupo_valor, Usuario) =>
                {
                    Grupo_valor.GVal_usuario_creacion = Usuario;
                    Grupo_valor.GVal_usuario_modificacion = Usuario;

                    return Grupo_valor;
                }, parameters, commandType: CommandType.StoredProcedure,
                    splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_valor WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
