﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_lugarRepository : IRepository<Grupo_lugar>
    {
        private string connectionString;

        public Grupo_lugarRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_lugar item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GL_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GL_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GL_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GL_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GL_descripcion_corta);
                parameters.Add("@_abreviacion", item.GL_abreviacion);
                parameters.Add("@_observacion", item.GL_observacion);
                parameters.Add("@_fecha_creacion", item.GL_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GL_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GL_secuencia);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_grupo_lugar", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Grupo_lugar item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.GL_id);
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GL_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GL_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GL_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GL_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GL_descripcion_corta);
                parameters.Add("@_abreviacion", item.GL_abreviacion);
                parameters.Add("@_observacion", item.GL_observacion);
                parameters.Add("@_fecha_creacion", item.GL_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GL_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GL_secuencia);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_grupo_lugar", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_lugar> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_lugar, Usuario, Hacienda, Grupo_lugar>("app_riego.getall_grupo_lugar", (Grupo_lugar, Usuario, Hacienda) =>
                {
                    Grupo_lugar.GL_usuario_creacion = Usuario;
                    Grupo_lugar.GL_usuario_modificacion = Usuario;
                    Grupo_lugar.Hacienda = Hacienda;

                    return Grupo_lugar;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GL_id, Users_id , Hac_id");


                return result;

            }
        }

        public Grupo_lugar FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Grupo_lugar, Usuario, Hacienda, Grupo_lugar>("app_riego.getall_grupo_lugar", (Grupo_lugar, Usuario, Hacienda) =>
                {
                    Grupo_lugar.GL_usuario_creacion = Usuario;
                    Grupo_lugar.GL_usuario_modificacion = Usuario;
                    Grupo_lugar.Hacienda = Hacienda;

                    return Grupo_lugar;

                }, parameters, commandType: CommandType.StoredProcedure,
                                splitOn: "GL_id, Users_id , Hac_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_lugar WHERE Id=@Id", new { Id = id });
            }
        }

      
    }
}
