﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_unidadRepository
    {
        private string connectionString;

        public Tipo_unidadRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_unidad item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TUn_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TUn_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TUn_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TUn_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TUn_descripcion_corta);
                parameters.Add("@_abreviacion", item.TUn_abreviacion);
                parameters.Add("@_observacion", item.TUn_observacion);

                parameters.Add("@_fecha_creacion", item.TUn_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TUn_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TUn_secuencia);

                dbConnection.Execute("app_riego.create_tipo_unidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }
                           


        public void Update(Tipo_unidad item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TUn_id);
                parameters.Add("@_codigo_empresa", item.TUn_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TUn_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TUn_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TUn_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TUn_descripcion_corta);
                parameters.Add("@_abreviacion", item.TUn_abreviacion);
                parameters.Add("@_observacion", item.TUn_observacion);

                parameters.Add("@_fecha_creacion", item.TUn_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TUn_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TUn_secuencia);

                dbConnection.Execute("app_riego.create_tipo_unidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }






        public IEnumerable<Tipo_unidad> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
               
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_unidad, Categoria_unidad, Usuario, Tipo_unidad>("app_riego.getall_tipo_unidad", (Tipo_unidad, Categoria_unidad, Usuario) =>
                {
                    Tipo_unidad.Categoria_unidad = Categoria_unidad;
                    Tipo_unidad.TUn_usuario_creacion = Usuario;
                    Tipo_unidad.TUn_usuario_modificacion = Usuario;

                    return Tipo_unidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CUn_id, E_id");


                return result;

            }
        }





        public Tipo_unidad FindByID(int id)
        {

            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_unidad, Categoria_unidad, Usuario, Tipo_unidad>("app_riego.getall_tipo_unidad", (Tipo_unidad, Categoria_unidad, Usuario) =>
                {
                    Tipo_unidad.Categoria_unidad = Categoria_unidad;
                    Tipo_unidad.TUn_usuario_creacion = Usuario;
                    Tipo_unidad.TUn_usuario_modificacion = Usuario;

                    return Tipo_unidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CUn_id, E_id");


                return result.FirstOrDefault();

            }
        }



       


        


        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_unidad WHERE Id=@Id", new { Id = id });
            }
        }





        public IEnumerable<Tipo_unidad> Getcmb_tipobycategoria_unidad(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_unidad>("app_riego.getcmb_tipobycategoria_unidad", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


    }
}
