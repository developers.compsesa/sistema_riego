﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_areaRepository
    {
        private string connectionString;

        public Categoria_areaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_area item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);               
                parameters.Add("@_codigo_alterno", item.CAre_codigo_alterno);
                parameters.Add("@_codigo_empresa", item.CAre_codigo_empresa);
                parameters.Add("@_descripcion_larga", item.CAre_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CAre_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CAre_descripcion_corta);
                parameters.Add("@_abreviacion", item.CAre_abreviacion);
                parameters.Add("@_observacion", item.CAre_observacion);
                parameters.Add("@_grupo_area_id", item.Grupo_area.GAre_id);
                parameters.Add("@_fecha_creacion", item.CAre_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CAre_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CAre_secuencia);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_categoria_area", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_area item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", item.CAre_id);               
                parameters.Add("@_codigo_alterno", item.CAre_codigo_alterno);
                parameters.Add("@_codigo_empresa", item.CAre_codigo_empresa);
                parameters.Add("@_descripcion_larga", item.CAre_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CAre_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CAre_descripcion_corta);
                parameters.Add("@_abreviacion", item.CAre_abreviacion);
                parameters.Add("@_observacion", item.CAre_observacion);
                parameters.Add("@_grupo_area_id", item.Grupo_area.GAre_id);
                parameters.Add("@_fecha_creacion", item.CAre_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CAre_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CAre_secuencia);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_categoria_area", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_area> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_area, Grupo_area, Usuario, Hacienda, Categoria_area>("app_riego.getallcategoria_area", (Categoria_area, Grupo_area, Usuario, Hacienda) =>
                {
                    Categoria_area.Grupo_area = Grupo_area;
                    Categoria_area.CAre_usuario_creacion = Usuario;
                    Categoria_area.CAre_usuario_modificacion = Usuario;
                    Categoria_area.Hacienda = Hacienda;
                    return Categoria_area;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "GAre_id, Users_id , Hac_id");
                return result;
            }
        }




        public Categoria_area FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_area, Grupo_area, Usuario, Hacienda, Categoria_area>("app_riego.getall_categoria_area", (Categoria_area, Grupo_area, Usuario, Hacienda) =>
                {
                    Categoria_area.Grupo_area = Grupo_area;
                    Categoria_area.CAre_usuario_creacion = Usuario;
                    Categoria_area.CAre_usuario_modificacion = Usuario;
                    Categoria_area.Hacienda = Hacienda;
                    return Categoria_area;
                }, parameters, commandType: CommandType.StoredProcedure,
                   splitOn: "GAre_id, Users_id , Hac_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("UPDATE app_riego.categoria_area SET estado = 'I' WHERE Id=@Id", new { Id = id });
            }
        }




        public IEnumerable<Categoria_area> Getcmb_categoriabygrupo_area(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_area>("app_riego.getcmb_categoriabygrupo_area", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }
    }
}
