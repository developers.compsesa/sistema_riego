﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_entidadRepository
    {
        private string connectionString;

        public Grupo_entidadRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_entidad item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GEn_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GEn_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GEn_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GEn_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GEn_descripcion_corta);
                parameters.Add("@_abreviacion", item.GEn_abreviacion);
                parameters.Add("@_observacion", item.GEn_observacion);
               
                parameters.Add("@_fecha_creacion", item.GEn_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GEn_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GEn_secuencia);

                dbConnection.Execute("app_riego.create_grupo_entidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_entidad item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", item.GEn_id);

                parameters.Add("@_codigo_empresa", item.GEn_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GEn_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GEn_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GEn_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GEn_descripcion_corta);
                parameters.Add("@_abreviacion", item.GEn_abreviacion);
                parameters.Add("@_observacion", item.GEn_observacion);

                parameters.Add("@_fecha_creacion", item.GEn_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GEn_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GEn_secuencia);

                dbConnection.Execute("app_riego.create_grupo_entidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_entidad> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_entidad, Usuario, Grupo_entidad>("app_riego.getall_grupo_entidad", (Grupo_entidad, Usuario) =>
                {
                    Grupo_entidad.GEn_usuario_creacion = Usuario;
                    Grupo_entidad.GEn_usuario_modificacion = Usuario;

                    return Grupo_entidad;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Grupo_entidad FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);



                var result = dbConnection.Query<Grupo_entidad, Usuario, Grupo_entidad>("app_riego.getall_grupo_entidad", (Grupo_entidad, Usuario) =>
                {
                    Grupo_entidad.GEn_usuario_creacion = Usuario;
                    Grupo_entidad.GEn_usuario_modificacion = Usuario;

                    return Grupo_entidad;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_entidad WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
