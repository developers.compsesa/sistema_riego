﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class PendienteRepository
    {
        private string connectionString;

        public PendienteRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Pendiente item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Pen_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Pen_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Pen_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Pen_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Pen_descripcion_corta);
                parameters.Add("@_abreviacion", item.Pen_abreviacion);
                parameters.Add("@_observacion", item.Pen_observacion);               
                parameters.Add("@_fecha_creacion", item.Pen_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Pen_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Pen_secuencia);

                dbConnection.Execute("app_riego.create_pendiente", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Pendiente item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Pen_id);
                parameters.Add("@_codigo_empresa", item.Pen_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Pen_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Pen_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Pen_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Pen_descripcion_corta);
                parameters.Add("@_abreviacion", item.Pen_abreviacion);
                parameters.Add("@_observacion", item.Pen_observacion);
                parameters.Add("@_fecha_creacion", item.Pen_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Pen_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Pen_secuencia);

                dbConnection.Execute("app_riego.create_pendiente", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Pendiente> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Pendiente, Usuario, Pendiente>("app_riego.getall_pendiente", (Pendiente, Usuario) =>
                {
                    Pendiente.Pen_usuario_creacion = Usuario;
                    Pendiente.Pen_usuario_modificacion = Usuario;

                    return Pendiente;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Pendiente FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Pendiente, Usuario, Pendiente>("app_riego.getall_pendiente", (Pendiente, Usuario) =>
                {
                    Pendiente.Pen_usuario_creacion = Usuario;
                    Pendiente.Pen_usuario_modificacion = Usuario;

                    return Pendiente;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Pendiente WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
