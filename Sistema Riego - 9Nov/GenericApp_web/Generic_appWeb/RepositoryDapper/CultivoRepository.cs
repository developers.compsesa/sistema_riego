﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class CultivoRepository : IRepository<Cultivo>
    {
        private string connectionString;

        public CultivoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Cultivo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Cult_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Cult_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Cult_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Cult_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Cult_descripcion_corta);
                parameters.Add("@_abreviacion", item.Cult_abreviacion);
                parameters.Add("@_observacion", item.Cult_observacion);
                parameters.Add("@_fecha_creacion", item.Cult_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Cult_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Cult_secuencia);
                parameters.Add("@_grupo_cultivo_id", item.Grupo_cultivo.GCul_id);
                parameters.Add("@_categoria_cultivo_id", item.Categoria_cultivo.CCul_id);
                parameters.Add("@_tipo_cultivo_id", item.Tipo_cultivo.TCul_id);


                dbConnection.Execute("app_riego.create_cultivo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Cultivo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);
                var result = dbConnection.Query<Cultivo, Grupo_cultivo, Categoria_cultivo, Tipo_cultivo, Usuario, Hacienda, Cultivo>("app_riego.getall_cultivo", (Cultivo, Grupo_cultivo, Categoria_cultivo, Tipo_cultivo, Usuario , Hacienda) =>
                {
                    Cultivo.Grupo_cultivo = Grupo_cultivo;
                    Cultivo.Categoria_cultivo = Categoria_cultivo;
                    Cultivo.Tipo_cultivo = Tipo_cultivo;
                    Cultivo.Hacienda = Hacienda;
                    Cultivo.Cult_usuario_creacion = Usuario;
                    Cultivo.Cult_usuario_modificacion = Usuario;

                    return Cultivo;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "MA_id, GMa_id, CMa_id, TMa_id, Users_id");


                return result;

            }
        }

        public Cultivo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Cultivo, Grupo_cultivo, Categoria_cultivo, Tipo_cultivo, Usuario, Hacienda, Cultivo>("app_riego.getall_cultivo", (Cultivo, Grupo_cultivo, Categoria_cultivo, Tipo_cultivo, Usuario, Hacienda) =>
                {
                    Cultivo.Grupo_cultivo = Grupo_cultivo;
                    Cultivo.Categoria_cultivo = Categoria_cultivo;
                    Cultivo.Tipo_cultivo = Tipo_cultivo;
                    Cultivo.Hacienda = Hacienda;
                    Cultivo.Cult_usuario_creacion = Usuario;
                    Cultivo.Cult_usuario_modificacion = Usuario;

                    return Cultivo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "MA_id, GMa_id, CMa_id, TMa_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.cultivos WHERE Id=@Id", new { Id = id });
            }
        }

        public void Update(Cultivo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Cult_id);
                parameters.Add("@_codigo_empresa", item.Cult_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Cult_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Cult_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Cult_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Cult_descripcion_corta);
                parameters.Add("@_abreviacion", item.Cult_abreviacion);
                parameters.Add("@_observacion", item.Cult_observacion);
                parameters.Add("@_fecha_creacion", item.Cult_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Cult_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Cult_secuencia);
                parameters.Add("@_grupo_cultivo_id", item.Grupo_cultivo.GCul_id);
                parameters.Add("@_categoria_cultivo_id", item.Categoria_cultivo.CCul_id);
                parameters.Add("@_tipo_cultivo_id", item.Tipo_cultivo.TCul_id);


                dbConnection.Execute("app_riego.create_cultivo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
