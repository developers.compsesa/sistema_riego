﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_dato_tecnicoRepository
    {
        private string connectionString;

        public Grupo_dato_tecnicoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_dato_tecnico item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GDTec_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GDTec_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GDTec_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GDTec_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GDTec_descripcion_corta);
                parameters.Add("@_abreviacion", item.GDTec_abreviacion);
                parameters.Add("@_observacion", item.GDTec_observacion);
               
                parameters.Add("@_fecha_creacion", item.GDTec_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GDTec_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GDTec_secuencia);

                dbConnection.Execute("app_riego.create_grupo_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_dato_tecnico item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.GDTec_id);
                parameters.Add("@_codigo_empresa", item.GDTec_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GDTec_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GDTec_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GDTec_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GDTec_descripcion_corta);
                parameters.Add("@_abreviacion", item.GDTec_abreviacion);
                parameters.Add("@_observacion", item.GDTec_observacion);

                parameters.Add("@_fecha_creacion", item.GDTec_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GDTec_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GDTec_secuencia);

                dbConnection.Execute("app_riego.create_grupo_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_dato_tecnico> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_dato_tecnico, Usuario, Grupo_dato_tecnico>("app_riego.getall_grupo_dato_tecnico", (Grupo_dato_tecnico, Usuario) =>
                {
                    Grupo_dato_tecnico.GDTec_usuario_creacion = Usuario;
                    Grupo_dato_tecnico.GDTec_usuario_modificacion = Usuario;

                    return Grupo_dato_tecnico;

                }, parameters, commandType: CommandType.StoredProcedure,

                 splitOn: "Users_id");


                return result;

            }
        }

        public Grupo_dato_tecnico FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Grupo_dato_tecnico, Usuario, Grupo_dato_tecnico>("app_riego.getall_grupo_dato_tecnico", (Grupo_dato_tecnico, Usuario) =>
                {
                    Grupo_dato_tecnico.GDTec_usuario_creacion = Usuario;
                    Grupo_dato_tecnico.GDTec_usuario_modificacion = Usuario;

                    return Grupo_dato_tecnico;

                }, parameters, commandType: CommandType.StoredProcedure,

                  splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_dato_tecnico WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
