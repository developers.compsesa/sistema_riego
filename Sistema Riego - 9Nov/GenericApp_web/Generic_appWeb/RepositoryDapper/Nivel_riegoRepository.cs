﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Nivel_riegoRepository
    {
        private string connectionString;

        public Nivel_riegoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Nivel_riego item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Nr_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Nr_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Nr_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Nr_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Nr_descripcion_corta);
                parameters.Add("@_abreviacion", item.Nr_abreviacion);
                parameters.Add("@_observacion", item.Nr_observacion);
               
                parameters.Add("@_fecha_creacion", item.Nr_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Nr_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Nr_secuencia);

                dbConnection.Execute("app_riego.create_nivel_riego", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Nivel_riego item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", item.Nr_id);
                parameters.Add("@_codigo_empresa", item.Nr_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Nr_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Nr_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Nr_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Nr_descripcion_corta);
                parameters.Add("@_abreviacion", item.Nr_abreviacion);
                parameters.Add("@_observacion", item.Nr_observacion);
             
                parameters.Add("@_fecha_creacion", item.Nr_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Nr_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Nr_secuencia);

                dbConnection.Execute("app_riego.create_nivel_riego", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Nivel_riego> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Nivel_riego, Usuario, Nivel_riego>("app_riego.getallNivel_riego", (Nivel_riego, Usuario) =>
                {
                    Nivel_riego.Nr_usuario_creacion = Usuario;
                    Nivel_riego.Nr_usuario_modificacion = Usuario;

                    return Nivel_riego;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Nivel_riego FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Nivel_riego, Usuario, Nivel_riego>("app_riego.getallNivel_riego", (Nivel_riego, Usuario) =>
                {
                    Nivel_riego.Nr_usuario_creacion = Usuario;
                    Nivel_riego.Nr_usuario_modificacion = Usuario;

                    return Nivel_riego;

                }, parameters, commandType: CommandType.StoredProcedure,
                                splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Nivel_riego WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
