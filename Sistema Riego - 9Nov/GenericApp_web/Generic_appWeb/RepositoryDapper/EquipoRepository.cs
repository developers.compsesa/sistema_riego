﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class EquipoRepository : IRepository<Equipo>
    {
        private string connectionString;
        private readonly ILogger _logger;
        public EquipoRepository(IConfiguration configuration)
        {
         
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Equipo item)
        {
            //try
            //{
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@_id", 0);
                    parameters.Add("@_codigo_empresa", item.Eq_codigo_empresa);
                    parameters.Add("@_codigo_alterno", item.Eq_codigo_alterno);
                    parameters.Add("@_descripcion_larga", item.Eq_descripcion_larga);
                    parameters.Add("@_descripcion_med", item.Eq_descripcion_med);
                    parameters.Add("@_descripcion_corta", item.Eq_descripcion_corta);
                    parameters.Add("@_abreviacion", item.Eq_abreviacion);
                    parameters.Add("@_observacion", item.Eq_observacion);
                    parameters.Add("@_fecha_creacion", item.Eq_fecha_creacion);
                    parameters.Add("@_fecha_modificacion", item.Eq_fecha_modificacion);
                    parameters.Add("@_usuario_creacion", 1);
                    parameters.Add("@_usuario_modificacion", 1);
                    parameters.Add("@_estado", "A");
                    parameters.Add("@_secuencia", item.Eq_secuencia);
                    parameters.Add("@_serial_n", item.Eq_serial);
                    parameters.Add("@_codigo_eui", item.Eq_codigo_EUI);
                    parameters.Add("@_status_id", item.Status);
                    parameters.Add("@_entidad_id", item.Entidad.En_id);
                    parameters.Add("@_descripcion_equipo_id", item.Descripcion_equipo.DEq_id);
                    parameters.Add("@_marca_modelo_id", item.Marca_Modelo.MM_id);
                    parameters.Add("@_conjunto_dato_tecnico_id", item.Conjunto_dato_tecnico.CDTec_id);
                    parameters.Add("@_conjunto_parametro_id", item.Conjunto_parametro.CParm_id);
                    parameters.Add("@_ubicacion_geografica_id", item.Ubicacion_geografica.Ug_id);
                    parameters.Add("@_lugar_ubicacion_id", item.Lugar_ubicacion.Luu_id);
                    dbConnection.Execute("app_riego.create_equipo", parameters, commandType: CommandType.StoredProcedure);
                }
            //}
            //catch (Exception e)
            //{
                
            //    _logger.LogInformation("Error:" + e.ToString());

            //}

        }


        public void Update(Equipo item)
        {



            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                //if (item.Tipos_equipos.TE_id == 0)
                //{
                //    item.Tipos_equipos.TE_id = item.Tipos_equipos.TE_Padre_id;
                //}
                var x = DateTime.Now;
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", item.Eq_id);
                parameters.Add("@_codigo_empresa", item.Eq_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Eq_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Eq_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Eq_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Eq_descripcion_corta);
                parameters.Add("@_abreviacion", item.Eq_abreviacion);
                parameters.Add("@_observacion", item.Eq_observacion);
                parameters.Add("@_fecha_creacion", item.Eq_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Eq_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Eq_secuencia);
                parameters.Add("@_serial_n", item.Eq_serial);
                parameters.Add("@_codigo_eui", item.Eq_codigo_EUI);
                parameters.Add("@_status_id", item.Status);
                parameters.Add("@_entidad_id", item.Entidad.En_id);
                parameters.Add("@_descripcion_equipo_id", item.Descripcion_equipo.DEq_id);
                parameters.Add("@_marca_modelo_id", item.Marca_Modelo.MM_id);
                parameters.Add("@_conjunto_dato_tecnico_id", item.Conjunto_dato_tecnico.CDTec_id);
                parameters.Add("@_conjunto_parametro_id", item.Conjunto_parametro.CParm_id);
                parameters.Add("@_ubicacion_geografica_id", item.Ubicacion_geografica.Ug_id);
                parameters.Add("@_lugar_ubicacion_id", item.Lugar_ubicacion.Luu_id);

                dbConnection.Execute("app_riego.create_equipo", parameters, commandType: CommandType.StoredProcedure);
            }

        }


       


        public IEnumerable<Equipo> FindAll()
        {
          
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@_opcion", 1);
                    parameters.Add("@_id", 0);

                var result = dbConnection.Query("app_riego.getall_equipo", new[]
               {
                    typeof(Equipo),

                    typeof(Status),
                    typeof(Entidad),
                    typeof(Descripcion_equipo),
                    typeof(Marca_Modelo),
                    typeof(Conjunto_dato_tecnico),
                    typeof(Conjunto_parametro),
                    typeof(Ubicacion_geografica),
                    typeof(Lugar_ubicacion),
                    typeof(Usuario)
                }
               , obj =>
               {
                   Equipo Equipo = obj[0] as Equipo;

                   Status Status = obj[1] as Status;
                   Entidad Entidad = obj[2] as Entidad;
                   Descripcion_equipo Descripcion_equipo = obj[3] as Descripcion_equipo;
                   Marca_Modelo Marca_Modelo = obj[4] as Marca_Modelo;
                   Conjunto_dato_tecnico Conjunto_dato_tecnico = obj[5] as Conjunto_dato_tecnico;
                   Conjunto_parametro Conjunto_parametro = obj[6] as Conjunto_parametro;
                   Ubicacion_geografica Ubicacion_geografica = obj[7] as Ubicacion_geografica;
                   Lugar_ubicacion Lugar_ubicacion = obj[8] as Lugar_ubicacion;
                   Usuario Usuario = obj[9] as Usuario;       


                   Equipo.Status = Status;
                   Equipo.Entidad = Entidad;
                   Equipo.Descripcion_equipo = Descripcion_equipo;
                   Equipo.Marca_Modelo = Marca_Modelo;

                   Equipo.Conjunto_dato_tecnico = Conjunto_dato_tecnico;
                   Equipo.Conjunto_parametro = Conjunto_parametro;
                   Equipo.Ubicacion_geografica = Ubicacion_geografica;
                   Equipo.Lugar_ubicacion = Lugar_ubicacion;

                   Equipo.Eq_usuario_creacion = Usuario;
                   Equipo.Eq_usuario_modificacion = Usuario;

                   return Equipo;

               }, parameters, commandType: CommandType.StoredProcedure,
                            splitOn: "St_id, En_id, DEq_id, MM_id,  CDTec_id, CParm_id, Ug_id, Luu_id, Users_id");

                return result;
            }
        }

    
        public IEnumerable<Equipo> FindAllFiltered(int? grupo_equipo_id, int? categoria_equipo_id, int? tipo_equipo_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (grupo_equipo_id != 0)
                {
                    if (categoria_equipo_id != 0)
                    {
                        if (tipo_equipo_id != 0)
                        {
                            flag = 3;
                            //if (subtipo_equipoid != 0)
                            //{
                            //    flag = 4;
                            //}
                            //else
                            //{
                            //    flag = 3;
                            //}
                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);

                parameters.Add("@_grupo_equipo_id", grupo_equipo_id);
                parameters.Add("@_categoria_equipo_id", categoria_equipo_id);
                parameters.Add("@_tipo_equipo_id", tipo_equipo_id);
                //parameters.Add("@_subtipo_equipoid", subtipo_equipoid);

                var result = dbConnection.Query("app_riego.getall_equipo", new[]
               {
                    typeof(Equipo),

                    typeof(Status),
                    typeof(Entidad),
                    typeof(Descripcion_equipo),
                    typeof(Marca_Modelo),
                    typeof(Conjunto_dato_tecnico),
                    typeof(Conjunto_parametro),
                    typeof(Ubicacion_geografica),
                    typeof(Lugar_ubicacion),
                    typeof(Usuario)
                }
               , obj =>
               {
                   Equipo Equipo = obj[0] as Equipo;

                   Status Status = obj[1] as Status;
                   Entidad Entidad = obj[2] as Entidad;
                   Descripcion_equipo Descripcion_equipo = obj[3] as Descripcion_equipo;
                   Marca_Modelo Marca_Modelo = obj[4] as Marca_Modelo;
                   Conjunto_dato_tecnico Conjunto_dato_tecnico = obj[5] as Conjunto_dato_tecnico;
                   Conjunto_parametro Conjunto_parametro = obj[6] as Conjunto_parametro;
                   Ubicacion_geografica Ubicacion_geografica = obj[7] as Ubicacion_geografica;
                   Lugar_ubicacion Lugar_ubicacion = obj[8] as Lugar_ubicacion;
                   Usuario Usuario = obj[9] as Usuario;


                   Equipo.Status = Status;
                   Equipo.Entidad = Entidad;
                   Equipo.Descripcion_equipo = Descripcion_equipo;
                   Equipo.Marca_Modelo = Marca_Modelo;

                   Equipo.Conjunto_dato_tecnico = Conjunto_dato_tecnico;
                   Equipo.Conjunto_parametro = Conjunto_parametro;
                   Equipo.Ubicacion_geografica = Ubicacion_geografica;
                   Equipo.Lugar_ubicacion = Lugar_ubicacion;

                   Equipo.Eq_usuario_creacion = Usuario;
                   Equipo.Eq_usuario_modificacion = Usuario;

                   return Equipo;

               }, parameters, commandType: CommandType.StoredProcedure,
                            splitOn: "St_id, En_id, DEq_id, MM_id,  CDTec_id, CParm_id, Ug_id, Luu_id, Users_id");

                return result;
            }
        }


        public IEnumerable<Equipo> GetallEquipoBytipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var Equipo = dbConnection.Query<Equipo>("app_riego.getall_equipo_bytipo", parameters, commandType: CommandType.StoredProcedure);

                return Equipo;
            }
        }




        public Equipo FindByID(int Id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", Id);

                var result = dbConnection.Query("app_riego.getall_equipo", new[]
               {
                    typeof(Equipo),

                    typeof(Status),
                    typeof(Entidad),
                    typeof(Descripcion_equipo),
                    typeof(Marca_Modelo),
                    typeof(Conjunto_dato_tecnico),
                    typeof(Conjunto_parametro),
                    typeof(Ubicacion_geografica),
                    typeof(Lugar_ubicacion),
                    typeof(Usuario)
                }
               , obj =>
               {
                   Equipo Equipo = obj[0] as Equipo;

                   Status Status = obj[1] as Status;
                   Entidad Entidad = obj[2] as Entidad;
                   Descripcion_equipo Descripcion_equipo = obj[3] as Descripcion_equipo;
                   Marca_Modelo Marca_Modelo = obj[4] as Marca_Modelo;
                   Conjunto_dato_tecnico Conjunto_dato_tecnico = obj[5] as Conjunto_dato_tecnico;
                   Conjunto_parametro Conjunto_parametro = obj[6] as Conjunto_parametro;
                   Ubicacion_geografica Ubicacion_geografica = obj[7] as Ubicacion_geografica;
                   Lugar_ubicacion Lugar_ubicacion = obj[8] as Lugar_ubicacion;
                   Usuario Usuario = obj[9] as Usuario;


                   Equipo.Status = Status;
                   Equipo.Entidad = Entidad;
                   Equipo.Descripcion_equipo = Descripcion_equipo;
                   Equipo.Marca_Modelo = Marca_Modelo;

                   Equipo.Conjunto_dato_tecnico = Conjunto_dato_tecnico;
                   Equipo.Conjunto_parametro = Conjunto_parametro;
                   Equipo.Ubicacion_geografica = Ubicacion_geografica;
                   Equipo.Lugar_ubicacion = Lugar_ubicacion;

                   Equipo.Eq_usuario_creacion = Usuario;
                   Equipo.Eq_usuario_modificacion = Usuario;

                   return Equipo;

               }, parameters, commandType: CommandType.StoredProcedure,
                            splitOn: "St_id, En_id, DEq_id, MM_id,  CDTec_id, CParm_id, Ug_id, Luu_id, Users_id");


                return result.FirstOrDefault();
            }
        }

        public void Remove(int id)
        {
          
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    dbConnection.Execute("DELETE FROM app_riego.equipo WHERE Id=@Id", new { Id = id });
                }
           
        }




        public Equipo GetActivoByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query("app_riego.getall_equipo", new[]
               {
                    typeof(Equipo),

                    typeof(Status),
                    typeof(Entidad),
                    typeof(Descripcion_equipo),
                    typeof(Marca_Modelo),
                    typeof(Conjunto_dato_tecnico),
                    typeof(Conjunto_parametro),
                    typeof(Ubicacion_geografica),
                    typeof(Lugar_ubicacion),
                    typeof(Usuario)
                }
               , obj =>
               {
                   Equipo Equipo = obj[0] as Equipo;

                   Status Status = obj[1] as Status;
                   Entidad Entidad = obj[2] as Entidad;
                   Descripcion_equipo Descripcion_equipo = obj[3] as Descripcion_equipo;
                   Marca_Modelo Marca_Modelo = obj[4] as Marca_Modelo;
                   Conjunto_dato_tecnico Conjunto_dato_tecnico = obj[5] as Conjunto_dato_tecnico;
                   Conjunto_parametro Conjunto_parametro = obj[6] as Conjunto_parametro;
                   Ubicacion_geografica Ubicacion_geografica = obj[7] as Ubicacion_geografica;
                   Lugar_ubicacion Lugar_ubicacion = obj[8] as Lugar_ubicacion;
                   Usuario Usuario = obj[9] as Usuario;


                   Equipo.Status = Status;
                   Equipo.Entidad = Entidad;
                   Equipo.Descripcion_equipo = Descripcion_equipo;
                   Equipo.Marca_Modelo = Marca_Modelo;

                   Equipo.Conjunto_dato_tecnico = Conjunto_dato_tecnico;
                   Equipo.Conjunto_parametro = Conjunto_parametro;
                   Equipo.Ubicacion_geografica = Ubicacion_geografica;
                   Equipo.Lugar_ubicacion = Lugar_ubicacion;

                   Equipo.Eq_usuario_creacion = Usuario;
                   Equipo.Eq_usuario_modificacion = Usuario;

                   return Equipo;

               }, parameters, commandType: CommandType.StoredProcedure,
                            splitOn: "St_id, En_id, DEq_id, MM_id,  CDTec_id, CParm_id, Ug_id, Luu_id, Users_id");



                return result.FirstOrDefault();
            }

        }


        public Equipo GetActivosByCodEmpresa1(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Equipo>("app_riego.getallequipo1_codigo_empresa", parameters, commandType: CommandType.StoredProcedure);


                return result.FirstOrDefault();

                

            }

        }

    }
}
