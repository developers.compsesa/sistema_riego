﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Dato_tecnicoRepository
    {
        private string connectionString;

        public Dato_tecnicoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Dato_tecnico item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.DTec_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.DTec_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.DTec_descripcion_larga);
                parameters.Add("@_descripcion_med", item.DTec_descripcion_med);
                parameters.Add("@_descripcion_corta", item.DTec_descripcion_corta);
                parameters.Add("@_abreviacion", item.DTec_abreviacion);
                parameters.Add("@_observacion", item.DTec_observacion);
                parameters.Add("@_fecha_creacion", item.DTec_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.DTec_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.DTec_secuencia);
                parameters.Add("@_descripcion_dato_tecnico_id", item.Descripcion_dato_tecnico.DDTec_id);
                parameters.Add("@_descripcion_unidad_id", item.Descripcion_unidad.Un_id);
                parameters.Add("@_descripcion_valor_id", item.Descripcion_valor.Val_id);

                dbConnection.Execute("app_riego.create_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Dato_tecnico item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.DTec_id);
                parameters.Add("@_codigo_empresa", item.DTec_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.DTec_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.DTec_descripcion_larga);
                parameters.Add("@_descripcion_med", item.DTec_descripcion_med);
                parameters.Add("@_descripcion_corta", item.DTec_descripcion_corta);
                parameters.Add("@_abreviacion", item.DTec_abreviacion);
                parameters.Add("@_observacion", item.DTec_observacion);
                parameters.Add("@_fecha_creacion", item.DTec_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.DTec_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.DTec_secuencia);
                parameters.Add("@_descripcion_dato_tecnico_id", item.Descripcion_dato_tecnico.DDTec_id);
                parameters.Add("@_descripcion_unidad_id", item.Descripcion_unidad.Un_id);
                parameters.Add("@_descripcion_valor_id", item.Descripcion_valor.Val_id);

                dbConnection.Execute("app_riego.create_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Dato_tecnico> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);


                var result = dbConnection.Query("app_riego.getall_inventario_dato_tecnico", new[]
                {
                    typeof(Dato_tecnico),
                    typeof(Descripcion_dato_tecnico),
                    typeof(Descripcion_unidad),
                    typeof(Descripcion_valor),
                    typeof(Usuario)

                }
                , obj =>
                {

                Dato_tecnico Dato_tecnico = obj[0] as Dato_tecnico;
                Descripcion_dato_tecnico Descripcion_dato_tecnico = obj[1] as Descripcion_dato_tecnico;
                Descripcion_unidad Descripcion_unidad = obj[2] as Descripcion_unidad;
                Descripcion_valor Descripcion_valor = obj[3] as Descripcion_valor;
                Usuario Usuario = obj[4] as Usuario;


                Dato_tecnico.Descripcion_dato_tecnico = Descripcion_dato_tecnico;
                Dato_tecnico.Descripcion_unidad = Descripcion_unidad;
                Dato_tecnico.Descripcion_valor = Descripcion_valor;
                Dato_tecnico.DTec_usuario_creacion = Usuario;
                Dato_tecnico.DTec_usuario_modificacion = Usuario;


                    return Dato_tecnico;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "DTec_id, DDTec_id, Un_id , Val_id , Users_id");


                return result;

            }
        }

        public Dato_tecnico FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Dato_tecnico, Descripcion_dato_tecnico, Descripcion_unidad, Descripcion_valor , Usuario, Dato_tecnico>("app_riego.getall_inventario_dato_tecnico", (Dato_tecnico, Descripcion_dato_tecnico, Descripcion_unidad, Descripcion_valor, Usuario) =>
                {

                    Dato_tecnico.Descripcion_dato_tecnico = Descripcion_dato_tecnico;
                    Dato_tecnico.Descripcion_unidad = Descripcion_unidad;
                    Dato_tecnico.Descripcion_valor = Descripcion_valor;
                    Dato_tecnico.DTec_usuario_creacion = Usuario;
                    Dato_tecnico.DTec_usuario_modificacion = Usuario;


                    return Dato_tecnico;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "DTec_id,DDTec_id, Un_id , Val_id , Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.dato_tecnico WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Dato_tecnico> FindAllFiltered(int? grupo_dato_tecnico_id, int? categoria_dato_tecnico_id, int? tipo_dato_tecnico_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (grupo_dato_tecnico_id != 0)
                {
                    if (categoria_dato_tecnico_id != 0)
                    {
                        if (tipo_dato_tecnico_id != 0)
                        {
                            flag = 3;
                            //if (subtipo_activo_id != 0)
                            //{
                            //    flag = 4;
                            //}
                            //else
                            //{
                            //    flag = 3;
                            //}
                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);

                parameters.Add("@_grupo_dato_tecnico_id", grupo_dato_tecnico_id);
                parameters.Add("@_categoria_dato_tecnico_id", categoria_dato_tecnico_id);
                parameters.Add("@_tipo_dato_tecnico_id", tipo_dato_tecnico_id);

                var result = dbConnection.Query("app_riego.getall_dato_tecnico_filtered", new[]
               {
                    typeof(Dato_tecnico),
                    typeof(Descripcion_dato_tecnico),
                    typeof(Descripcion_unidad),
                    typeof(Descripcion_valor),                    
                    typeof(Usuario)

                }
               , obj =>
               {
                   Dato_tecnico Dato_tecnico = obj[0] as Dato_tecnico;

                   Descripcion_dato_tecnico Descripcion_dato_tecnico = obj[1] as Descripcion_dato_tecnico;
                   Descripcion_unidad Descripcion_unidad = obj[2] as Descripcion_unidad;
                   Descripcion_valor Descripcion_valor = obj[3] as Descripcion_valor;
                   Usuario Usuario = obj[4] as Usuario;

                   Dato_tecnico.Descripcion_dato_tecnico = Descripcion_dato_tecnico;
                   Dato_tecnico.Descripcion_unidad = Descripcion_unidad;
                   Dato_tecnico.Descripcion_valor = Descripcion_valor;
                   Dato_tecnico.DTec_usuario_creacion = Usuario;
                   Dato_tecnico.DTec_usuario_modificacion = Usuario;

                   return Dato_tecnico;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "DTec_id, DDTec_id, Un_id , Val_id , Users_id");


                return result;
            }
        }


        public IEnumerable<Dato_tecnico> GetalldatostecnicosByTipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Dato_tecnico>("app_riego.getall_tipo_bycategoria_datos_tecnicos", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        //public Dato_tecnico GetDatosTecnicosByCodEmpresa(string codigo_empresa)
        //{
        //    using (IDbConnection dbConnection = Connection)
        //    {
        //        dbConnection.Open();
        //        DynamicParameters parameters = new DynamicParameters();
           
        //        parameters.Add("@_codigo_empresa", codigo_empresa);

        //        var result = dbConnection.Query<Dato_tecnico, Grupos_datos_tecnicos, Categorias_datos_tecnicos, Tipo_dato_tecnico, Descripcion_unidad, Valor , Estados, Dato_tecnico>("app_riego.getallinventario_datos_tecnicos", (Dato_tecnico, Grupos_datos_tecnicos, Categorias_datos_tecnicos, Tipos_datos_tecnicos, Unidades, Valor, Estados) =>
        //        {
        //            Dato_tecnico.Inventario_descripciones_datos_tecnicos.Grupos_datos_tecnicos = Grupos_datos_tecnicos;
        //            Dato_tecnico.Inventario_descripciones_datos_tecnicos.Categorias_datos_tecnicos = Categorias_datos_tecnicos;
        //            Dato_tecnico.Inventario_descripciones_datos_tecnicos.Tipos_datos_tecnicos = Tipos_datos_tecnicos;
        //            Dato_tecnico.Estados = Estados;
        //            Dato_tecnico.Unidades = Unidades;
        //            Dato_tecnico.Valor = Valor;

        //            return Dato_tecnico;
        //        }, parameters, commandType: CommandType.StoredProcedure,
        //                       splitOn: "GDTec_id,CDTec_id,TDTec_id, Un_id , Val_id , E_id");


        //        return result.FirstOrDefault();

        //    }

        //}









    }
}
