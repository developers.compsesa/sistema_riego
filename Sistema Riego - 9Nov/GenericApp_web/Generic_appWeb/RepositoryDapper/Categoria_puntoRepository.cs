﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_puntoRepository
    {
        private string connectionString;

        public Categoria_puntoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_punto item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);               
                parameters.Add("@_codigo_alterno", item.CPu_codigo_alterno);
                parameters.Add("@_codigo_empresa", item.CPu_codigo_empresa);
                parameters.Add("@_descripcion_larga", item.CPu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CPu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CPu_descripcion_corta);
                parameters.Add("@_abreviacion", item.CPu_abreviacion);
                parameters.Add("@_observacion", item.CPu_observacion);
                parameters.Add("@_fecha_creacion", item.CPu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CPu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CPu_secuencia);
                parameters.Add("@_grupo_punto_id", item.Grupo_punto.GPu_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_categoria_punto", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_punto item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.CPu_id);
                parameters.Add("@_codigo_alterno", item.CPu_codigo_alterno);
                parameters.Add("@_codigo_empresa", item.CPu_codigo_empresa);
                parameters.Add("@_descripcion_larga", item.CPu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CPu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CPu_descripcion_corta);
                parameters.Add("@_abreviacion", item.CPu_abreviacion);
                parameters.Add("@_observacion", item.CPu_observacion);
                parameters.Add("@_fecha_creacion", item.CPu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CPu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CPu_secuencia);
                parameters.Add("@_grupo_punto_id", item.Grupo_punto.GPu_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_categoria_punto", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_punto> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_punto, Grupo_punto, Usuario, Hacienda, Categoria_punto>("app_riego.getall_categoria_punto", (Categoria_punto, Grupo_punto, Usuario, Hacienda) =>
                {
                    Categoria_punto.Grupo_punto = Grupo_punto;
                    Categoria_punto.CPu_usuario_creacion = Usuario;
                    Categoria_punto.CPu_usuario_modificacion = Usuario;
                    Categoria_punto.Hacienda = Hacienda;
                    return Categoria_punto;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "GPu_id, Users_id , Hac_id");
                return result;
            }
        }




        public Categoria_punto FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_punto, Grupo_punto, Usuario, Hacienda, Categoria_punto>("app_riego.getall_categoria_punto", (Categoria_punto, Grupo_punto, Usuario, Hacienda) =>
                {
                    Categoria_punto.Grupo_punto = Grupo_punto;
                    Categoria_punto.CPu_usuario_creacion = Usuario;
                    Categoria_punto.CPu_usuario_modificacion = Usuario;
                    Categoria_punto.Hacienda = Hacienda;
                    return Categoria_punto;
                }, parameters, commandType: CommandType.StoredProcedure,
                   splitOn: "GPu_id, Users_id , Hac_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Categoria_punto WHERE Id=@Id", new { Id = id });
            }
        }





        public IEnumerable<Categoria_punto> Getcmb_categoriabygrupo_punto(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_punto>("app_riego.Getcmb_categoriabygrupo_punto", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }






    }
}
