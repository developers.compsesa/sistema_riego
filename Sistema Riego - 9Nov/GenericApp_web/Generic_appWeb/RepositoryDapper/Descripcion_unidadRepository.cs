﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Descripcion_unidadRepository
    {
        private string connectionString;

        public Descripcion_unidadRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Descripcion_unidad item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Un_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Un_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Un_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Un_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Un_descripcion_corta);
                parameters.Add("@_abreviacion", item.Un_abreviacion);
                parameters.Add("@_observacion", item.Un_observacion);
               
                parameters.Add("@_fecha_creacion", item.Un_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Un_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Un_secuencia);
                parameters.Add("@_grupo_unidad_id", item.Grupo_unidad.GUn_id);
                parameters.Add("@_categoria_unidad_id", item.Categoria_unidad.CUn_id);
                parameters.Add("@_tipo_unidad_id", item.Tipo_unidad.TUn_id);
                dbConnection.Execute("app_riego.create_descripcion_unidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Descripcion_unidad item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Un_id);
                parameters.Add("@_codigo_empresa", item.Un_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Un_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Un_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Un_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Un_descripcion_corta);
                parameters.Add("@_abreviacion", item.Un_abreviacion);
                parameters.Add("@_observacion", item.Un_observacion);

                parameters.Add("@_fecha_creacion", item.Un_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Un_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Un_secuencia);
                parameters.Add("@_grupo_unidad_id", item.Grupo_unidad.GUn_id);
                parameters.Add("@_categoria_unidad_id", item.Categoria_unidad.CUn_id);
                parameters.Add("@_tipo_unidad_id", item.Tipo_unidad.TUn_id);

                dbConnection.Execute("app_riego.create_descripcion_unidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Descripcion_unidad> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Descripcion_unidad, Grupo_unidad, Categoria_unidad, Tipo_unidad, Usuario, Descripcion_unidad>("app_riego.getallunidades", (Descripcion_unidad, Grupo_unidad, Categoria_unidad, Tipo_unidad, Usuario) =>
                {
                    Descripcion_unidad.Grupo_unidad = Grupo_unidad;
                    Descripcion_unidad.Categoria_unidad = Categoria_unidad;
                    Descripcion_unidad.Tipo_unidad = Tipo_unidad;
                    Descripcion_unidad.Un_usuario_creacion = Usuario;
                    Descripcion_unidad.Un_usuario_modificacion = Usuario;

                    return Descripcion_unidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GUn_id,CUn_id,TUn_id, Users_id");


                return result;

            }
        }

      

        public Descripcion_unidad FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Descripcion_unidad, Grupo_unidad, Categoria_unidad, Tipo_unidad, Usuario, Descripcion_unidad>("app_riego.getall_descripcion_unidad", (Descripcion_unidad, Grupo_unidad, Categoria_unidad, Tipo_unidad, Usuario) =>
                {
                    Descripcion_unidad.Grupo_unidad = Grupo_unidad;
                    Descripcion_unidad.Categoria_unidad = Categoria_unidad;
                    Descripcion_unidad.Tipo_unidad = Tipo_unidad;
                    Descripcion_unidad.Un_usuario_creacion = Usuario;
                    Descripcion_unidad.Un_usuario_modificacion = Usuario;

                    return Descripcion_unidad;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GUn_id,CUn_id,TUn_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.unidades WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Descripcion_unidad> FindAllFiltered(int? grupo_unidades_id, int? categoria_unidades_id, int? tipo_unidades_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (grupo_unidades_id != 0)
                {
                    if (categoria_unidades_id != 0)
                    {
                        if (tipo_unidades_id != 0)
                        {
                            flag = 3;
                            //if (subtipo_activo_id != 0)
                            //{
                            //    flag = 4;
                            //}
                            //else
                            //{
                            //    flag = 3;
                            //}
                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);

                parameters.Add("@_grupo_unidades_id", grupo_unidades_id);
                parameters.Add("@_categoria_unidades_id", categoria_unidades_id);
                parameters.Add("@_tipo_unidades_id", tipo_unidades_id);
                //parameters.Add("@_subtipo_activo_id", subtipo_activo_id);

                var result = dbConnection.Query<Descripcion_unidad, Grupo_unidad, Categoria_unidad, Tipo_unidad, Usuario, Descripcion_unidad>("app_riego.getallunidades", (Descripcion_unidad, Grupo_unidad, Categoria_unidad, Tipo_unidad, Usuario) =>
                {
                    Descripcion_unidad.Grupo_unidad = Grupo_unidad;
                    Descripcion_unidad.Categoria_unidad = Categoria_unidad;
                    Descripcion_unidad.Tipo_unidad = Tipo_unidad;
                    Descripcion_unidad.Un_usuario_creacion = Usuario;
                    Descripcion_unidad.Un_usuario_modificacion = Usuario;

                    return Descripcion_unidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "GUn_id,CUn_id,TUn_id, E_id");

                                
                return result;
            }
        }


        public IEnumerable<Descripcion_unidad> GetallUnidadesByTipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Descripcion_unidad>("app_riego.getalltiposbycategoriaunidades", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public Descripcion_unidad GetUnidadesByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
           
                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Descripcion_unidad, Grupo_unidad, Categoria_unidad, Tipo_unidad, Usuario, Descripcion_unidad>("app_riego.getallunidades", (Unidades, Grupo_unidad, Categoria_unidad, Tipo_unidad, Usuario) =>
                {
                    Unidades.Grupo_unidad = Grupo_unidad;
                    Unidades.Categoria_unidad = Categoria_unidad;
                    Unidades.Tipo_unidad = Tipo_unidad;
                    Unidades.Un_usuario_creacion = Usuario;
                    Unidades.Un_usuario_modificacion = Usuario;

                    return Unidades;
                }, parameters, commandType: CommandType.StoredProcedure,
                               splitOn: "GUn_id,CUn_id,TUn_id, E_id");


                return result.FirstOrDefault();

            }

        }


        public IEnumerable<Descripcion_unidad> FindAllUnidades()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();


                var result = dbConnection.Query<Descripcion_unidad>("app_riego.getallunidades2", commandType: CommandType.StoredProcedure);
                return result;

            }
        }








    }
}
