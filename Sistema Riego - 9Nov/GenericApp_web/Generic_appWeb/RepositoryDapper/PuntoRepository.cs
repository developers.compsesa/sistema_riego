﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class PuntoRepository : IRepository<Punto>
    {
        private string connectionString;

        public PuntoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Punto item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Pu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Pu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Pu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Pu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Pu_descripcion_corta);
                parameters.Add("@_abreviacion", item.Pu_abreviacion);
                parameters.Add("@_observacion", item.Pu_observacion);
                parameters.Add("@_fecha_creacion", item.Pu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Pu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", "admin");
                parameters.Add("@_usuario_modificacion", "admin");
                parameters.Add("@_grupo_punto_id", item.Grupo_punto.GPu_id);
                parameters.Add("@_categoria_punto_id", item.Categoria_punto.CPu_id);
                parameters.Add("@_tipo_punto_id", item.Tipo_punto.TPu_id);
                parameters.Add("@_sitio_id", item.Sitio.Si_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_punto", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Punto item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Pu_id);
                parameters.Add("@_codigo_empresa", item.Pu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Pu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Pu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Pu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Pu_descripcion_corta);
                parameters.Add("@_abreviacion", item.Pu_abreviacion);
                parameters.Add("@_observacion", item.Pu_observacion);
                parameters.Add("@_fecha_creacion", item.Pu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Pu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", "admin");
                parameters.Add("@_usuario_modificacion", "admin");
                parameters.Add("@_grupo_punto_id", item.Grupo_punto.GPu_id);
                parameters.Add("@_categoria_punto_id", item.Categoria_punto.CPu_id);
                parameters.Add("@_tipo_punto_id", item.Tipo_punto.TPu_id);
                parameters.Add("@_sitio_id", item.Sitio.Si_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_punto", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        //public IEnumerable<PuntoViewModel> FindAllToExport()
        //{
        //    using (IDbConnection dbConnection = Connection)
        //    {

        //        dbConnection.Open();
        //        DynamicParameters parameters = new DynamicParameters();
        //        parameters.Add("@_opcion", 1);
        //        parameters.Add("@_id", 0);

        //        var result = dbConnection.Query<PuntoViewModel>("app_riego.getallPunto", parameters, commandType: CommandType.StoredProcedure);
        //        return result;

        //    }
        //}


        public IEnumerable<Punto> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query("app_riego.getall_punto", new[]
                {
                    typeof(Punto),
                    typeof(Grupo_punto),
                    typeof(Categoria_punto),
                    typeof(Tipo_punto),
                    typeof(Sitio),
                    typeof(Usuario),
                    typeof(Hacienda)

                }
                , obj =>
                {
                    Punto Punto = obj[0] as Punto;
                    Grupo_punto Grupo_punto = obj[1] as Grupo_punto;
                    Categoria_punto Categoria_punto = obj[2] as Categoria_punto;
                    Tipo_punto Tipo_punto = obj[3] as Tipo_punto;
                    Sitio Sitio = obj[4] as Sitio;
                    Usuario Usuario = obj[5] as Usuario;
                    Hacienda Hacienda = obj[5] as Hacienda;


                    Punto.Grupo_punto = Grupo_punto;
                    Punto.Categoria_punto = Categoria_punto;
                    Punto.Tipo_punto = Tipo_punto;
                    Punto.Sitio = Sitio;
                    Punto.Pu_usuario_creacion = Usuario;
                    Punto.Pu_usuario_modificacion = Usuario;
                    Punto.Hacienda = Hacienda;

                    return Punto;
                }, parameters, commandType: CommandType.StoredProcedure,
                             splitOn: "GPu_id, CPu_id, TPu_id, Hac_id, Users_id");
                            

                return result;
            }
        }


        public Punto FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query("app_riego.getall_punto", new[]
                 {
                    typeof(Punto),
                    typeof(Grupo_punto),
                    typeof(Categoria_punto),
                    typeof(Tipo_punto),
                    typeof(Sitio),
                    typeof(Usuario),
                    typeof(Hacienda)

                }
                 , obj =>
                 {
                     Punto Punto = obj[0] as Punto;
                     Grupo_punto Grupo_punto = obj[1] as Grupo_punto;
                     Categoria_punto Categoria_punto = obj[2] as Categoria_punto;
                     Tipo_punto Tipo_punto = obj[3] as Tipo_punto;
                     Sitio Sitio = obj[4] as Sitio;
                     Usuario Usuario = obj[5] as Usuario;
                     Hacienda Hacienda = obj[5] as Hacienda;


                     Punto.Grupo_punto = Grupo_punto;
                     Punto.Categoria_punto = Categoria_punto;
                     Punto.Tipo_punto = Tipo_punto;
                     Punto.Sitio = Sitio;
                     Punto.Pu_usuario_creacion = Usuario;
                     Punto.Pu_usuario_modificacion = Usuario;
                     Punto.Hacienda = Hacienda;

                     return Punto;
                 }, parameters, commandType: CommandType.StoredProcedure,
                              splitOn: "GPu_id, CPu_id, TPu_id, Hac_id, Users_id");


                return result.FirstOrDefault();
            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Punto WHERE Id=@Id", new { Id = id });
            }
        }


       



        public IEnumerable<Punto> GetallPuntoBySitios(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Punto>("app_riego.GetallPuntoBySitios", parameters, commandType: CommandType.StoredProcedure);
                return result;

            }
        }


        public IEnumerable<Punto> GetallPuntoBytipos_puntos(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Punto>("app_riego.GetallPuntoBytipos_puntos", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public Punto GetPuntoByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Punto>("app_riego.getallpuntos_codigo_empresa", parameters, commandType: CommandType.StoredProcedure);


                return result.FirstOrDefault();

                //return dbConnection.Query<Lugares>("Select descripcion_corta from app_riego.lugares where codigo_empresa = @codigo_empresa ", new { codigo_empresa }).SingleOrDefault();

            }

        }
    }
}
