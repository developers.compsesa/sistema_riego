﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class BloqueRepository
    {
        private string connectionString;

        public BloqueRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Bloque_riego item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.BRi_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.BRi_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.BRi_descripcion_larga);
                parameters.Add("@_descripcion_med", item.BRi_descripcion_med);
                parameters.Add("@_descripcion_corta", item.BRi_descripcion_corta);
                parameters.Add("@_abreviacion", item.BRi_abreviacion);
                parameters.Add("@_observacion", item.BRi_observacion);               
                parameters.Add("@_fecha_creacion", item.BRi_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.BRi_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.BRi_secuencia);
                parameters.Add("@_superficie_hm2", item.BRi_supercifie_hm2);
                parameters.Add("@_densidad_cultivo", item.BRi_densidad_cultivo);
                parameters.Add("@_profundidad_raiz", item.BRi_profundidad_raiz);
                parameters.Add("@_forma_bloque", item.BRi_forma_bloque);
                parameters.Add("@_radio", item.BRi_radio);
                parameters.Add("@_puntos_multipoligonos", item.BRi_puntos_multipoligonos);
                parameters.Add("@_entidad_id", item.Entidad.En_id);
                parameters.Add("@_ubicacion_geografica_id", item.Ubicacion_geografica.Ug_id);
                parameters.Add("@_lugar_ubicacion_id", item.Lugar_ubicacion.Luu_id);
                parameters.Add("@_pendiente_id", item.Pendiente.Pen_id);
                parameters.Add("@_tipo_suelo_id", item.Tipo_suelo.TSu_id);
                parameters.Add("@_cultivo_id", item.Cultivo.Cult_id);
                parameters.Add("@_etapa_cultivo_id", item.Etapa_cultivo.ECul_id);

                dbConnection.Execute("app_riego.create_bloque_riego", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Bloque_riego item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.BRi_id);
                parameters.Add("@_codigo_empresa", item.BRi_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.BRi_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.BRi_descripcion_larga);
                parameters.Add("@_descripcion_med", item.BRi_descripcion_med);
                parameters.Add("@_descripcion_corta", item.BRi_descripcion_corta);
                parameters.Add("@_abreviacion", item.BRi_abreviacion);
                parameters.Add("@_observacion", item.BRi_observacion);
                parameters.Add("@_fecha_creacion", item.BRi_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.BRi_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.BRi_secuencia);
                parameters.Add("@_superficie_hm2", item.BRi_supercifie_hm2);
                parameters.Add("@_densidad_cultivo", item.BRi_densidad_cultivo);
                parameters.Add("@_profundidad_raiz", item.BRi_profundidad_raiz);
                parameters.Add("@_forma_bloque", item.BRi_forma_bloque);
                parameters.Add("@_radio", item.BRi_radio);
                parameters.Add("@_puntos_multipoligonos", item.BRi_puntos_multipoligonos);
                parameters.Add("@_entidad_id", item.Entidad.En_id);
                parameters.Add("@_ubicacion_geografica_id", item.Ubicacion_geografica.Ug_id);
                parameters.Add("@_lugar_ubicacion_id", item.Lugar_ubicacion.Luu_id);
                parameters.Add("@_pendiente_id", item.Pendiente.Pen_id);
                parameters.Add("@_tipo_suelo_id", item.Tipo_suelo.TSu_id);
                parameters.Add("@_cultivo_id", item.Cultivo.Cult_id);
                parameters.Add("@_etapa_cultivo_id", item.Etapa_cultivo.ECul_id);

                dbConnection.Execute("app_riego.create_bloque_riego", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Bloque_riego> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query("app_riego.getall_bloque_riego", new[]
              {
                    typeof(Bloque_riego),

                    typeof(Pendiente),
                    typeof(Entidad),
                    typeof(Tipo_suelo),
                    typeof(Cultivo),
                    typeof(Etapa_cultivo),
                    typeof(Ubicacion_geografica),
                    typeof(Lugar_ubicacion),
                    typeof(Usuario)
                }
              , obj =>
              {
                  Bloque_riego Bloque_riego = obj[0] as Bloque_riego;

                  Pendiente Pendiente = obj[1] as Pendiente;
                  Entidad Entidad = obj[2] as Entidad;
                  Tipo_suelo Tipo_suelo = obj[3] as Tipo_suelo;
                  Cultivo Cultivo = obj[4] as Cultivo;
                  Etapa_cultivo Etapa_cultivo = obj[5] as Etapa_cultivo;
                  Ubicacion_geografica Ubicacion_geografica = obj[6] as Ubicacion_geografica;
                  Lugar_ubicacion Lugar_ubicacion = obj[7] as Lugar_ubicacion;
                  Usuario Usuario = obj[8] as Usuario;


                  Bloque_riego.Pendiente = Pendiente;
                  Bloque_riego.Entidad = Entidad;
                  Bloque_riego.Tipo_suelo = Tipo_suelo;
                  Bloque_riego.Cultivo = Cultivo;

                  Bloque_riego.Etapa_cultivo = Etapa_cultivo;
                  Bloque_riego.Ubicacion_geografica = Ubicacion_geografica;
                  Bloque_riego.Lugar_ubicacion = Lugar_ubicacion;

                  Bloque_riego.BRi_usuario_creacion = Usuario;
                  Bloque_riego.BRi_usuario_modificacion = Usuario;

                  return Bloque_riego;

              }, parameters, commandType: CommandType.StoredProcedure,
                           splitOn: "BRi_id, PEn_id, En_id, TSi_id, Cult_id, ECul_id, Ug_id, Luu_id, Users_id");

                return result;

            }
        }

        public Bloque_riego FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query("app_riego.getall_bloque_riego", new[]
             {
                    typeof(Bloque_riego),

                    typeof(Pendiente),
                    typeof(Entidad),
                    typeof(Tipo_suelo),
                    typeof(Cultivo),
                    typeof(Etapa_cultivo),
                    typeof(Ubicacion_geografica),
                    typeof(Lugar_ubicacion),
                    typeof(Usuario)
                }
             , obj =>
             {
                 Bloque_riego Bloque_riego = obj[0] as Bloque_riego;

                 Pendiente Pendiente = obj[1] as Pendiente;
                 Entidad Entidad = obj[2] as Entidad;
                 Tipo_suelo Tipo_suelo = obj[3] as Tipo_suelo;
                 Cultivo Cultivo = obj[4] as Cultivo;
                 Etapa_cultivo Etapa_cultivo = obj[5] as Etapa_cultivo;
                 Ubicacion_geografica Ubicacion_geografica = obj[6] as Ubicacion_geografica;
                 Lugar_ubicacion Lugar_ubicacion = obj[7] as Lugar_ubicacion;
                 Usuario Usuario = obj[8] as Usuario;


                 Bloque_riego.Pendiente = Pendiente;
                 Bloque_riego.Entidad = Entidad;
                 Bloque_riego.Tipo_suelo = Tipo_suelo;
                 Bloque_riego.Cultivo = Cultivo;

                 Bloque_riego.Etapa_cultivo = Etapa_cultivo;
                 Bloque_riego.Ubicacion_geografica = Ubicacion_geografica;
                 Bloque_riego.Lugar_ubicacion = Lugar_ubicacion;

                 Bloque_riego.BRi_usuario_creacion = Usuario;
                 Bloque_riego.BRi_usuario_modificacion = Usuario;

                 return Bloque_riego;

             }, parameters, commandType: CommandType.StoredProcedure,
                          splitOn: "BRi_id, PEn_id, En_id, TSi_id, Cult_id, ECul_id, Ug_id, Luu_id, Users_id");



                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("UPDATE app_riego.bloque_riego SET estado = 'I' WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
