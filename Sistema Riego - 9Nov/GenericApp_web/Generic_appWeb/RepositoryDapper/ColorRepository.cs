﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class ColorRepository
    {
        private string connectionString;

        public ColorRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Color item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Co_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Co_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Co_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Co_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Co_descripcion_corta);
                parameters.Add("@_abreviacion", item.Co_abreviacion);
                parameters.Add("@_observacion", item.Co_observacion);               
                parameters.Add("@_fecha_creacion", item.Co_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Co_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Co_secuencia);
                parameters.Add("@_hexadecimal", item.Co_hexadecimal);
                parameters.Add("@_tr",  2);
                parameters.Add("@_tg",  3);
                parameters.Add("@_tb",  5);

                dbConnection.Execute("app_riego.create_color", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Color item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Co_id);
                parameters.Add("@_codigo_empresa", item.Co_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Co_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Co_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Co_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Co_descripcion_corta);
                parameters.Add("@_abreviacion", item.Co_abreviacion);
                parameters.Add("@_observacion", item.Co_observacion);

                parameters.Add("@_fecha_creacion", item.Co_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Co_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Co_secuencia);
                parameters.Add("@_hexadecimal", item.Co_hexadecimal);
                parameters.Add("@_tr", 2);
                parameters.Add("@_tg", 3);
                parameters.Add("@_tb", 5);

                dbConnection.Execute("app_riego.create_color", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Color> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Color, Usuario, Color>("app_riego.getall_color", (Color, Usuario) =>
                {
                    Color.Co_usuario_creacion = Usuario;
                    Color.Co_usuario_modificacion = Usuario;

                    return Color;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Color FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Color, Usuario, Color>("app_riego.getall_color", (Color, Usuario) =>
                {
                    Color.Co_usuario_creacion = Usuario;
                    Color.Co_usuario_modificacion = Usuario;

                    return Color;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.color WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
