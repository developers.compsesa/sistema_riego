﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_dato_tecnicoRepository 
    {
        private string connectionString;

        public Tipo_dato_tecnicoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_dato_tecnico item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TDTec_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TDTec_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TDTec_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TDTec_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TDTec_descripcion_corta);
                parameters.Add("@_abreviacion", item.TDTec_abreviacion);
                parameters.Add("@_observacion", item.TDTec_observacion);            
               
                parameters.Add("@_fecha_creacion", item.TDTec_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TDTec_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TDTec_secuencia);
                parameters.Add("@_categoria_dato_tecnico_id",item.Categoria_dato_tecnico.CDTec_id);

                dbConnection.Execute("app_riego.create_tipo_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
            }
        }



       


        public void Update(Tipo_dato_tecnico item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TDTec_id);
                parameters.Add("@_codigo_empresa", item.TDTec_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TDTec_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TDTec_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TDTec_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TDTec_descripcion_corta);
                parameters.Add("@_abreviacion", item.TDTec_abreviacion);
                parameters.Add("@_observacion", item.TDTec_observacion);

                parameters.Add("@_fecha_creacion", item.TDTec_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TDTec_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TDTec_secuencia);
                parameters.Add("@_categoria_dato_tecnico_id", item.Categoria_dato_tecnico.CDTec_id);

                dbConnection.Execute("app_riego.create_tipo_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        public IEnumerable<Tipo_dato_tecnico> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
               
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_dato_tecnico, Categoria_dato_tecnico, Usuario, Tipo_dato_tecnico>("app_riego.getall_tipo_dato_tecnico", (Tipo_dato_tecnico,Categoria_dato_tecnico, Usuario) =>
                {
                    Tipo_dato_tecnico.Categoria_dato_tecnico = Categoria_dato_tecnico;
                    Tipo_dato_tecnico.TDTec_usuario_creacion = Usuario;
                    Tipo_dato_tecnico.TDTec_usuario_modificacion = Usuario;

                    return Tipo_dato_tecnico;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CDTec_id, Users_id");


                return result;

            }
        }


        public Tipo_dato_tecnico FindByID(int id)
        {

            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_dato_tecnico, Categoria_dato_tecnico, Usuario, Tipo_dato_tecnico>("app_riego.getall_tipo_dato_tecnico", (Tipo_dato_tecnico, Categoria_dato_tecnico, Usuario) =>
                {
                    Tipo_dato_tecnico.Categoria_dato_tecnico = Categoria_dato_tecnico;
                    Tipo_dato_tecnico.TDTec_usuario_creacion = Usuario;
                    Tipo_dato_tecnico.TDTec_usuario_modificacion = Usuario;

                    return Tipo_dato_tecnico;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CDTec_id, Users_id");


                return result.FirstOrDefault();

            }
        }
      


        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipos_datos_tecnicos WHERE Id=@Id", new { Id = id });
            }
        }



        public IEnumerable<Tipo_dato_tecnico> Getcmb_tipobycategoria_dato_tecnico(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_dato_tecnico>("app_riego.getcmb_tipobycategoria_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


    }
}
