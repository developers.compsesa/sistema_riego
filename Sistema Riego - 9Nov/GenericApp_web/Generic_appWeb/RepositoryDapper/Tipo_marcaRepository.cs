﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_marcaRepository : IRepository<Tipo_marca>
    {
        private string connectionString;

        public Tipo_marcaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_marca item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TMa_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TMa_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TMa_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TMa_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TMa_descripcion_corta);
                parameters.Add("@_abreviacion", item.TMa_abreviacion);
                parameters.Add("@_observacion", item.TMa_observacion);
                parameters.Add("@_fecha_creacion", item.TMa_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TMa_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TMa_secuencia);
                parameters.Add("@_categoria_marca_id", item.Categoria_marca.CMa_id);

                dbConnection.Execute("app_riego.create_tipo_marca", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Tipo_marca item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TMa_id);
                parameters.Add("@_codigo_empresa", item.TMa_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TMa_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TMa_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TMa_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TMa_descripcion_corta);
                parameters.Add("@_abreviacion", item.TMa_abreviacion);
                parameters.Add("@_observacion", item.TMa_observacion);
                parameters.Add("@_fecha_creacion", item.TMa_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TMa_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TMa_secuencia);
                parameters.Add("@_categoria_marca_id", item.Categoria_marca.CMa_id);

                dbConnection.Execute("app_riego.create_tipo_marca", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public IEnumerable<Tipo_marca> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
               
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_marca, Categoria_marca, Usuario, Tipo_marca>("app_riego.getall_tipo_marca", (Tipo_marca, Categoria_marca, Usuario) =>
                {
                    Tipo_marca.Categoria_marca = Categoria_marca;
                    Tipo_marca.TMa_usuario_creacion = Usuario;
                    Tipo_marca.TMa_usuario_modificacion = Usuario;

                    return Tipo_marca;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CMa_id, Users_id");


                return result;

            }
        }



        public Tipo_marca FindByID(int id)
        {

            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_marca, Categoria_marca, Usuario, Tipo_marca>("app_riego.getall_tipo_marca", (Tipo_marca, Categoria_marca, Usuario) =>
                {
                    Tipo_marca.Categoria_marca = Categoria_marca;
                    Tipo_marca.TMa_usuario_creacion = Usuario;
                    Tipo_marca.TMa_usuario_modificacion = Usuario;

                    return Tipo_marca;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "CMa_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public IEnumerable<Tipo_marca> GetSubtipos(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_padre_id", id);

                var result = dbConnection.Query<Tipo_marca>("app_riego.GetSubtiposmarcas", parameters, commandType: CommandType.StoredProcedure);
               
              
                return result;

            }
        }



        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_marca WHERE Id=@Id", new { Id = id });
            }
        }

        






        public IEnumerable<Tipo_marca> Getcmb_tipobycategoria_marca(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_marca>("app_riego.getcmb_tipobycategoria_marca", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }








    }
}
