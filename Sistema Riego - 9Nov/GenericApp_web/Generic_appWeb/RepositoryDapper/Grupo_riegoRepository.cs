﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_riegoRepository
    {
        private string connectionString;

        public Grupo_riegoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_riego item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);

                parameters.Add("@_codigo_empresa", item.Gr_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Gr_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Gr_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Gr_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Gr_descripcion_corta);
                parameters.Add("@_abreviacion", item.Gr_abreviacion);
                parameters.Add("@_observacion", item.Gr_observacion);
               
                parameters.Add("@_fecha_creacion", item.Gr_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Gr_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Gr_secuencia);
                parameters.Add("@_hacienda_id", item.Hacienda);


                dbConnection.Execute("app_riego.create_grupo_riego", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_riego item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Gr_id);
                parameters.Add("@_codigo_empresa", item.Gr_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Gr_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Gr_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Gr_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Gr_descripcion_corta);
                parameters.Add("@_abreviacion", item.Gr_abreviacion);
                parameters.Add("@_observacion", item.Gr_observacion);

                parameters.Add("@_fecha_creacion", item.Gr_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Gr_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Gr_secuencia);
                parameters.Add("@_hacienda_id", item.Hacienda);

                dbConnection.Execute("app_riego.create_grupo_riego", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_riego> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_riego, Usuario, Hacienda, Grupo_riego>("app_riego.getall_grupo_riego", (Grupo_riego, Usuario, Hacienda) =>
                {
                    Grupo_riego.Gr_usuario_creacion = Usuario;
                    Grupo_riego.Gr_usuario_creacion = Usuario;
                    Grupo_riego.Hacienda = Hacienda;


                    return Grupo_riego;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id, Hac_id");


                return result;

            }
        }

        public Grupo_riego FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);



                var result = dbConnection.Query<Grupo_riego, Usuario, Hacienda, Grupo_riego>("app_riego.getall_grupo_riego", (Grupo_riego, Usuario, Hacienda) =>
                {
                    Grupo_riego.Gr_usuario_creacion = Usuario;
                    Grupo_riego.Gr_usuario_creacion = Usuario;
                    Grupo_riego.Hacienda = Hacienda;


                    return Grupo_riego;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id, Hac_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_riego WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
