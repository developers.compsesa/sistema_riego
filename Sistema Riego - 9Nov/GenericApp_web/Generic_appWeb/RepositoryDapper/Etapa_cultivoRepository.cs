﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Etapa_cultivoRepository
    {
        private string connectionString;

        public Etapa_cultivoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Etapa_cultivo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.ECul_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.ECul_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.ECul_descripcion_larga);
                parameters.Add("@_descripcion_med", item.ECul_descripcion_med);
                parameters.Add("@_descripcion_corta", item.ECul_descripcion_corta);
                parameters.Add("@_abreviacion", item.ECul_abreviacion);
                parameters.Add("@_observacion", item.ECul_observacion);               
                parameters.Add("@_fecha_creacion", item.ECul_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.ECul_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.ECul_secuencia);

                dbConnection.Execute("app_riego.create_etapa_cultivo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Etapa_cultivo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.ECul_id);
                parameters.Add("@_codigo_empresa", item.ECul_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.ECul_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.ECul_descripcion_larga);
                parameters.Add("@_descripcion_med", item.ECul_descripcion_med);
                parameters.Add("@_descripcion_corta", item.ECul_descripcion_corta);
                parameters.Add("@_abreviacion", item.ECul_abreviacion);
                parameters.Add("@_observacion", item.ECul_observacion);
                parameters.Add("@_fecha_creacion", item.ECul_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.ECul_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.ECul_secuencia);

                dbConnection.Execute("app_riego.create_etapa_cultivo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Etapa_cultivo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Etapa_cultivo, Usuario, Etapa_cultivo>("app_riego.getall_etapa_cultivo", (Etapa_cultivo, Usuario) =>
                {
                    Etapa_cultivo.ECul_usuario_creacion = Usuario;
                    Etapa_cultivo.ECul_usuario_modificacion = Usuario;

                    return Etapa_cultivo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Etapa_cultivo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Etapa_cultivo, Usuario, Etapa_cultivo>("app_riego.getall_etapa_cultivo", (Etapa_cultivo, Usuario) =>
                {
                    Etapa_cultivo.ECul_usuario_creacion = Usuario;
                    Etapa_cultivo.ECul_usuario_modificacion = Usuario;

                    return Etapa_cultivo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Etapa_cultivo WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
