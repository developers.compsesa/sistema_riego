﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class PaisRepository
    {
        private string connectionString;

        public PaisRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Pais item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Pa_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Pa_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Pa_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Pa_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Pa_descripcion_corta);
                parameters.Add("@_abreviacion", item.Pa_abreviacion);
                parameters.Add("@_observacion", item.Pa_observacion);
               
                parameters.Add("@_fecha_creacion", item.Pa_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Pa_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Pa_secuencia);

                dbConnection.Execute("app_riego.create_pais", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Pais item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Pa_id);
                parameters.Add("@_codigo_empresa", item.Pa_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Pa_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Pa_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Pa_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Pa_descripcion_corta);
                parameters.Add("@_abreviacion", item.Pa_abreviacion);
                parameters.Add("@_observacion", item.Pa_observacion);

                parameters.Add("@_fecha_creacion", item.Pa_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Pa_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Pa_secuencia);

                dbConnection.Execute("app_riego.create_pais", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Pais> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Pais, Usuario, Pais>("app_riego.getallpaises", (Pais, Usuario) =>
                {
                    Pais.Pa_usuario_creacion = Usuario;
                    Pais.Pa_usuario_modificacion = Usuario;

                    return Pais;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Pais FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Pais, Usuario, Pais>("app_riego.getallpaises", (Pais, Usuario) =>
                {
                    Pais.Pa_usuario_creacion = Usuario;
                    Pais.Pa_usuario_modificacion = Usuario;

                    return Pais;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.pais WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
