﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class AlertaRepository : IRepository<Alerta>
    {
        private string connectionString;

        public AlertaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Alerta item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Al_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Al_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Al_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Al_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Al_descripcion_corta);
                parameters.Add("@_abreviacion", item.Al_abreviacion);
                parameters.Add("@_observacion", item.Al_observacion);
                parameters.Add("@_fecha_creacion", item.Al_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Al_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Al_secuencia);
                parameters.Add("@_mensaje", item.Al_mensaje);
                parameters.Add("@_grupo_alerta_id", item.Grupo_alerta.GAl_id);
                parameters.Add("@_categoria_alerta_id", item.Categoria_alerta.CAl_id);
                parameters.Add("@_tipo_alerta_id", item.Tipo_alerta.TAl_id);
                parameters.Add("@_hacienda_id", item.Tipo_alerta.TAl_id);


                dbConnection.Execute("app_riego.create_alerta", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Alerta item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Al_id);
                parameters.Add("@_codigo_empresa", item.Al_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Al_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Al_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Al_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Al_descripcion_corta);
                parameters.Add("@_abreviacion", item.Al_abreviacion);
                parameters.Add("@_observacion", item.Al_observacion);
                parameters.Add("@_fecha_creacion", item.Al_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Al_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Al_secuencia);
                parameters.Add("@_mensaje", item.Al_mensaje);
                parameters.Add("@_grupo_alerta_id", item.Grupo_alerta.GAl_id);
                parameters.Add("@_categoria_alerta_id", item.Categoria_alerta.CAl_id);
                parameters.Add("@_tipo_alerta_id", item.Tipo_alerta.TAl_id);
                parameters.Add("@_hacienda_id", item.Tipo_alerta.TAl_id);


                dbConnection.Execute("app_riego.create_alerta", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Alerta> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);
                var result = dbConnection.Query<Alerta, Grupo_alerta, Categoria_alerta, Tipo_alerta, Usuario, Alerta>("app_riego.getall_alerta", (Alerta, Grupo_alerta, Categoria_alerta, Tipo_alerta, Usuario ) =>
                {
                    Alerta.Grupo_alerta = Grupo_alerta;
                    Alerta.Categoria_alerta = Categoria_alerta;
                    Alerta.Tipo_alerta = Tipo_alerta;
                    Alerta.Al_usuario_creacion = Usuario;
                    Alerta.Al_usuario_modificacion = Usuario;

                    return Alerta;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "Al_id, GAl_id, CAl_id, TAl_id, Users_id");


                return result;

            }
        }

        public Alerta FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Alerta, Grupo_alerta, Categoria_alerta, Tipo_alerta, Usuario, Alerta>("app_riego.getall_alerta", (Alerta, Grupo_alerta, Categoria_alerta, Tipo_alerta, Usuario) =>
                {
                    Alerta.Grupo_alerta = Grupo_alerta;
                    Alerta.Categoria_alerta = Categoria_alerta;
                    Alerta.Tipo_alerta = Tipo_alerta;
                    Alerta.Al_usuario_creacion = Usuario;
                    Alerta.Al_usuario_modificacion = Usuario;

                    return Alerta;
                }, parameters, commandType: CommandType.StoredProcedure,
               splitOn: "Al_id, GAl_id, CAl_id, TAl_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("UPDATE app_riego.categoria_alerta SET estado = 'I' WHERE Id=@Id", new { Id = id });
            }
        }

       
    }
}
