﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_lugarRepository : IRepository<Tipo_lugar>
    {
        private string connectionString;

        public Tipo_lugarRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_lugar item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TL_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TL_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TL_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TL_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TL_descripcion_corta);
                parameters.Add("@_abreviacion", item.TL_abreviacion);
                parameters.Add("@_observacion", item.TL_observacion);
                parameters.Add("@_fecha_creacion", item.TL_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TL_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TL_secuencia);
                parameters.Add("@_categoria_lugar_id", item.Categoria_lugar.CL_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_tipo_lugar", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        public void Update(Tipo_lugar item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TL_id);
                parameters.Add("@_codigo_empresa", item.TL_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TL_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TL_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TL_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TL_descripcion_corta);
                parameters.Add("@_abreviacion", item.TL_abreviacion);
                parameters.Add("@_observacion", item.TL_observacion);
                parameters.Add("@_fecha_creacion", item.TL_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TL_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TL_secuencia);
                parameters.Add("@_categoria_lugar_id", item.Categoria_lugar.CL_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_tipo_lugar", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public IEnumerable<Tipo_lugar> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
               
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_lugar, Categoria_lugar, Usuario, Hacienda,Tipo_lugar>("app_riego.getall_tipo_lugar", (Tipo_lugar, Categoria_lugar, Usuario , Hacienda) =>
                {
                    Tipo_lugar.Categoria_lugar = Categoria_lugar;
                    Tipo_lugar.TL_usuario_creacion = Usuario;
                    Tipo_lugar.TL_usuario_modificacion = Usuario;
                    Tipo_lugar.Hacienda = Hacienda;
                    return Tipo_lugar;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CL_id, Hac_id , Users_id");


                return result;

            }
        }



        public Tipo_lugar FindByID(int id)
        {

            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_lugar, Categoria_lugar, Usuario, Hacienda, Tipo_lugar>("app_riego.getall_tipo_lugar", (Tipo_lugar, Categoria_lugar, Usuario, Hacienda) =>
                {
                    Tipo_lugar.Categoria_lugar = Categoria_lugar;
                    Tipo_lugar.TL_usuario_creacion = Usuario;
                    Tipo_lugar.TL_usuario_modificacion = Usuario;
                    Tipo_lugar.Hacienda = Hacienda;
                    return Tipo_lugar;

                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "CL_id, Hac_id , Users_id");


                return result.FirstOrDefault();

            }
        }

        public IEnumerable<Tipo_lugar> GetSubtipos(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_padre_id", id);

                var result = dbConnection.Query<Tipo_lugar>("app_riego.GetSubtiposlugares", parameters, commandType: CommandType.StoredProcedure);
               
              
                return result;

            }
        }



        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_lugar WHERE Id=@Id", new { Id = id });
            }
        }

        






        public IEnumerable<Tipo_lugar> Getcmb_tipobycategoria_lugar(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_lugar>("app_riego.getcmb_tipobycategoria_lugar", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }








    }
}
