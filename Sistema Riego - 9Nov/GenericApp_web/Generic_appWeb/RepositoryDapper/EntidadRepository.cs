﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class EntidadRepository
    {
        private string connectionString;

        public EntidadRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Entidad item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.En_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.En_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.En_descripcion_larga);
                parameters.Add("@_descripcion_med", item.En_descripcion_med);
                parameters.Add("@_descripcion_corta", item.En_descripcion_corta);
                parameters.Add("@_abreviacion", item.En_abreviacion);
                parameters.Add("@_observacion", item.En_observacion);
                parameters.Add("@_nombres", item.En_nombre);
                parameters.Add("@_apellidos", item.En_apellido);
                parameters.Add("@_razon_social", item.En_razon_Social);                
                parameters.Add("@_identificacion", item.En_identificacion);
                parameters.Add("@_fecha_creacion", item.En_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.En_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.En_secuencia);
                parameters.Add("@_grupo_entidad_id", item.Grupo_entidad.GEn_id);
                parameters.Add("@_categoria_entidad_id", item.Categoria_entidad.CEn_id);
                parameters.Add("@_tipo_entidad_id", item.Tipo_entidad.TEn_id);
                dbConnection.Execute("monitor.create_entidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Entidad item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.En_id);
                parameters.Add("@_codigo_empresa", item.En_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.En_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.En_descripcion_larga);
                parameters.Add("@_descripcion_med", item.En_descripcion_med);
                parameters.Add("@_descripcion_corta", item.En_descripcion_corta);
                parameters.Add("@_abreviacion", item.En_abreviacion);
                parameters.Add("@_observacion", item.En_observacion);
                parameters.Add("@_nombres", item.En_nombre);
                parameters.Add("@_apellidos", item.En_apellido);
                parameters.Add("@_razon_social", item.En_razon_Social);
                parameters.Add("@_identificacion", item.En_identificacion);
                parameters.Add("@_fecha_creacion", item.En_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.En_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.En_secuencia);
                parameters.Add("@_grupo_entidad_id", item.Grupo_entidad.GEn_id);
                parameters.Add("@_categoria_entidad_id", item.Categoria_entidad.CEn_id);
                parameters.Add("@_tipo_entidad_id", item.Tipo_entidad.TEn_id);
                dbConnection.Execute("monitor.create_Entidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Entidad> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Entidad, Grupo_entidad, Categoria_entidad,Tipo_entidad,Usuario, Entidad>("app_riego.getall_entidad", (Entidad, Grupo_entidad, Categoria_entidad, Tipo_entidad,Usuario) =>
                {
                    Entidad.Grupo_entidad = Grupo_entidad;
                    Entidad.Categoria_entidad = Categoria_entidad;
                    Entidad.Tipo_entidad = Tipo_entidad;
                    Entidad.En_usuario_creacion = Usuario;
                    Entidad.En_usuario_modificacion = Usuario;

                    return Entidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GEn_id, CEn_id, TEn_id, Users_id");


                return result;

            }
        }

        public Entidad FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Entidad, Grupo_entidad, Categoria_entidad, Tipo_entidad, Usuario, Entidad>("monitor.getallEntidad", (Entidad, Grupo_entidad, Categoria_entidad, Tipo_entidad, Usuario) =>
                {
                    Entidad.Grupo_entidad = Grupo_entidad;
                    Entidad.Categoria_entidad = Categoria_entidad;
                    Entidad.Tipo_entidad = Tipo_entidad;
                    Entidad.En_usuario_creacion = Usuario;
                    Entidad.En_usuario_modificacion = Usuario;

                    return Entidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GEn_id, CEn_id, TEn_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM monitor.Entidad WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Entidad> FindAllFiltered(int? grupo_entidad_id, int? categoria_entidad_id, int? tipo_entidad_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (grupo_entidad_id != 0)
                {
                    if (categoria_entidad_id != 0)
                    {
                        if (tipo_entidad_id != 0)
                        {
                            flag = 3;
                            //if (subtipo_activo_id != 0)
                            //{
                            //    flag = 4;
                            //}
                            //else
                            //{
                            //    flag = 3;
                            //}
                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);

                parameters.Add("@_grupo_entidad_id", grupo_entidad_id);
                parameters.Add("@_categoria_entidad_id", categoria_entidad_id);
                parameters.Add("@_tipo_entidad_id", tipo_entidad_id);
                //parameters.Add("@_subtipo_activo_id", subtipo_activo_id);


                var result = dbConnection.Query<Entidad, Grupo_entidad, Categoria_entidad, Tipo_entidad, Usuario, Entidad>("monitor.getallEntidad", (Entidad, Grupo_entidad, Categoria_entidad, Tipo_entidad, Usuario) =>
                {
                    Entidad.Grupo_entidad = Grupo_entidad;
                    Entidad.Categoria_entidad = Categoria_entidad;
                    Entidad.Tipo_entidad = Tipo_entidad;
                    Entidad.En_usuario_creacion = Usuario;
                    Entidad.En_usuario_modificacion = Usuario;

                    return Entidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GEn_id, CEn_id, TEn_id, Users_id");

                return result;
            }
        }


        public IEnumerable<Entidad> GetallEntidadByTipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Entidad>("monitor.GetallEntidadByTipo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }







        public Entidad GetEntidadByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
           
                parameters.Add("@_codigo_empresa", codigo_empresa);


                var result = dbConnection.Query<Entidad, Grupo_entidad, Categoria_entidad, Tipo_entidad, Usuario, Entidad>("monitor.getallEntidad", (Entidad, Grupo_entidad, Categoria_entidad, Tipo_entidad, Usuario) =>
                {
                    Entidad.Grupo_entidad = Grupo_entidad;
                    Entidad.Categoria_entidad = Categoria_entidad;
                    Entidad.Tipo_entidad = Tipo_entidad;
                    Entidad.En_usuario_creacion = Usuario;
                    Entidad.En_usuario_modificacion = Usuario;

                    return Entidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GEn_id, CEn_id, TEn_id, Users_id");


                return result.FirstOrDefault();

            }

        }


        public IEnumerable<Entidad> FindAll2()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                
                var result = dbConnection.Query<Entidad>("monitor.getallentidades2", commandType: CommandType.StoredProcedure);
                return result;

            }
        }




    }
}
