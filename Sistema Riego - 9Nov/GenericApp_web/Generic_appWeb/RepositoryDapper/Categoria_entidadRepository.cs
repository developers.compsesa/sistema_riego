﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_entidadRepository
    {
        private string connectionString;

        public Categoria_entidadRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_entidad item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CEn_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CEn_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CEn_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CEn_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CEn_descripcion_corta);
                parameters.Add("@_abreviacion", item.CEn_abreviacion);
                parameters.Add("@_observacion", item.CEn_observacion);
               
                parameters.Add("@_fecha_creacion", item.CEn_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CEn_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CEn_secuencia);
                parameters.Add("@_grupo_entidad_id", item.Grupo_entidad.GEn_id);
                dbConnection.Execute("app_riego.create_categoria_entidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_entidad item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", item.CEn_id);
                parameters.Add("@_codigo_empresa", item.CEn_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CEn_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CEn_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CEn_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CEn_descripcion_corta);
                parameters.Add("@_abreviacion", item.CEn_abreviacion);
                parameters.Add("@_observacion", item.CEn_observacion);

                parameters.Add("@_fecha_creacion", item.CEn_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CEn_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CEn_secuencia);
                parameters.Add("@_grupo_entidad_id", item.Grupo_entidad.GEn_id);
                dbConnection.Execute("app_riego.create_categoria_entidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_entidad> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_entidad, Grupo_entidad, Usuario, Categoria_entidad>("app_riego.getall_categoria_entidad", (Categoria_entidad, Grupo_entidad, Usuario) =>
                {
                    Categoria_entidad.Grupo_entidad = Grupo_entidad;
                    Categoria_entidad.CEn_usuario_creacion = Usuario;
                    Categoria_entidad.CEn_usuario_modificacion = Usuario;

                    return Categoria_entidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id,GEn_id");


                return result;

            }
        }

        public Categoria_entidad FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_entidad, Grupo_entidad, Usuario, Categoria_entidad>("app_riego.getall_categoria_entidad", (Categoria_entidad, Grupo_entidad, Usuario) =>
                {
                    Categoria_entidad.Grupo_entidad = Grupo_entidad;
                    Categoria_entidad.CEn_usuario_creacion = Usuario;
                    Categoria_entidad.CEn_usuario_modificacion = Usuario;

                    return Categoria_entidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "Users_id,GEn_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Categoria_entidad WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Categoria_entidad> Getcmb_categoriabygrupo_entidad(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_entidad>("app_riego.Getcmb_categoriabygrupo_entidad", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



    }
}
