﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_areaRepository 
    {
        private string connectionString;

        public Grupo_areaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_area item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GAre_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GAre_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GAre_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GAre_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GAre_descripcion_corta);
                parameters.Add("@_abreviacion", item.GAre_abreviacion);
                parameters.Add("@_observacion", item.GAre_observacion);
                parameters.Add("@_fecha_creacion", item.GAre_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GAre_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GAre_secuencia);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_grupo_area", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_area item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                
                parameters.Add("@_id", item.GAre_id);
                parameters.Add("@_codigo_empresa", item.GAre_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GAre_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GAre_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GAre_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GAre_descripcion_corta);
                parameters.Add("@_abreviacion", item.GAre_abreviacion);
                parameters.Add("@_observacion", item.GAre_observacion);
                parameters.Add("@_fecha_creacion", item.GAre_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GAre_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GAre_secuencia);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_grupo_area", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_area> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_area, Usuario, Hacienda,  Grupo_area>("app_riego.getallGrupo_area", (Grupo_area, Usuario , Hacienda) =>
                {
                  
                    Grupo_area.GAre_usuario_creacion = Usuario;
                    Grupo_area.GAre_usuario_modificacion = Usuario;
                    Grupo_area.Hacienda = Hacienda;
                    return Grupo_area;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "GAre_id,Users_id, Hac_id");
                return result;
            }
        }

      


        public Grupo_area FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Grupo_area, Usuario, Hacienda, Grupo_area>("app_riego.getallGrupo_area", (Grupo_area, Usuario, Hacienda) =>
                {

                    Grupo_area.GAre_usuario_creacion = Usuario;
                    Grupo_area.GAre_usuario_modificacion = Usuario;
                    Grupo_area.Hacienda = Hacienda;
                    return Grupo_area;
                }, parameters, commandType: CommandType.StoredProcedure,
                   splitOn: "GAre_id,Users_id, Hac_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_area WHERE Id=@Id", new { Id = id });
            }
        }

       

        

    }
}
