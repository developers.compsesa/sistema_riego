﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_usuarioRepository
    {
        private string connectionString;

        public Tipo_usuarioRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_usuario item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TUs_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TUs_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TUs_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TUs_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TUs_descripcion_corta);
                parameters.Add("@_abreviacion", item.TUs_abreviacion);
                parameters.Add("@_observacion", item.TUs_observacion);               
                parameters.Add("@_fecha_creacion", item.TUs_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TUs_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TUs_secuencia);
                parameters.Add("@_categoria_usuario_id", item.Categoria_Usuario.CUs_id);

                dbConnection.Execute("app_riego.create_categoria_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Tipo_usuario item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TUs_id);
                parameters.Add("@_codigo_empresa", item.TUs_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TUs_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TUs_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TUs_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TUs_descripcion_corta);
                parameters.Add("@_abreviacion", item.TUs_abreviacion);
                parameters.Add("@_observacion", item.TUs_observacion);
                parameters.Add("@_fecha_creacion", item.TUs_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TUs_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TUs_secuencia);
                parameters.Add("@_categoria_usuario_id", item.Categoria_Usuario.CUs_id);

                dbConnection.Execute("app_riego.create_categoria_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Tipo_usuario> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_usuario, Categoria_usuario, Usuario, Tipo_usuario>("app_riego.getall_categoria_valor", (Tipo_usuario, Categoria_usuario, Usuario) =>
                {
                    Tipo_usuario.Categoria_Usuario = Categoria_usuario;
                    Tipo_usuario.TUs_usuario_creacion = Usuario;
                    Tipo_usuario.TUs_usuario_modificacion = Usuario;

                    return Tipo_usuario;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id");


                return result;

            }
        }

        public Tipo_usuario FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Tipo_usuario, Categoria_usuario, Usuario, Tipo_usuario>("app_riego.getall_categoria_valor", (Tipo_usuario, Categoria_usuario, Usuario) =>
                {
                    Tipo_usuario.Categoria_Usuario = Categoria_usuario;
                    Tipo_usuario.TUs_usuario_creacion = Usuario;
                    Tipo_usuario.TUs_usuario_modificacion = Usuario;

                    return Tipo_usuario;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_usuario WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Tipo_usuario> GetallTipo_usuarioBygrupo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_usuario>("app_riego.getallcategoriasvalorbygrupo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



    }
}
