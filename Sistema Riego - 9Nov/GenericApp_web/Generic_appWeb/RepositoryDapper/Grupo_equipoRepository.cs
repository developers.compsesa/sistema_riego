﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_equipoRepository : IRepository<Grupo_equipo>
    {
        private string connectionString;

        public Grupo_equipoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_equipo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GE_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GE_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GE_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GE_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GE_descripcion_corta);
                parameters.Add("@_abreviacion", item.GE_abreviacion);
                parameters.Add("@_observacion", item.GE_observacion);
                parameters.Add("@_fecha_creacion", item.GE_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GE_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GE_secuencia);

                dbConnection.Execute("app_riego.create_grupo_equipo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Grupo_equipo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", item.GE_id);
                parameters.Add("@_codigo_empresa", item.GE_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GE_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GE_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GE_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GE_descripcion_corta);
                parameters.Add("@_abreviacion", item.GE_abreviacion);
                parameters.Add("@_observacion", item.GE_observacion);
                parameters.Add("@_fecha_creacion", item.GE_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GE_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GE_secuencia);

                dbConnection.Execute("app_riego.create_grupo_equipo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_equipo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_equipo, Usuario, Grupo_equipo>("app_riego.getall_grupo_equipo", (Grupo_equipo, Usuario) =>
                {
                    Grupo_equipo.GE_usuario_creacion = Usuario;
                    Grupo_equipo.GE_usuario_modificacion = Usuario;

                    return Grupo_equipo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GE_id, Users_id");


                return result;

            }
        }

        public Grupo_equipo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_equipo, Usuario, Grupo_equipo>("app_riego.getall_grupo_equipo", (Grupo_equipo, Usuario) =>
                {
                    Grupo_equipo.GE_usuario_creacion = Usuario;
                    Grupo_equipo.GE_usuario_modificacion = Usuario;

                    return Grupo_equipo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GE_id, Users_id");



                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.grupos_activos WHERE Id=@Id", new { Id = id });
            }
        }

       
    }
}
