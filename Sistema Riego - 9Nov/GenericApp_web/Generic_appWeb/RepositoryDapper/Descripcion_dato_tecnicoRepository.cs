﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Descripcion_dato_tecnicoRepository
    {
        private string connectionString;

        public Descripcion_dato_tecnicoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {

                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Descripcion_dato_tecnico item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.DDTec_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.DDTec_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.DDTec_descripcion_larga);
                parameters.Add("@_descripcion_med", item.DDTec_descripcion_med);
                parameters.Add("@_descripcion_corta", item.DDTec_descripcion_corta);
                parameters.Add("@_abreviacion", item.DDTec_abreviacion);
                parameters.Add("@_observacion", item.DDTec_observacion);
                parameters.Add("@_fecha_creacion", item.DDTec_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.DDTec_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.DDTec_secuencia);
                parameters.Add("@_grupo_dato_tecnico_id", item.Grupo_dato_tecnico.GDTec_id);
                parameters.Add("@_categoria_dato_tecnico_id", item.Categoria_dato_tecnico.CDTec_id);
                parameters.Add("@_tipo_dato_tecnico_id", item.Tipo_dato_tecnico.TDTec_id);     

                dbConnection.Execute("app_riego.create_descripcion_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Descripcion_dato_tecnico item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.DDTec_id);
                parameters.Add("@_codigo_empresa", item.DDTec_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.DDTec_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.DDTec_descripcion_larga);
                parameters.Add("@_descripcion_med", item.DDTec_descripcion_med);
                parameters.Add("@_descripcion_corta", item.DDTec_descripcion_corta);
                parameters.Add("@_abreviacion", item.DDTec_abreviacion);
                parameters.Add("@_observacion", item.DDTec_observacion);
                parameters.Add("@_fecha_creacion", item.DDTec_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.DDTec_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.DDTec_secuencia);
                parameters.Add("@_grupo_dato_tecnico_id", item.Grupo_dato_tecnico.GDTec_id);
                parameters.Add("@_categoria_dato_tecnico_id", item.Categoria_dato_tecnico.CDTec_id);
                parameters.Add("@_tipo_dato_tecnico_id", item.Tipo_dato_tecnico.TDTec_id);

                dbConnection.Execute("app_riego.create_descripcion_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Descripcion_dato_tecnico> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Descripcion_dato_tecnico, Grupo_dato_tecnico, Categoria_dato_tecnico, Tipo_dato_tecnico, Usuario, Descripcion_dato_tecnico>("app_riego.getall_descripcion_dato_tecnico", (Descripcion_dato_tecnico, Grupo_dato_tecnico, Categoria_dato_tecnico, Tipo_dato_tecnico, Usuario) =>
                {
                    Descripcion_dato_tecnico.Grupo_dato_tecnico = Grupo_dato_tecnico;
                    Descripcion_dato_tecnico.Categoria_dato_tecnico = Categoria_dato_tecnico;
                    Descripcion_dato_tecnico.Tipo_dato_tecnico = Tipo_dato_tecnico;
                    Descripcion_dato_tecnico.DDTec_usuario_creacion = Usuario;
                    Descripcion_dato_tecnico.DDTec_usuario_modificacion = Usuario;

                    return Descripcion_dato_tecnico;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "DTec_id, GDTec_id,CDTec_id,TDTec_id, Users_id");


                return result;

            }
        }

        public Descripcion_dato_tecnico FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Descripcion_dato_tecnico, Grupo_dato_tecnico, Categoria_dato_tecnico, Tipo_dato_tecnico, Usuario, Descripcion_dato_tecnico>("app_riego.getall_descripcion_dato_tecnico", (Descripcion_dato_tecnico, Grupo_dato_tecnico, Categoria_dato_tecnico, Tipo_dato_tecnico, Usuario) =>
                {
                    Descripcion_dato_tecnico.Grupo_dato_tecnico = Grupo_dato_tecnico;
                    Descripcion_dato_tecnico.Categoria_dato_tecnico = Categoria_dato_tecnico;
                    Descripcion_dato_tecnico.Tipo_dato_tecnico = Tipo_dato_tecnico;
                    Descripcion_dato_tecnico.DDTec_usuario_creacion = Usuario;
                    Descripcion_dato_tecnico.DDTec_usuario_modificacion = Usuario;

                    return Descripcion_dato_tecnico;

                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "DTec_id, GDTec_id,CDTec_id,TDTec_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.inventario_descripciones_datos_tecnicos WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Descripcion_dato_tecnico> FindAllFiltered(int? grupo_dato_tecnico_id, int? categoria_dato_tecnico_id, int? tipo_dato_tecnico_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (grupo_dato_tecnico_id != 0)
                {
                    if (categoria_dato_tecnico_id != 0)
                    {
                        if (tipo_dato_tecnico_id != 0)
                        {
                            flag = 3;
                            //if (subtipo_activo_id != 0)
                            //{
                            //    flag = 4;
                            //}
                            //else
                            //{
                            //    flag = 3;
                            //}
                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);
                parameters.Add("@_grupo_dato_tecnico_id", grupo_dato_tecnico_id);
                parameters.Add("@_categoria_dato_tecnico_id", categoria_dato_tecnico_id);
                parameters.Add("@_tipo_dato_tecnico_id", tipo_dato_tecnico_id);
                //parameters.Add("@_subtipo_activo_id", subtipo_activo_id);

                var result = dbConnection.Query<Descripcion_dato_tecnico, Grupo_dato_tecnico, Categoria_dato_tecnico, Tipo_dato_tecnico, Usuario, Descripcion_dato_tecnico>("app_riego.getall_descripcion_datos_tecnicos_filtered", (Dato_tecnico, Grupo_dato_tecnico, Categoria_dato_tecnico, Tipo_dato_tecnico, Usuario) =>
                {
                    Dato_tecnico.Grupo_dato_tecnico = Grupo_dato_tecnico;
                    Dato_tecnico.Categoria_dato_tecnico = Categoria_dato_tecnico;
                    Dato_tecnico.Tipo_dato_tecnico = Tipo_dato_tecnico;
                    Dato_tecnico.DDTec_usuario_creacion = Usuario;
                    Dato_tecnico.DDTec_usuario_modificacion = Usuario;


                    return Dato_tecnico;
                }, parameters, commandType: CommandType.StoredProcedure,

                 splitOn: "GDTec_id,CDTec_id,TDTec_id, Users_id");


                return result;
            }
        }


        public IEnumerable<Descripcion_dato_tecnico> GetalldatostecnicosByTipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Descripcion_dato_tecnico>("app_riego.getalltiposbycategoriadatos_tecnicos", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public Descripcion_dato_tecnico GetDatosTecnicosByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
           
                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Descripcion_dato_tecnico, Grupo_dato_tecnico, Categoria_dato_tecnico, Tipo_dato_tecnico, Usuario, Descripcion_dato_tecnico>("app_riego.getall_dato_tecnico", (Dato_tecnico, Grupo_dato_tecnico, Categoria_dato_tecnico, Tipo_dato_tecnico, Usuario) =>
                {

                    Dato_tecnico.Grupo_dato_tecnico = Grupo_dato_tecnico;
                    Dato_tecnico.Categoria_dato_tecnico = Categoria_dato_tecnico;
                    Dato_tecnico.Tipo_dato_tecnico = Tipo_dato_tecnico;
                    Dato_tecnico.DDTec_usuario_creacion = Usuario;
                    Dato_tecnico.DDTec_usuario_modificacion = Usuario;


                    return Dato_tecnico;
                }, parameters, commandType: CommandType.StoredProcedure,

                 splitOn: "GDTec_id,CDTec_id,TDTec_id, Users_id");


                return result.FirstOrDefault();

            }

        }









    }
}
