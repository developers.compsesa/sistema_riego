﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Ubicacion_geograficaRepository
    {
        private string connectionString;

        public Ubicacion_geograficaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Ubicacion_geografica item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Ug_Codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Ug_Codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Ug_Descripcion_larga);
                parameters.Add("@_descripcion_med", item.Ug_Descripcion_med);
                parameters.Add("@_descripcion_corta", item.Ug_Descripcion_corta);
                parameters.Add("@_abreviacion", item.Ug_Abreviacion);
                parameters.Add("@_observacion", item.Ug_Observacion);               
                parameters.Add("@_fecha_creacion", item.Ug_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Ug_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Ug_secuencia);
                parameters.Add("@_pais_id", item.Pais.Pa_id);
                parameters.Add("@_provincia_id", item.Provincia.Pr_id);
                parameters.Add("@_ciudad_id", item.Ciudad.Ci_id);

                dbConnection.Execute("app_riego.create_ubicacion_geografica", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Ubicacion_geografica item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Ug_id);
                parameters.Add("@_codigo_empresa", item.Ug_Codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Ug_Codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Ug_Descripcion_larga);
                parameters.Add("@_descripcion_med", item.Ug_Descripcion_med);
                parameters.Add("@_descripcion_corta", item.Ug_Descripcion_corta);
                parameters.Add("@_abreviacion", item.Ug_Abreviacion);
                parameters.Add("@_observacion", item.Ug_Observacion);              
                parameters.Add("@_fecha_creacion", item.Ug_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Ug_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Ug_secuencia);
                parameters.Add("@_pais_id", item.Pais.Pa_id);
                parameters.Add("@_provincia_id", item.Provincia.Pr_id);
                parameters.Add("@_ciudad_id", item.Ciudad.Ci_id);

                dbConnection.Execute("app_riego.create_ubicacion_geografica", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Ubicacion_geografica> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Ubicacion_geografica, Pais, Provincia, Ciudad, Usuario, Ubicacion_geografica>("app_riego.getallubicacion_geografica", (Ubicacion_geografica, Pais, Provincia, Ciudad, Usuario) =>
                {
                    Ubicacion_geografica.Pais = Pais;
                    Ubicacion_geografica.Provincia = Provincia;
                    Ubicacion_geografica.Ciudad = Ciudad;
                    Ubicacion_geografica.Ug_usuario_creacion = Usuario;
                    Ubicacion_geografica.Ug_usuario_modificacion = Usuario;

                    return Ubicacion_geografica;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Pa_id,Pr_id,Ci_id,Users_id");


                return result;

            }
        }

      

        public Ubicacion_geografica FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Ubicacion_geografica, Pais, Provincia, Ciudad, Usuario, Ubicacion_geografica>("app_riego.getallubicacion_geografica", (Ubicacion_geografica, Pais, Provincia, Ciudad, Usuario) =>
                {
                    Ubicacion_geografica.Pais = Pais;
                    Ubicacion_geografica.Provincia = Provincia;
                    Ubicacion_geografica.Ciudad = Ciudad;
                    Ubicacion_geografica.Ug_usuario_creacion = Usuario;
                    Ubicacion_geografica.Ug_usuario_modificacion = Usuario;

                    return Ubicacion_geografica;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Pa_id,Pr_id,Ci_id,Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.ubicacion_geografica WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Ubicacion_geografica> FindAllFiltered(int? pais_id, int? provincia_id, int? ciudad_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (pais_id != 0)
                {
                    if (provincia_id != 0)
                    {
                        if (ciudad_id != 0)
                        {
                            flag = 3;
                            //if (subtipo_activo_id != 0)
                            //{
                            //    flag = 4;
                            //}
                            //else
                            //{
                            //    flag = 3;
                            //}
                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);

                parameters.Add("@_pais_id", pais_id);
                parameters.Add("@_provincia_id", provincia_id);
                parameters.Add("@_ciudad_id", ciudad_id);
                //parameters.Add("@_subtipo_activo_id", subtipo_activo_id);

                var result = dbConnection.Query<Ubicacion_geografica, Pais, Provincia, Ciudad, Usuario, Ubicacion_geografica>("app_riego.getallubicacion_geografica", (Ubicacion_geografica, Pais, Provincia, Ciudad, Usuario) =>
                {
                    Ubicacion_geografica.Pais = Pais;
                    Ubicacion_geografica.Provincia = Provincia;
                    Ubicacion_geografica.Ciudad = Ciudad;
                    Ubicacion_geografica.Ug_usuario_creacion = Usuario;
                    Ubicacion_geografica.Ug_usuario_modificacion = Usuario;

                    return Ubicacion_geografica;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "Ug_id,Pa_id,Pr_id,Ci_id, E_id");

                                
                return result;
            }
        }


        public IEnumerable<Ubicacion_geografica> GetallProvinciabyPais(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Ubicacion_geografica>("app_riego.getallprovinciabypais", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public IEnumerable<Ubicacion_geografica> GetallCiudadByProvincia(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Ubicacion_geografica>("app_riego.getallciudadesbyprovincias", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        //public Inventario_valores GetValorByCodEmpresa(string codigo_empresa)
        //{
        //    using (IDbConnection dbConnection = Connection)
        //    {
        //        dbConnection.Open();
        //        DynamicParameters parameters = new DynamicParameters();
           
        //        parameters.Add("@_codigo_empresa", codigo_empresa);

        //        var result = dbConnection.Query<Inventario_valores, Grupos_valor, Categorias_valor, Tipos_valor, Usuario, Inventario_valores>("app_riego.getallvalores", (Inventario_valores, Grupos_valor, Categorias_valor, Tipos_valor, Usuario) =>
        //        {
        //            Inventario_valores.Grupos_valor = Grupos_valor;
        //            Inventario_valores.Categorias_valor = Categorias_valor;
        //            Inventario_valores.Tipos_valor = Tipos_valor;
        //            Inventario_valores.Usuario = Usuario;

        //            return Inventario_valores;
        //        }, parameters, commandType: CommandType.StoredProcedure,
        //        splitOn: "GVal_id,CVal_id,TVal_id, E_id");


        //        return result.FirstOrDefault();

        //    }

        //}


        public IEnumerable<Ubicacion_geografica> FindAllUbicacion_geografica()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();


                var result = dbConnection.Query<Ubicacion_geografica>("app_riego.getallunidades2", commandType: CommandType.StoredProcedure);
                return result;

            }
        }


        public IEnumerable<Ubicacion_geografica> FindAll_Ubicacion_geografica()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();


                var result = dbConnection.Query<Ubicacion_geografica>("app_riego.getallubicacion_geografica2", commandType: CommandType.StoredProcedure);
                return result;

            }
        }





    }
}
