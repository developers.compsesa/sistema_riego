﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class ProvinciaRepository : IRepository<Provincia>
    {
        private string connectionString;

        public ProvinciaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Provincia item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Pr_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Pr_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Pr_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Pr_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Pr_descripcion_corta);
                parameters.Add("@_abreviacion", item.Pr_abreviacion);
                parameters.Add("@_observacion", item.Pr_observacion);
                parameters.Add("@_fecha_creacion", item.Pr_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Pr_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Pr_secuencia);

                dbConnection.Execute("app_riego.create_provincia", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Provincia> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Provincia, Pais, Usuario, Provincia>("app_riego.getall_provincia", (Provincia, Pais, Usuario) =>
                {
                    Provincia.Pais = Pais;
                    Provincia.Pr_usuario_creacion = Usuario;
                    Provincia.Pr_usuario_modificacion = Usuario;

                    return Provincia;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Pa_id, Users_id");


                return result;

            }
        }

        public Provincia FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Provincia, Pais, Usuario, Provincia>("app_riego.getall_provincia", (Provincia, Pais, Usuario) =>
                {
                    Provincia.Pais = Pais;
                    Provincia.Pr_usuario_creacion = Usuario;
                    Provincia.Pr_usuario_modificacion = Usuario;

                    return Provincia;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "Pa_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public IEnumerable<Provincia> GetallProvinciabyPais(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Provincia>("app_riego.getallprovinciabypais", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Provincia WHERE Id=@Id", new { Id = id });
            }
        }

        public void Update(Provincia item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Pr_id);
                parameters.Add("@_codigo_empresa", item.Pr_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Pr_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Pr_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Pr_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Pr_descripcion_corta);
                parameters.Add("@_abreviacion", item.Pr_abreviacion);
                parameters.Add("@_observacion", item.Pr_observacion);
                parameters.Add("@_fecha_creacion", item.Pr_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Pr_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Pr_secuencia);

                dbConnection.Execute("app_riego.create_provincia", parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
