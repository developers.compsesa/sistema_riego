﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_parametroRepository 
    {
        private string connectionString;

        public Tipo_parametroRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_parametro item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TPar_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TPar_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TPar_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TPar_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TPar_descripcion_corta);
                parameters.Add("@_abreviacion", item.TPar_abreviacion);
                parameters.Add("@_observacion", item.TPar_observacion);               
               
                parameters.Add("@_fecha_creacion", item.TPar_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TPar_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TPar_secuencia);
                parameters.Add("@_categoria_parametro_id",item.Categoria_parametro.CPar_id);

                dbConnection.Execute("app_riego.create_tipo_parametro", parameters, commandType: CommandType.StoredProcedure);
            }
        }



       


        public void Update(Tipo_parametro item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TPar_id);

                parameters.Add("@_codigo_empresa", item.TPar_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TPar_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TPar_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TPar_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TPar_descripcion_corta);
                parameters.Add("@_abreviacion", item.TPar_abreviacion);
                parameters.Add("@_observacion", item.TPar_observacion);
                parameters.Add("@_fecha_creacion", item.TPar_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TPar_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TPar_secuencia);
                parameters.Add("@_categoria_parametro_id", item.Categoria_parametro.CPar_id);
            }
        }


        public IEnumerable<Tipo_parametro> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
               
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_parametro, Categoria_parametro, Usuario, Tipo_parametro>("app_riego.getall_tipo_parametro", (Tipo_parametro, Categoria_parametro, Usuario) =>
                {
                    Tipo_parametro.Categoria_parametro = Categoria_parametro;
                    Tipo_parametro.TPar_usuario_creacion = Usuario;
                    Tipo_parametro.TPar_usuario_modificacion = Usuario;

                    return Tipo_parametro;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CPar_id, Users_id");

                return result;

            }
        }


        public Tipo_parametro FindByID(int id)
        {

            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_parametro, Categoria_parametro, Usuario, Tipo_parametro>("app_riego.getall_tipo_parametro", (Tipo_parametro, Categoria_parametro, Usuario) =>
                {
                    Tipo_parametro.Categoria_parametro = Categoria_parametro;
                    Tipo_parametro.TPar_usuario_creacion = Usuario;
                    Tipo_parametro.TPar_usuario_modificacion = Usuario;

                    return Tipo_parametro;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CPar_id, Users_id");


                return result.FirstOrDefault();

            }
        }
      


        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipos_parametros WHERE Id=@Id", new { Id = id });
            }
        }



        public IEnumerable<Tipo_parametro> GetallTipos_parametrosByCategoria(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_parametro>("app_riego.getalltiposparametrosbycategorias", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public IEnumerable<Tipo_parametro> GetallTiposparametros(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_parametro>("app_riego.getalltiposparametros", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



        public IEnumerable<Tipo_parametro> FindAllTipos_valor()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();


                var result = dbConnection.Query<Tipo_parametro>("app_riego.getalltipos_parametros2", commandType: CommandType.StoredProcedure);
                return result;

            }
        }






        public IEnumerable<Categoria_parametro> Getcmb_tipobycategoria_parametro(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_parametro>("app_riego.getcmb_tipobycategoria_parametro", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }











    }
}
