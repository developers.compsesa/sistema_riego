﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_areaRepository : IRepository<Tipo_area>
    {
        private string connectionString;

        public Tipo_areaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_area item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TAre_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TAre_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TAre_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TAre_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TAre_descripcion_corta);
                parameters.Add("@_abreviacion", item.TAre_abreviacion);
                parameters.Add("@_observacion", item.TAre_observacion);
                parameters.Add("@_fecha_creacion", item.TAre_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TAre_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TAre_secuencia);
                parameters.Add("@_categoria_area_id", item.Categoria_area.CAre_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_Tipo_area", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Tipo_area item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TAre_id);
                parameters.Add("@_codigo_empresa", item.TAre_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TAre_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TAre_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TAre_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TAre_descripcion_corta);
                parameters.Add("@_abreviacion", item.TAre_abreviacion);
                parameters.Add("@_observacion", item.TAre_observacion);
                parameters.Add("@_fecha_creacion", item.TAre_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TAre_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TAre_secuencia);
                parameters.Add("@_categoria_area_id", item.Categoria_area.CAre_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_Tipo_area", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Tipo_area> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_area, Categoria_area, Usuario, Hacienda, Tipo_area>("app_riego.getalltipos_areas", (Tipo_area, Categoria_area, Usuario , Hacienda) =>
                {
                    Tipo_area.Categoria_area = Categoria_area;
                    Tipo_area.TAre_usuario_creacion = Usuario;
                    Tipo_area.TAre_usuario_modificacion = Usuario;
                    Tipo_area.Hacienda = Hacienda;

                    return Tipo_area;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CAre_id, E_id, En_id");


                return result;

            }
        }

      


        public Tipo_area FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_area, Categoria_area, Usuario, Hacienda, Tipo_area >("app_riego.getall_tipo_area", (Tipo_area, Categoria_area, Usuario , Hacienda) =>
                {
                    Tipo_area.Categoria_area = Categoria_area;
                    Tipo_area.TAre_usuario_creacion  = Usuario;
                    Tipo_area.TAre_usuario_modificacion = Usuario;
                    Tipo_area.Hacienda = Hacienda;
                    return Tipo_area;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn:"CAre_id, Hac_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_area WHERE Id=@Id", new { Id = id });
            }
        }





        public IEnumerable<Tipo_area> Getcmb_tipobycategoria_area(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_area>("app_riego.getcmb_tipobycategoria_area", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }

    }
}
