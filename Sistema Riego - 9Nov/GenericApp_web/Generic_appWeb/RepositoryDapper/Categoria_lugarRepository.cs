﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_lugarRepository : IRepository<Categoria_lugar>
    {
        private string connectionString;

        public Categoria_lugarRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_lugar item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CL_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CL_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CL_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CL_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CL_descripcion_corta);
                parameters.Add("@_abreviacion", item.CL_abreviacion);
                parameters.Add("@_observacion", item.CL_observacion);
               
                parameters.Add("@_fecha_creacion", item.CL_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CL_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CL_secuencia);
                parameters.Add("@_grupo_lugar_id", item.Grupo_lugar.GL_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_categoria_lugar", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Categoria_lugar item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", item.CL_id);
                parameters.Add("@_codigo_empresa", item.CL_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CL_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CL_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CL_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CL_descripcion_corta);
                parameters.Add("@_abreviacion", item.CL_abreviacion);
                parameters.Add("@_observacion", item.CL_observacion);

                parameters.Add("@_fecha_creacion", item.CL_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CL_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CL_secuencia);
                parameters.Add("@_grupo_lugar_id", item.Grupo_lugar.GL_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_categoria_lugar", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_lugar> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_lugar, Grupo_lugar, Usuario, Hacienda, Categoria_lugar >("app_riego.getall_categoria_lugar", (Categoria_lugar, Grupo_lugar, Usuario, Hacienda) =>
                {
                    Categoria_lugar.Grupo_lugar = Grupo_lugar;
                    Categoria_lugar.CL_usuario_creacion = Usuario;
                    Categoria_lugar.CL_usuario_modificacion = Usuario;
                    Categoria_lugar.Hacienda = Hacienda;

                    return Categoria_lugar;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CL_id, GL_id, Users_id, Hac_id");


                return result;

            }
        }

    



        public Categoria_lugar FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_lugar, Grupo_lugar, Usuario, Hacienda, Categoria_lugar>("app_riego.getall_categoria_lugar", (Categoria_lugar, Grupo_lugar, Usuario, Hacienda) =>
                {
                    Categoria_lugar.Grupo_lugar = Grupo_lugar;
                    Categoria_lugar.CL_usuario_creacion = Usuario;
                    Categoria_lugar.CL_usuario_modificacion = Usuario;
                    Categoria_lugar.Hacienda = Hacienda;

                    return Categoria_lugar;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CL_id, GL_id, Users_id, Hac_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Categoria_lugar WHERE Id=@Id", new { Id = id });
            }
        }




        public IEnumerable<Categoria_lugar> Getcmb_categoriabygrupo_lugar(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_lugar>("app_riego.Getcmb_categoriabygrupo_lugar", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }








    }
}
