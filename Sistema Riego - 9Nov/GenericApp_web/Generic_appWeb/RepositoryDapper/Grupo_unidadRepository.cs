﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_unidadRepository
    {
        private string connectionString;

        public Grupo_unidadRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_unidad item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GUn_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GUn_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GUn_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GUn_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GUn_descripcion_corta);
                parameters.Add("@_abreviacion", item.GUn_abreviacion);
                parameters.Add("@_observacion", item.GUn_observacion);               
                parameters.Add("@_fecha_creacion", item.GUn_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GUn_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GUn_secuencia);

                dbConnection.Execute("app_riego.create_grupo_unidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_unidad item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.GUn_id);
                parameters.Add("@_codigo_empresa", item.GUn_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GUn_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GUn_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GUn_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GUn_descripcion_corta);
                parameters.Add("@_abreviacion", item.GUn_abreviacion);
                parameters.Add("@_observacion", item.GUn_observacion);
                parameters.Add("@_fecha_creacion", item.GUn_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GUn_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GUn_secuencia);

                dbConnection.Execute("app_riego.create_grupo_unidad", parameters, commandType: CommandType.StoredProcedure);

            }
        }

        public IEnumerable<Grupo_unidad> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_unidad, Usuario, Grupo_unidad>("app_riego.getall_grupo_unidad", (Grupo_unidad, Usuario) =>
                {
                    Grupo_unidad.GUn_usuario_creacion = Usuario;
                    Grupo_unidad.GUn_usuario_modificacion = Usuario;

                    return Grupo_unidad;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Grupo_unidad FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Grupo_unidad, Usuario, Grupo_unidad>("app_riego.getall_grupo_unidad", (Grupo_unidad, Usuario) =>
                {
                    Grupo_unidad.GUn_usuario_creacion = Usuario;
                    Grupo_unidad.GUn_usuario_modificacion = Usuario;

                    return Grupo_unidad;

                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id");



                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_unidad WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
