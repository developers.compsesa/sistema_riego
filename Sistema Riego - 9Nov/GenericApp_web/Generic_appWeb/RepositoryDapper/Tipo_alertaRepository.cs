﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_alertaRepository
    {
        private string connectionString;

        public Tipo_alertaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_alerta item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TAl_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TAl_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TAl_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TAl_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TAl_descripcion_corta);
                parameters.Add("@_abreviacion", item.TAl_abreviacion);
                parameters.Add("@_observacion", item.TAl_observacion);               
                parameters.Add("@_fecha_creacion", item.TAl_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TAl_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TAl_secuencia);
                parameters.Add("@_color_id", item.Color.Co_id);
                parameters.Add("@_icono_id", item.Icono.Ico_id);
                parameters.Add("@_categoria_alerta_id", item.Categoria_alerta.CAl_id);

                dbConnection.Execute("app_riego.create_tipo_alerta", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Tipo_alerta item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TAl_id);
                parameters.Add("@_codigo_empresa", item.TAl_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TAl_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TAl_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TAl_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TAl_descripcion_corta);
                parameters.Add("@_abreviacion", item.TAl_abreviacion);
                parameters.Add("@_observacion", item.TAl_observacion);
                parameters.Add("@_fecha_creacion", item.TAl_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TAl_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TAl_secuencia);
                parameters.Add("@_color_id", item.Color.Co_id);
                parameters.Add("@_icono_id", item.Icono.Ico_id);
                parameters.Add("@_categoria_alerta_id", item.Categoria_alerta.CAl_id);

                dbConnection.Execute("app_riego.create_tipo_alerta", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Tipo_alerta> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_alerta,  Usuario, Color, Icono, Categoria_alerta, Tipo_alerta>("app_riego.getall_tipo_alerta", (Tipo_alerta,  Usuario, Color, Icono, Categoria_alerta) =>
                {
           
                    Tipo_alerta.TAl_usuario_creacion = Usuario;
                    Tipo_alerta.TAl_usuario_modificacion = Usuario;
                    Tipo_alerta.Color = Color;
                    Tipo_alerta.Icono = Icono;
                    Tipo_alerta.Categoria_alerta = Categoria_alerta;

                    return Tipo_alerta;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id, Co_id, Ico_id, CAl_id");


                return result;

            }
        }

        public Tipo_alerta FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Tipo_alerta, Usuario, Color, Icono, Categoria_alerta, Tipo_alerta>("app_riego.getall_tipo_alerta", (Tipo_alerta,  Usuario, Color, Icono, Categoria_alerta) =>
                {

                    Tipo_alerta.TAl_usuario_creacion = Usuario;
                    Tipo_alerta.TAl_usuario_modificacion = Usuario;
                    Tipo_alerta.Color = Color;
                    Tipo_alerta.Icono = Icono;
                    Tipo_alerta.Categoria_alerta = Categoria_alerta;

                    return Tipo_alerta;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id, Co_id, Ico_id, CAl_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_alerta WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Tipo_alerta> Getcmb_tipobycategoria_alerta(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_alerta>("app_riego.getcmb_tipobycategoria_alerta", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



    }
}
