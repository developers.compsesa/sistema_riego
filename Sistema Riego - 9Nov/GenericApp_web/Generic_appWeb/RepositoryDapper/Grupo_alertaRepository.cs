﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_alertaRepository
    {
        private string connectionString;

        public Grupo_alertaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_alerta item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GAl_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GAl_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GAl_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GAl_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GAl_descripcion_corta);
                parameters.Add("@_abreviacion", item.GAl_abreviacion);
                parameters.Add("@_observacion", item.GAl_observacion);
               
                parameters.Add("@_fecha_creacion", item.GAl_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GAl_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GAl_secuencia);

                dbConnection.Execute("app_riego.create_grupo_alerta", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_alerta item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.GAl_id);
                parameters.Add("@_codigo_empresa", item.GAl_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GAl_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GAl_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GAl_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GAl_descripcion_corta);
                parameters.Add("@_abreviacion", item.GAl_abreviacion);
                parameters.Add("@_observacion", item.GAl_observacion);

                parameters.Add("@_fecha_creacion", item.GAl_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GAl_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GAl_secuencia);

                dbConnection.Execute("app_riego.create_grupo_alerta", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_alerta> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_alerta, Usuario, Grupo_alerta>("app_riego.getall_grupo_alerta", (Grupo_alerta, Usuario) =>
                {
                    Grupo_alerta.GAl_usuario_creacion = Usuario;
                    Grupo_alerta.GAl_usuario_modificacion = Usuario;

                    return Grupo_alerta;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Grupo_alerta FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Grupo_alerta, Usuario, Grupo_alerta>("app_riego.getall_grupo_alerta", (Grupo_alerta, Usuario) =>
                {
                    Grupo_alerta.GAl_usuario_creacion = Usuario;
                    Grupo_alerta.GAl_usuario_modificacion = Usuario;

                    return Grupo_alerta;
                }, parameters, commandType: CommandType.StoredProcedure,
                    splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_alerta WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
