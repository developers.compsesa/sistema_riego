﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_valorRepository 
    {
        private string connectionString;

        public Tipo_valorRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_valor item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TVal_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TVal_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TVal_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TVal_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TVal_descripcion_corta);
                parameters.Add("@_abreviacion", item.TVal_abreviacion);
                parameters.Add("@_observacion", item.TVal_observacion);
                parameters.Add("@_fecha_creacion", item.TVal_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TVal_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TVal_secuencia);
                parameters.Add("@_categoria_valor_id",item.Categoria_valor.CVal_id);

                dbConnection.Execute("app_riego.create_tipo_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }



       


        public void Update(Tipo_valor item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TVal_id);
                parameters.Add("@_codigo_empresa", item.TVal_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TVal_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TVal_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TVal_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TVal_descripcion_corta);
                parameters.Add("@_abreviacion", item.TVal_abreviacion);
                parameters.Add("@_observacion", item.TVal_observacion);
                parameters.Add("@_fecha_creacion", item.TVal_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TVal_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TVal_secuencia);
                parameters.Add("@_categoria_valor_id", item.Categoria_valor.CVal_id);

                dbConnection.Execute("app_riego.create_tipo_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        public IEnumerable<Tipo_valor> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
               
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_valor, Categoria_valor, Usuario, Tipo_valor>("app_riego.getall_tipo_valor", (Tipo_valor,Categoria_valor, Usuario) =>
                {
                    Tipo_valor.Categoria_valor = Categoria_valor;
                    Tipo_valor.TVal_usuario_creacion = Usuario;
                    Tipo_valor.TVal_usuario_modificacion = Usuario;

                    return Tipo_valor;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CVal_id, Users_id");


                return result;

            }
        }


        public Tipo_valor FindByID(int id)
        {

            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_valor, Categoria_valor, Usuario, Tipo_valor>("app_riego.getall_tipo_valor", (Tipo_valor, Categoria_valor, Usuario) =>
                {
                    Tipo_valor.Categoria_valor = Categoria_valor;
                    Tipo_valor.TVal_usuario_creacion = Usuario;
                    Tipo_valor.TVal_usuario_modificacion = Usuario;

                    return Tipo_valor;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CVal_id, Users_id");


                return result.FirstOrDefault();

            }
        }
      


        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_valor WHERE Id=@Id", new { Id = id });
            }
        }



        public IEnumerable<Tipo_valor> GetallTipo_valorByCategoria(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_valor>("app_riego.getalltiposvalorbycategorias", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public IEnumerable<Tipo_valor> GetallTiposvalor(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_valor>("app_riego.getalltiposvalor", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



        public IEnumerable<Tipo_valor> FindAllTipo_valor()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();


                var result = dbConnection.Query<Tipo_valor>("app_riego.getalltipos_valor2", commandType: CommandType.StoredProcedure);
                return result;

            }
        }


    }
}
