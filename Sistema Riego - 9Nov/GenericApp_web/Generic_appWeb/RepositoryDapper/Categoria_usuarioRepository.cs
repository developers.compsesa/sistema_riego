﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_usuarioRepository
    {
        private string connectionString;

        public Categoria_usuarioRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_usuario item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CUs_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CUs_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CUs_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CUs_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CUs_descripcion_corta);
                parameters.Add("@_abreviacion", item.CUs_abreviacion);
                parameters.Add("@_observacion", item.CUs_observacion);               
                parameters.Add("@_fecha_creacion", item.CUs_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CUs_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CUs_secuencia);

                dbConnection.Execute("app_riego.create_categoria_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_usuario item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.CUs_id);
                parameters.Add("@_codigo_empresa", item.CUs_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CUs_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CUs_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CUs_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CUs_descripcion_corta);
                parameters.Add("@_abreviacion", item.CUs_abreviacion);
                parameters.Add("@_observacion", item.CUs_observacion);
                parameters.Add("@_fecha_creacion", item.CUs_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CUs_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CUs_secuencia);

                dbConnection.Execute("app_riego.create_categoria_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_usuario> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_usuario, Usuario, Categoria_usuario>("app_riego.getall_categoria_valor", (Categoria_usuario, Usuario) =>
                {

                    Categoria_usuario.CUs_usuario_creacion = Usuario;
                    Categoria_usuario.CUs_usuario_modificacion = Usuario;

                    return Categoria_usuario;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id");


                return result;

            }
        }

        public Categoria_usuario FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Categoria_usuario, Usuario, Categoria_usuario>("app_riego.getall_categoria_valor", (Categoria_usuario, Usuario) =>
                {                   
                    Categoria_usuario.CUs_usuario_creacion = Usuario;
                    Categoria_usuario.CUs_usuario_modificacion = Usuario;

                    return Categoria_usuario;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Categoria_usuario WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Categoria_usuario> GetallCategoria_usuarioBygrupo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_usuario>("app_riego.getallcategoriasvalorbygrupo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



    }
}
