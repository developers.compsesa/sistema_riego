﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_alertaRepository
    {
        private string connectionString;

        public Categoria_alertaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_alerta item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CAl_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CAl_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CAl_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CAl_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CAl_descripcion_corta);
                parameters.Add("@_abreviacion", item.CAl_abreviacion);
                parameters.Add("@_observacion", item.CAl_observacion);
               
                parameters.Add("@_fecha_creacion", item.CAl_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CAl_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CAl_secuencia);
                parameters.Add("@_grupo_alerta_id", item.Grupo_alerta.GAl_id);

                dbConnection.Execute("app_riego.create_categoria_alerta", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_alerta item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", item.CAl_id);
                parameters.Add("@_codigo_empresa", item.CAl_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CAl_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CAl_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CAl_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CAl_descripcion_corta);
                parameters.Add("@_abreviacion", item.CAl_abreviacion);
                parameters.Add("@_observacion", item.CAl_observacion);
                parameters.Add("@_fecha_creacion", item.CAl_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CAl_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CAl_secuencia);
                parameters.Add("@_grupo_alerta_id", item.Grupo_alerta.GAl_id);
                dbConnection.Execute("app_riego.create_categoria_alerta", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_alerta> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_alerta, Grupo_alerta, Usuario, Categoria_alerta>("app_riego.getall_categoria_alerta", (Categoria_alerta, Grupo_alerta, Usuario) =>
                {
                    Categoria_alerta.Grupo_alerta = Grupo_alerta;
                    Categoria_alerta.CAl_usuario_creacion = Usuario;
                    Categoria_alerta.CAl_usuario_modificacion = Usuario;
                    return Categoria_alerta;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id,Gal_id ");
                return result;
            }
        }




        public Categoria_alerta FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_alerta, Grupo_alerta, Usuario, Categoria_alerta>("app_riego.getall_categoria_alerta", (Categoria_alerta, Grupo_alerta, Usuario) =>
                {
                    Categoria_alerta.Grupo_alerta = Grupo_alerta;
                    Categoria_alerta.CAl_usuario_creacion = Usuario;
                    Categoria_alerta.CAl_usuario_modificacion = Usuario;

                    return Categoria_alerta;
                }, parameters, commandType: CommandType.StoredProcedure,
                   splitOn: "Users_id,Gal_id ");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("UPDATE app_riego.categoria_alerta SET estado = 'I' WHERE Id=@Id", new { Id = id });
            }
        }




        public IEnumerable<Categoria_alerta> Getcmb_categoriabygrupo_alerta(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_alerta>("app_riego.getcmb_categoriabygrupo_alerta", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }
    }
}
