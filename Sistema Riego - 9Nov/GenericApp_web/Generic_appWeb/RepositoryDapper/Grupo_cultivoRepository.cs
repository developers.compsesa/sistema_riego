﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_cultivoRepository
    {
        private string connectionString;

        public Grupo_cultivoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_cultivo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GCul_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GCul_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GCul_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GCul_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GCul_descripcion_corta);
                parameters.Add("@_abreviacion", item.GCul_abreviacion);
                parameters.Add("@_observacion", item.GCul_observacion);               
                parameters.Add("@_fecha_creacion", item.GCul_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GCul_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GCul_secuencia);
                dbConnection.Execute("app_riego.create_grupo_cultivo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_cultivo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.GCul_id);
                parameters.Add("@_codigo_empresa", item.GCul_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GCul_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GCul_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GCul_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GCul_descripcion_corta);
                parameters.Add("@_abreviacion", item.GCul_abreviacion);
                parameters.Add("@_observacion", item.GCul_observacion);
                parameters.Add("@_fecha_creacion", item.GCul_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GCul_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GCul_secuencia);
                dbConnection.Execute("app_riego.create_grupo_cultivo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_cultivo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_cultivo, Usuario, Grupo_cultivo>("app_riego.getall_grupo_cultivo", (Grupo_cultivo, Usuario) =>
                {
                    Grupo_cultivo.GCul_usuario_creacion = Usuario;
                    Grupo_cultivo.GCul_usuario_modificacion = Usuario;

                    return Grupo_cultivo;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Grupo_cultivo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Grupo_cultivo, Usuario, Grupo_cultivo>("app_riego.getall_grupo_cultivo", (Grupo_cultivo, Usuario) =>
                {
                    Grupo_cultivo.GCul_usuario_creacion = Usuario;
                    Grupo_cultivo.GCul_usuario_modificacion = Usuario;

                    return Grupo_cultivo;

                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_cultivo WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
