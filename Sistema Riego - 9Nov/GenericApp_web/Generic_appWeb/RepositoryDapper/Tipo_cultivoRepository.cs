﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_cultivoRepository
    {
        private string connectionString;

        public Tipo_cultivoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_cultivo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TCul_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TCul_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TCul_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TCul_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TCul_descripcion_corta);
                parameters.Add("@_abreviacion", item.TCul_abreviacion);
                parameters.Add("@_observacion", item.TCul_observacion);               
                parameters.Add("@_fecha_creacion", item.TCul_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TCul_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TCul_secuencia);
                parameters.Add("@_categoria_cultivo_id", item.Categoria_cultivo.CCul_id);

                dbConnection.Execute("app_riego.create_tipo_cultivo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Tipo_cultivo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TCul_id);
                parameters.Add("@_codigo_empresa", item.TCul_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TCul_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TCul_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TCul_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TCul_descripcion_corta);
                parameters.Add("@_abreviacion", item.TCul_abreviacion);
                parameters.Add("@_observacion", item.TCul_observacion);
                parameters.Add("@_fecha_creacion", item.TCul_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TCul_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TCul_secuencia);
                parameters.Add("@_categoria_cultivo_id", item.Categoria_cultivo.CCul_id);

                dbConnection.Execute("app_riego.create_tipo_cultivo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Tipo_cultivo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_cultivo, Categoria_cultivo, Usuario, Tipo_cultivo>("app_riego.getall_tipo_cultivo", (Tipo_cultivo, Categoria_cultivo, Usuario) =>
                {
                    Tipo_cultivo.Categoria_cultivo = Categoria_cultivo;
                    Tipo_cultivo.TCul_usuario_creacion = Usuario;
                    Tipo_cultivo.TCul_usuario_modificacion = Usuario;

                    return Tipo_cultivo;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id");


                return result;

            }
        }

        public Tipo_cultivo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Tipo_cultivo, Categoria_cultivo, Usuario, Tipo_cultivo>("app_riego.getall_tipo_cultivo", (Tipo_cultivo, Categoria_cultivo, Usuario) =>
                {
                    Tipo_cultivo.Categoria_cultivo = Categoria_cultivo;
                    Tipo_cultivo.TCul_usuario_creacion = Usuario;
                    Tipo_cultivo.TCul_usuario_modificacion = Usuario;

                    return Tipo_cultivo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_cultivo WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Tipo_cultivo> Getcmb_tipobycategoria_cultivo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_cultivo>("app_riego.getcmb_tipobycategoria_cultivo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



    }
}
