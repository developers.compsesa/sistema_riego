﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class ParametroRepository
    {
        private string connectionString;

        public ParametroRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Parametro item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Par_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Par_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Par_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Par_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Par_descripcion_corta);
                parameters.Add("@_abreviacion", item.Par_abreviacion);
                parameters.Add("@_observacion", item.Par_observacion);
                parameters.Add("@_fecha_creacion", item.Par_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Par_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Par_secuencia);
                parameters.Add("@_descripcion_parametro_id", item.Descripcion_parametro.DPar_id);
                parameters.Add("@_descripcion_unidad_id", item.Descripcion_unidad.Un_id);
                parameters.Add("@_descripcion_valor_id", item.Descripcion_valor.Val_id);
                parameters.Add("@_entidad_id", item.Entidad.En_id);


                dbConnection.Execute("app_riego.create_parametro", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Parametro item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Par_id);
                parameters.Add("@_codigo_empresa", item.Par_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Par_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Par_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Par_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Par_descripcion_corta);
                parameters.Add("@_abreviacion", item.Par_abreviacion);
                parameters.Add("@_observacion", item.Par_observacion);
                parameters.Add("@_fecha_creacion", item.Par_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Par_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Par_secuencia);
                parameters.Add("@_descripcion_parametro_id", item.Descripcion_parametro.DPar_id);
                parameters.Add("@_descripcion_unidad_id", item.Descripcion_unidad.Un_id);
                parameters.Add("@_descripcion_valor_id", item.Descripcion_valor.Val_id);
                parameters.Add("@_entidad_id", item.Entidad.En_id);


                dbConnection.Execute("app_riego.create_parametro", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Parametro> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);


                var result = dbConnection.Query("app_riego.getall_inventario_parametros", new[]
                {
                    typeof(Parametro),
                    typeof(Descripcion_parametro),
                    typeof(Descripcion_unidad),
                    typeof(Descripcion_valor),
                    typeof(Usuario)

                }
                , obj =>
                {

                    Parametro Parametro = obj[0] as Parametro;
                    Descripcion_parametro Descripcion_parametro = obj[1] as Descripcion_parametro;
                    Descripcion_unidad Descripcion_unidad = obj[2] as Descripcion_unidad;
                    Descripcion_valor Descripcion_valor = obj[3] as Descripcion_valor;
                    Usuario Usuario = obj[4] as Usuario;


                    Parametro.Descripcion_parametro = Descripcion_parametro;
                    Parametro.Descripcion_unidad = Descripcion_unidad;
                    Parametro.Descripcion_valor = Descripcion_valor;
                    Parametro.Par_usuario_creacion = Usuario;
                    Parametro.Par_usuario_modificacion = Usuario;


                    return Parametro;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Par_id, DPar_id, Un_id , Val_id , E_id");


                return result;

            }
        }

        public Parametro FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);
                var result = dbConnection.Query("app_riego.getall_inventario_parametros", new[]
                               {
                    typeof(Parametro),
                    typeof(Descripcion_parametro),
                    typeof(Descripcion_unidad),
                    typeof(Descripcion_valor),
                    typeof(Usuario)

                }
                               , obj =>
                               {

                                   Parametro Parametro = obj[0] as Parametro;
                                   Descripcion_parametro Descripcion_parametro = obj[1] as Descripcion_parametro;
                                   Descripcion_unidad Descripcion_unidad = obj[2] as Descripcion_unidad;
                                   Descripcion_valor Descripcion_valor = obj[3] as Descripcion_valor;
                                   Usuario Usuario = obj[4] as Usuario;


                                   Parametro.Descripcion_parametro = Descripcion_parametro;
                                   Parametro.Descripcion_unidad = Descripcion_unidad;
                                   Parametro.Descripcion_valor = Descripcion_valor;
                                   Parametro.Par_usuario_creacion = Usuario;
                                   Parametro.Par_usuario_modificacion = Usuario;


                                   return Parametro;
                               }, parameters, commandType: CommandType.StoredProcedure,
                                splitOn: "Par_id, DPar_id, Un_id , Val_id , E_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.inventario_parametros WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Parametro> FindAllFiltered(int? grupo_parametros_id, int? categoria_parametros_id, int? tipo_parametros_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (grupo_parametros_id != 0)
                {
                    if (categoria_parametros_id != 0)
                    {
                        if (tipo_parametros_id != 0)
                        {
                            flag = 3;
                            //if (subtipo_activo_id != 0)
                            //{
                            //    flag = 4;
                            //}
                            //else
                            //{
                            //    flag = 3;
                            //}
                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);

                parameters.Add("@_grupo_parametros_id", grupo_parametros_id);
                parameters.Add("@_categoria_parametros_id", categoria_parametros_id);
                parameters.Add("@_tipo_parametros_id", tipo_parametros_id);
                //parameters.Add("@_subtipo_activo_id", subtipo_activo_id);

                var result = dbConnection.Query("app_riego.getall_inventario_parametros", new[]
                {
                    typeof(Parametro),
                    typeof(Descripcion_parametro),
                    typeof(Descripcion_unidad),
                    typeof(Descripcion_valor),
                    typeof(Usuario)

                }
                , obj =>
                {

                    Parametro Parametro = obj[0] as Parametro;
                    Descripcion_parametro Descripcion_parametro = obj[1] as Descripcion_parametro;
                    Descripcion_unidad Descripcion_unidad = obj[2] as Descripcion_unidad;
                    Descripcion_valor Descripcion_valor = obj[3] as Descripcion_valor;
                    Usuario Usuario = obj[4] as Usuario;


                    Parametro.Descripcion_parametro = Descripcion_parametro;
                    Parametro.Descripcion_unidad = Descripcion_unidad;
                    Parametro.Descripcion_valor = Descripcion_valor;
                    Parametro.Par_usuario_creacion = Usuario;
                    Parametro.Par_usuario_modificacion = Usuario;


                    return Parametro;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Par_id, DPar_id, Un_id , Val_id , E_id");


                return result;
            }
        }


        public IEnumerable<Parametro> GetalldatostecnicosByTipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Parametro>("app_riego.getalltiposbycategoriaparametros", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public Parametro GetDatosTecnicosByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query("app_riego.getall_inventario_parametros", new[]
               {
                    typeof(Parametro),
                    typeof(Descripcion_parametro),
                    typeof(Descripcion_unidad),
                    typeof(Descripcion_valor),
                    typeof(Usuario)

                }
               , obj =>
               {

                   Parametro Parametro = obj[0] as Parametro;
                   Descripcion_parametro Descripcion_parametro = obj[1] as Descripcion_parametro;
                   Descripcion_unidad Descripcion_unidad = obj[2] as Descripcion_unidad;
                   Descripcion_valor Descripcion_valor = obj[3] as Descripcion_valor;
                   Usuario Usuario = obj[4] as Usuario;


                   Parametro.Descripcion_parametro = Descripcion_parametro;
                   Parametro.Descripcion_unidad = Descripcion_unidad;
                   Parametro.Descripcion_valor = Descripcion_valor;
                   Parametro.Par_usuario_creacion = Usuario;
                   Parametro.Par_usuario_modificacion = Usuario;


                   return Parametro;
               }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "Par_id, DPar_id, Un_id , Val_id , E_id");


                return result.FirstOrDefault();

            }

        }









    }
}
