﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_valvulaRepository
    {
        private string connectionString;

        public Grupo_valvulaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_valvula item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Gv_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Gv_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Gv_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Gv_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Gv_descripcion_corta);
                parameters.Add("@_abreviacion", item.Gv_abreviacion);
                parameters.Add("@_observacion", item.Gv_observacion);               
                parameters.Add("@_fecha_creacion", item.Gv_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Gv_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Gv_secuencia); 

                dbConnection.Execute("app_riego.create_grupo_valvula", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_valvula item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Gv_id);

                parameters.Add("@_codigo_empresa", item.Gv_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Gv_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Gv_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Gv_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Gv_descripcion_corta);
                parameters.Add("@_abreviacion", item.Gv_abreviacion);
                parameters.Add("@_observacion", item.Gv_observacion);
                parameters.Add("@_fecha_creacion", item.Gv_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Gv_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Gv_secuencia);

                dbConnection.Execute("app_riego.create_grupo_valvula", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_valvula> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_valvula, Usuario, Grupo_valvula>("app_riego.getallGrupo_valvula", (Grupo_valvula, Usuario) =>
                {
                    Grupo_valvula.Gv_usuario_creacion = Usuario;
                    Grupo_valvula.Gv_usuario_modificacion = Usuario;

                    return Grupo_valvula;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Grupo_valvula FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Grupo_valvula, Usuario, Grupo_valvula>("app_riego.getallGrupo_valvula", (Grupo_valvula, Usuario) =>
                {
                    Grupo_valvula.Gv_usuario_creacion = Usuario;
                    Grupo_valvula.Gv_usuario_modificacion = Usuario;

                    return Grupo_valvula;

                }, parameters, commandType: CommandType.StoredProcedure,
                                splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_valvula WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
