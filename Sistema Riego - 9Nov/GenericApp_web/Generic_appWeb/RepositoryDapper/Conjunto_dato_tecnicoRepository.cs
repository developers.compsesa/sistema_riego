﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Conjunto_dato_tecnicoRepository
    {
        private string connectionString;

        public Conjunto_dato_tecnicoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Conjunto_dato_tecnico item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("@_id", 0);
                    parameters.Add("@_codigo_empresa", item.CDTec_codigo_empresa);
                    parameters.Add("@_codigo_alterno", item.CDTec_codigo_alterno);
                    parameters.Add("@_descripcion_larga", item.CDTec_descripcion_larga);
                    parameters.Add("@_descripcion_med", item.CDTec_descripcion_med);
                    parameters.Add("@_descripcion_corta", item.CDTec_descripcion_corta);
                    parameters.Add("@_abreviacion", item.CDTec_abreviacion);
                    parameters.Add("@_observacion", item.CDTec_observacion);
                    parameters.Add("@_fecha_creacion", item.CDTec_fecha_creacion);
                    parameters.Add("@_fecha_modificacion", item.CDTec_fecha_creacion);
                    parameters.Add("@_usuario_creacion", 1);
                    parameters.Add("@_usuario_modificacion", 1);
                    parameters.Add("@_estado", "A");
                    parameters.Add("@_secuencia", item.CDTec_secuencia);
                    parameters.Add("@_descripcion_conjunto_dato_tecnico_id", item.descripcion_conjunto_dato_tecnico_id);
                    parameters.Add("@_dato_tecnico_id", item.Dato_tecnico.DTec_id);
                    parameters.Add("@_valor", item.valor);

                dbConnection.Execute("app_riego.create_conjunto_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
                
            }
        }
       
                                          
          public void Update(Conjunto_dato_tecnico item)
          {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.CDTec_id);
                parameters.Add("@_codigo_empresa", item.CDTec_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CDTec_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CDTec_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CDTec_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CDTec_descripcion_corta);
                parameters.Add("@_abreviacion", item.CDTec_abreviacion);
                parameters.Add("@_observacion", item.CDTec_observacion);
                parameters.Add("@_fecha_creacion", item.CDTec_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CDTec_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CDTec_secuencia);
                    parameters.Add("@_descripcion_conjunto_dato_tecnico_id", item.descripcion_conjunto_dato_tecnico_id);
                    parameters.Add("@_dato_tecnico_id", item.Dato_tecnico.DTec_id);
                    parameters.Add("@_valor", item.valor);

                dbConnection.Execute("app_riego.create_conjunto_dato_tecnico", parameters, commandType: CommandType.StoredProcedure);
            }
          }

        public IEnumerable<Conjunto_dato_tecnico> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Conjunto_dato_tecnico, Dato_tecnico, Usuario, Conjunto_dato_tecnico>("app_riego.getall_conjunto_dato_tecnico", (Conjunto_dato_tecnico, Dato_tecnico , Usuario) =>
                {
                    Conjunto_dato_tecnico.Dato_tecnico = Dato_tecnico;
                    Conjunto_dato_tecnico.CDTec_usuario_creacion = Usuario;
                    Conjunto_dato_tecnico.CDTec_usuario_modificacion = Usuario;

                    return Conjunto_dato_tecnico;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "DTec_id , Users_id");


                return result;

            }
        }

        public Conjunto_dato_tecnico FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Conjunto_dato_tecnico, Dato_tecnico, Usuario, Conjunto_dato_tecnico>("app_riego.getall_conjunto_dato_tecnico", (Conjunto_dato_tecnico, Dato_tecnico, Usuario) =>
                {
                    Conjunto_dato_tecnico.Dato_tecnico = Dato_tecnico;
                    Conjunto_dato_tecnico.CDTec_usuario_creacion = Usuario;
                    Conjunto_dato_tecnico.CDTec_usuario_modificacion = Usuario;

                    return Conjunto_dato_tecnico;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "DTec_id , Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Conjunto_datos_tecnicos WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
