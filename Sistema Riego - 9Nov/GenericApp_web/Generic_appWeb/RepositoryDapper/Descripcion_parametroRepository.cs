﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Descripcion_parametroRepository
    {
        private string connectionString;

        public Descripcion_parametroRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {

                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Descripcion_parametro item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.DPar_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.DPar_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.DPar_descripcion_larga);
                parameters.Add("@_descripcion_med", item.DPar_descripcion_med);
                parameters.Add("@_descripcion_corta", item.DPar_descripcion_corta);
                parameters.Add("@_abreviacion", item.DPar_abreviacion);
                parameters.Add("@_observacion", item.DPar_observacion);
                parameters.Add("@_fecha_creacion", item.DPar_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.DPar_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.DPar_secuencia);
                parameters.Add("@_grupo_parametro_id", item.Grupo_parametro.GPar_id);
                parameters.Add("@_categoria_parametro_id", item.Categoria_parametro.CPar_id);
                parameters.Add("@_tipo_parametro_id", item.Tipo_parametro.TPar_id);    
                
                dbConnection.Execute("app_riego.create_descripcion_parametro", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Descripcion_parametro item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_codigo_empresa", item.DPar_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.DPar_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.DPar_descripcion_larga);
                parameters.Add("@_descripcion_med", item.DPar_descripcion_med);
                parameters.Add("@_descripcion_corta", item.DPar_descripcion_corta);
                parameters.Add("@_abreviacion", item.DPar_abreviacion);
                parameters.Add("@_observacion", item.DPar_observacion);
                parameters.Add("@_fecha_creacion", item.DPar_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.DPar_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.DPar_secuencia);
                parameters.Add("@_grupo_parametro_id", item.Grupo_parametro.GPar_id);
                parameters.Add("@_categoria_parametro_id", item.Categoria_parametro.CPar_id);
                parameters.Add("@_tipo_parametro_id", item.Tipo_parametro.TPar_id);

                dbConnection.Execute("app_riego.create_descripcion_parametro", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Descripcion_parametro> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Descripcion_parametro, Grupo_parametro, Categoria_parametro, Tipo_parametro, Usuario, Descripcion_parametro>("app_riego.getall_descripcion_parametro", (Descripcion_parametro, Grupo_parametro, Categoria_parametro, Tipo_parametro, Usuario) =>
                {
                    Descripcion_parametro.Grupo_parametro = Grupo_parametro;
                    Descripcion_parametro.Categoria_parametro = Categoria_parametro;
                    Descripcion_parametro.Tipo_parametro = Tipo_parametro;
                    Descripcion_parametro.DPar_usuario_creacion = Usuario;
                    Descripcion_parametro.DPar_usuario_modificacion = Usuario;

                    return Descripcion_parametro;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "DPar_id, GPar_id,CPar_id,TPar_id, E_id");


                return result;

            }
        }

        public Descripcion_parametro FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Descripcion_parametro, Grupo_parametro, Categoria_parametro, Tipo_parametro, Usuario, Descripcion_parametro>("app_riego.getall_descripcion_parametro", (Descripcion_parametro, Grupo_parametro, Categoria_parametro, Tipo_parametro, Usuario) =>
                {
                    Descripcion_parametro.Grupo_parametro = Grupo_parametro;
                    Descripcion_parametro.Categoria_parametro = Categoria_parametro;
                    Descripcion_parametro.Tipo_parametro = Tipo_parametro;
                    Descripcion_parametro.DPar_usuario_creacion = Usuario;
                    Descripcion_parametro.DPar_usuario_modificacion = Usuario;

                    return Descripcion_parametro;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "DPar_id, GPar_id,CPar_id,TPar_id, E_id");



                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.inventario_descripciones_parametros WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Descripcion_parametro> FindAllFiltered(int? grupo_parametro_id, int? categoria_parametro_id, int? tipo_parametro_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (grupo_parametro_id != 0)
                {
                    if (categoria_parametro_id != 0)
                    {
                        if (tipo_parametro_id != 0)
                        {
                            flag = 3;
                            //if (subtipo_activo_id != 0)
                            //{
                            //    flag = 4;
                            //}
                            //else
                            //{
                            //    flag = 3;
                            //}
                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);
                parameters.Add("@_grupo_parametro_id", grupo_parametro_id);
                parameters.Add("@_categoria_parametro_id", categoria_parametro_id);
                parameters.Add("@_tipo_parametro_id", tipo_parametro_id);
                //parameters.Add("@_subtipo_activo_id", subtipo_activo_id);

                var result = dbConnection.Query<Descripcion_parametro, Grupo_parametro, Categoria_parametro, Tipo_parametro, Usuario, Descripcion_parametro>("app_riego.getall_descripcion_parametro", (Descripcion_parametro, Grupo_parametro, Categoria_parametro, Tipo_parametro, Usuario) =>
                {
                    Descripcion_parametro.Grupo_parametro = Grupo_parametro;
                    Descripcion_parametro.Categoria_parametro = Categoria_parametro;
                    Descripcion_parametro.Tipo_parametro = Tipo_parametro;
                    Descripcion_parametro.DPar_usuario_creacion = Usuario;
                    Descripcion_parametro.DPar_usuario_modificacion = Usuario;

                    return Descripcion_parametro;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "DPar_id, GPar_id,CPar_id,TPar_id, E_id");



                return result;
            }
        }


        public IEnumerable<Descripcion_parametro> GetalldatostecnicosByTipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Descripcion_parametro>("app_riego.getall_tipos_bycategoria_parametros", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public Descripcion_parametro GetDatosTecnicosByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
           
                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Descripcion_parametro, Grupo_parametro, Categoria_parametro, Tipo_parametro, Usuario, Descripcion_parametro>("app_riego.getall_descripcion_parametro", (Descripcion_parametro, Grupo_parametro, Categoria_parametro, Tipo_parametro, Usuario) =>
                {
                    Descripcion_parametro.Grupo_parametro = Grupo_parametro;
                    Descripcion_parametro.Categoria_parametro = Categoria_parametro;
                    Descripcion_parametro.Tipo_parametro = Tipo_parametro;
                    Descripcion_parametro.DPar_usuario_creacion = Usuario;
                    Descripcion_parametro.DPar_usuario_modificacion = Usuario;

                    return Descripcion_parametro;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "DPar_id, GPar_id,CPar_id,TPar_id, E_id");



                return result.FirstOrDefault();

            }

        }









    }
}
