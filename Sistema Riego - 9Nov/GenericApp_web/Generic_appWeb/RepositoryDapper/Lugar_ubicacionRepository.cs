﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Lugar_ubicacionRepository : IRepository<Lugar_ubicacion>
    {
        private string connectionString;

        public Lugar_ubicacionRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Lugar_ubicacion item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Luu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Luu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Luu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Luu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Luu_descripcion_corta);
                parameters.Add("@_abreviacion", item.Luu_abreviacion);
                parameters.Add("@_observacion", item.Luu_observacion);
                parameters.Add("@_fecha_creacion", item.Luu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Luu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Luu_secuencia);
                parameters.Add("@_lugar_id", item.Lugar.Lu_id);
                parameters.Add("@_sitio_id", item.Sitio.Si_id);
                parameters.Add("@_area_id", item.Area.Are_id);
                parameters.Add("@_punto_id", item.Punto.Pu_id);

                dbConnection.Execute("app_riego.create_lugar_ubicacion", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Lugar_ubicacion item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Luu_id);
                parameters.Add("@_codigo_empresa", item.Luu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Luu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Luu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Luu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Luu_descripcion_corta);
                parameters.Add("@_abreviacion", item.Luu_abreviacion);
                parameters.Add("@_observacion", item.Luu_observacion);
                parameters.Add("@_fecha_creacion", item.Luu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Luu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Luu_secuencia);
                parameters.Add("@_lugar_id", item.Lugar.Lu_id);
                parameters.Add("@_sitio_id", item.Sitio.Si_id);
                parameters.Add("@_area_id", item.Area.Are_id);
                parameters.Add("@_punto_id", item.Punto.Pu_id);

                dbConnection.Execute("app_riego.create_lugar_ubicacion", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        //public IEnumerable<PuntosViewModel> FindAllToExport()
        //{
        //    using (IDbConnection dbConnection = Connection)
        //    {

        //        dbConnection.Open();
        //        DynamicParameters parameters = new DynamicParameters();
        //        parameters.Add("@_opcion", 1);
        //        parameters.Add("@_id", 0);

        //        var result = dbConnection.Query<PuntosViewModel>("app_riego.getallPuntos", parameters, commandType: CommandType.StoredProcedure);
        //        return result;

        //    }
        //}


        public IEnumerable<Lugar_ubicacion> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query("app_riego.get_alllugar_ubicacion", new[]
                {
                    typeof(Lugar_ubicacion),
                    typeof(Lugar),
                    typeof(Sitio),
                    typeof(Area),
                    typeof(Punto),
                    typeof(Usuario)

                }
                , obj =>
                {
                    Lugar_ubicacion Lugar_ubicacion = obj[0] as Lugar_ubicacion;

                    Lugar Lugar = obj[1] as Lugar;
                    Sitio Sitio = obj[2] as Sitio;
                    Area Area = obj[3] as Area;   
                    Punto Punto = obj[4] as Punto;
                    Usuario Usuario = obj[5] as Usuario;

                                      

                    Lugar_ubicacion.Lugar = Lugar;
                    Lugar_ubicacion.Sitio = Sitio;
                    Lugar_ubicacion.Area = Area;
                    Lugar_ubicacion.Punto = Punto;
                    Lugar_ubicacion.Luu_usuario_creacion = Usuario;
                    Lugar_ubicacion.Luu_usuario_modificacion = Usuario;

                    return Lugar_ubicacion;
                }, parameters, commandType: CommandType.StoredProcedure,
                             splitOn: "Luu_id, Lu_id, Are_id, Si_id, Pu_id, Users_id");

                return result;
            }
        }


        public Lugar_ubicacion FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query("app_riego.get_alllugar_ubicacion", new[]
                {
                    typeof(Lugar_ubicacion),
                    typeof(Lugar),
                    typeof(Sitio),
                    typeof(Area),
                    typeof(Punto),
                    typeof(Usuario)

                }
                , obj =>
                {
                    Lugar_ubicacion Lugar_ubicacion = obj[0] as Lugar_ubicacion;

                    Lugar Lugar = obj[1] as Lugar;
                    Sitio Sitio = obj[2] as Sitio;
                    Area Area = obj[3] as Area;
                    Punto Punto = obj[4] as Punto;
                    Usuario Usuario = obj[5] as Usuario;



                    Lugar_ubicacion.Lugar = Lugar;
                    Lugar_ubicacion.Sitio = Sitio;
                    Lugar_ubicacion.Area = Area;
                    Lugar_ubicacion.Punto = Punto;
                    Lugar_ubicacion.Luu_usuario_creacion = Usuario;
                    Lugar_ubicacion.Luu_usuario_modificacion = Usuario;

                    return Lugar_ubicacion;
                }, parameters, commandType: CommandType.StoredProcedure,
                             splitOn: "Luu_id, Lu_id, Are_id, Si_id, Pu_id, Users_id");

                return result.FirstOrDefault();
            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.lugar_ubicacion WHERE Id=@Id", new { Id = id });
            }
        }






        //public IEnumerable<Puntos> GetallPuntosBySitios(int id)
        //{
        //    using (IDbConnection dbConnection = Connection)
        //    {

        //        dbConnection.Open();
        //        DynamicParameters parameters = new DynamicParameters();
        //        parameters.Add("@_id", id);

        //        var result = dbConnection.Query<Puntos>("app_riego.GetallPuntosBySitios", parameters, commandType: CommandType.StoredProcedure);
        //        return result;

        //    }
        //}


        //public IEnumerable<Puntos> GetallPuntosBytipos_puntos(int id)
        //{
        //    using (IDbConnection dbConnection = Connection)
        //    {
        //        dbConnection.Open();
        //        DynamicParameters parameters = new DynamicParameters();
        //        parameters.Add("@_id", id);

        //        var result = dbConnection.Query<Puntos>("app_riego.GetallPuntosBytipos_puntos", parameters, commandType: CommandType.StoredProcedure);
        //        return result;
        //    }
        //}


        public Lugar_ubicacion Getlugar_ubicacionByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Lugar_ubicacion>("app_riego.getalllugar_ubicacion_codigo_empresa", parameters, commandType: CommandType.StoredProcedure);


                return result.FirstOrDefault();

                //return dbConnection.Query<Lugares>("Select descripcion_corta from app_riego.lugares where codigo_empresa = @codigo_empresa ", new { codigo_empresa }).SingleOrDefault();

            }

            }
        }
}
