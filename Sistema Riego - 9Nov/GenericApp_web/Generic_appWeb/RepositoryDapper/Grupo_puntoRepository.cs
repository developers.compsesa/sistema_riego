﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_puntoRepository 
    {
        private string connectionString;

        public Grupo_puntoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_punto item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GPu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GPu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GPu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GPu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GPu_descripcion_corta);
                parameters.Add("@_abreviacion", item.GPu_abreviacion);
                parameters.Add("@_observacion", item.GPu_observacion);
                parameters.Add("@_fecha_creacion", item.GPu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GPu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GPu_secuencia);
                parameters.Add("@_entidades_id", item.Hacienda.Hac_id);
                dbConnection.Execute("app_riego.create_grupo_punto", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_punto item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.GPu_id);
                parameters.Add("@_codigo_empresa", item.GPu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GPu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GPu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GPu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GPu_descripcion_corta);
                parameters.Add("@_abreviacion", item.GPu_abreviacion);
                parameters.Add("@_observacion", item.GPu_observacion);
                parameters.Add("@_fecha_creacion", item.GPu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GPu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GPu_secuencia);
                parameters.Add("@_entidades_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_grupo_punto", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_punto> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_punto, Usuario, Hacienda, Grupo_punto>("app_riego.getall_grupo_punto", (Grupo_punto, Usuario , Hacienda) =>
                {
                  
                    Grupo_punto.GPu_usuario_creacion = Usuario;
                    Grupo_punto.GPu_usuario_modificacion = Usuario;
                    Grupo_punto.Hacienda = Hacienda;

                    return Grupo_punto;

                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "GPu_id,Hac_id, Users_id");
                return result;
            }
        }

      


        public Grupo_punto FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Grupo_punto, Usuario, Hacienda, Grupo_punto>("app_riego.getall_grupo_punto", (Grupo_punto, Usuario, Hacienda) =>
                {

                    Grupo_punto.GPu_usuario_creacion = Usuario;
                    Grupo_punto.GPu_usuario_modificacion = Usuario;
                    Grupo_punto.Hacienda = Hacienda;

                    return Grupo_punto;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GPu_id,Hac_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_punto WHERE Id=@Id", new { Id = id });
            }
        }

       

        

    }
}
