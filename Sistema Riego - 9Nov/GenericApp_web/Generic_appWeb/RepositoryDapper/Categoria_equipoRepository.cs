﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_equipoRepository : IRepository<Categoria_equipo>
    {
        private string connectionString;

        public Categoria_equipoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_equipo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CE_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CE_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CE_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CE_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CE_descripcion_corta);
                parameters.Add("@_abreviacion", item.CE_abreviacion);
                parameters.Add("@_observacion", item.CE_observacion);
                parameters.Add("@_fecha_creacion", item.CE_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CE_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CE_secuencia);
                parameters.Add("@_grupo_id", item.Grupo_equipo.GE_id);
                
                dbConnection.Execute("app_riego.create_categoria_equipo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_equipo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", item.CE_id);
                parameters.Add("@_codigo_empresa", item.CE_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CE_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CE_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CE_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CE_descripcion_corta);
                parameters.Add("@_abreviacion", item.CE_abreviacion);
                parameters.Add("@_observacion", item.CE_observacion);
                parameters.Add("@_fecha_creacion", item.CE_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CE_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CE_secuencia);
                parameters.Add("@_grupo_id", item.Grupo_equipo.GE_id);
                dbConnection.Execute("app_riego.create_categoria_equipo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_equipo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_equipo, Grupo_equipo, Usuario, Categoria_equipo>("app_riego.getallcategoria_equipo", (Categoria_equipo, Grupo_equipo, Usuario) =>
                {
                    Categoria_equipo.CE_usuario_creacion = Usuario;
                    Categoria_equipo.CE_usuario_modificacion = Usuario;
                    Categoria_equipo.Grupo_equipo = Grupo_equipo;
                    return Categoria_equipo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CE_id, GE_id, Users_id");


                return result;

            }
        }


        public Categoria_equipo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_equipo, Grupo_equipo, Usuario, Categoria_equipo>("app_riego.getallcategoria_equipo", (Categoria_equipo, Grupo_equipo, Usuario) =>
                {
                    Categoria_equipo.CE_usuario_creacion = Usuario;
                    Categoria_equipo.CE_usuario_modificacion = Usuario;
                    Categoria_equipo.Grupo_equipo = Grupo_equipo;
                    return Categoria_equipo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CE_id, GE_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Categoria_equipo WHERE Id=@Id", new { Id = id });
            }
        }

       




        public IEnumerable<Categoria_equipo> Getcmb_categoriabygrupo_equipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_equipo>("app_riego.getcmb_categoriabygrupo_equipo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }






    }
}
