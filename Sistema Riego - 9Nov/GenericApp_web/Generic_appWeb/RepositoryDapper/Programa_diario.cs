﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class ProgramaRepository
    {
        private string connectionString;

        public ProgramaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Programa_diario item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.PDi_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.PDi_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.PDi_descripcion_larga);
                parameters.Add("@_descripcion_med", item.PDi_descripcion_med);
                parameters.Add("@_descripcion_corta", item.PDi_descripcion_corta);
                parameters.Add("@_abreviacion", item.PDi_abreviacion);
                parameters.Add("@_observacion", item.PDi_observacion);               
                parameters.Add("@_fecha_creacion", item.PDi_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.PDi_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.PDi_secuencia);
                parameters.Add("@_programa_standard", item.PDi_programa_standard);
                parameters.Add("@_categoria_programa_id", item.Categoria_programa_diario.CPD_id);
                parameters.Add("@_tipo_programa_id", item.Tipo_programa_diario.TPD_id);
                parameters.Add("@_equipo_id", item.Equipo.Eq_id);

                dbConnection.Execute("app_riego.create_programa_diario", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Programa_diario item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.PDi_id);
                parameters.Add("@_codigo_empresa", item.PDi_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.PDi_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.PDi_descripcion_larga);
                parameters.Add("@_descripcion_med", item.PDi_descripcion_med);
                parameters.Add("@_descripcion_corta", item.PDi_descripcion_corta);
                parameters.Add("@_abreviacion", item.PDi_abreviacion);
                parameters.Add("@_observacion", item.PDi_observacion);
                parameters.Add("@_fecha_creacion", item.PDi_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.PDi_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.PDi_secuencia);
                parameters.Add("@_programa_standard", item.PDi_programa_standard);
                parameters.Add("@_categoria_programa_id", item.Categoria_programa_diario.CPD_id);
                parameters.Add("@_tipo_programa_id", item.Tipo_programa_diario.TPD_id);
                parameters.Add("@_equipo_id", item.Equipo.Eq_id);

                dbConnection.Execute("app_riego.create_programa_diario", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Programa_diario> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Programa_diario, Usuario, Categoria_programa_diario, Tipo_programa_diario, Equipo, Programa_diario>("app_riego.getall_programa_diario", (Programa_diario, Usuario, Categoria_programa_diario, Tipo_programa_diario, Equipo) =>
                {
                    Programa_diario.PDi_usuario_creacion = Usuario;
                    Programa_diario.PDi_usuario_modificacion = Usuario;
                    Programa_diario.Categoria_programa_diario = Categoria_programa_diario;
                    Programa_diario.Tipo_programa_diario = Tipo_programa_diario;
                    Programa_diario.Equipo = Equipo;

                    return Programa_diario;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id, CPD_id, TPD_id, E_id");


                return result;

            }
        }

        public Programa_diario FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Programa_diario, Usuario, Categoria_programa_diario, Tipo_programa_diario, Equipo, Programa_diario>("app_riego.getall_programa_diario", (Programa_diario, Usuario, Categoria_programa_diario, Tipo_programa_diario, Equipo) =>
                {
                    Programa_diario.PDi_usuario_creacion = Usuario;
                    Programa_diario.PDi_usuario_modificacion = Usuario;
                    Programa_diario.Categoria_programa_diario = Categoria_programa_diario;
                    Programa_diario.Tipo_programa_diario = Tipo_programa_diario;
                    Programa_diario.Equipo = Equipo;

                    return Programa_diario;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id, CPD_id, TPD_id, E_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Programa_diario WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
