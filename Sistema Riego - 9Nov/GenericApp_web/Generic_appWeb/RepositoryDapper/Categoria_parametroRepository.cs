﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_parametroRepository
    {
        private string connectionString;

        public Categoria_parametroRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_parametro item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CPar_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CPar_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CPar_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CPar_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CPar_descripcion_corta);
                parameters.Add("@_abreviacion", item.CPar_abreviacion);
                parameters.Add("@_observacion", item.CPar_observacion);               
                parameters.Add("@_fecha_creacion", item.CPar_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CPar_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CPar_secuencia);
                parameters.Add("@_grupo_parametros_id", item.Grupo_parametro.GPar_id);

                dbConnection.Execute("app_riego.create_categorias_parametros", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_parametro item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_codigo_empresa", item.CPar_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CPar_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CPar_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CPar_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CPar_descripcion_corta);
                parameters.Add("@_abreviacion", item.CPar_abreviacion);
                parameters.Add("@_observacion", item.CPar_observacion);
                parameters.Add("@_fecha_creacion", item.CPar_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CPar_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CPar_secuencia);

                dbConnection.Execute("app_riego.create_categorias_parametros", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_parametro> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_parametro, Grupo_parametro, Usuario, Categoria_parametro>("app_riego.getallcategorias_parametros", (Categoria_parametro, Grupo_parametro, Usuario) =>
                {
                    Categoria_parametro.Grupo_parametro = Grupo_parametro;
                    Categoria_parametro.CPar_usuario_creacion = Usuario;
                    Categoria_parametro.CPar_usuario_modificacion = Usuario;

                    return Categoria_parametro;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "GPar_id,E_id");


                return result;

            }
        }

        public Categoria_parametro FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Categoria_parametro, Grupo_parametro, Usuario, Categoria_parametro>("app_riego.getallcategorias_parametros", (Categoria_parametro, Grupo_parametro, Usuario) =>
                {
                    Categoria_parametro.Grupo_parametro = Grupo_parametro;
                    Categoria_parametro.CPar_usuario_creacion = Usuario;
                    Categoria_parametro.CPar_usuario_modificacion = Usuario;

                    return Categoria_parametro;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GPar_id,E_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Categoria_parametro WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Categoria_parametro> Getcmb_categoriabygrupo_parametro(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_parametro>("app_riego.getcmb_categoriabygrupo_parametro", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



    }
}
