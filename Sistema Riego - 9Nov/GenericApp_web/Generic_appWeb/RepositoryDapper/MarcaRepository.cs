﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class MarcaRepository : IRepository<Marca>
    {
        private string connectionString;

        public MarcaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Marca item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Ma_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Ma_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Ma_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Ma_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Ma_descripcion_corta);
                parameters.Add("@_abreviacion", item.Ma_abreviacion);
                parameters.Add("@_observacion", item.Ma_observacion);
                parameters.Add("@_fecha_creacion", item.Ma_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Ma_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Ma_secuencia);
                parameters.Add("@_grupo_marca_id", item.Grupo_marca.GMa_id);
                parameters.Add("@_categoria_marca_id", item.Categoria_marca.CMa_id);
                parameters.Add("@_tipo_marca_id", item.Tipo_marca.TMa_id);

                dbConnection.Execute("app_riego.create_marca", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Marca> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);
                var result = dbConnection.Query<Marca, Usuario, Marca>("app_riego.getall_marca", (Marca, Usuario) =>
                {
                    Marca.Ma_usuario_creacion = Usuario;
                    Marca.Ma_usuario_modificacion = Usuario;

                    return Marca;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "MA_id, Users_id");


                return result;

            }
        }

        public Marca FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Marca, Usuario, Marca>("app_riego.getall_marca", (Marca, Usuario) =>
                {
                    Marca.Ma_usuario_creacion = Usuario;
                    Marca.Ma_usuario_modificacion = Usuario;

                    return Marca;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "MA_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.marcas WHERE Id=@Id", new { Id = id });
            }
        }

        public void Update(Marca item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Ma_id);
                parameters.Add("@_codigo_empresa", item.Ma_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Ma_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Ma_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Ma_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Ma_descripcion_corta);
                parameters.Add("@_abreviacion", item.Ma_abreviacion);
                parameters.Add("@_observacion", item.Ma_observacion);
                parameters.Add("@_fecha_creacion", item.Ma_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Ma_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Ma_secuencia);
                parameters.Add("@_grupo_marca_id", item.Grupo_marca.GMa_id);
                parameters.Add("@_categoria_marca_id", item.Categoria_marca.CMa_id);
                parameters.Add("@_tipo_marca_id", item.Tipo_marca.TMa_id);

                dbConnection.Execute("app_riego.create_marca", parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
