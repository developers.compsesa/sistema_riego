﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_equipoRepository 
    {
        private string connectionString;

        public Tipo_equipoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_equipo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TE_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TE_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TE_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TE_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TE_descripcion_corta);
                parameters.Add("@_abreviacion", item.TE_abreviacion);               
                parameters.Add("@_observacion", item.TE_observacion);

                parameters.Add("@_fecha_creacion", item.TE_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TE_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TE_secuencia);
                parameters.Add("@_categoria_id", item.Categoria_equipo.CE_id);
                parameters.Add("@_color", item.Color.Co_id);
                parameters.Add("@_imagen", item.Icono.Ico_id);

                dbConnection.Execute("app_riego.create_tipo_equipo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Tipo_equipo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TE_id);
                parameters.Add("@_codigo_empresa", item.TE_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TE_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TE_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TE_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TE_descripcion_corta);
                parameters.Add("@_abreviacion", item.TE_abreviacion);
                parameters.Add("@_observacion", item.TE_observacion);

                parameters.Add("@_fecha_creacion", item.TE_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TE_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TE_secuencia);
                parameters.Add("@_categoria_id", item.Categoria_equipo.CE_id);
                parameters.Add("@_color", item.Color.Co_id);
                parameters.Add("@_imagen", item.Icono.Ico_id);

                dbConnection.Execute("app_riego.create_tipo_equipo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Tipo_equipo> FindAll(int num)
        {
           

            using (IDbConnection dbConnection = Connection)
            {
               
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", num);
                parameters.Add("@_id", 0);
                parameters.Add("@_tar_id", 0);

                var result = dbConnection.Query<Tipo_equipo, Categoria_equipo, Usuario, Tipo_equipo>("app_riego.getall_tipo_equipo", (Tipo_equipo, Categoria_equipo, Usuario) =>
                {
                    Tipo_equipo.Categoria_equipo = Categoria_equipo;
                    Tipo_equipo.TE_usuario_creacion = Usuario;
                    Tipo_equipo.TE_usuario_modificacion = Usuario;

                    return Tipo_equipo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CA_id, E_id");


                return result;

            }
        }


        /// 
        ///	  Codifica una imagen en Base64
        /// 
        public string EncodeImage(Image objImage, System.Drawing.Imaging.ImageFormat intFormat)
        {
            byte[] arrBytImages;

            // Abre un stream en memoria para obtener los bytes de la imagen		
            using (System.IO.MemoryStream stmMemory = new System.IO.MemoryStream())
            { // Graba la imagen en el stream en memoria
                objImage.Save(stmMemory, intFormat);
                // Convierte la imagen en un array de bytes
                arrBytImages = stmMemory.ToArray();
                // Cierra el stream
                stmMemory.Close();
            }
            // Devuelve el array de bytes convertidos
            return Convert.ToBase64String(arrBytImages);
        }

        /// 
        ///		Obtiene una imagen a partir de la codificación en Base64
        /// 
        public Image GetImage(string strEncoded)
        {
            byte[] arrBytImage = Convert.FromBase64String(strEncoded);
            Image objImage = null;

            // Convierte un array de bytes a una imagen
            using (System.IO.MemoryStream stmBitmap = new System.IO.MemoryStream(arrBytImage, 0, arrBytImage.Length))
            { // Escribe el array de bytes en el stream de memoria
                stmBitmap.Write(arrBytImage, 0, arrBytImage.Length);
                // Carga la imagen del stream
                objImage = Image.FromStream(stmBitmap);
                // Cierra el stream
                stmBitmap.Close();
            }
            // Devuelve la imagen
            return objImage;
        }



        public Tipo_equipo FindByID(int id, int tar_id)
        {
            int num = 5;
            if (tar_id == 0)
            {
                num = 4;
            }
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", num);
                parameters.Add("@_id", id);
                parameters.Add("@_tar_id", tar_id);

                var result = dbConnection.Query<Tipo_equipo, Categoria_equipo, Usuario, Tipo_equipo>("app_riego.getall_tipo_equipo", (Tipo_equipo, Categoria_equipo, Usuario) =>
                {
                    Tipo_equipo.Categoria_equipo = Categoria_equipo;
                    Tipo_equipo.TE_usuario_creacion = Usuario;
                    Tipo_equipo.TE_usuario_modificacion = Usuario;

                    return Tipo_equipo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CA_id, E_id");


                return result.FirstOrDefault();

            }
        }

        public IEnumerable<Tipo_equipo> GetSubtipos(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_padre_id", id);

                var result = dbConnection.Query<Tipo_equipo>("app_riego.GetSubtiposactivos", parameters, commandType: CommandType.StoredProcedure);
               
              
                return result;

            }
        }



        public void Remove(int id, int tar_id, int tar_padre_id )
        {
          

                
                string delete1 = "DELETE FROM app_riego.tipos_activos WHERE Id=@Id";

                string delete2 = "DELETE FROM app_riego.tipos_activos_relacion WHERE Id=@Id";
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                if (tar_id == 0)
                {
                    dbConnection.Execute(delete1, new { Id = id });
                }
                else
                {
                    if (tar_id != 0 && tar_padre_id == 0 )
                    {
                        dbConnection.Execute(delete2, new { Id = tar_id });
                        dbConnection.Execute(delete1, new { Id = id });
                    }
                    if (tar_id != 0 && tar_padre_id != 0)
                    {
                        dbConnection.Execute(delete2, new { Id = tar_id });
                    }
                }
               
            }
                
        }

       



        public IEnumerable<Tipo_equipo> Getcmb_tipobycategoria_equipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_equipo>("app_riego.Getcmb_tipobycategoria_equipo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }
    }
}
