﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_marcaRepository : IRepository<Grupo_marca>
    {
        private string connectionString;

        public Grupo_marcaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_marca item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GMa_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GMa_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GMa_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GMa_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GMa_descripcion_corta);
                parameters.Add("@_abreviacion", item.GMa_abreviacion);
                parameters.Add("@_observacion", item.GMa_observacion);
                parameters.Add("@_fecha_creacion", item.GMa_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GMa_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GMa_secuencia);

                dbConnection.Execute("app_riego.create_grupo_marca", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        public void Update(Grupo_marca item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.GMa_id);
                parameters.Add("@_codigo_empresa", item.GMa_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GMa_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GMa_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GMa_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GMa_descripcion_corta);
                parameters.Add("@_abreviacion", item.GMa_abreviacion);
                parameters.Add("@_observacion", item.GMa_observacion);
                parameters.Add("@_fecha_creacion", item.GMa_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GMa_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GMa_secuencia);

                dbConnection.Execute("app_riego.create_grupo_marca", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public IEnumerable<Grupo_marca> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_marca, Usuario, Grupo_marca>("app_riego.getall_grupo_marca", (Grupo_marca, Usuario) =>
                {
                    Grupo_marca.GMa_usuario_creacion = Usuario;
                    Grupo_marca.GMa_usuario_modificacion = Usuario;

                    return Grupo_marca;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GMa_id, Users_id");


                return result;

            }
        }

        public Grupo_marca FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Grupo_marca, Usuario, Grupo_marca>("app_riego.getall_grupo_marca", (Grupo_marca, Usuario) =>
                {
                    Grupo_marca.GMa_usuario_creacion = Usuario;
                    Grupo_marca.GMa_usuario_modificacion = Usuario;

                    return Grupo_marca;

                }, parameters, commandType: CommandType.StoredProcedure,
                   splitOn: "GMa_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_marca WHERE Id=@Id", new { Id = id });
            }
        }

       
    }
}
