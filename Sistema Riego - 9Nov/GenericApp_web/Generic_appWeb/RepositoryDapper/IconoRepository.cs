﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class IconoRepository
    {
        private string connectionString;

        public IconoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Icono item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Ico_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Ico_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Ico_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Ico_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Ico_descripcion_corta);
                parameters.Add("@_abreviacion", item.Ico_abreviacion);
                parameters.Add("@_observacion", item.Ico_observacion);               
                parameters.Add("@_fecha_creacion", item.Ico_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Ico_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Ico_secuencia);
                parameters.Add("@_tipo_icono_id", item.Tipo_icono.TIco_id);
                parameters.Add("@_ruta", item.Ico_secuencia);

                dbConnection.Execute("app_riego.create_icono", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Icono item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Ico_id);
                parameters.Add("@_codigo_empresa", item.Ico_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Ico_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Ico_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Ico_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Ico_descripcion_corta);
                parameters.Add("@_abreviacion", item.Ico_abreviacion);
                parameters.Add("@_observacion", item.Ico_observacion);
                parameters.Add("@_fecha_creacion", item.Ico_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Ico_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Ico_secuencia);

                dbConnection.Execute("app_riego.create_icono", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Icono> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Icono, Usuario, Tipo_icono, Icono>("app_riego.getall_icono", (Icono, Usuario, Tipo_icono) =>
                {
                    Icono.Ico_usuario_creacion = Usuario;
                    Icono.Ico_usuario_modificacion = Usuario;
                    Icono.Tipo_icono = Tipo_icono;

                    return Icono;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id, TIc_id");


                return result;

            }
        }

        public Icono FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Icono, Usuario, Tipo_icono, Icono>("app_riego.getall_icono", (Icono, Usuario, Tipo_icono) =>
                {
                    Icono.Ico_usuario_creacion = Usuario;
                    Icono.Ico_usuario_modificacion = Usuario;
                    Icono.Tipo_icono = Tipo_icono;

                    return Icono;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id, TIc_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Icono WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
