﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class ModeloRepository : IRepository<Modelo>
    {
        private string connectionString;

        public ModeloRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Modelo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Mo_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Mo_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Mo_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Mo_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Mo_descripcion_corta);
                parameters.Add("@_abreviacion", item.Mo_abreviacion);
                parameters.Add("@_observacion", item.Mo_observacion);
                parameters.Add("@_fecha_creacion", item.Mo_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Mo_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Mo_secuencia);

                dbConnection.Execute("app_riego.create_modelo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Modelo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Mo_id);
                parameters.Add("@_codigo_empresa", item.Mo_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Mo_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Mo_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Mo_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Mo_descripcion_corta);
                parameters.Add("@_abreviacion", item.Mo_abreviacion);
                parameters.Add("@_observacion", item.Mo_observacion);
                parameters.Add("@_fecha_creacion", item.Mo_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Mo_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Mo_secuencia);

                dbConnection.Execute("app_riego.create_modelo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public IEnumerable<Modelo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Modelo,Usuario, Modelo>("app_riego.getallmodelos", (Modelo, Usuario) =>
                {
                    Modelo.Mo_usuario_creacion = Usuario;
                    Modelo.Mo_usuario_modificacion = Usuario;

                    return Modelo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Mo_id, Users_id");


                return result;

            }
        }

        public Modelo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Modelo, Usuario, Modelo>("app_riego.getallmodelos", (Modelo, Usuario) =>
                {
                    Modelo.Mo_usuario_creacion = Usuario;
                    Modelo.Mo_usuario_modificacion = Usuario;

                    return Modelo;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Mo_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.modelos WHERE Id=@Id", new { Id = id });
            }
        }

       

        
    }
}
