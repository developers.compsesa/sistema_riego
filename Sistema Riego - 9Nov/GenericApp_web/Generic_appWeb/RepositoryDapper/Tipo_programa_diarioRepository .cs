﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_programa_diarioRepository
    {
        private string connectionString;

        public Tipo_programa_diarioRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_programa_diario item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TPD_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TPD_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TPD_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TPD_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TPD_descripcion_corta);
                parameters.Add("@_abreviacion", item.TPD_abreviacion);
                parameters.Add("@_observacion", item.TPD_observacion);               
                parameters.Add("@_fecha_creacion", item.TPD_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TPD_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TPD_secuencia);

                dbConnection.Execute("app_riego.create_tipo_programa_diario", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Tipo_programa_diario item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TPD_id);
                parameters.Add("@_codigo_empresa", item.TPD_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TPD_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TPD_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TPD_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TPD_descripcion_corta);
                parameters.Add("@_abreviacion", item.TPD_abreviacion);
                parameters.Add("@_observacion", item.TPD_observacion);
                parameters.Add("@_fecha_creacion", item.TPD_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TPD_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TPD_secuencia);

                dbConnection.Execute("app_riego.create_tipo_programa_diario", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Tipo_programa_diario> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_programa_diario, Usuario, Tipo_programa_diario>("app_riego.getall_tipo_programa_diario", (Tipo_programa_diario, Usuario) =>
                {

                    Tipo_programa_diario.TPD_usuario_creacion = Usuario;
                    Tipo_programa_diario.TPD_usuario_modificacion = Usuario;

                    return Tipo_programa_diario;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id");


                return result;

            }
        }

        public Tipo_programa_diario FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Tipo_programa_diario, Usuario, Tipo_programa_diario>("app_riego.getall_tipo_programa_diario", (Tipo_programa_diario, Usuario) =>
                {                   
                    Tipo_programa_diario.TPD_usuario_creacion = Usuario;
                    Tipo_programa_diario.TPD_usuario_modificacion = Usuario;

                    return Tipo_programa_diario;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_programa_diario WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Tipo_programa_diario> GetallTipo_programa_diarioBygrupo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_programa_diario>("app_riego.getalltiposvalorbygrupo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



    }
}
