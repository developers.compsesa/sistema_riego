﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Grupo_sitioRepository 
    {
        private string connectionString;

        public Grupo_sitioRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Grupo_sitio item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.GSit_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GSit_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GSit_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GSit_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GSit_descripcion_corta);
                parameters.Add("@_abreviacion", item.GSit_abreviacion);
                parameters.Add("@_observacion", item.GSit_observacion);
                parameters.Add("@_fecha_creacion", item.GSit_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GSit_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GSit_secuencia);
                parameters.Add("@_hacienda_id", item.Hacienda);

                dbConnection.Execute("app_riego.create_grupo_sitio", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Grupo_sitio item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.GSit_id);
                parameters.Add("@_codigo_empresa", item.GSit_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.GSit_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.GSit_descripcion_larga);
                parameters.Add("@_descripcion_med", item.GSit_descripcion_med);
                parameters.Add("@_descripcion_corta", item.GSit_descripcion_corta);
                parameters.Add("@_abreviacion", item.GSit_abreviacion);
                parameters.Add("@_observacion", item.GSit_observacion);
                parameters.Add("@_fecha_creacion", item.GSit_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.GSit_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.GSit_secuencia);
                parameters.Add("@_hacienda_id", item.Hacienda);

                dbConnection.Execute("app_riego.create_grupo_sitio", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Grupo_sitio> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Grupo_sitio, Usuario, Hacienda, Grupo_sitio>("app_riego.getall_grupo_sitio", (Grupo_sitio, Usuario, Hacienda) =>
                {
                  
                    Grupo_sitio.GSit_usuario_creacion = Usuario;
                    Grupo_sitio.GSit_usuario_modificacion = Usuario;
                    Grupo_sitio.Hacienda = Hacienda;
                    return Grupo_sitio;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "GSit_id,Hac_id, Users_id");
                return result;
            }
        }
              


        public Grupo_sitio FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Grupo_sitio, Usuario, Hacienda, Grupo_sitio>("app_riego.getall_grupo_sitio", (Grupo_sitio, Usuario , Hacienda) =>
                {

                    Grupo_sitio.GSit_usuario_creacion = Usuario;
                    Grupo_sitio.GSit_usuario_modificacion = Usuario;
                    Grupo_sitio.Hacienda = Hacienda;
                    return Grupo_sitio;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "GSit_id,Hac_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Grupo_sitio WHERE Id=@Id", new { Id = id });
            }
        }

       

        

    }
}
