﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class UsuarioRepository
    {
        private string connectionString;

        public UsuarioRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Usuario item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Users_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Users_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Users_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Users_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Users_descripcion_corta);
                parameters.Add("@_abreviacion", item.Users_abreviacion);
                parameters.Add("@_observacion", item.Users_observacion);               
                parameters.Add("@_fecha_creacion", item.Users_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Users_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Users_secuencia);

                dbConnection.Execute("app_riego.create_usuario", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Usuario item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Users_id);
                parameters.Add("@_codigo_empresa", item.Users_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Users_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Users_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Users_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Users_descripcion_corta);
                parameters.Add("@_abreviacion", item.Users_abreviacion);
                parameters.Add("@_observacion", item.Users_observacion);
                parameters.Add("@_fecha_creacion", item.Users_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Users_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Users_secuencia);

                dbConnection.Execute("app_riego.create_usuario", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Usuario> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Usuario, Usuario, Usuario>("app_riego.getallusuarios", (Usuario, Usuarios) =>
                {
                    Usuarios.Users_usuario_creacion = Usuario;
                    Usuarios.Users_usuario_modificacion = Usuario;

                    return Usuario;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Usuario FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Usuario, Usuario, Usuario>("app_riego.getallusuarios", (Usuario, Usuarios) =>
                {
                    Usuarios.Users_usuario_creacion = Usuario;
                    Usuarios.Users_usuario_modificacion = Usuario;

                    return Usuario;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.usuarios WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
