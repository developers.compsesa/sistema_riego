﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_sitioRepository
    {
        private string connectionString;

        public Categoria_sitioRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_sitio item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);               
                parameters.Add("@_codigo_alterno", item.CSit_codigo_alterno);
                parameters.Add("@_codigo_empresa", item.CSit_codigo_empresa);
                parameters.Add("@_descripcion_larga", item.CSit_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CSit_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CSit_descripcion_corta);
                parameters.Add("@_abreviacion", item.CSit_abreviacion);
                parameters.Add("@_observacion", item.CSit_observacion);
                parameters.Add("@_fecha_creacion", item.CSit_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CSit_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CSit_secuencia);
                parameters.Add("@_entidades_id", item.Hacienda.Hac_id);

                dbConnection.Execute("monitor.create_categoria_sitio", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Categoria_sitio item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.CSit_id);
                parameters.Add("@_codigo_alterno", item.CSit_codigo_alterno);
                parameters.Add("@_codigo_empresa", item.CSit_codigo_empresa);
                parameters.Add("@_descripcion_larga", item.CSit_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CSit_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CSit_descripcion_corta);
                parameters.Add("@_abreviacion", item.CSit_abreviacion);
                parameters.Add("@_observacion", item.CSit_observacion);
                parameters.Add("@_fecha_creacion", item.CSit_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CSit_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CSit_secuencia);
                parameters.Add("@_entidades_id", item.Hacienda.Hac_id);

                dbConnection.Execute("monitor.create_categoria_sitio", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_sitio> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_sitio, Grupo_sitio, Usuario, Hacienda, Categoria_sitio>("monitor.getall_categoria_sitio", (Categoria_sitio, Grupo_sitio, Usuario , Hacienda) =>
                {
                    Categoria_sitio.Grupo_sitio = Grupo_sitio;
                    Categoria_sitio.CSit_usuario_creacion = Usuario;
                    Categoria_sitio.CSit_usuario_modificacion = Usuario;
                    Categoria_sitio.Hacienda = Hacienda;

                    return Categoria_sitio;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "GSit_id, Users_id, Hac_id");
                return result;
            }
        }




        public Categoria_sitio FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_sitio, Grupo_sitio, Usuario, Hacienda, Categoria_sitio>("monitor.getall_categoria_sitio", (Categoria_sitio, Grupo_sitio, Usuario, Hacienda) =>
                {
                    Categoria_sitio.Grupo_sitio = Grupo_sitio;
                    Categoria_sitio.CSit_usuario_creacion = Usuario;
                    Categoria_sitio.CSit_usuario_modificacion = Usuario;
                    Categoria_sitio.Hacienda = Hacienda;

                    return Categoria_sitio;
                }, parameters, commandType: CommandType.StoredProcedure,
                   splitOn: "GSit_id, Users_id, Hac_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM monitor.Categoria_sitio WHERE Id=@Id", new { Id = id });
            }
        }




        public IEnumerable<Categoria_sitio> GetallCategoria_sitioBygrupo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_sitio>("monitor.GetallCategoria_sitioBygrupo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }




        public IEnumerable<Categoria_sitio> Getcmb_categoriabygrupo_sitio(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_sitio>("app_riego.Getcmb_categoriabygrupo_sitio", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


    }
}
