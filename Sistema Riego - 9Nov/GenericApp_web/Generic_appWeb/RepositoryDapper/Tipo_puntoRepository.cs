﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_puntoRepository : IRepository<Tipo_punto>
    {
        private string connectionString;

        public Tipo_puntoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_punto item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TPu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TPu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TPu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TPu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TPu_descripcion_corta);
                parameters.Add("@_abreviacion", item.TPu_abreviacion);
                parameters.Add("@_observacion", item.TPu_observacion);
                parameters.Add("@_fecha_creacion", item.TPu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TPu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TPu_secuencia);
                parameters.Add("@_categoria_punto_id", item.Categoria_punto.CPu_id);
                parameters.Add("@_entidades_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_tipo_punto", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Tipo_punto item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TPu_id);
                parameters.Add("@_codigo_empresa", item.TPu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TPu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TPu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TPu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TPu_descripcion_corta);
                parameters.Add("@_abreviacion", item.TPu_abreviacion);
                parameters.Add("@_observacion", item.TPu_observacion);
                parameters.Add("@_fecha_creacion", item.TPu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TPu_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TPu_secuencia);
                parameters.Add("@_categoria_punto_id", item.Categoria_punto.CPu_id);
                parameters.Add("@_entidades_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_tipo_punto", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Tipo_punto> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_punto, Categoria_punto, Usuario, Hacienda, Tipo_punto>("app_riego.getalltipos_puntos", (Tipo_punto, Categorias_puntos, Usuario , Hacienda) =>
                {
                    Tipo_punto.Categoria_punto = Categorias_puntos;
                    Tipo_punto.TPu_usuario_creacion = Usuario;
                    Tipo_punto.TPu_usuario_modificacion = Usuario;
                    Tipo_punto.Hacienda = Hacienda;
                    return Tipo_punto;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CPu_id, E_id , En_id");


                return result;

            }
        }

      


        public Tipo_punto FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_punto, Categoria_punto, Usuario, Hacienda, Tipo_punto>("app_riego.getall_tipo_punto", (Tipo_punto, Categoria_punto, Usuario , Hacienda) =>
                {
                    Tipo_punto.Categoria_punto = Categoria_punto;
                    Tipo_punto.TPu_usuario_creacion  = Usuario;
                    Tipo_punto.TPu_usuario_modificacion = Usuario;
                    Tipo_punto.Hacienda = Hacienda;

                    return Tipo_punto;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn:"CPu_id, E_id , En_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_punto WHERE Id=@Id", new { Id = id });
            }
        }





        public IEnumerable<Categoria_punto> Getcmb_tipobycategoria_punto(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_punto>("app_riego.getcmb_tipobycategoria_punto", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }







    }
}
