﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_sitioRepository : IRepository<Tipo_sitio>
    {
        private string connectionString;

        public Tipo_sitioRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_sitio item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TSit_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TSit_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TSit_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TSit_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TSit_descripcion_corta);
                parameters.Add("@_abreviacion", item.TSit_abreviacion);
                parameters.Add("@_observacion", item.TSit_observacion);
                parameters.Add("@_fecha_creacion", item.TSit_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TSit_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TSit_secuencia);
                parameters.Add("@_categoria_sitio_id", item.Categoria_sitio.CSit_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_tipo_sitio", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Tipo_sitio item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TSit_id);
                parameters.Add("@_codigo_empresa", item.TSit_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TSit_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TSit_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TSit_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TSit_descripcion_corta);
                parameters.Add("@_abreviacion", item.TSit_abreviacion);
                parameters.Add("@_observacion", item.TSit_observacion);
                parameters.Add("@_fecha_creacion", item.TSit_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TSit_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TSit_secuencia);
                parameters.Add("@_categoria_sitio_id", item.Categoria_sitio.CSit_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);

                dbConnection.Execute("app_riego.create_tipo_sitio", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Tipo_sitio> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_sitio, Categoria_sitio, Usuario, Hacienda, Tipo_sitio>("app_riego.getalltipos_sitios", (Tipo_sitio, Categoria_sitio, Usuario , Hacienda) =>
                {
                    Tipo_sitio.Categoria_sitio = Categoria_sitio;
                    Tipo_sitio.TSit_usuario_creacion = Usuario;
                    Tipo_sitio.TSit_usuario_modificacion = Usuario;
                    Tipo_sitio.Hacienda = Hacienda;
                    return Tipo_sitio;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CSit_id, Users_id , Hac_id");


                return result;

            }
        }

      


        public Tipo_sitio FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_sitio, Categoria_sitio, Usuario, Hacienda, Tipo_sitio>("app_riego.getall_tipo_sitio", (Tipo_sitio, Categoria_sitio, Usuario , Hacienda) =>
                {
                    Tipo_sitio.Categoria_sitio = Categoria_sitio;
                    Tipo_sitio.TSit_usuario_creacion  = Usuario;
                    Tipo_sitio.TSit_usuario_modificacion = Usuario;
                    Tipo_sitio.Hacienda = Hacienda;
                    return Tipo_sitio;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn:"CSit_id, Users_id , Hac_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_sitio WHERE Id=@Id", new { Id = id });
            }
        }




        public IEnumerable<Tipo_sitio> Getalltipos_sitiosBycategoria(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_sitio>("app_riego.Getalltipos_sitiosBycategoria", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }





        public IEnumerable<Tipo_sitio> Getcmb_tipobycategoria_sitio(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Tipo_sitio>("app_riego.getcmb_tipobycategoria_sitio", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }

    }
}
