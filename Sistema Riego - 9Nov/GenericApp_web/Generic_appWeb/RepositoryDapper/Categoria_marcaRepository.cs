﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_marcaRepository : IRepository<Categoria_marca>
    {
        private string connectionString;

        public Categoria_marcaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_marca item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CMa_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CMa_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CMa_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CMa_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CMa_descripcion_corta);
                parameters.Add("@_abreviacion", item.CMa_abreviacion);
                parameters.Add("@_observacion", item.CMa_observacion);               
                parameters.Add("@_fecha_creacion", item.CMa_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CMa_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CMa_secuencia);
                parameters.Add("@_grupo_marca_id", item.Grupo_marca.GMa_id);
                
                dbConnection.Execute("monitor.create_categoria_marca", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Categoria_marca item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.CMa_id);
                parameters.Add("@_codigo_empresa", item.CMa_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CMa_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CMa_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CMa_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CMa_descripcion_corta);
                parameters.Add("@_abreviacion", item.CMa_abreviacion);
                parameters.Add("@_observacion", item.CMa_observacion);
                parameters.Add("@_fecha_creacion", item.CMa_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CMa_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CMa_secuencia);
                parameters.Add("@_grupo_marca_id", item.Grupo_marca.GMa_id);

                dbConnection.Execute("monitor.create_categoria_marca", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public IEnumerable<Categoria_marca> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_marca, Grupo_marca, Usuario, Categoria_marca>("monitor.getall_categoria_marca", (Categoria_marca, Grupo_marca, Usuario) =>
                {
                    Categoria_marca.Grupo_marca = Grupo_marca;
                    Categoria_marca.CMa_usuario_creacion = Usuario;
                    Categoria_marca.CMa_usuario_modificacion = Usuario;

                    return Categoria_marca;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CMa_id, GMa_id, Users_id");


                return result;

            }
        }

    



        public Categoria_marca FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_marca, Grupo_marca, Usuario, Categoria_marca>("monitor.getall_categoria_marca", (Categoria_marca, Grupo_marca, Usuario) =>
                {
                    Categoria_marca.Grupo_marca = Grupo_marca;
                    Categoria_marca.CMa_usuario_creacion = Usuario;
                    Categoria_marca.CMa_usuario_modificacion = Usuario;

                    return Categoria_marca;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "CMa_id, GMa_id, Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM monitor.Categoria_marca WHERE Id=@Id", new { Id = id });
            }
        }

      



        public IEnumerable<Categoria_marca> Getcmb_categoriabygrupo_marca(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_marca>("monitor.getcmb_categoriabygrupo_marca", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }








    }
}
