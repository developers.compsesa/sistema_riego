﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class StatusRepository
    {
        private string connectionString;

        public StatusRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Status item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.St_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.St_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.St_descripcion_larga);
                parameters.Add("@_descripcion_med", item.St_descripcion_med);
                parameters.Add("@_descripcion_corta", item.St_descripcion_corta);
                parameters.Add("@_abreviacion", item.St_abreviacion);
                parameters.Add("@_observacion", item.St_observacion);               
                parameters.Add("@_fecha_creacion", item.St_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.St_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.St_secuencia);

                dbConnection.Execute("app_riego.create_status", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Status item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.St_id);
                parameters.Add("@_codigo_empresa", item.St_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.St_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.St_descripcion_larga);
                parameters.Add("@_descripcion_med", item.St_descripcion_med);
                parameters.Add("@_descripcion_corta", item.St_descripcion_corta);
                parameters.Add("@_abreviacion", item.St_abreviacion);
                parameters.Add("@_observacion", item.St_observacion);
                parameters.Add("@_fecha_creacion", item.St_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.St_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.St_secuencia);

                dbConnection.Execute("app_riego.create_status", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Status> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Status, Usuario, Status>("app_riego.getall_status", (Status, Usuario) =>
                {
                    Status.St_usuario_creacion = Usuario;
                    Status.St_usuario_modificacion = Usuario;

                    return Status;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Status FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Status, Usuario, Status>("app_riego.getall_status", (Status, Usuario) =>
                {
                    Status.St_usuario_creacion = Usuario;
                    Status.St_usuario_modificacion = Usuario;

                    return Status;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.status WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
