﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Descripcion_equipoRepository
    {
        private string connectionString;

        public Descripcion_equipoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {

                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Descripcion_equipo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.DEq_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.DEq_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.DEq_descripcion_larga);
                parameters.Add("@_descripcion_med", item.DEq_descripcion_med);
                parameters.Add("@_descripcion_corta", item.DEq_descripcion_corta);
                parameters.Add("@_abreviacion", item.DEq_abreviacion);
                parameters.Add("@_observacion", item.DEq_observacion);
                parameters.Add("@_fecha_creacion", item.DEq_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.DEq_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.DEq_secuencia);
                parameters.Add("@_grupo_equipo_id", item.Grupo_equipo.GE_id);
                parameters.Add("@_categoria_equipo_id", item.Categoria_equipo.CE_id);
                parameters.Add("@_tipo_equipo_id", item.Tipo_equipo.TE_id);
                parameters.Add("@_entidad_id", item.Entidad.En_id);

                dbConnection.Execute("app_riego.create_descripcion_equipo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Descripcion_equipo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.DEq_id);
                parameters.Add("@_codigo_empresa", item.DEq_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.DEq_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.DEq_descripcion_larga);
                parameters.Add("@_descripcion_med", item.DEq_descripcion_med);
                parameters.Add("@_descripcion_corta", item.DEq_descripcion_corta);
                parameters.Add("@_abreviacion", item.DEq_abreviacion);
                parameters.Add("@_observacion", item.DEq_observacion);
                parameters.Add("@_fecha_creacion", item.DEq_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.DEq_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.DEq_secuencia);
                parameters.Add("@_grupo_equipo_id", item.Grupo_equipo.GE_id);
                parameters.Add("@_categoria_equipo_id", item.Categoria_equipo.CE_id);
                parameters.Add("@_tipo_equipo_id", item.Tipo_equipo.TE_id);
                parameters.Add("@_entidad_id", item.Entidad.En_id);

                dbConnection.Execute("app_riego.create_descripcion_equipo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Descripcion_equipo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Descripcion_equipo, Grupo_equipo, Categoria_equipo, Tipo_equipo, Usuario, Entidad, Descripcion_equipo>("app_riego.getall_descripcion_equipo", (Descripcion_equipo, Grupo_equipo, Categoria_equipo, Tipo_equipo, Usuario, Entidad) =>
                {
                    Descripcion_equipo.Grupo_equipo = Grupo_equipo;
                    Descripcion_equipo.Categoria_equipo = Categoria_equipo;
                    Descripcion_equipo.Tipo_equipo = Tipo_equipo;
                    Descripcion_equipo.DEq_usuario_creacion = Usuario;
                    Descripcion_equipo.DEq_usuario_modificacion = Usuario;
                    Descripcion_equipo.Entidad = Entidad;

                    return Descripcion_equipo;

                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "DEq_id, GE_id,CE_id,TE_id, Users_id, En_id");


                return result;

            }
        }

        public Descripcion_equipo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Descripcion_equipo, Grupo_equipo, Categoria_equipo, Tipo_equipo, Usuario, Descripcion_equipo>("app_riego.getall_descripcion_equipo", (Descripcion_equipo, Grupo_equipo, Categoria_equipo, Tipo_equipo, Usuario) =>
                {
                    Descripcion_equipo.Grupo_equipo = Grupo_equipo;
                    Descripcion_equipo.Categoria_equipo = Categoria_equipo;
                    Descripcion_equipo.Tipo_equipo = Tipo_equipo;
                    Descripcion_equipo.DEq_usuario_creacion = Usuario;
                    Descripcion_equipo.DEq_usuario_modificacion = Usuario;

                    return Descripcion_equipo;

                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "DEq_id, GE_id,CE_id,TE_id, Users_id, E_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.inventario_descripciones_datos_tecnicos WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Descripcion_equipo> FindAllFiltered(int? grupo_equipo_id, int? categoria_equipo_id, int? tipo_equipo_id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                int flag = 0;
                if (grupo_equipo_id != 0)
                {
                    if (categoria_equipo_id != 0)
                    {
                        if (tipo_equipo_id != 0)
                        {
                            flag = 3;
                            //if (subtipo_activo_id != 0)
                            //{
                            //    flag = 4;
                            //}
                            //else
                            //{
                            //    flag = 3;
                            //}
                        }
                        else
                        {
                            flag = 2;
                        }
                    }
                    else
                    {
                        flag = 1;
                    }
                }
                else
                {
                    flag = 0;
                }

                parameters.Add("@_flag", flag);
                parameters.Add("@_grupo_equipo_id", grupo_equipo_id);
                parameters.Add("@_categoria_equipo_id", categoria_equipo_id);
                parameters.Add("@_tipo_equipo_id", tipo_equipo_id);
                //parameters.Add("@_subtipo_activo_id", subtipo_activo_id);

                var result = dbConnection.Query<Descripcion_equipo, Grupo_equipo, Categoria_equipo, Tipo_equipo, Usuario, Descripcion_equipo>("app_riego.getall_descripcion_datos_tecnicos_filtered", (Dato_tecnico, Grupo_equipo, Categoria_equipo, Tipo_equipo, Usuario) =>
                {
                    Dato_tecnico.Grupo_equipo = Grupo_equipo;
                    Dato_tecnico.Categoria_equipo = Categoria_equipo;
                    Dato_tecnico.Tipo_equipo = Tipo_equipo;
                    Dato_tecnico.DEq_usuario_creacion = Usuario;
                    Dato_tecnico.DEq_usuario_modificacion = Usuario;


                    return Dato_tecnico;
                }, parameters, commandType: CommandType.StoredProcedure,

                 splitOn: "GE_id,CE_id,TE_id, Users_id");


                return result;
            }
        }


        public IEnumerable<Descripcion_equipo> GetalldatostecnicosByTipo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Descripcion_equipo>("app_riego.getalltiposbycategoriadatos_tecnicos", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public Descripcion_equipo GetDatosTecnicosByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
           
                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Descripcion_equipo, Grupo_equipo, Categoria_equipo, Tipo_equipo, Usuario, Descripcion_equipo>("app_riego.getall_equipo", (Dato_tecnico, Grupo_equipo, Categoria_equipo, Tipo_equipo, Usuario) =>
                {

                    Dato_tecnico.Grupo_equipo = Grupo_equipo;
                    Dato_tecnico.Categoria_equipo = Categoria_equipo;
                    Dato_tecnico.Tipo_equipo = Tipo_equipo;
                    Dato_tecnico.DEq_usuario_creacion = Usuario;
                    Dato_tecnico.DEq_usuario_modificacion = Usuario;


                    return Dato_tecnico;
                }, parameters, commandType: CommandType.StoredProcedure,

                 splitOn: "GE_id,CE_id,TE_id, Users_id");


                return result.FirstOrDefault();

            }

        }









    }
}
