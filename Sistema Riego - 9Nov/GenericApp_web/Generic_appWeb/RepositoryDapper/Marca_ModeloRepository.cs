﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Marca_ModeloRepository : IRepository<Marca_Modelo>
    {
        private string connectionString;

        public Marca_ModeloRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Marca_Modelo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.MM_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.MM_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.MM_descripcion_larga);
                parameters.Add("@_descripcion_med", item.MM_descripcion_med);
                parameters.Add("@_descripcion_corta", item.MM_descripcion_corta);
                parameters.Add("@_abreviacion", item.MM_abreviacion);
                parameters.Add("@_observacion", item.MM_observacion);
                parameters.Add("@_fecha_creacion", item.MM_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.MM_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.MM_secuencia);
                parameters.Add("@_modelo_id", item.Modelo.Mo_id);
                parameters.Add("@_marca_id", item.Marca.Ma_id);

                dbConnection.Execute("app_riego.create_marca_modelo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Marca_Modelo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.MM_id);
                parameters.Add("@_codigo_empresa", item.MM_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.MM_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.MM_descripcion_larga);
                parameters.Add("@_descripcion_med", item.MM_descripcion_med);
                parameters.Add("@_descripcion_corta", item.MM_descripcion_corta);
                parameters.Add("@_abreviacion", item.MM_abreviacion);
                parameters.Add("@_observacion", item.MM_observacion);
                parameters.Add("@_fecha_creacion", item.MM_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.MM_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.MM_secuencia);
                parameters.Add("@_modelo_id", item.Modelo.Mo_id);
                parameters.Add("@_marca_id", item.Marca.Ma_id);

                dbConnection.Execute("app_riego.create_marca_modelo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public IEnumerable<Marca_Modelo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Marca_Modelo ,Marca, Modelo, Usuario, Marca_Modelo>("app_riego.getall_marca_modelo", (Marca_Modelo, Marca, Modelo, Usuario) =>
                {
                   
                    Marca_Modelo.Marca = Marca;
                    Marca_Modelo.Modelo = Modelo;
                    Marca_Modelo.MM_usuario_creacion = Usuario;
                    Marca_Modelo.MM_usuario_modificacion = Usuario;

                    return Marca_Modelo;

                }, parameters, commandType: CommandType.StoredProcedure,

                 splitOn: "MM_id, Ma_id, Mo_id ,Users_id");


                return result;

            }
        }

        public Marca_Modelo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Marca_Modelo, Marca, Modelo, Usuario, Marca_Modelo>("app_riego.getall_marca_modelo", (Marca_Modelo, Marca, Modelo, Usuario) =>
                {

                    Marca_Modelo.Marca = Marca;
                    Marca_Modelo.Modelo = Modelo;
                    Marca_Modelo.MM_usuario_creacion = Usuario;
                    Marca_Modelo.MM_usuario_modificacion = Usuario;

                    return Marca_Modelo;

                }, parameters, commandType: CommandType.StoredProcedure,

                 splitOn: "MM_id, Ma_id, Mo_id ,Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.inventario_marca_modelo WHERE Id=@Id", new { Id = id });
            }
        }

       

        //public IEnumerable<Marca_Modelo> GetallmodelosBymarca(int id)
        //{
        //    using (IDbConnection dbConnection = Connection)
        //    {
        //        dbConnection.Open();
        //        DynamicParameters parameters = new DynamicParameters();
        //        parameters.Add("@_id", id);

        //        var result = dbConnection.Query<Marca_Modelo, Marca, Modelo, Usuario, Marca_Modelo>("app_riego.getall_marca_modelo", (Marca_Modelo, Marca, Modelo, Usuario) =>
        //        {

        //            Marca_Modelo.Marca = Marca;
        //            Marca_Modelo.Modelo = Modelo;
        //            Marca_Modelo.MM_usuario_creacion = Usuario;
        //            Marca_Modelo.MM_usuario_modificacion = Usuario;

        //            return Marca_Modelo;

        //        }, parameters, commandType: CommandType.StoredProcedure,

        //         splitOn: "MM_id, Ma_id, Mo_id ,Users_id");


        //        return result;
        //    }
        //}
    }
}
