﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class SitioRepository : IRepository<Sitio>
    {
        private string connectionString;

        public SitioRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Sitio item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Si_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Si_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Si_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Si_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Si_descripcion_corta);
                parameters.Add("@_abreviacion", item.Si_abreviacion);
                parameters.Add("@_observacion", item.Si_observacion);
                parameters.Add("@_fecha_creacion", item.Si_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Si_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Si_secuencia);
                parameters.Add("@_grupo_sitio_id", item.Grupo_sitio.GSit_id);
                parameters.Add("@_categoria_sitio_id", item.Categoria_sitio.CSit_id);
                parameters.Add("@_tipo_sitio_id", item.Tipo_sitio.TSit_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                parameters.Add("@_area_id", item.Area.Are_id);
                
                dbConnection.Execute("app_riego.create_sitio", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Sitio item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Si_id);
                parameters.Add("@_codigo_empresa", item.Si_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Si_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Si_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Si_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Si_descripcion_corta);
                parameters.Add("@_abreviacion", item.Si_abreviacion);
                parameters.Add("@_observacion", item.Si_observacion);
                parameters.Add("@_fecha_creacion", item.Si_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Si_fecha_modificacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Si_secuencia);
                parameters.Add("@_grupo_sitio_id", item.Grupo_sitio.GSit_id);
                parameters.Add("@_categoria_sitio_id", item.Categoria_sitio.CSit_id);
                parameters.Add("@_tipo_sitio_id", item.Tipo_sitio.TSit_id);
                parameters.Add("@_hacienda_id", item.Hacienda.Hac_id);
                parameters.Add("@_area_id", item.Area.Are_id);

                dbConnection.Execute("app_riego.create_sitio", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Sitio> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query("app_riego.getallSitio", new[]
                {
                    typeof(Sitio),
                    typeof(Grupo_sitio),
                    typeof(Categoria_sitio),
                    typeof(Tipo_sitio),
                    typeof(Area),
                    typeof(Hacienda),
                    typeof(Usuario)

                }
                , obj =>
                {
                    Sitio Sitio = obj[0] as Sitio;
                    Grupo_sitio Grupo_sitio = obj[1] as Grupo_sitio;
                    Categoria_sitio Categoria_sitio = obj[2] as Categoria_sitio;
                    Tipo_sitio Tipo_sitio = obj[3] as Tipo_sitio;
                    Area Area = obj[4] as Area;
                    Hacienda Hacienda = obj[5] as Hacienda;
                    Usuario Usuario = obj[6] as Usuario;


                    Sitio.Grupo_sitio = Grupo_sitio;
                    Sitio.Categoria_sitio = Categoria_sitio;
                    Sitio.Tipo_sitio = Tipo_sitio;
                    Sitio.Area = Area;
                    Sitio.Hacienda = Hacienda;
                    Sitio.Si_usuario_creacion = Usuario;
                    Sitio.Si_usuario_modificacion = Usuario;

                    return Sitio;
                }, parameters, commandType: CommandType.StoredProcedure,
                             splitOn: "GSit_id, CSit_id, TSit_id, Hac_id , Users_id");

                return result;
            }
        }


        public Sitio FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query("app_riego.getallSitio", new[]
                 {
                    typeof(Sitio),
                    typeof(Grupo_sitio),
                    typeof(Categoria_sitio),
                    typeof(Tipo_sitio),
                    typeof(Area),
                    typeof(Hacienda),
                    typeof(Usuario)

                }
                 , obj =>
                 {
                     Sitio Sitio = obj[0] as Sitio;
                     Grupo_sitio Grupo_sitio = obj[1] as Grupo_sitio;
                     Categoria_sitio Categoria_sitio = obj[2] as Categoria_sitio;
                     Tipo_sitio Tipo_sitio = obj[3] as Tipo_sitio;
                     Area Area = obj[4] as Area;
                     Hacienda Hacienda = obj[5] as Hacienda;
                     Usuario Usuario = obj[6] as Usuario;


                     Sitio.Grupo_sitio = Grupo_sitio;
                     Sitio.Categoria_sitio = Categoria_sitio;
                     Sitio.Tipo_sitio = Tipo_sitio;
                     Sitio.Area = Area;
                     Sitio.Hacienda = Hacienda;
                     Sitio.Si_usuario_creacion = Usuario;
                     Sitio.Si_usuario_modificacion = Usuario;

                     return Sitio;
                 }, parameters, commandType: CommandType.StoredProcedure,
                              splitOn: "GSit_id, CSit_id, TSit_id, Hac_id , Users_id");

                return result.FirstOrDefault();
            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Sitio WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Sitio> GetallSitioByLugares(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Sitio>("app_riego.GetallSitioByLugares", parameters, commandType: CommandType.StoredProcedure);
                return result;

            }
        }


        public IEnumerable<Sitio> GetallSitioBytipos_sitios(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Sitio>("app_riego.GetallSitioBytipos_sitios", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }


        public Sitio GetSitioByCodEmpresa(string codigo_empresa)
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_codigo_empresa", codigo_empresa);

                var result = dbConnection.Query<Sitio>("app_riego.getallsitios_codigo_empresa", parameters, commandType: CommandType.StoredProcedure);


                return result.FirstOrDefault();

                //return dbConnection.Query<Lugares>("Select descripcion_corta from app_riego.lugares where codigo_empresa = @codigo_empresa ", new { codigo_empresa }).SingleOrDefault();

            }

        }


    }
}
