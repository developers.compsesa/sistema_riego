﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_unidadRepository
    {
        private string connectionString;

        public Categoria_unidadRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_unidad item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CUn_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CUn_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CUn_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CUn_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CUn_descripcion_corta);
                parameters.Add("@_abreviacion", item.CUn_abreviacion);
                parameters.Add("@_observacion", item.CUn_observacion);
               
                parameters.Add("@_fecha_creacion", item.CUn_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CUn_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CUn_secuencia);
                parameters.Add("@_grupo_unidad_id", item.Grupo_unidad.GUn_id);
                dbConnection.Execute("app_riego.create_categoria_unidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_unidad item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.CUn_id);
                parameters.Add("@_codigo_empresa", item.CUn_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CUn_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CUn_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CUn_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CUn_descripcion_corta);
                parameters.Add("@_abreviacion", item.CUn_abreviacion);
                parameters.Add("@_observacion", item.CUn_observacion);

                parameters.Add("@_fecha_creacion", item.CUn_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CUn_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CUn_secuencia);
                parameters.Add("@_grupo_unidad_id", item.Grupo_unidad.GUn_id);

                dbConnection.Execute("app_riego.create_categoria_unidad", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_unidad> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_unidad, Grupo_unidad, Usuario, Categoria_unidad>("app_riego.getall_categoria_unidad", (Categoria_unidad, Grupo_unidad, Usuario) =>
                {
                    Categoria_unidad.Grupo_unidad = Grupo_unidad;
                    Categoria_unidad.CUn_usuario_creacion = Usuario;
                    Categoria_unidad.CUn_usuario_modificacion = Usuario;

                    return Categoria_unidad;
                }, parameters, commandType: CommandType.StoredProcedure,
               splitOn: "GUn_id,Users_id");


                return result;

            }
        }

        public Categoria_unidad FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Categoria_unidad, Grupo_unidad, Usuario, Categoria_unidad>("app_riego.getall_categoria_unidad", (Categoria_unidad, Grupo_unidad, Usuario) =>
                {
                    Categoria_unidad.Grupo_unidad = Grupo_unidad;
                    Categoria_unidad.CUn_usuario_creacion = Usuario;
                    Categoria_unidad.CUn_usuario_modificacion = Usuario;

                    return Categoria_unidad;
                }, parameters, commandType: CommandType.StoredProcedure,
                splitOn: "GUn_id,Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Categoria_unidad WHERE Id=@Id", new { Id = id });
            }
        }




        public IEnumerable<Categoria_unidad> Getcmb_categoriabygrupo_unidad(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_unidad>("app_riego.Getcmb_categoriabygrupo_unidad", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



    }
}
