﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_iconoRepository
    {
        private string connectionString;

        public Tipo_iconoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_icono item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TIco_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TIco_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TIco_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TIco_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TIco_descripcion_corta);
                parameters.Add("@_abreviacion", item.TIco_abreviacion);
                parameters.Add("@_observacion", item.TIco_observacion);               
                parameters.Add("@_fecha_creacion", item.TIco_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TIco_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TIco_secuencia);

                dbConnection.Execute("app_riego.create_tipo_icono", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Tipo_icono item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TIco_id);
                parameters.Add("@_codigo_empresa", item.TIco_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TIco_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TIco_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TIco_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TIco_descripcion_corta);
                parameters.Add("@_abreviacion", item.TIco_abreviacion);
                parameters.Add("@_observacion", item.TIco_observacion);
                parameters.Add("@_fecha_creacion", item.TIco_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TIco_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TIco_secuencia);

                dbConnection.Execute("app_riego.create_tipo_icono", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Tipo_icono> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_icono, Usuario, Tipo_icono>("app_riego.getall_tipo_icono", (Tipo_icono, Usuario) =>
                {
                 
                    Tipo_icono.TIco_usuario_creacion = Usuario;
                    Tipo_icono.TIco_usuario_modificacion = Usuario;

                    return Tipo_icono;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "Users_id");


                return result;

            }
        }

        public Tipo_icono FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Tipo_icono, Usuario, Tipo_icono>("app_riego.getall_tipo_icono", (Tipo_icono, Usuario) =>
                {
                    
                    Tipo_icono.TIco_usuario_creacion = Usuario;
                    Tipo_icono.TIco_usuario_modificacion = Usuario;

                    return Tipo_icono;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_icono WHERE Id=@Id", new { Id = id });
            }
        }





    }
}
