﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Categoria_valorRepository
    {
        private string connectionString;

        public Categoria_valorRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Categoria_valor item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.CVal_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CVal_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CVal_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CVal_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CVal_descripcion_corta);
                parameters.Add("@_abreviacion", item.CVal_abreviacion);
                parameters.Add("@_observacion", item.CVal_observacion);
               
                parameters.Add("@_fecha_creacion", item.CVal_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CVal_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CVal_secuencia);
                parameters.Add("@_grupo_valor_id", item.Grupo_valor.GVal_id);

                dbConnection.Execute("app_riego.create_categoria_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Categoria_valor item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.CVal_id);
                parameters.Add("@_codigo_empresa", item.CVal_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.CVal_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.CVal_descripcion_larga);
                parameters.Add("@_descripcion_med", item.CVal_descripcion_med);
                parameters.Add("@_descripcion_corta", item.CVal_descripcion_corta);
                parameters.Add("@_abreviacion", item.CVal_abreviacion);
                parameters.Add("@_observacion", item.CVal_observacion);

                parameters.Add("@_fecha_creacion", item.CVal_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.CVal_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.CVal_secuencia);
                parameters.Add("@_grupo_valor_id", item.Grupo_valor.GVal_id);

                dbConnection.Execute("app_riego.create_categoria_valor", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Categoria_valor> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Categoria_valor, Grupo_valor, Usuario, Categoria_valor>("app_riego.getall_categoria_valor", (Categoria_valor, Grupos_valor, Usuario) =>
                {
                    Categoria_valor.Grupo_valor = Grupos_valor;
                    Categoria_valor.CVal_usuario_creacion = Usuario;
                    Categoria_valor.CVal_usuario_modificacion = Usuario;

                    return Categoria_valor;
                }, parameters, commandType: CommandType.StoredProcedure,
                  splitOn: "GVal_id,Users_id");


                return result;

            }
        }

        public Categoria_valor FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Categoria_valor, Grupo_valor, Usuario, Categoria_valor>("app_riego.getall_categoria_valor", (Categoria_valor, Grupos_valor, Usuario) =>
                {
                    Categoria_valor.Grupo_valor = Grupos_valor;
                    Categoria_valor.CVal_usuario_creacion = Usuario;
                    Categoria_valor.CVal_usuario_modificacion = Usuario;

                    return Categoria_valor;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "GVal_id,Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Categoria_valor WHERE Id=@Id", new { Id = id });
            }
        }


        public IEnumerable<Categoria_valor> GetallCategoria_valorBygrupo(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_id", id);

                var result = dbConnection.Query<Categoria_valor>("app_riego.getallcategoriasvalorbygrupo", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }



    }
}
