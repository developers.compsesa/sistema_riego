﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class HaciendaRepository
    {
        private string connectionString;

        public HaciendaRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Hacienda item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.Hac_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Hac_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Hac_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Hac_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Hac_descripcion_corta);
                parameters.Add("@_abreviacion", item.Hac_abreviacion);
                parameters.Add("@_observacion", item.Hac_observacion);               
                parameters.Add("@_fecha_creacion", item.Hac_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Hac_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Hac_secuencia);
                parameters.Add("@_superficie_hectareas", item.Hac_superficie_hectareas);
                parameters.Add("@_puntos_multipoligonos", item.Hac_puntos_multipoligonos);
                parameters.Add("@_radio", item.Hac_radio);
                parameters.Add("@_forma", item.Hac_forma);
                parameters.Add("@_entidad_id", item.Entidad.En_id);
                parameters.Add("@_ubicacion_geografica_id", item.Ubicacion_geografica.Ug_id);


                dbConnection.Execute("app_riego.create_hacienda", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Hacienda item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.Hac_id);
                parameters.Add("@_codigo_empresa", item.Hac_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.Hac_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.Hac_descripcion_larga);
                parameters.Add("@_descripcion_med", item.Hac_descripcion_med);
                parameters.Add("@_descripcion_corta", item.Hac_descripcion_corta);
                parameters.Add("@_abreviacion", item.Hac_abreviacion);
                parameters.Add("@_observacion", item.Hac_observacion);
                parameters.Add("@_fecha_creacion", item.Hac_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.Hac_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.Hac_secuencia);
                parameters.Add("@_superficie_hectareas", item.Hac_superficie_hectareas);
                parameters.Add("@_puntos_multipoligonos", item.Hac_puntos_multipoligonos);
                parameters.Add("@_radio", item.Hac_radio);
                parameters.Add("@_forma", item.Hac_forma);
                parameters.Add("@_entidad_id", item.Entidad.En_id);
                parameters.Add("@_ubicacion_geografica_id", item.Ubicacion_geografica.Ug_id);

                dbConnection.Execute("app_riego.create_hacienda", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Hacienda> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query("app_riego.getall_hacienda", new[]
              {
                    typeof(Hacienda),
                    typeof(Usuario),
                    typeof(Entidad),
                    typeof(Ubicacion_geografica)
                    
                }
              , obj =>
              {
                  
                  Hacienda Hacienda = obj[0] as Hacienda;
                  Usuario Usuario = obj[1] as Usuario;
                  Entidad Entidad = obj[2] as Entidad;
                  Ubicacion_geografica Ubicacion_geografica = obj[3] as Ubicacion_geografica;
                  


                 
                  Hacienda.Hac_usuario_creacion = Usuario;
                  Hacienda.Hac_usuario_modificacion = Usuario;
                  Hacienda.Entidad = Entidad;
                  Hacienda.Ubicacion_geografica = Ubicacion_geografica;

                  return Hacienda;

              }, parameters, commandType: CommandType.StoredProcedure,
                           splitOn: "Users_id, en_id, ug_id");
                           //splitOn: "Users_id");

                return result;

            }
        }

        public Hacienda FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);

                var result = dbConnection.Query("app_riego.getall_hacienda", new[]
                             {
                    typeof(Hacienda),
                    typeof(Usuario),
                    typeof(Entidad),
                    typeof(Ubicacion_geografica)
                    
                }
                             , obj =>
                             {
                                 Hacienda Hacienda = obj[0] as Hacienda;
                                 Usuario Usuario = obj[1] as Usuario;
                                 Entidad Entidad = obj[2] as Entidad;
                                 Ubicacion_geografica Ubicacion_geografica = obj[3] as Ubicacion_geografica;



                                 Hacienda.Hac_usuario_creacion = Usuario;
                                 Hacienda.Hac_usuario_modificacion = Usuario;
                                 Hacienda.Entidad = Entidad;
                                 Hacienda.Ubicacion_geografica = Ubicacion_geografica;

                                 return Hacienda;

                             }, parameters, commandType: CommandType.StoredProcedure,
                                          splitOn: "Hac_id, En_id, Ug_id, Users_id");

                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("UPDATE app_riego.hacienda SET estado = 'I' WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
