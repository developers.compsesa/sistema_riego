﻿using Dapper;
using Generic_appWeb.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Generic_appWeb.Repository
{
    public class Tipo_sueloRepository
    {
        private string connectionString;

        public Tipo_sueloRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Tipo_suelo item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", 0);
                parameters.Add("@_codigo_empresa", item.TSu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TSu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TSu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TSu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TSu_descripcion_corta);
                parameters.Add("@_abreviacion", item.TSu_abreviacion);
                parameters.Add("@_observacion", item.TSu_observacion);               
                parameters.Add("@_fecha_creacion", item.TSu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TSu_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TSu_secuencia);

                dbConnection.Execute("app_riego.create_tipo_suelo", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void Update(Tipo_suelo item)
        {

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@_id", item.TSu_id);
                parameters.Add("@_codigo_empresa", item.TSu_codigo_empresa);
                parameters.Add("@_codigo_alterno", item.TSu_codigo_alterno);
                parameters.Add("@_descripcion_larga", item.TSu_descripcion_larga);
                parameters.Add("@_descripcion_med", item.TSu_descripcion_med);
                parameters.Add("@_descripcion_corta", item.TSu_descripcion_corta);
                parameters.Add("@_abreviacion", item.TSu_abreviacion);
                parameters.Add("@_observacion", item.TSu_observacion);
                parameters.Add("@_fecha_creacion", item.TSu_fecha_creacion);
                parameters.Add("@_fecha_modificacion", item.TSu_fecha_creacion);
                parameters.Add("@_usuario_creacion", 1);
                parameters.Add("@_usuario_modificacion", 1);
                parameters.Add("@_estado", "A");
                parameters.Add("@_secuencia", item.TSu_secuencia);

                dbConnection.Execute("app_riego.create_tipo_suelo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Tipo_suelo> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 1);
                parameters.Add("@_id", 0);

                var result = dbConnection.Query<Tipo_suelo, Usuario, Tipo_suelo>("app_riego.getallTipo_suelo", (Tipo_suelo, Usuario) =>
                {
                    Tipo_suelo.TSu_usuario_creacion = Usuario;
                    Tipo_suelo.TSu_usuario_modificacion = Usuario;

                    return Tipo_suelo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result;

            }
        }

        public Tipo_suelo FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@_opcion", 2);
                parameters.Add("@_id", id);


                var result = dbConnection.Query<Tipo_suelo, Usuario, Tipo_suelo>("app_riego.getallTipo_suelo", (Tipo_suelo, Usuario) =>
                {
                    Tipo_suelo.TSu_usuario_creacion = Usuario;
                    Tipo_suelo.TSu_usuario_modificacion = Usuario;

                    return Tipo_suelo;
                }, parameters, commandType: CommandType.StoredProcedure,
                 splitOn: "Users_id");


                return result.FirstOrDefault();

            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM app_riego.Tipo_suelo WHERE Id=@Id", new { Id = id });
            }
        }

       


    }
}
