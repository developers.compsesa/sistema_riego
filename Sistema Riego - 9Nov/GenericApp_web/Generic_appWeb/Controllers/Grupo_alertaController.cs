﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_alertaController : Controller
    {

       
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_alertaRepository _Grupo_alertaRepository;
        public readonly HaciendaRepository _haciendaRepository;

        public Grupo_alertaController(IConfiguration configuration)
        {
           
            
            _Grupo_alertaRepository = new Grupo_alertaRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
            //_haciendaRepository = new HaciendaRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Grupo_alertaRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            //ViewBag.haciendaList = _haciendaRepository.FindAll();

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Grupo_alerta obj)
        {
            
            _Grupo_alertaRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

                Grupo_alerta obj = _Grupo_alertaRepository.FindByID(id.Value);

                if (obj.GAl_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Grupo_alerta obj)
        {
            try
            {
                _Grupo_alertaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Grupo_alertaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}