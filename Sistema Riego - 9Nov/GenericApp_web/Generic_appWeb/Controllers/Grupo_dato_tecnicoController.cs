﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_dato_tecnicoController : Controller
    {

        private readonly Grupo_dato_tecnicoRepository _grupos_datos_tecnicosRepository;
        //private readonly UsuarioRepository _usuarioRepository;
       

        public Grupo_dato_tecnicoController(IConfiguration configuration)
        {


            _grupos_datos_tecnicosRepository = new Grupo_dato_tecnicoRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_grupos_datos_tecnicosRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Grupo_dato_tecnico Grupo_dato_tecnico)
        {
            if (ModelState.IsValid)
            {
                _grupos_datos_tecnicosRepository.Add(Grupo_dato_tecnico);

                return RedirectToAction("Index");
            }
            return View(Grupo_dato_tecnico);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Grupo_dato_tecnico obj = _grupos_datos_tecnicosRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Grupo_dato_tecnico obj)
        {
            try
            {
                _grupos_datos_tecnicosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _grupos_datos_tecnicosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        
    }
}