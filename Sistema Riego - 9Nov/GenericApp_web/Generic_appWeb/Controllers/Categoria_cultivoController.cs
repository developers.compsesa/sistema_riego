﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_cultivoController : Controller
    {


        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_cultivoRepository _Grupos_cultivosRepository;
        public Categoria_cultivoRepository _Categoria_cultivoRepository;


        public Categoria_cultivoController(IConfiguration configuration)
        {
            _Grupos_cultivosRepository = new Grupo_cultivoRepository(configuration);
            _Categoria_cultivoRepository = new Categoria_cultivoRepository(configuration);

            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Categoria_cultivoRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.grupo_cultivoList = _Grupos_cultivosRepository.FindAll();

            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_cultivo obj)
        {

            _Categoria_cultivoRepository.Add(obj);

            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.grupo_cultivoList = _Grupos_cultivosRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();

                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Categoria_cultivo obj = _Categoria_cultivoRepository.FindByID(id.Value);

                if (obj.CCul_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_cultivo obj)
        {
            try
            {
                _Categoria_cultivoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Categoria_cultivoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }






        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_cultivo(int GCul_id)
        {
            var list = _Categoria_cultivoRepository.Getcmb_categoriabygrupo_cultivo(GCul_id);
            return Json(list);
        }














    }
}