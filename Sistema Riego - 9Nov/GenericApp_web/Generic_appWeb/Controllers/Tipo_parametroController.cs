﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_parametroController : Controller
    {

        private readonly Tipo_parametroRepository _tipos_parametrosRepository;
        private readonly Categoria_parametroRepository _categorias_parametrosRepository;
        //private readonly EstadosRepository _estadosRepository;
       

        public Tipo_parametroController(IConfiguration configuration)
        {

            _categorias_parametrosRepository = new Categoria_parametroRepository(configuration);
            _tipos_parametrosRepository = new Tipo_parametroRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_tipos_parametrosRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {

            //ViewBag.tipos_entidadesList = _tipos_entidadesRepository.FindAll();
            ViewBag.categorias_parametrosList = _categorias_parametrosRepository.FindAll();
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tipo_parametro Tipos_parametros)
        {
            if (ModelState.IsValid)
            {
                _tipos_parametrosRepository.Add(Tipos_parametros);

                return RedirectToAction("Index");
            }
            return View(Tipos_parametros);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                
                ViewBag.categorias_parametrosList = _categorias_parametrosRepository.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Tipo_parametro obj = _tipos_parametrosRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tipo_parametro obj)
        {
            try
            {
                _tipos_parametrosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _tipos_parametrosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_parametro(int CPar_id)
        {

            var categorias_parametrosList = _tipos_parametrosRepository.Getcmb_tipobycategoria_parametro(CPar_id);
            return Json(categorias_parametrosList);
        }




    }
}