﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Etapa_cultivoController : Controller
    {

        private readonly Etapa_cultivoRepository _etapas_cultivoRepository;
        //private readonly UsuarioRepository _usuarioRepository;
       

        public Etapa_cultivoController(IConfiguration configuration)
        {


            _etapas_cultivoRepository = new Etapa_cultivoRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_etapas_cultivoRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Etapa_cultivo Etapa_cultivo)
        {
            if (ModelState.IsValid)
            {
                _etapas_cultivoRepository.Add(Etapa_cultivo);

                return RedirectToAction("Index");
            }
            return View(Etapa_cultivo);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Etapa_cultivo obj = _etapas_cultivoRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Etapa_cultivo obj)
        {
            try
            {
                _etapas_cultivoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _etapas_cultivoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        
    }
}