﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_valorController : Controller
    {

       
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_valorRepository _Grupos_valorRepository;
        public Categoria_valorRepository _Categoria_valorRepository;

        public Categoria_valorController(IConfiguration configuration)
        {
            _Grupos_valorRepository = new Grupo_valorRepository(configuration);
            _Categoria_valorRepository = new Categoria_valorRepository(configuration);
          
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Categoria_valorRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.grupos_valorList = _Grupos_valorRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_valor obj)
        {

            _Categoria_valorRepository.Add(obj);
            
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.grupos_valorList = _Grupos_valorRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Categoria_valor obj = _Categoria_valorRepository.FindByID(id.Value);

                if (obj.CVal_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_valor obj)
        {
            try
            {
                _Categoria_valorRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Categoria_valorRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult GetallCategoria_valorBygrupo(int GVal_id)
        {
            var list = _Categoria_valorRepository.GetallCategoria_valorBygrupo(GVal_id);
            return Json(list);
        }





    }
}