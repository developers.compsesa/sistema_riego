﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Marca_ModeloController : Controller
    {

        private readonly ModeloRepository _modelosRepository;
        private readonly MarcaRepository _marcasRepository;
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Marca_ModeloRepository _inventario_marca_modeloRepository;


        public Marca_ModeloController(IConfiguration configuration)
        {
            _modelosRepository = new ModeloRepository(configuration);
            _marcasRepository = new MarcaRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
            _inventario_marca_modeloRepository = new Marca_ModeloRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_inventario_marca_modeloRepository.FindAll());
        }

       

        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.modelos_activosList = _modelosRepository.FindAll();
            ViewBag.marcas_activosList = _marcasRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();
           
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Marca_Modelo obj)
        {

            _inventario_marca_modeloRepository.Add(obj);
          
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.marcas_activosList = _marcasRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.modelos_activosList = _modelosRepository.FindAll();

                Marca_Modelo obj = _inventario_marca_modeloRepository.FindByID(id.Value);
                if (obj.MM_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Marca_Modelo obj)
        {
            try
            {
                _inventario_marca_modeloRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _inventario_marca_modeloRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}