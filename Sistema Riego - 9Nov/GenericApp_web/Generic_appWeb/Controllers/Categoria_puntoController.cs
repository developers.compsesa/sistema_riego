﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_puntoController : Controller
    {

       
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_puntoRepository _Grupos_puntosRepository;
        public Categoria_puntoRepository _Categoria_puntoRepository;
        public EntidadRepository _entidadRepository;

        public Categoria_puntoController(IConfiguration configuration)
        {
            _Grupos_puntosRepository = new Grupo_puntoRepository(configuration);
            _Categoria_puntoRepository = new Categoria_puntoRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Categoria_puntoRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.grupos_puntosList = _Grupos_puntosRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.entidadeList = _entidadRepository.FindAll();

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_punto obj)
        {
           
            _Categoria_puntoRepository.Add(obj);
            
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.grupos_puntosList = _Grupos_puntosRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.entidadList = _entidadRepository.FindAll();
                Categoria_punto obj = _Categoria_puntoRepository.FindByID(id.Value);


                if (obj.CPu_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_punto obj)
        {
            try
            {
                _Categoria_puntoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Categoria_puntoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }






        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_punto(int GEn_id)
        {

            var categorias_puntos_puntosList = _Categoria_puntoRepository.Getcmb_categoriabygrupo_punto(GEn_id);
            return Json(categorias_puntos_puntosList);
        }













    }
}