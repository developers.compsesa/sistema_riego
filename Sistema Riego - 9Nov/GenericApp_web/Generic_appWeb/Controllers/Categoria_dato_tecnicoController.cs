﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_dato_tecnicoController : Controller
    {

       
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_dato_tecnicoRepository _Grupos_datos_tecnicosRepository;
        public Categoria_dato_tecnicoRepository _Categoria_dato_tecnicoRepository;

        public Categoria_dato_tecnicoController(IConfiguration configuration)
        {
            _Grupos_datos_tecnicosRepository = new Grupo_dato_tecnicoRepository(configuration);
            _Categoria_dato_tecnicoRepository = new Categoria_dato_tecnicoRepository(configuration);
          
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Categoria_dato_tecnicoRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.grupos_datos_tecnicosList = _Grupos_datos_tecnicosRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_dato_tecnico obj)
        {

            _Categoria_dato_tecnicoRepository.Add(obj);
            
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.grupos_datos_tecnicosList = _Grupos_datos_tecnicosRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Categoria_dato_tecnico obj = _Categoria_dato_tecnicoRepository.FindByID(id.Value);

                if (obj.CDTec_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_dato_tecnico obj)
        {
            try
            {
                _Categoria_dato_tecnicoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Categoria_dato_tecnicoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_dato_tecnico(int GDTec_id)
        {
            var list = _Categoria_dato_tecnicoRepository.Getcmb_categoriabygrupo_dato_tecnico(GDTec_id);
            return Json(list);
        }





    }
}