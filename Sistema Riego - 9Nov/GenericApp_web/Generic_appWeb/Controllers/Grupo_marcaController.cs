﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_marcaController : Controller
    {

        private readonly Grupo_marcaRepository _grupo_marcaRepository;
        private readonly UsuarioRepository _usuarioRepository;


        public Grupo_marcaController(IConfiguration configuration)
        {
            _grupo_marcaRepository = new Grupo_marcaRepository(configuration);
            _usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_grupo_marcaRepository.FindAll());
        }
        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.estadosList = _usuarioRepository.FindAll();
           // ViewBag.tipos_activosList = _grupos_marcasRepository.FindAll();
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Grupo_marca obj)
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            _grupo_marcaRepository.Add(obj);

            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _usuarioRepository.FindAll();
             
                Grupo_marca obj = _grupo_marcaRepository.FindByID(id.Value);
                if (obj.GMa_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Grupo_marca obj)
        {
            try
            {
                _grupo_marcaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
                //return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _grupo_marcaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}