﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_valorController : Controller
    {

        private readonly Tipo_valorRepository _tipos_valorRepository;
        private readonly Categoria_valorRepository _categorias_valorRepository;
        //private readonly EstadosRepository _estadosRepository;
       

        public Tipo_valorController(IConfiguration configuration)
        {

            _categorias_valorRepository = new Categoria_valorRepository(configuration);
            _tipos_valorRepository = new Tipo_valorRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_tipos_valorRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {

            //ViewBag.tipos_entidadesList = _tipos_entidadesRepository.FindAll();
            ViewBag.categorias_valorList = _categorias_valorRepository.FindAll();
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tipo_valor Tipos_valor)
        {
            if (ModelState.IsValid)
            {
                _tipos_valorRepository.Add(Tipos_valor);

                return RedirectToAction("Index");
            }
            return View(Tipos_valor);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                
                ViewBag.categorias_valorList = _categorias_valorRepository.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Tipo_valor obj = _tipos_valorRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tipo_valor obj)
        {
            try
            {
                _tipos_valorRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _tipos_valorRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener tipos por categoria
        public JsonResult GetallTipos_valorByCategoria(int CVal_id)
        {

            var categorias_valorList = _tipos_valorRepository.GetallTipo_valorByCategoria(CVal_id);
            return Json(categorias_valorList);
        }


        public JsonResult GetallTiposvalorbyId(int TVal_id)
        {

            var tipos_valorList = _tipos_valorRepository.GetallTiposvalor(TVal_id);
            return Json(tipos_valorList);
        }


    }
}