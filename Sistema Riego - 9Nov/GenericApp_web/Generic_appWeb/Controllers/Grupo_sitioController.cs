﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_sitioController : Controller
    {

       
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_sitioRepository _Grupo_sitioRepository;
        public readonly EntidadRepository _entidadRepository;

        public Grupo_sitioController(IConfiguration configuration)
        {
           
            
            _Grupo_sitioRepository = new Grupo_sitioRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Grupo_sitioRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.entidadList = _entidadRepository.FindAll();

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Grupo_sitio obj)
        {
            
            _Grupo_sitioRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.entidadList = _entidadRepository.FindAll();
                Grupo_sitio obj = _Grupo_sitioRepository.FindByID(id.Value);

                if (obj.GSit_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Grupo_sitio obj)
        {
            try
            {
                _Grupo_sitioRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Grupo_sitioRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}