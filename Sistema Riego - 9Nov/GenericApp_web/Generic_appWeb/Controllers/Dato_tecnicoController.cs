﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Dato_tecnicoController : Controller
    {


        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Descripcion_unidadRepository _unidadesRepository;

        private readonly Grupo_dato_tecnicoRepository _grupos_datos_tecnicosRepository;
        private readonly Categoria_dato_tecnicoRepository _categorias_datos_tecnicosRepository;
        private readonly Tipo_dato_tecnicoRepository _tipos_datos_tecnicosRepository;

        private readonly Grupo_unidadRepository _grupos_unidadesRepository;
        private readonly Categoria_unidadRepository _categorias_unidadesRepository;
        private readonly Tipo_unidadRepository _tipos_unidadesRepository;

        private readonly Descripcion_valorRepository _inv_valorRepository;

        private readonly Descripcion_dato_tecnicoRepository _inventario_descripciones_datos_tecnicosRepository;

        private readonly Dato_tecnicoRepository _inv_datos_tecnicosRepository;

        public Dato_tecnicoController(IConfiguration configuration)
        {

            //_provinciasRepository = new ProvinciasRepository(configuration);
            //_ciudadesRepository = new CiudadesRepository(configuration);

            //_lugaresRepository = new LugaresRepository(configuration);

            //_usuarioRepository = new UsuarioRepository(configuration);

            _unidadesRepository = new Descripcion_unidadRepository(configuration);

            _grupos_datos_tecnicosRepository = new Grupo_dato_tecnicoRepository(configuration);
            _categorias_datos_tecnicosRepository = new Categoria_dato_tecnicoRepository(configuration);
            _tipos_datos_tecnicosRepository = new Tipo_dato_tecnicoRepository(configuration);
            _inventario_descripciones_datos_tecnicosRepository = new Descripcion_dato_tecnicoRepository(configuration);
            _inv_datos_tecnicosRepository = new Dato_tecnicoRepository(configuration);


            _grupos_unidadesRepository = new Grupo_unidadRepository(configuration);
            _categorias_unidadesRepository = new Categoria_unidadRepository(configuration);
            _tipos_unidadesRepository = new Tipo_unidadRepository(configuration);
            _inv_valorRepository = new Descripcion_valorRepository(configuration);
           

            

        }


        public IActionResult Index()
        {


            return View(_inv_datos_tecnicosRepository.FindAll());
        }


        public IActionResult PartialIndex(int? grupo_datos_tecnicos_id, int? categoria_dtos_tecnicos_id, int? tipo_datos_tecnicos_id)
        {
            try
            {
                return PartialView(_inv_datos_tecnicosRepository.FindAllFiltered(grupo_datos_tecnicos_id, categoria_dtos_tecnicos_id, tipo_datos_tecnicos_id));
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }

        public IActionResult PartialIndex_multiSelect(int? grupo_datos_tecnicos_id, int? categoria_datos_tecnicos_id, int? tipos_datos_tecnicos_id)
        {
            try
            {
                return PartialView(_inv_datos_tecnicosRepository.FindAllFiltered(grupo_datos_tecnicos_id, categoria_datos_tecnicos_id, tipos_datos_tecnicos_id));
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }


        public IActionResult Create()
        {
            if (ModelState.IsValid)
            {
                //ViewBag.grupos_datos_tecnicosList = _grupos_datos_tecnicosRepository.FindAll();  
                ViewBag.inv_descripciones_datos_tecnicosList = _inventario_descripciones_datos_tecnicosRepository.FindAll();
                ViewBag.inv_valor2List = _inv_valorRepository.FindAll();
                ViewBag.unidades2List = _unidadesRepository.FindAllUnidades();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            }



            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Dato_tecnico Dato_tecnico)
        {
            if (ModelState.IsValid)
            {
                _inv_datos_tecnicosRepository.Add(Dato_tecnico);

                return RedirectToAction("Index");
            }
            _inv_datos_tecnicosRepository.Add(Dato_tecnico);
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.inv_descripciones_datos_tecnicosList = _inventario_descripciones_datos_tecnicosRepository.FindAll();
                //ViewBag.grupos_datos_tecnicosList = _grupos_datos_tecnicosRepository.FindAll();
                //ViewBag.categorias_datos_tecnicosList = _categorias_datos_tecnicosRepository.GetallCategorias_datos_tecnicosBygrupo(gdtec_id);
                //ViewBag.tipos_datos_tecnicosList = _tipos_datos_tecnicosRepository.GetallTiposByCategoria_datos_tecnicos(cdtec_id);

                ViewBag.unidades2List = _unidadesRepository.FindAll();
                
                ViewBag.inv_valor2List = _inv_valorRepository.FindAll();

                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosDispobilidadList = _usuarioRepository.FindAll();

                Dato_tecnico obj = _inv_datos_tecnicosRepository.FindByID(id.Value);
                if (obj.DTec_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Dato_tecnico obj)
        {
            try
            {

                _inv_datos_tecnicosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _inventario_descripciones_datos_tecnicosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_dato_tecnico(int GDTec_id)
        {
            var categorias_datos_tecnicosList = _categorias_datos_tecnicosRepository.Getcmb_categoriabygrupo_dato_tecnico(GDTec_id);
            return Json(categorias_datos_tecnicosList);
        }


        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_dato_tecnico(int CDTec_id)
        {

            var tipos_datos_tecnicosList = _tipos_datos_tecnicosRepository.Getcmb_tipobycategoria_dato_tecnico(CDTec_id);
            return Json(tipos_datos_tecnicosList);
        }




        //public JsonResult GetallUnidadesBytipos_unidades(int TUn_id)
        //{

        //    var UnidadesList = _unidadesRepository.GetallUnidadesByTipo(TUn_id);
        //    return Json(UnidadesList);
        //}



        //public JsonResult GetSitioByCodEmpresa(string codigo_empresa)
        //{
        //    var sitios = _unidadesRepository.GetUnidadesByCodEmpresa(codigo_empresa);
        //    //return Json(new { success = true, message = "Order updated successfully" });
        //    if (sitios != null)
        //    {
        //        return Json(new { success = true, message = "Successful", sitios });
        //    }
        //    else
        //    {
        //        return Json(new { success = false, message = "Not Successful" });
        //    }

        //}





    }
}