﻿using CsvHelper;
using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Generic_appWeb.Controllers
{
    public class Lugar_ubicacionController : Controller
    {
        private readonly Lugar_ubicacionRepository _lugar_ubicacionRepository;

        private readonly Grupo_lugarRepository _grupos_lugaresRepository;
        private readonly Categoria_lugarRepository _categorias_lugaresRepository;
        private readonly Tipo_lugarRepository _tipos_lugaresRepository;
        private readonly LugarRepository _lugaresRepository;

        private readonly Grupo_sitioRepository _grupos_sitiosRepository;
        private readonly Categoria_sitioRepository _categorias_sitiosRepository;
        private readonly Tipo_sitioRepository _tipos_sitiosRepository;
        private readonly SitioRepository _sitiosRepository;

        private readonly Grupo_areaRepository _grupos_areasRepository;
        private readonly Categoria_areaRepository _categorias_areasRepository;
        private readonly Tipo_areaRepository _tipos_areasRepository;
        private readonly AreaRepository _areasRepository;

        private readonly Grupo_puntoRepository _grupos_puntosRepository;
        private readonly Categoria_puntoRepository _categorias_puntosRepository;
        private readonly Tipo_puntoRepository _tipos_puntosRepository;
        private readonly PuntoRepository _puntosRepository;

        private readonly ILogger _logger;
                
        private readonly UsuarioRepository _usuarioRepository;

        public Lugar_ubicacionController(IConfiguration configuration, IHostingEnvironment hostingEnvironment, ILogger<EquipoController> logger)
        {

            _lugar_ubicacionRepository = new Lugar_ubicacionRepository(configuration);
            _grupos_puntosRepository = new Grupo_puntoRepository(configuration);
            _categorias_puntosRepository = new Categoria_puntoRepository(configuration);
            _tipos_puntosRepository = new Tipo_puntoRepository(configuration);
            _puntosRepository = new PuntoRepository(configuration);

            _grupos_lugaresRepository = new Grupo_lugarRepository(configuration);
            _categorias_lugaresRepository = new Categoria_lugarRepository(configuration);
            _tipos_lugaresRepository = new Tipo_lugarRepository(configuration);
            _lugaresRepository = new LugarRepository(configuration);

            _grupos_sitiosRepository = new Grupo_sitioRepository(configuration);
            _categorias_sitiosRepository = new Categoria_sitioRepository(configuration);
            _tipos_sitiosRepository = new Tipo_sitioRepository(configuration);
            _sitiosRepository = new SitioRepository(configuration);


            _grupos_areasRepository = new Grupo_areaRepository(configuration);
            _categorias_areasRepository = new Categoria_areaRepository(configuration);
            _tipos_areasRepository = new Tipo_areaRepository(configuration);
            _areasRepository = new AreaRepository(configuration);

            _hostingEnvironment = hostingEnvironment;

            _logger = logger;

            _usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            try
            {
                return View(_lugar_ubicacionRepository.FindAll());
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
           
        }

        private readonly IHostingEnvironment _hostingEnvironment;

        //public async Task<IActionResult> Export(CancellationToken cancellationToken)
        //{

        //    try
        //    {
        //        // query data from database  
        //        await Task.Yield();

        //        var stream = new MemoryStream();

        //        using (var package = new ExcelPackage(stream))
        //        {
        //            var workSheet = package.Workbook.Worksheets.Add("JEARQUIA DE POSICIONES");
        //            //workSheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Justify;



        //            workSheet.Cells.LoadFromCollection(_puntosRepository.FindAllToExport(), true);
        //            package.Save();
        //        }

        //        stream.Position = 0;
        //        string excelName = $"POSICIONES-{DateTime.Now.ToString("yyyyMMddHHmmss")}.xlsx";

        //        //return File(stream, "application/octet-stream", excelName);  
        //        return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        //    }
        //    catch (Exception e)
        //    {
        //        _logger.LogCritical(

        //              DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

        //            );
        //        throw;
        //    }
           
        //}



      
        public IActionResult Create()
        {
            try
            {
                ViewBag.grupos_puntosList = _grupos_puntosRepository.FindAll();
                ViewBag.grupos_sitiosList = _grupos_sitiosRepository.FindAll();
                ViewBag.grupos_areasList = _grupos_areasRepository.FindAll();
                ViewBag.grupos_lugaresList = _grupos_lugaresRepository.FindAll();
                ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");


                return View();
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
           
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Lugar_ubicacion lugar_ubicacion)
        {
            try
            {
                _lugar_ubicacionRepository.Add(lugar_ubicacion);

              
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
           

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int gl_id, int cl_id, int tl_id ,int gsit_id, int csit_id, int tsit_id, int gare_id, int care_id, int tare_id, int gpu_id, int cpu_id, int tpu_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }




                ViewBag.grupos_lugaresList = _grupos_lugaresRepository.FindAll();
                ViewBag.categorias_lugaresList = _categorias_lugaresRepository.Getcmb_categoriabygrupo_lugar(gl_id);
                ViewBag.tipos_lugaresList = _tipos_lugaresRepository.Getcmb_tipobycategoria_lugar(cl_id);
                ViewBag.lugaresList = _lugaresRepository.GetallLugarBytipos_lugares(tl_id);
                

                ViewBag.grupos_sitiosList = _grupos_sitiosRepository.FindAll();
                ViewBag.categorias_sitiosList = _categorias_sitiosRepository.Getcmb_categoriabygrupo_sitio(gsit_id);
                ViewBag.tipos_sitiosList = _tipos_sitiosRepository.Getcmb_tipobycategoria_sitio(csit_id);
                ViewBag.sitiosList = _sitiosRepository.GetallSitioBytipos_sitios(tsit_id);


                ViewBag.grupos_areasList = _grupos_areasRepository.FindAll();
                ViewBag.categorias_areasList = _categorias_areasRepository.Getcmb_categoriabygrupo_area(gare_id);
                ViewBag.tipos_areasList = _tipos_areasRepository.Getcmb_tipobycategoria_area(care_id);
                ViewBag.areasList = _areasRepository.GetallAreaByLugar(tare_id);


                ViewBag.grupos_puntosList = _grupos_puntosRepository.FindAll();
                ViewBag.categorias_puntosList = _categorias_puntosRepository.Getcmb_categoriabygrupo_punto(gpu_id);
                ViewBag.tipos_puntosList = _tipos_puntosRepository.Getcmb_tipobycategoria_punto(cpu_id);
                ViewBag.puntosList = _puntosRepository.GetallPuntoBytipos_puntos(tpu_id);
                

                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.estadosList = _usuarioRepository.FindAll();

                Lugar_ubicacion obj = _lugar_ubicacionRepository.FindByID(id.Value);
                if (obj.Luu_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Lugar_ubicacion obj)
        {
            try
            {

                _lugar_ubicacionRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                _lugar_ubicacionRepository.Remove(id.Value);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
            
        }



        public JsonResult GetallPuntosBySitios(int Lu_id)
        {
            try
            {
                var PuntosList = _puntosRepository.GetallPuntoBySitios(Lu_id);
                return Json(PuntosList);
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
            
        }







        public JsonResult GetLugar_ubicacionByCodEmpresa(string codigo_empresa)
        {
            var lugar_ubicacion = _lugar_ubicacionRepository.Getlugar_ubicacionByCodEmpresa(codigo_empresa);
            //return Json(new { success = true, message = "Order updated successfully" });
            if (lugar_ubicacion != null)
            {
                return Json(new { success = true, message = "Successful", lugar_ubicacion });
            }
            else
            {
                return Json(new { success = false, message = "Not Successful" });
            }

        }



    }
}