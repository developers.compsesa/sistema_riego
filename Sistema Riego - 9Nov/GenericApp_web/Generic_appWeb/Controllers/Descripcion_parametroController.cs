﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Descripcion_parametroController : Controller
    {

        private readonly Descripcion_parametroRepository _descripcion_parametroRepository;
        private readonly Grupo_parametroRepository _grupo_parametroRepository;
        private readonly Categoria_parametroRepository _categoria_parametroRepository;
        private readonly Tipo_parametroRepository _tipo_parametroRepository;
        private readonly EntidadRepository _entidadRepository;

        
        public Descripcion_parametroController(IConfiguration configuration)
        {

            _grupo_parametroRepository = new Grupo_parametroRepository(configuration);
            _categoria_parametroRepository = new Categoria_parametroRepository(configuration);
            _tipo_parametroRepository = new Tipo_parametroRepository(configuration);
            _descripcion_parametroRepository = new Descripcion_parametroRepository(configuration);          
            _entidadRepository = new EntidadRepository(configuration);


        }


        public IActionResult Index()
        {


            return View(_descripcion_parametroRepository.FindAll());
        }


        public IActionResult PartialIndex(int? grupo_parametro_id, int? categoria_parametro_id, int? tipo_parametro_id)
        {
            try
            {
                return PartialView(_descripcion_parametroRepository.FindAllFiltered(grupo_parametro_id, categoria_parametro_id, tipo_parametro_id));
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }

        public IActionResult PartialIndex_multiSelect(int? grupo_parametro_id, int? categoria_parametro_id, int? tipo_parametro_id)
        {
            try
            {
                return PartialView(_descripcion_parametroRepository.FindAllFiltered(grupo_parametro_id, categoria_parametro_id, tipo_parametro_id));
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }


        public IActionResult Create()
        {
            if (ModelState.IsValid)
            {
                ViewBag.Grupo_parametroList = _grupo_parametroRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            }



            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Descripcion_parametro Descripcion_parametro)
        {
            if (ModelState.IsValid)
            {
                _descripcion_parametroRepository.Add(Descripcion_parametro);

                return RedirectToAction("Index");
            }
            _descripcion_parametroRepository.Add(Descripcion_parametro);
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int gpar_id, int cpar_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.Grupo_parametroList = _grupo_parametroRepository.FindAll();
                ViewBag.Categoria_parametroList = _categoria_parametroRepository.Getcmb_categoriabygrupo_parametro(gpar_id);
                ViewBag.Tipo_parametroList = _tipo_parametroRepository.Getcmb_tipobycategoria_parametro(cpar_id);        
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

                Descripcion_parametro obj = _descripcion_parametroRepository.FindByID(id.Value);
                if (obj.DPar_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Descripcion_parametro obj)
        {
            try
            {

                _descripcion_parametroRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _descripcion_parametroRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_parametro(int GPar_id)
        {
            var Categoria_parametroList = _categoria_parametroRepository.Getcmb_categoriabygrupo_parametro(GPar_id);
            return Json(Categoria_parametroList);
        }


        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_parametro(int CPar_id)
        {

            var Tipo_parametroList = _tipo_parametroRepository.Getcmb_tipobycategoria_parametro(CPar_id);
            return Json(Tipo_parametroList);
        }




        //public JsonResult GetallUnidadesBytipos_unidades(int TUn_id)
        //{

        //    var UnidadesList = _unidadesRepository.GetallUnidadesByTipo(TUn_id);
        //    return Json(UnidadesList);
        //}



        //public JsonResult GetSitioByCodEmpresa(string codigo_empresa)
        //{
        //    var sitios = _unidadesRepository.GetUnidadesByCodEmpresa(codigo_empresa);
        //    //return Json(new { success = true, message = "Order updated successfully" });
        //    if (sitios != null)
        //    {
        //        return Json(new { success = true, message = "Successful", sitios });
        //    }
        //    else
        //    {
        //        return Json(new { success = false, message = "Not Successful" });
        //    }

        //}





    }
}