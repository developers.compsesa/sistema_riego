﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_alertaController : Controller
    {


        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_alertaRepository _Grupos_alertasRepository;
        public Categoria_alertaRepository _Categoria_alertaRepository;
       

        public Categoria_alertaController(IConfiguration configuration)
        {
            _Grupos_alertasRepository = new Grupo_alertaRepository(configuration);
            _Categoria_alertaRepository = new Categoria_alertaRepository(configuration);
            
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Categoria_alertaRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.grupo_alertaList = _Grupos_alertasRepository.FindAll();
            
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_alerta obj)
        {

            _Categoria_alertaRepository.Add(obj);

            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.grupo_alertaList = _Grupos_alertasRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Categoria_alerta obj = _Categoria_alertaRepository.FindByID(id.Value);

                if (obj.CAl_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_alerta obj)
        {
            try
            {
                _Categoria_alertaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Categoria_alertaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }






        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_alerta(int GAl_id)
        {
            var list = _Categoria_alertaRepository.Getcmb_categoriabygrupo_alerta(GAl_id);
            return Json(list);
        }














    }
}