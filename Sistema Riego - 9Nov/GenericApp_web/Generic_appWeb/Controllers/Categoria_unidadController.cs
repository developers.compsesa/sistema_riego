﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_unidadController : Controller
    {
        private readonly Categoria_unidadRepository _categorias_unidadesRepository;
        private readonly Grupo_unidadRepository _grupos_unidadesRepository;
        //private readonly UsuarioRepository _usuarioRepository;
       

        public Categoria_unidadController(IConfiguration configuration)
        {

            _categorias_unidadesRepository = new Categoria_unidadRepository(configuration);
            _grupos_unidadesRepository = new Grupo_unidadRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_categorias_unidadesRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            ViewBag.grupos_unidadesList = _grupos_unidadesRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Categoria_unidad Categoria_unidad)
        {
            if (ModelState.IsValid)
            {
                _categorias_unidadesRepository.Add(Categoria_unidad);

                return RedirectToAction("Index");
            }
            return View(Categoria_unidad);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                ViewBag.grupos_unidadesList = _grupos_unidadesRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Categoria_unidad obj = _categorias_unidadesRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Categoria_unidad obj)
        {
            try
            {
                _categorias_unidadesRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _categorias_unidadesRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }







        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_unidad(int GEn_id)
        {

            var categorias_unidads_unidadsList = _categorias_unidadesRepository.Getcmb_categoriabygrupo_unidad(GEn_id);
            return Json(categorias_unidads_unidadsList);
        }

















    }
}