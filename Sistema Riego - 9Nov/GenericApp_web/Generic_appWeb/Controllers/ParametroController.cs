﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class ParametroController : Controller
    {


        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Descripcion_unidadRepository _unidadesRepository;

        private readonly Grupo_parametroRepository _grupos_parametrosRepository;
        private readonly Categoria_parametroRepository _categorias_parametrosRepository;
        private readonly Tipo_parametroRepository _tipos_parametrosRepository;

        private readonly Grupo_unidadRepository _grupos_unidadesRepository;
        private readonly Categoria_unidadRepository _categorias_unidadesRepository;
        private readonly Tipo_unidadRepository _tipos_unidadesRepository;

        private readonly Descripcion_valorRepository _inv_valorRepository;

        private readonly Descripcion_parametroRepository _inventario_descripciones_parametrosRepository;

        private readonly ParametroRepository _inventario_parametrosRepository;

        public ParametroController(IConfiguration configuration)
        {

            //_provinciasRepository = new ProvinciasRepository(configuration);
            //_ciudadesRepository = new CiudadesRepository(configuration);

            //_lugaresRepository = new LugaresRepository(configuration);

            //_usuarioRepository = new UsuarioRepository(configuration);

            _unidadesRepository = new Descripcion_unidadRepository(configuration);

            _grupos_parametrosRepository = new Grupo_parametroRepository(configuration);
            _categorias_parametrosRepository = new Categoria_parametroRepository(configuration);
            _tipos_parametrosRepository = new Tipo_parametroRepository(configuration);
            _inventario_descripciones_parametrosRepository = new Descripcion_parametroRepository(configuration);
            _inventario_parametrosRepository = new ParametroRepository(configuration);


            _grupos_unidadesRepository = new Grupo_unidadRepository(configuration);
            _categorias_unidadesRepository = new Categoria_unidadRepository(configuration);
            _tipos_unidadesRepository = new Tipo_unidadRepository(configuration);
            _inv_valorRepository = new Descripcion_valorRepository(configuration);




        }


        public IActionResult Index()
        {


            return View(_inventario_parametrosRepository.FindAll());
        }


        public IActionResult PartialIndex(int? grupo_parametros_id, int? categoria_parametros_id, int? tipo_parametros_id)
        {
            try
            {
                return PartialView(_inventario_parametrosRepository.FindAllFiltered(grupo_parametros_id, categoria_parametros_id, tipo_parametros_id));
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }

        public IActionResult PartialIndex_multiSelect(int? grupo_parametros_id, int? categoria_parametros_id, int? tipos_parametros_id)
        {
            try
            {
                return PartialView(_inventario_parametrosRepository.FindAllFiltered(grupo_parametros_id, categoria_parametros_id, tipos_parametros_id));
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }


        public IActionResult Create()
        {
            if (ModelState.IsValid)
            {
                //ViewBag.grupos_parametrosList = _grupos_parametrosRepository.FindAll();  
                ViewBag.inventario_descripciones_parametrosList = _inventario_descripciones_parametrosRepository.FindAll();
                ViewBag.inv_valor2List = _inv_valorRepository.FindAll();
                ViewBag.unidades2List = _unidadesRepository.FindAllUnidades();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            }



            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Parametro Inventario_parametros)
        {
            if (ModelState.IsValid)
            {
                _inventario_parametrosRepository.Add(Inventario_parametros);

                return RedirectToAction("Index");
            }
            _inventario_parametrosRepository.Add(Inventario_parametros);
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.inventario_descripciones_parametrosList = _inventario_descripciones_parametrosRepository.FindAll();
                //ViewBag.grupos_parametrosList = _grupos_parametrosRepository.FindAll();
                //ViewBag.categorias_parametrosList = _categorias_parametrosRepository.GetallCategorias_parametrosBygrupo(gdtec_id);
                //ViewBag.tipos_parametrosList = _tipos_parametrosRepository.GetallTiposByCategoria_parametros(cdtec_id);

                ViewBag.unidades2List = _unidadesRepository.FindAll();

                ViewBag.inv_valor2List = _inv_valorRepository.FindAll();



                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosDispobilidadList = _usuarioRepository.FindAll();

                Parametro obj = _inventario_parametrosRepository.FindByID(id.Value);
                if (obj.Par_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Parametro obj)
        {
            try
            {

                _inventario_parametrosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _inventario_parametrosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult GetallCategoriasBygrupo_parametros(int GPar_id)
        {
            var categorias_parametrosList = _categorias_parametrosRepository.Getcmb_categoriabygrupo_parametro(GPar_id);
            return Json(categorias_parametrosList);
        }


        // comobo box obtener tipos por categoria
        public JsonResult GetallTiposBycategoria_parametros(int CPar_id)
        {

            var tipos_parametrosList = _tipos_parametrosRepository.Getcmb_tipobycategoria_parametro(CPar_id);
            return Json(tipos_parametrosList);
        }




        //public JsonResult GetallUnidadesBytipos_unidades(int TUn_id)
        //{

        //    var UnidadesList = _unidadesRepository.GetallUnidadesByTipo(TUn_id);
        //    return Json(UnidadesList);
        //}



        //public JsonResult GetSitioByCodEmpresa(string codigo_empresa)
        //{
        //    var sitios = _unidadesRepository.GetUnidadesByCodEmpresa(codigo_empresa);
        //    //return Json(new { success = true, message = "Order updated successfully" });
        //    if (sitios != null)
        //    {
        //        return Json(new { success = true, message = "Successful", sitios });
        //    }
        //    else
        //    {
        //        return Json(new { success = false, message = "Not Successful" });
        //    }

        //}





    }
}