﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_valvulaController : Controller
    {

        private readonly Grupo_valvulaRepository _grupos_valvulasRepository;
        //private readonly UsuarioRepository _usuarioRepository;
       

        public Grupo_valvulaController(IConfiguration configuration)
        {


            _grupos_valvulasRepository = new Grupo_valvulaRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_grupos_valvulasRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Grupo_valvula Grupo_valvula)
        {
            if (ModelState.IsValid)
            {
                _grupos_valvulasRepository.Add(Grupo_valvula);

                return RedirectToAction("Index");
            }
            return View(Grupo_valvula);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Grupo_valvula obj = _grupos_valvulasRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Grupo_valvula obj)
        {
            try
            {
                _grupos_valvulasRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _grupos_valvulasRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        
    }
}