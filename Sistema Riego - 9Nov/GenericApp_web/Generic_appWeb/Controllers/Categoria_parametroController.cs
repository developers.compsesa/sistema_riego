﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_parametroController : Controller
    {

       
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_parametroRepository _Grupos_parametrosRepository;
        public Categoria_parametroRepository _Categoria_parametroRepository;

        public Categoria_parametroController(IConfiguration configuration)
        {
            _Grupos_parametrosRepository = new Grupo_parametroRepository(configuration);
            _Categoria_parametroRepository = new Categoria_parametroRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Categoria_parametroRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.grupos_parametrosList = _Grupos_parametrosRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_parametro obj)
        {

            _Categoria_parametroRepository.Add(obj);
            
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.grupos_parametrosList = _Grupos_parametrosRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Categoria_parametro obj = _Categoria_parametroRepository.FindByID(id.Value);

                if (obj.CPar_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_parametro obj)
        {
            try
            {
                _Categoria_parametroRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Categoria_parametroRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_parametro(int GPar_id)
        {
            var list = _Categoria_parametroRepository.Getcmb_categoriabygrupo_parametro(GPar_id);
            return Json(list);
        }





    }
}