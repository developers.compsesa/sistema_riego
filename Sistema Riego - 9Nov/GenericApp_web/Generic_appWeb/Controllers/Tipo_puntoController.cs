﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_puntoController : Controller
    {

        private readonly Categoria_puntoRepository _categorias_puntos;
        //private readonly EstadosRepository _estadosRepository;
        private readonly Tipo_puntoRepository _tipos_puntosRepository;
        public readonly EntidadRepository _entidadRepository;

        public Tipo_puntoController(IConfiguration configuration)
        {
            _categorias_puntos = new Categoria_puntoRepository(configuration);
             //_estadosRepository = new EstadosRepository(configuration);
            _tipos_puntosRepository = new Tipo_puntoRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_tipos_puntosRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.categorias_puntosList = _categorias_puntos.FindAll();
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.entidadesList = _entidadRepository.FindAll();


            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Tipo_punto obj)
        {
            
            _tipos_puntosRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                ViewBag.categorias_puntosList = _categorias_puntos.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.entidadesList = _entidadRepository.FindAll();
                Tipo_punto obj = _tipos_puntosRepository.FindByID(id.Value);

                if (obj.TPu_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Tipo_punto obj)
        {
            try
            {
                _tipos_puntosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _tipos_puntosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }


        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_punto(int CPu_id)
        {

            var list = _tipos_puntosRepository.Getcmb_tipobycategoria_punto(CPu_id);
            return Json(list);
        }


    }
}