﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_parametroController : Controller
    {

        private readonly Grupo_parametroRepository _grupos_parametrosRepository;
        //private readonly UsuarioRepository _usuarioRepository;
       

        public Grupo_parametroController(IConfiguration configuration)
        {


            _grupos_parametrosRepository = new Grupo_parametroRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_grupos_parametrosRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Grupo_parametro Grupo_parametro)
        {
            if (ModelState.IsValid)
            {
                _grupos_parametrosRepository.Add(Grupo_parametro);

                return RedirectToAction("Index");
            }
            return View(Grupo_parametro);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Grupo_parametro obj = _grupos_parametrosRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Grupo_parametro obj)
        {
            try
            {
                _grupos_parametrosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _grupos_parametrosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        
    }
}