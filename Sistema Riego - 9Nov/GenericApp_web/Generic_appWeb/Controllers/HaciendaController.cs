﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class HaciendaController : Controller
    {

        private readonly HaciendaRepository _haciendaRepository;
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly EntidadRepository _entidadRepository;
        private readonly Ubicacion_geograficaRepository _ubicacion_geograficaRepository;

        public HaciendaController(IConfiguration configuration)
        {
            _haciendaRepository = new HaciendaRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
            _ubicacion_geograficaRepository = new Ubicacion_geograficaRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_haciendaRepository.FindAll());
        }

        public IActionResult Create()
        {
            
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.EntidadList = _entidadRepository.FindAll();
            ViewBag.Ubicacion_geograficaList = _ubicacion_geograficaRepository.FindAll();
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Hacienda obj)
        {
            _haciendaRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                ViewBag.EntidadList = _entidadRepository.FindAll();
                ViewBag.Ubicacion_geograficaList = _ubicacion_geograficaRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Hacienda obj = _haciendaRepository.FindByID(id.Value);

                if (obj.Hac_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Hacienda obj)
        {
            try
            {
                _haciendaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _haciendaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}