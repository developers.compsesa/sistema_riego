﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_lugarController : Controller
    {

        private readonly Grupo_lugarRepository _grupos_lugaresRepository;
        //private readonly UsuarioRepository _usuarioRepository;
        public readonly EntidadRepository _entidadesRepository;


        public Grupo_lugarController(IConfiguration configuration)
        {
            _grupos_lugaresRepository = new Grupo_lugarRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
            _entidadesRepository = new EntidadRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_grupos_lugaresRepository.FindAll());
        }
        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            //ViewBag.entidadList = _entidadRepository.FindAll2();
            // ViewBag.tipos_activosList = _grupos_lugaresRepository.FindAll();
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Grupo_lugar obj)
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            
            _grupos_lugaresRepository.Add(obj);

            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.entidadesList = _entidadesRepository.FindAll();
                Grupo_lugar obj = _grupos_lugaresRepository.FindByID(id.Value);
                if (obj.GL_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Grupo_lugar obj)
        {
            try
            {
                _grupos_lugaresRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _grupos_lugaresRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}