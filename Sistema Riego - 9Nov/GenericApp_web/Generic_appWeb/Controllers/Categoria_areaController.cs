﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_areaController : Controller
    {

       
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_areaRepository _Grupos_areasRepository;
        public Categoria_areaRepository _Categoria_areaRepository;
        public EntidadRepository _entidadRepository;

        public Categoria_areaController(IConfiguration configuration)
        {
            _Grupos_areasRepository = new Grupo_areaRepository(configuration);
            _Categoria_areaRepository = new Categoria_areaRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Categoria_areaRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.grupos_areasList = _Grupos_areasRepository.FindAll();
            ViewBag.entidadesList = _entidadRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_area obj)
        {
           
            _Categoria_areaRepository.Add(obj);
            
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.grupos_areasList = _Grupos_areasRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.entidadesList = _entidadRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Categoria_area obj = _Categoria_areaRepository.FindByID(id.Value);

                if (obj.CAre_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_area obj)
        {
            try
            {
                _Categoria_areaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Categoria_areaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }






        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_area(int GAre_id)
        {
            var list = _Categoria_areaRepository.Getcmb_categoriabygrupo_area(GAre_id);
            return Json(list);
        }














    }
}