﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class MarcaController : Controller
    {

        private readonly MarcaRepository _marcaRepository;
        //private readonly UsuarioRepository _usuarioRepository;
        //private readonly Grupo_marcaRepository _grupo_marcaRepository;
        //private readonly Categoria_marcaRepository _categoria_marcaRepository;
        //private readonly Tipo_marcaRepository _tipo_marcaRepository;


        public MarcaController(IConfiguration configuration)
        {
            _marcaRepository = new MarcaRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_marcaRepository.FindAll());
        }

       

        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            //ViewBag.estadosList = _usuarioRepository.FindAll();
           // ViewBag.grupos_marcasList = _grupos_marcasRepository.FindAll();
          
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Marca obj)
        {
            
            _marcaRepository.Add(obj);

            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _usuarioRepository.FindAll();
               //
                Marca obj = _marcaRepository.FindByID(id.Value);
                if (obj.Ma_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Marca obj)
        {
            try
            {
                _marcaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _marcaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}