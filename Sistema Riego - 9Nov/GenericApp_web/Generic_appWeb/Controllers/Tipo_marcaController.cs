﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_marcaController : Controller
    {

        private readonly Tipo_marcaRepository _tipo_marcaRepository;
        //private readonly EstadosRepository _estadosRepository;
        private readonly Categoria_marcaRepository _categorias_marcasRepository;


        public Tipo_marcaController(IConfiguration configuration)
        {
            _tipo_marcaRepository = new Tipo_marcaRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
            _categorias_marcasRepository = new Categoria_marcaRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_tipo_marcaRepository.FindAll());
        }

       

        public IActionResult Create()
        {
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            ViewBag.categorias_marcasList = _categorias_marcasRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Tipo_marca tipo_marca)
        {
            _tipo_marcaRepository.Add(tipo_marca);

            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.categorias_marcasList = _categorias_marcasRepository.FindAll();
                Tipo_marca obj = _tipo_marcaRepository.FindByID(id.Value);
                if (obj.TMa_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Tipo_marca obj)
        {
            try
            {
                _tipo_marcaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _tipo_marcaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }







        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_tipobycategoria_marca(int CMa_id)
        {

            var categorias_marcas_marcasList = _tipo_marcaRepository.Getcmb_tipobycategoria_marca(CMa_id);
            return Json(categorias_marcas_marcasList);
        }










    }
}