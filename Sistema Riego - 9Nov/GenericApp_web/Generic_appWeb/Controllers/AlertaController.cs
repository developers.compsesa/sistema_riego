﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class AlertaController : Controller
    {


        private readonly AlertaRepository _alertaRepository;
        private readonly Grupo_alertaRepository _grupo_alertaRepository;
        private readonly Categoria_alertaRepository _categoria_alertaRepository;
        private readonly Tipo_alertaRepository _tipo_alertaRepository;
        private readonly HaciendaRepository _haciendaRepository;

        public AlertaController(IConfiguration configuration)
        {

            //_alertaRepository = new AlertaRepository(configuration);
            _grupo_alertaRepository = new Grupo_alertaRepository(configuration);
            _categoria_alertaRepository = new Categoria_alertaRepository(configuration);
            _tipo_alertaRepository = new Tipo_alertaRepository(configuration);           
            _haciendaRepository = new HaciendaRepository(configuration);

        }


        public IActionResult Index()
        {


            return View(_alertaRepository.FindAll());
        }

        public IActionResult Create()
        {
            if (ModelState.IsValid)
            {
                ViewBag.Grupo_alertaList = _grupo_alertaRepository.FindAll();
                ViewBag.HaciendaList = _haciendaRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            }
          


            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Alerta Alerta)
        {
            if (ModelState.IsValid)
            {
                _alertaRepository.Add(Alerta);

                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int GAl_id, int CAl_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.Grupo_alertaList = _grupo_alertaRepository.FindAll();
                ViewBag.Categoria_alertaList = _categoria_alertaRepository.Getcmb_categoriabygrupo_alerta(GAl_id);
                ViewBag.Tipo_alertaList = _tipo_alertaRepository.Getcmb_tipobycategoria_alerta(CAl_id);
                ViewBag.HaciendaList = _haciendaRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _usuarioRepository.FindAll();

                Alerta obj = _alertaRepository.FindByID(id.Value);
                if (obj.Al_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Alerta obj)
        {
            try
            {

                _alertaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _alertaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }




        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_alerta(int GAre_id)
        {
            var categorias_areasList = _categoria_alertaRepository.Getcmb_categoriabygrupo_alerta(GAre_id);
            return Json(categorias_areasList);
        }


        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_alerta(int CAre_id)
        {

            var categorias_areas_activosList = _tipo_alertaRepository.Getcmb_tipobycategoria_alerta(CAre_id);
            return Json(categorias_areas_activosList);
        }




      


        //public JsonResult GetAlertaByCodEmpresa(string codigo_empresa)
        //{
        //    var areas = _alertaRepository.(codigo_empresa);
        //    //return Json(new { success = true, message = "Order updated successfully" });
        //    if (areas != null)
        //    {
        //        return Json(new { success = true, message = "Successful", areas });
        //    }
        //    else
        //    {
        //        return Json(new { success = false, message = "Not Successful" });
        //    }

        //}






    }
}