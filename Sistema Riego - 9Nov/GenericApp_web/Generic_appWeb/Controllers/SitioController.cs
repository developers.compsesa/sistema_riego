﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class SitioController : Controller
    {


        //private readonly ProvinciasRepository _provinciasRepository;
        //private readonly CiudadesRepository _ciudadesRepository;
      
        //private readonly LugaresRepository _lugaresRepository;
        private readonly SitioRepository _sitiosRepository;
        //private readonly EstadosRepository _estadosRepository;
        private readonly Grupo_sitioRepository _grupos_sitiosRepository;
        private readonly Categoria_sitioRepository _categorias_sitiosRepository;
        private readonly Tipo_sitioRepository _tipos_sitiosRepository;
        private readonly EntidadRepository _entidadRepository;
        public SitioController(IConfiguration configuration)
        {

            //_provinciasRepository = new ProvinciasRepository(configuration);
            //_ciudadesRepository = new CiudadesRepository(configuration);
           
            //_lugaresRepository = new LugaresRepository(configuration);
            _sitiosRepository = new SitioRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);

            _grupos_sitiosRepository = new Grupo_sitioRepository(configuration);
            _categorias_sitiosRepository = new Categoria_sitioRepository(configuration);
            _tipos_sitiosRepository = new Tipo_sitioRepository(configuration);

        }


        public IActionResult Index()
        {


            return View(_sitiosRepository.FindAll());
        }

        public IActionResult Create()
        {
            if (ModelState.IsValid)
            {
                ViewBag.grupos_sitiosList = _grupos_sitiosRepository.FindAll();

                ViewBag.entidadesList = _entidadRepository.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            }
          


            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Sitio Sitio)
        {
            if (ModelState.IsValid)
            {
                _sitiosRepository.Add(Sitio);

                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int gsit_id, int csit_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.grupos_sitiosList = _grupos_sitiosRepository.FindAll();
                ViewBag.categorias_sitiosList = _categorias_sitiosRepository.Getcmb_categoriabygrupo_sitio(gsit_id);
                ViewBag.tipos_sitiosList = _tipos_sitiosRepository.Getcmb_tipobycategoria_sitio(csit_id);
                ViewBag.entidadesList = _entidadRepository.FindAll();


                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _estadosRepository.FindAll();

                Sitio obj = _sitiosRepository.FindByID(id.Value);
                if (obj.Si_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Sitio obj)
        {
            try
            {

                _sitiosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _sitiosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        public JsonResult GetallSitioByLugares(int Lu_id)
        {
            var SitioList = _sitiosRepository.GetallSitioByLugares(Lu_id);
            return Json(SitioList);
        }




        // comobo box obtener categorias por grupo
        public JsonResult GetallCategoriasBygrupo_sitios(int GSit_id)
        {
            var categorias_sitiosList = _categorias_sitiosRepository.Getcmb_categoriabygrupo_sitio(GSit_id);
            return Json(categorias_sitiosList);
        }


        // comobo box obtener tipos por categoria
        public JsonResult GetallTiposBycategoria_sitios(int CSit_id)
        {

            var categorias_sitios_activosList = _tipos_sitiosRepository.Getcmb_tipobycategoria_sitio(CSit_id);
            return Json(categorias_sitios_activosList);
        }




        public JsonResult GetallSitioBytipos_sitios(int TSit_id)
        {

            var SitioList = _sitiosRepository.GetallSitioBytipos_sitios(TSit_id);
            return Json(SitioList);
        }



        public JsonResult GetSitioByCodEmpresa(string codigo_empresa)
        {
            var sitios = _sitiosRepository.GetSitioByCodEmpresa(codigo_empresa);
            //return Json(new { success = true, message = "Order updated successfully" });
            if (sitios != null)
            {
                return Json(new { success = true, message = "Successful", sitios });
            }
            else
            {
                return Json(new { success = false, message = "Not Successful" });
            }

        }





    }
}