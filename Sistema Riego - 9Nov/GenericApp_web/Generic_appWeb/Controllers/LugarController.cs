﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class LugarController : Controller
    {
       
        private readonly Grupo_lugarRepository _grupos_lugaresRepository;
        private readonly Categoria_lugarRepository _categorias_lugaresRepository;
        private readonly Tipo_lugarRepository _tipos_lugaresRepository;
      
       
        private readonly ProvinciaRepository _provinciasRepository;
        private readonly CiudadRepository _ciudadesRepository;
       
        private readonly LugarRepository _lugaresRepository;
        private readonly SitioRepository _sitiosRepository;
        private readonly AreaRepository _areasRepository;



        private readonly EntidadRepository _entidadesRepository;




        public LugarController(IConfiguration configuration)
        {
            
            _grupos_lugaresRepository = new Grupo_lugarRepository(configuration);
            _categorias_lugaresRepository = new Categoria_lugarRepository(configuration);
            _tipos_lugaresRepository = new Tipo_lugarRepository(configuration);

            _provinciasRepository = new ProvinciaRepository(configuration);
            _ciudadesRepository = new CiudadRepository(configuration);
        
            _lugaresRepository = new LugarRepository(configuration);
            _sitiosRepository = new SitioRepository(configuration);
            _areasRepository = new AreaRepository(configuration);
            _entidadesRepository = new EntidadRepository(configuration);



        }


        public IActionResult Index()
        {
            return View(_lugaresRepository.FindAll());
        }

        //modal para buscar lugares
        public IActionResult PartialIndex(int grupo_lugar_id, int categoria_lugar_id, int tipo_lugar_id)
        {
            return PartialView(_lugaresRepository.FindAllLugarFiltered(grupo_lugar_id, categoria_lugar_id, tipo_lugar_id));
        }

        public IActionResult Create()
        {
            ViewBag.grupos_lugaresList = _grupos_lugaresRepository.FindAll();
            ViewBag.entidadesList = _entidadesRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            return View();
        }


        public IActionResult Asignar_sitios_Create()
        {
            ViewBag.grupos_lugaresList = _grupos_lugaresRepository.FindAll();
            //ViewBag.grupos_sitiosList = _grupos_sitiosRepository.FindAll(); 


            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Lugar obj)
        {

            _lugaresRepository.Add(obj);

            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int gr_id, int ca_id, int pr_id, int ci_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                ViewBag.grupos_lugaresList = _grupos_lugaresRepository.FindAll();
                ViewBag.categorias_lugaresList = _categorias_lugaresRepository.Getcmb_categoriabygrupo_lugar(gr_id);
                ViewBag.tipos_lugaresList = _tipos_lugaresRepository.Getcmb_tipobycategoria_lugar(ca_id);
                ViewBag.entidadesList = _entidadesRepository.FindAll();
                //ViewBag.provinciasList = _provinciasRepository.FindAll();
                //ViewBag.ciudadesList = _ciudadesRepository.GetallCiudadesbyProvincias(pr_id);

                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Lugar obj = _lugaresRepository.FindByID(id.Value);
                if (obj.Lu_id == 0) 
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Lugar obj)
        {
            try
            {

                _lugaresRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _lugaresRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        // comobo box obtener categorias por grupo
        public JsonResult GetallCategoriasBygrupo_lugares(int GL_id)
        {
            var categorias_lugaresList = _categorias_lugaresRepository.Getcmb_categoriabygrupo_lugar(GL_id);
            return Json(categorias_lugaresList);
        }
        
        // comobo box obtener tipos por categoria
        public JsonResult GetallTiposBycategoria_lugares(int CL_id)
        {

            var categorias_lugares_activosList = _tipos_lugaresRepository.Getcmb_tipobycategoria_lugar(CL_id);
            return Json(categorias_lugares_activosList);
        }

        public JsonResult GetallLugarBytipos_lugares(int TL_id )
        {

            var lugaresList = _lugaresRepository.GetallLugarBytipos_lugares(TL_id);
            return Json(lugaresList);
        }



        public JsonResult GetLugarByCodEmpresa(string codigo_empresa)
        {
            var lugares = _lugaresRepository.GetLugarByCodEmpresa(codigo_empresa);
            //return Json(new { success = true, message = "Order updated successfully" });
            if (lugares != null)
            {
                return Json(new { success = true, message = "Successful", lugares });
            }
            else
            {
                return Json(new { success = false, message = "Not Successful" });
            }

        }




        public JsonResult GetallCiudadesbyProvincias(int Pr_id)
        {

            var ciudadesList = _ciudadesRepository.GetallCiudadbyProvincias(Pr_id);
            return Json(ciudadesList);
        }

        public JsonResult GetallLugarByCiudades(int Ci_id)
        {
            var zonasList = _lugaresRepository.GetallLugarByCiudades(Ci_id);
            return Json(zonasList);
        }


        //public JsonResult GetallLugarByZonas(int Zo_id)
        //{
        //    var LugarList = _lugaresRepository.GetallLugarByZonas(Zo_id);
        //    return Json(LugarList);
        //}

        public JsonResult GetallSitiosByLugar(int Lu_id)
        {
            var LugarList =_sitiosRepository.GetallSitioByLugares(Lu_id);
            return Json(LugarList);
        }
        public JsonResult GetallAreasBySitios(int Si_id)
        {
            var AreasList = _areasRepository.GetallAreaByLugar(Si_id);
            return Json(AreasList);
        }

    }
}