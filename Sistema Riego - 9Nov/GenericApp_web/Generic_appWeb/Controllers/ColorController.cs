﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class ColorController : Controller
    {

        private readonly ColorRepository _coloresRepository;
        //private readonly UsuarioRepository _usuarioRepository;


        public ColorController(IConfiguration configuration)
        {


            _coloresRepository = new ColorRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_coloresRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Color Color)
        {
            //if (ModelState.IsValid)
            //{
            _coloresRepository.Add(Color);

            return RedirectToAction("Index");
            //}
            // return View(Color);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                Color obj = _coloresRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;

            }


        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Color obj)
        {
            try
            {
                _coloresRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _coloresRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }


    }
}