﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_equipoController : Controller
    {

        private readonly Categoria_equipoRepository _categorias_equipoRepository;
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_equipoRepository _grupos_equipoRepository;

        public Categoria_equipoController(IConfiguration configuration)
        {
            _categorias_equipoRepository = new Categoria_equipoRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
            _grupos_equipoRepository = new Grupo_equipoRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_categorias_equipoRepository.FindAll());
        }

        public IActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.grupos_equipoList = _grupos_equipoRepository.FindAll();
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_equipo obj)
        {
            _categorias_equipoRepository.Add(obj);

            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.grupos_equipoList = _grupos_equipoRepository.FindAll();
                Categoria_equipo obj = _categorias_equipoRepository.FindByID(id.Value);

                if (obj.CE_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_equipo obj)
        {
            try
            {
                _categorias_equipoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _categorias_equipoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }





        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_equipo(int GEn_id)
        {

            var categorias_equipos_equiposList = _categorias_equipoRepository.Getcmb_categoriabygrupo_equipo(GEn_id);
            return Json(categorias_equipos_equiposList);
        }















    }
}