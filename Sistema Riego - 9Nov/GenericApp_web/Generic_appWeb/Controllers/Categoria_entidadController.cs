﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_entidadController : Controller
    {
        private readonly Categoria_entidadRepository _categorias_entidadesRepository;
        private readonly Grupo_entidadRepository _grupos_entidadesRepository;
        //private readonly UsuarioRepository _usuarioRepository;
       

        public Categoria_entidadController(IConfiguration configuration)
        {

            _categorias_entidadesRepository = new Categoria_entidadRepository(configuration);
            _grupos_entidadesRepository = new Grupo_entidadRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_categorias_entidadesRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            ViewBag.grupos_entidadesList = _grupos_entidadesRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Categoria_entidad Categoria_entidad)
        {
            if (ModelState.IsValid)
            {
                _categorias_entidadesRepository.Add(Categoria_entidad);

                return RedirectToAction("Index");
            }
            return View(Categoria_entidad);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                ViewBag.grupos_entidadesList = _grupos_entidadesRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Categoria_entidad obj = _categorias_entidadesRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Categoria_entidad obj)
        {
            try
            {
                _categorias_entidadesRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _categorias_entidadesRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }





        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_entidad(int GEn_id)
        {
            
               var categorias_entidades_entidadesList = _categorias_entidadesRepository.Getcmb_categoriabygrupo_entidad(GEn_id);
            return Json(categorias_entidades_entidadesList);
        }


















    }
}