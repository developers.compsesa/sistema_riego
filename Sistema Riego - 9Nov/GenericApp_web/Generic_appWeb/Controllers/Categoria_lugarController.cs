﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_lugarController : Controller
    {

        private readonly Categoria_lugarRepository _categorias_LugaresRepository;
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_lugarRepository _grupos_lugaresRepository;
        private readonly EntidadRepository _entidadesRepository;

        public Categoria_lugarController(IConfiguration configuration)
        {
            _categorias_LugaresRepository = new Categoria_lugarRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
            _grupos_lugaresRepository = new Grupo_lugarRepository(configuration);
            _entidadesRepository = new EntidadRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_categorias_LugaresRepository.FindAll());
        }

        public IActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.grupos_lugaresList = _grupos_lugaresRepository.FindAll();
            ViewBag.entidadesList = _entidadesRepository.FindAll();
            //_entidadesRepository.FindAll2();

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_lugar obj)
        {
            _categorias_LugaresRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.grupos_lugaresList = _grupos_lugaresRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.entidadesList = _entidadesRepository.FindAll();
                Categoria_lugar obj = _categorias_LugaresRepository.FindByID(id.Value);

                if (obj.CL_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_lugar obj)
        {
            try
            {
                _categorias_LugaresRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _categorias_LugaresRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }







        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_lugar(int GEn_id)
        {

            var categorias_lugares_lugaresList = _categorias_LugaresRepository.Getcmb_categoriabygrupo_lugar(GEn_id);
            return Json(categorias_lugares_lugaresList);
        }




    }
}