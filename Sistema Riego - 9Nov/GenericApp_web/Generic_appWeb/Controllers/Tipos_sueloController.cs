﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_sueloController : Controller
    {

        private readonly Tipo_sueloRepository _tipo_sueloRepository;
        //private readonly EstadosRepository _estadosRepository;
       

        public Tipo_sueloController(IConfiguration configuration)
        {


            _tipo_sueloRepository = new Tipo_sueloRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_tipo_sueloRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _estadosRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tipo_suelo Tipo_suelo)
        {
            if (ModelState.IsValid)
            {
                _tipo_sueloRepository.Add(Tipo_suelo);

                return RedirectToAction("Index");
            }
            return View(Tipo_suelo);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Tipo_suelo obj = _tipo_sueloRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tipo_suelo obj)
        {
            try
            {
                _tipo_sueloRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _tipo_sueloRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        
    }
}