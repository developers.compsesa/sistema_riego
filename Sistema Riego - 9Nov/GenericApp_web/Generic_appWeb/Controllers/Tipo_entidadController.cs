﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_entidadController : Controller
    {

        private readonly Tipo_entidadRepository _tipos_entidadesRepository;
        private readonly Categoria_entidadRepository _categorias_entidadesRepository;
        //private readonly EstadosRepository _estadosRepository;
       

        public Tipo_entidadController(IConfiguration configuration)
        {

            _categorias_entidadesRepository = new Categoria_entidadRepository(configuration);
            _tipos_entidadesRepository = new Tipo_entidadRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_tipos_entidadesRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {

            //ViewBag.tipos_entidadesList = _tipos_entidadesRepository.FindAll();
            ViewBag.categorias_entidadesList = _categorias_entidadesRepository.FindAll();
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tipo_entidad Tipo_entidad)
        {
            if (ModelState.IsValid)
            {
                _tipos_entidadesRepository.Add(Tipo_entidad);

                return RedirectToAction("Index");
            }
            return View(Tipo_entidad);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                
                ViewBag.categorias_entidadesList = _categorias_entidadesRepository.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Tipo_entidad obj = _tipos_entidadesRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tipo_entidad obj)
        {
            try
            {
                _tipos_entidadesRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _tipos_entidadesRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_entidad(int CEn_id)
        {

            var categorias_documentos_documentosList = _tipos_entidadesRepository.Getcmb_tipobycategoria_entidad(CEn_id);
            return Json(categorias_documentos_documentosList);
        }



    }
}