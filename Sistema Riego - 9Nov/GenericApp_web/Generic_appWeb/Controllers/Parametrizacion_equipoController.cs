﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Parametrizacion_equipoController : Controller
    {
        

        private readonly EquipoRepository _equipoRepository;
        private readonly BloqueRepository _bloque_riegoRepository;
        private readonly Grupo_riegoRepository _grupos_riegoRepository;
        private readonly Grupo_valvulaRepository _grupos_valvulasRepository;
        private readonly Nivel_riegoRepository _nivel_riegoRepository;

        public Parametrizacion_equipoRepository _estructura_riegoRepository;



        public Parametrizacion_equipoController(IConfiguration configuration)
        {
            

            _equipoRepository = new EquipoRepository(configuration);
            _bloque_riegoRepository = new BloqueRepository(configuration);
            _grupos_riegoRepository = new Grupo_riegoRepository(configuration);
            _grupos_valvulasRepository = new Grupo_valvulaRepository(configuration);
            _nivel_riegoRepository = new Nivel_riegoRepository(configuration);

            _estructura_riegoRepository = new Parametrizacion_equipoRepository(configuration);

        }


        public IActionResult Index()
        {

            return View(_estructura_riegoRepository.FindAll());
        }


        public IActionResult Create()
        {
            ViewBag.equipoList = _equipoRepository.FindAll();
            ViewBag.bloque_riegoList = _bloque_riegoRepository.FindAll();
            ViewBag.grupo_riegoList = _grupos_riegoRepository.FindAll();
            ViewBag.grupo_valvulaList = _grupos_valvulasRepository.FindAll();
            ViewBag.nivel_riegoList = _nivel_riegoRepository.FindAll();




            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            return View();
        }





        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Parametrizacion_equipo obj)
        {

            _estructura_riegoRepository.Add(obj);

            return RedirectToAction("Index");
        }




        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                ViewBag.equipoList = _equipoRepository.FindAll();
                ViewBag.bloque_riegoList = _bloque_riegoRepository.FindAll();
                ViewBag.grupo_riegoList = _grupos_riegoRepository.FindAll();
                ViewBag.grupo_valvulaList = _grupos_valvulasRepository.FindAll();
                ViewBag.nivel_riegoList = _nivel_riegoRepository.FindAll();

                
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");


                Parametrizacion_equipo obj = _estructura_riegoRepository.FindByID(id.Value);

                if (obj.PEq_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }



        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Parametrizacion_equipo obj)
        {
            try
            {
                _estructura_riegoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _estructura_riegoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }









    }
}