﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class CiudadController : Controller
    {

        private readonly CiudadRepository _ciudadesRepository;
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly ProvinciaRepository _provinciasRepository;

        public CiudadController(IConfiguration configuration)
        {
            _ciudadesRepository = new CiudadRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
            _provinciasRepository = new ProvinciaRepository(configuration);

        }


        public IActionResult Index()
        {

            return View(_ciudadesRepository.FindAll());
        }

        public IActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.provinciasList = _provinciasRepository.FindAll();

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Ciudad obj)
        {
           
            _ciudadesRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.provinciasList= _provinciasRepository.FindAll();
                Ciudad obj = _ciudadesRepository.FindByID(id.Value);

                if (obj.Ci_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Ciudad obj)
        {
            try
            {
                _ciudadesRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _ciudadesRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}