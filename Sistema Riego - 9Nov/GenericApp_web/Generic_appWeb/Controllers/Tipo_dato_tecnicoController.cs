﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_dato_tecnicoController : Controller
    {

        private readonly Tipo_dato_tecnicoRepository _tipos_datos_tecnicosRepository;
        private readonly Categoria_dato_tecnicoRepository _categorias_datos_tecnicosRepository;
        //private readonly EstadosRepository _estadosRepository;
       

        public Tipo_dato_tecnicoController(IConfiguration configuration)
        {

            _categorias_datos_tecnicosRepository = new Categoria_dato_tecnicoRepository(configuration);
            _tipos_datos_tecnicosRepository = new Tipo_dato_tecnicoRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_tipos_datos_tecnicosRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {

            //ViewBag.tipos_entidadesList = _tipos_entidadesRepository.FindAll();
            ViewBag.categorias_datos_tecnicosList = _categorias_datos_tecnicosRepository.FindAll();
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tipo_dato_tecnico Tipos_datos_tecnicos)
        {
            if (ModelState.IsValid)
            {
                _tipos_datos_tecnicosRepository.Add(Tipos_datos_tecnicos);

                return RedirectToAction("Index");
            }
            return View(Tipos_datos_tecnicos);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                
                ViewBag.categorias_datos_tecnicosList = _categorias_datos_tecnicosRepository.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Tipo_dato_tecnico obj = _tipos_datos_tecnicosRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tipo_dato_tecnico obj)
        {
            try
            {
                _tipos_datos_tecnicosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _tipos_datos_tecnicosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_dato_tecnico(int CDTec_id)
        {

            var categorias_datos_tecnicosList = _tipos_datos_tecnicosRepository.Getcmb_tipobycategoria_dato_tecnico(CDTec_id);
            return Json(categorias_datos_tecnicosList);
        }



    }
}