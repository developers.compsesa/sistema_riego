﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_lugarController : Controller
    {

        private readonly Tipo_lugarRepository _tipos_lugaresRepository;
        //private readonly EstadosRepository _estadosRepository;
        private readonly Categoria_lugarRepository _categorias_lugaresRepository;
        public readonly EntidadRepository _entidadRepository;



        public Tipo_lugarController(IConfiguration configuration)
        {
            _tipos_lugaresRepository = new Tipo_lugarRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
            _categorias_lugaresRepository = new Categoria_lugarRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_tipos_lugaresRepository.FindAll());
        }

       

        public IActionResult Create()
        {
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            ViewBag.categorias_lugaresList = _categorias_lugaresRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.entidadesList = _entidadRepository.FindAll();
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Tipo_lugar tipos_lugares)
        {
            _tipos_lugaresRepository.Add(tipos_lugares);

            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.categorias_lugaresList = _categorias_lugaresRepository.FindAll();
                ViewBag.entidadesList = _entidadRepository.FindAll();
                Tipo_lugar obj = _tipos_lugaresRepository.FindByID(id.Value);
                if (obj.TL_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Tipo_lugar obj)
        {
            try
            {
                _tipos_lugaresRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _tipos_lugaresRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }




        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_tipobycategoria_lugar(int GEn_id)
        {

            var categorias_lugares_lugaresList = _tipos_lugaresRepository.Getcmb_tipobycategoria_lugar(GEn_id);
            return Json(categorias_lugares_lugaresList);
        }








    }
}