﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_marcaController : Controller
    {

        private readonly Categoria_marcaRepository _categorias_marcasRepository;
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_marcaRepository _grupos_marcasRepository;

        public Categoria_marcaController(IConfiguration configuration)
        {
            _categorias_marcasRepository = new Categoria_marcaRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
            _grupos_marcasRepository = new Grupo_marcaRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_categorias_marcasRepository.FindAll());
        }

        public IActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.grupos_marcasList = _grupos_marcasRepository.FindAll();
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_marca obj)
        {
            _categorias_marcasRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.grupos_marcasList = _grupos_marcasRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Categoria_marca obj = _categorias_marcasRepository.FindByID(id.Value);

                if (obj.CMa_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_marca obj)
        {
            try
            {
                _categorias_marcasRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _categorias_marcasRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }


        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_marca(int GEn_id)
        {

            var categorias_marcas_marcasList = _categorias_marcasRepository.Getcmb_categoriabygrupo_marca(GEn_id);
            return Json(categorias_marcas_marcasList);
        }







    }
}