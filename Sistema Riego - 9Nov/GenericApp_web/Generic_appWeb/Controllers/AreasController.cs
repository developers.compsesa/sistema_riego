﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class AreaController : Controller
    {


        private readonly AreaRepository _areaRepository;
        private readonly Grupo_areaRepository _grupo_areaRepository;
        private readonly Categoria_areaRepository _categoria_areaRepository;
        private readonly Tipo_areaRepository _tipo_areaRepository;
        private readonly EntidadRepository _entidadRepository;
        private readonly LugarRepository _lugarRepository;

        public AreaController(IConfiguration configuration)
        {

           
            _grupo_areaRepository = new Grupo_areaRepository(configuration);
            _categoria_areaRepository = new Categoria_areaRepository(configuration);
            _tipo_areaRepository = new Tipo_areaRepository(configuration);
            _lugarRepository = new LugarRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
            _areaRepository = new AreaRepository(configuration);

        }


        public IActionResult Index()
        {


            return View(_areaRepository.FindAll());
        }

        public IActionResult Create()
        {
            if (ModelState.IsValid)
            {
                ViewBag.grupos_areasList = _grupo_areaRepository.FindAll();
                ViewBag.entidadList = _entidadRepository.FindAll();
                ViewBag.lugaresList = _lugarRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            }
          


            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Area Area)
        {
            if (ModelState.IsValid)
            {
                _areaRepository.Add(Area);

                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int GAre_id, int CAre_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.grupos_areasList = _grupo_areaRepository.FindAll();
                ViewBag.categorias_areasList = _categoria_areaRepository.Getcmb_categoriabygrupo_area(GAre_id);
                ViewBag.tipos_areasList = _tipo_areaRepository.Getcmb_tipobycategoria_area(CAre_id);
                ViewBag.entidadesList = _entidadRepository.FindAll();
                ViewBag.lugaresList = _lugarRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _usuarioRepository.FindAll();

                Area obj = _areaRepository.FindByID(id.Value);
                if (obj.Are_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Area obj)
        {
            try
            {

                _areaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _areaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_area(int GAre_id)
        {
            var Categoria_areaList = _categoria_areaRepository.Getcmb_categoriabygrupo_area(GAre_id);
            return Json(Categoria_areaList);
        }


        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_area(int CAre_id)
        {

            var Tipo_areaList = _tipo_areaRepository.Getcmb_tipobycategoria_area(CAre_id);
            return Json(Tipo_areaList);
        }



        public JsonResult GetAreaByCodEmpresa(string codigo_empresa)
        {
            var areas = _areaRepository.GetAreaByCodEmpresa(codigo_empresa);
            //return Json(new { success = true, message = "Order updated successfully" });
            if (areas != null)
            {
                return Json(new { success = true, message = "Successful", areas });
            }
            else
            {
                return Json(new { success = false, message = "Not Successful" });
            }

        }






    }
}