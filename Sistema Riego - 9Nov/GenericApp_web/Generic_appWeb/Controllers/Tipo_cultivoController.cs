﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_cultivoController : Controller
    {


        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Categoria_cultivoRepository _Categorias_cultivosRepository;
        public Tipo_cultivoRepository _Tipo_cultivoRepository;


        public Tipo_cultivoController(IConfiguration configuration)
        {
            _Categorias_cultivosRepository = new Categoria_cultivoRepository(configuration);
            _Tipo_cultivoRepository = new Tipo_cultivoRepository(configuration);

            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Tipo_cultivoRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.Categoria_cultivoList = _Categorias_cultivosRepository.FindAll();

            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Tipo_cultivo obj)
        {

            _Tipo_cultivoRepository.Add(obj);

            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.Categoria_cultivoList = _Categorias_cultivosRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();

                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Tipo_cultivo obj = _Tipo_cultivoRepository.FindByID(id.Value);

                if (obj.TCul_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Tipo_cultivo obj)
        {
            try
            {
                _Tipo_cultivoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Tipo_cultivoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }






        // comobo box obtener Tipos por Categoria
        public JsonResult Getcmb_tipobycategoria_cultivo(int CCul_id)
        {
            var list = _Tipo_cultivoRepository.Getcmb_tipobycategoria_cultivo(CCul_id);
            return Json(list);
        }














    }
}