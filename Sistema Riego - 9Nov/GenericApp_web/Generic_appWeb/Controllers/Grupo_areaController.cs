﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_areaController : Controller
    {

       
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_areaRepository _Grupo_areaRepository;
        public readonly HaciendaRepository _haciendaRepository;

        public Grupo_areaController(IConfiguration configuration)
        {
           
            
            _Grupo_areaRepository = new Grupo_areaRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
            _haciendaRepository = new HaciendaRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Grupo_areaRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.haciendaList = _haciendaRepository.FindAll();

            return View();
        }


        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Grupo_area obj)
        {
            
            _Grupo_areaRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MMss");
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.haciendaList = _haciendaRepository.FindAll();
                Grupo_area obj = _Grupo_areaRepository.FindByID(id.Value);

                if (obj.GAre_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Grupo_area obj)
        {
            try
            {
                _Grupo_areaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Grupo_areaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}