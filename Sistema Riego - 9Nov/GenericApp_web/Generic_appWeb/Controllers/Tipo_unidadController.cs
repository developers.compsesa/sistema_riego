﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_unidadController : Controller
    {

        private readonly Tipo_unidadRepository _tipos_unidadesRepository;
        //private readonly EstadosRepository _estadosRepository;
        private readonly Categoria_unidadRepository _categorias_unidadesRepository;


        public Tipo_unidadController(IConfiguration configuration)
        {
            _tipos_unidadesRepository = new Tipo_unidadRepository(configuration);

            _categorias_unidadesRepository = new Categoria_unidadRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_tipos_unidadesRepository.FindAll());
        }



        public IActionResult Create()
        {

            ViewBag.Tipo_unidadList = _tipos_unidadesRepository.FindAll();
            ViewBag.categorias_unidadesList = _categorias_unidadesRepository.FindAll();
            //ViewBag.estadosList = _estadosRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Tipo_unidad Tipo_unidad)
        {
            _tipos_unidadesRepository.Add(Tipo_unidad);

            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }



                ViewBag.categorias_unidadesList = _categorias_unidadesRepository.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAll();
                ViewBag.Tipo_unidadList = _tipos_unidadesRepository.FindAll();
                Tipo_unidad obj = _tipos_unidadesRepository.FindByID(id.Value);
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                if (obj.TUn_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Tipo_unidad obj)
        {
            try
            {
                _tipos_unidadesRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _tipos_unidadesRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }





        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_tipobycategoria_unidad(int GEn_id)
        {

            var categorias_unidads_unidadsList = _tipos_unidadesRepository.Getcmb_tipobycategoria_unidad(GEn_id);
            return Json(categorias_unidads_unidadsList);
        }







    }
}