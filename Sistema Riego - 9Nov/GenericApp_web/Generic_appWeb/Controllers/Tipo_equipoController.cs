﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Generic_appWeb.Controllers
{
    public class Tipo_equipoController : Controller
    {

        private readonly Tipo_equipoRepository _tipo_equipoRepository;
        //private readonly EstadosRepository _estadosRepository;
        private readonly Categoria_equipoRepository _categorias_activosRepository;
        private readonly IHostingEnvironment _hostEnvironment;

        public Tipo_equipoController(IConfiguration configuration, IHostingEnvironment hostEnvironment)
        {
            _tipo_equipoRepository = new Tipo_equipoRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
            _categorias_activosRepository = new Categoria_equipoRepository(configuration);
            this._hostEnvironment = hostEnvironment;

        }


        public IActionResult Index()
        {

            return View(_tipo_equipoRepository.FindAll(3));
        }

       

        public IActionResult Create()
        {

            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            //ViewBag.colorList = _tipo_equipoRepository.FindAllColor();
            ViewBag.categorias_activosList = _categorias_activosRepository.FindAll();
            ViewBag.tipo_equipoList = _tipo_equipoRepository.FindAll(3);
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Tipo_equipo tipo_equipo)
        {
            #region SaveImage


                _tipo_equipoRepository.Add(tipo_equipo);
            
           
            #endregion SaveImage          

            return RedirectToAction("Index");


        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int tar_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.tipo_equipoList = _tipo_equipoRepository.FindAll(3);
                ViewBag.categorias_activosList = _categorias_activosRepository.FindAll();
                Tipo_equipo obj = _tipo_equipoRepository.FindByID(id.Value, tar_id);
                if (obj.TE_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Tipo_equipo obj)
        {
            try
            {
                _tipo_equipoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id, int tar_id, int tar_padre_id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _tipo_equipoRepository.Remove(id.Value, tar_id, tar_padre_id);
            return RedirectToAction("Index");
        }






        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_tipobycategoria_equipo(int GEn_id)
        {

            var categorias_entidades_entidadesList = _tipo_equipoRepository.Getcmb_tipobycategoria_equipo(GEn_id);
            return Json(categorias_entidades_entidadesList);
        }

    }
}