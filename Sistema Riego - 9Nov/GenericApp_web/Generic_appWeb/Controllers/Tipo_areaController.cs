﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_areaController : Controller
    {

        private readonly Categoria_areaRepository _categorias_areas;
        //private readonly EstadosRepository _estadosRepository;
        private readonly Tipo_areaRepository _tipos_areasRepository;
        public readonly EntidadRepository _entidadRepository;

        public Tipo_areaController(IConfiguration configuration)
        {
            _categorias_areas = new Categoria_areaRepository(configuration);
             //_estadosRepository = new EstadosRepository(configuration);
            _tipos_areasRepository = new Tipo_areaRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_tipos_areasRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.categorias_areasList = _categorias_areas.FindAll();
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.entidadesList = _entidadesRepository.FindAll2();

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Tipo_area obj)
        {
            
            _tipos_areasRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                ViewBag.categorias_areasList = _categorias_areas.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.entidadesList = _entidadesRepository.FindAll2();
                Tipo_area obj = _tipos_areasRepository.FindByID(id.Value);

                if (obj.TAre_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Tipo_area obj)
        {
            try
            {
                _tipos_areasRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _tipos_areasRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }


        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_area(int CAre_id)
        {

            var list = _tipos_areasRepository.Getcmb_tipobycategoria_area(CAre_id);
            return Json(list);
        }


    }
}