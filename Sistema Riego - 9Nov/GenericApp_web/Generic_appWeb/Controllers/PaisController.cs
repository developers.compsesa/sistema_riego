﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class PaisController : Controller
    {

        private readonly PaisRepository _paisRepository;
        //private readonly EstadosRepository _estadosRepository;
       

        public PaisController(IConfiguration configuration)
        {


            _paisRepository = new PaisRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_paisRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _estadosRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Pais Pais)
        {
            if (ModelState.IsValid)
            {
                _paisRepository.Add(Pais);

                return RedirectToAction("Index");
            }
            return View(Pais);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Pais obj = _paisRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Pais obj)
        {
            try
            {
                _paisRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _paisRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        
    }
}