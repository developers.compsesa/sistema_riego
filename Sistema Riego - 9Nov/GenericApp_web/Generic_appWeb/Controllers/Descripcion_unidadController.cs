﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Descripcion_unidadController : Controller
    {
        private readonly Descripcion_unidadRepository _descripcion_unidadRepository;
        private readonly Grupo_unidadRepository _grupo_unidadRepository;
        private readonly Categoria_unidadRepository _categoria_unidadRepository;
        private readonly Tipo_unidadRepository _tipo_unidadRepository;
       

        public Descripcion_unidadController(IConfiguration configuration)
        {
            _descripcion_unidadRepository = new Descripcion_unidadRepository(configuration);
            _grupo_unidadRepository = new Grupo_unidadRepository(configuration);
            _categoria_unidadRepository = new Categoria_unidadRepository(configuration);
            _tipo_unidadRepository = new Tipo_unidadRepository(configuration);
            

        }


        public IActionResult Index()
        {


            return View(_descripcion_unidadRepository.FindAll());
        }

        public IActionResult Create()
        {
            if (ModelState.IsValid)
            {
                ViewBag.Grupo_unidadList = _grupo_unidadRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            }
          


            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Descripcion_unidad Descripcion_unidad)
        {
            if (ModelState.IsValid)
            {
                _descripcion_unidadRepository.Add(Descripcion_unidad);

                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int gun_id, int cun_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.grupos_unidadesList = _grupo_unidadRepository.FindAll();
                ViewBag.categorias_unidadesList = _categoria_unidadRepository.Getcmb_categoriabygrupo_unidad(gun_id);
                ViewBag.tipos_unidadesList = _tipo_unidadRepository.Getcmb_tipobycategoria_unidad(cun_id);



                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosDispobilidadList = _usuarioRepository.FindAll();

                Descripcion_unidad obj = _descripcion_unidadRepository.FindByID(id.Value);
                if (obj.Un_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Descripcion_unidad obj)
        {
            try
            {

                _descripcion_unidadRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _descripcion_unidadRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult GetallCategoriasBygrupo_unidades(int GUn_id)
        {
            var categorias_unidadesList = _categoria_unidadRepository.Getcmb_categoriabygrupo_unidad(GUn_id);
            return Json(categorias_unidadesList);
        }


        // comobo box obtener tipos por categoria
        public JsonResult GetallTiposBycategoria_unidades(int CUn_id)
        {

            var categorias_unidadesList = _tipo_unidadRepository.Getcmb_tipobycategoria_unidad(CUn_id);
            return Json(categorias_unidadesList);
        }




        //public JsonResult GetallDescripcion_unidad()
        //{

        //    var unidades2List = _unidadRepository.FindAllDescripcion_unidad();
        //    return Json(unidades2List);
        //}



        //public JsonResult GetSitioByCodEmpresa(string codigo_empresa)
        //{
        //    var sitios = _unidadRepository.GetDescripcion_unidadByCodEmpresa(codigo_empresa);
        //    //return Json(new { success = true, message = "Order updated successfully" });
        //    if (sitios != null)
        //    {
        //        return Json(new { success = true, message = "Successful", sitios });
        //    }
        //    else
        //    {
        //        return Json(new { success = false, message = "Not Successful" });
        //    }

        //}





    }
}