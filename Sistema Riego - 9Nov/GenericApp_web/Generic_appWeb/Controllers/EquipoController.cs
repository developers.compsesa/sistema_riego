﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;

namespace Generic_appWeb.Controllers
{

    public class EquipoController : Controller
    {
        private readonly EquipoRepository _equipoRepository;
        private readonly StatusRepository _statusRepository;
        private readonly Grupo_equipoRepository _grupo_equipoRepository;
        private readonly Categoria_equipoRepository _categoria_equipoRepository;
        private readonly Tipo_equipoRepository _tipo_equipoRepository;
        private readonly MarcaRepository _marcasRepository;
        private readonly ModeloRepository _modelosRepository;
        private readonly ILogger _logger;

        public EquipoController(IConfiguration configuration, ILogger<EquipoController> logger)
        {
            //_logger = logger;
            _equipoRepository = new EquipoRepository(configuration);
            _statusRepository = new StatusRepository(configuration);
            _grupo_equipoRepository = new Grupo_equipoRepository(configuration);
            _categoria_equipoRepository = new Categoria_equipoRepository(configuration);
            _tipo_equipoRepository = new Tipo_equipoRepository(configuration);
            _marcasRepository = new MarcaRepository(configuration);
            _modelosRepository = new ModeloRepository(configuration);
        }



        public IActionResult Index()
        {
            try
            {
                return View(_equipoRepository.FindAll());
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }



        }



        // modal pára buscar equipo
        public IActionResult PartialIndex(int? grupo_activo_id, int? categoria_activo_id, int? tipo_activo_id)
        {
            try
            {
                return PartialView(_equipoRepository.FindAllFiltered(grupo_activo_id, categoria_activo_id, tipo_activo_id));
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }

        public IActionResult PartialIndex_multiSelect(int? grupo_equipo_id, int? categoria_equipo_id, int? tipo_equipo_id)
        {
            try
            {
                return PartialView(_equipoRepository.FindAllFiltered(grupo_equipo_id, categoria_equipo_id, tipo_equipo_id));
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }


        public IActionResult Create()
        {
            try
            {

                ViewBag.statusList = _statusRepository.FindAll();
                ViewBag.grupo_equipoList = _grupo_equipoRepository.FindAll();
                ViewBag.marca_equipoList = _marcasRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                return View();
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Equipo equipo)
        {
            try
            {

                //    if (equipo.Tipos_equipo.TA_id == 0)  //este if es paara cuando cambio de lugar en el front los tipos y sub tipos
                //    {
                //        // si no tiene subtipos un tipo, entonces el subtipo en el front end lo paso al tipo.
                //        equipo.Tipos_equipo.TA_id = equipo.Tipos_equipo.TA_Padre_id;
                //    }
                _equipoRepository.Add(equipo);

                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int ma_id, int ga_id, int ca_id, int ta_id, int a_tipo_equipo1_id, int a_tipo_equipo2_id, int a_tipo_equipo3_id, int a_tipo_equipo4_id, int a_tipo_equipo5_id, int a_tipo_equipo6_id, int a_tipo_equipo7_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                ViewBag.statusList = _statusRepository.FindAll();
                ViewBag.grupos_equipoList = _grupo_equipoRepository.FindAll();
                ViewBag.categorias_equipoList = _categoria_equipoRepository.Getcmb_categoriabygrupo_equipo(ga_id);

                ViewBag.marcas_equipoList = _marcasRepository.FindAll();
                //ViewBag.modelos_equipoList = _modelosRepository.GetallmodelosBymarca(ma_id);

                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Equipo obj = _equipoRepository.FindByID(id.Value);
                if (obj.Eq_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Equipo obj)
        {
            try
            {
                _equipoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                _equipoRepository.Remove(id.Value);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }

        // comobo box obtener modelos por marca
//        public JsonResult GetallmodelosBymarca(int Ma_id)
//        {
//            try
//            {
//                //var modelos_equipoList = _modelosRepository.GetallmodelosBymarca(Ma_id);
//                //return Json(modelos_equipoList);
//            }
//            catch (Exception e)
//            {
////                _logger.LogCritical(

//                    //  DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

//                    //);
//                throw;
//            }
            
//        }

        // comobo box obtener categorias por grupo
        public JsonResult GetallCategoriasBygrupo(int GA_id)
        {
            try
            {
                var categoria_equipo_equipoList = _categoria_equipoRepository.Getcmb_categoriabygrupo_equipo(GA_id);
                return Json(categoria_equipo_equipoList);
            }
            catch (Exception e)
            {
            //    _logger.LogCritical(

            //          DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

            //        );
                throw;
            }
           
        }


        // comobo box obtener tipos por categoria
        public JsonResult GetallTiposBycategoria(int CA_id)
        {
            try
            {
                var categoria_equipo_equipoList = _tipo_equipoRepository.Getcmb_tipobycategoria_equipo(CA_id);
                return Json(categoria_equipo_equipoList);
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }
            
        }

        // comobo box obtener equipo por tipo
        public JsonResult GetallEquipoBytipo(int TA_id)
        {
            try
            {
                var _equipo_equipoList = _equipoRepository.GetallEquipoBytipo(TA_id);
                return Json(_equipo_equipoList);
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }
           
        }

        public JsonResult GetActivoByCodEmpresa(string codigo_empresa)
        {
            var activo = _equipoRepository.GetActivosByCodEmpresa1(codigo_empresa);
            //return Json(new { success = true, message = "Order updated successfully" });
            if (activo != null)
            {
                return Json(new { success = true, message = "Successful", activo });
            }
            else
            {
                return Json(new { success = false, message = "Not Successful" });
            }

        }


    }
}