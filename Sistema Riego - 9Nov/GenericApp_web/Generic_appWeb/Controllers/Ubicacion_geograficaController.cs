﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Ubicacion_geograficaController : Controller
    {


        //private readonly EstadosRepository _estadosRepository;
        private readonly PaisRepository _paisRepository;
        private readonly ProvinciaRepository _provinciasRepository;
        private readonly CiudadRepository _ciudadesRepository;
        private readonly Ubicacion_geograficaRepository _ubicacion_geograficaRepository;

        public Ubicacion_geograficaController(IConfiguration configuration)
        {

            //_provinciasRepository = new ProvinciasRepository(configuration);
            //_ciudadesRepository = new CiudadesRepository(configuration);
           
            //_lugaresRepository = new LugaresRepository(configuration);
            
            //_estadosRepository = new EstadosRepository(configuration);

            _paisRepository = new PaisRepository(configuration);
            _provinciasRepository = new ProvinciaRepository(configuration);
            _ciudadesRepository = new CiudadRepository(configuration);
            _ubicacion_geograficaRepository = new Ubicacion_geograficaRepository(configuration);

        }


        public IActionResult Index()
        {


            return View(_ubicacion_geograficaRepository.FindAll());
        }

        public IActionResult Create()
        {
            if (ModelState.IsValid)
            {
                ViewBag.paisList = _paisRepository.FindAll();
                //ViewBag.categorias_valorList = _categorias_valorRepository.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            }
          


            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Ubicacion_geografica Ubicacion_geografica)
        {
            if (ModelState.IsValid)
            {
                _ubicacion_geograficaRepository.Add(Ubicacion_geografica);

                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int pa_id, int pr_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.paisList = _paisRepository.FindAll();
                ViewBag.provinciasList = _provinciasRepository.GetallProvinciabyPais(pa_id);
                ViewBag.ciudadesList = _ciudadesRepository.GetallCiudadbyProvincias(pr_id);
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosDispobilidadList = _estadosRepository.FindAllDisponibilidad();

                Ubicacion_geografica obj = _ubicacion_geograficaRepository.FindByID(id.Value);
                if (obj.Ug_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Ubicacion_geografica obj)
        {
            try
            {

                _ubicacion_geograficaRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _ubicacion_geograficaRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult GetallProvinciabyPais(int Pa_id)
        {
            var provinciasList = _provinciasRepository.GetallProvinciabyPais(Pa_id);
            return Json(provinciasList);
        }


        // comobo box obtener tipos por categoria
        public JsonResult GetallCiudadesbyProvincias(int Pr_id)
        {

            var ciudadesList = _ciudadesRepository.GetallCiudadbyProvincias(Pr_id);
            return Json(ciudadesList);
        }




        //public JsonResult GetallUnidades()
        //{

        //    var unidades2List = _unidadesRepository.FindAllUnidades();
        //    return Json(unidades2List);
        //}



        //public JsonResult GetSitioByCodEmpresa(string codigo_empresa)
        //{
        //    var sitios = _unidadesRepository.GetUnidadesByCodEmpresa(codigo_empresa);
        //    //return Json(new { success = true, message = "Order updated successfully" });
        //    if (sitios != null)
        //    {
        //        return Json(new { success = true, message = "Successful", sitios });
        //    }
        //    else
        //    {
        //        return Json(new { success = false, message = "Not Successful" });
        //    }

        //}





    }
}