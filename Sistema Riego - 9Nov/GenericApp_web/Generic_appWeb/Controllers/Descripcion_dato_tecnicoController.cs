﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Descripcion_dato_tecnicoController : Controller
    {

        private readonly Grupo_dato_tecnicoRepository _grupo_dato_tecnicoRepository;
        private readonly Categoria_dato_tecnicoRepository _categoria_dato_tecnicoRepository;
        private readonly Tipo_dato_tecnicoRepository _tipos_datos_tecnicosRepository;

        private readonly Grupo_valorRepository _grupos_valorRepository;
        private readonly Categoria_valorRepository _categorias_valorRepository;
        private readonly Tipo_valorRepository _tipos_valorRepository;


        private readonly Descripcion_dato_tecnicoRepository _inventario_descripciones_datos_tecnicosRepository;

        public Descripcion_dato_tecnicoController(IConfiguration configuration)
        {

            _grupo_dato_tecnicoRepository = new Grupo_dato_tecnicoRepository(configuration);
            _categoria_dato_tecnicoRepository = new Categoria_dato_tecnicoRepository(configuration);
            _tipos_datos_tecnicosRepository = new Tipo_dato_tecnicoRepository(configuration);
            _inventario_descripciones_datos_tecnicosRepository = new Descripcion_dato_tecnicoRepository(configuration);
          
            _grupos_valorRepository = new Grupo_valorRepository(configuration);
            _categorias_valorRepository = new Categoria_valorRepository(configuration);
            _tipos_valorRepository = new Tipo_valorRepository(configuration);

            

        }


        public IActionResult Index()
        {


            return View(_inventario_descripciones_datos_tecnicosRepository.FindAll());
        }


        public IActionResult PartialIndex(int? grupo_datos_tecnicos_id, int? categoria_dtos_tecnicos_id, int? tipo_datos_tecnicos_id)
        {
            try
            {
                return PartialView(_inventario_descripciones_datos_tecnicosRepository.FindAllFiltered(grupo_datos_tecnicos_id, categoria_dtos_tecnicos_id, tipo_datos_tecnicos_id));
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }

        public IActionResult PartialIndex_multiSelect(int? grupo_datos_tecnicos_id, int? categoria_datos_tecnicos_id, int? tipos_datos_tecnicos_id)
        {
            try
            {
                return PartialView(_inventario_descripciones_datos_tecnicosRepository.FindAllFiltered(grupo_datos_tecnicos_id, categoria_datos_tecnicos_id, tipos_datos_tecnicos_id));
            }
            catch (Exception e)
            {
                //_logger.LogCritical(

                //      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                //    );
                throw;
            }

        }


        public IActionResult Create()
        {
            if (ModelState.IsValid)
            {
                ViewBag.grupos_datos_tecnicosList = _grupo_dato_tecnicoRepository.FindAll();
                //ViewBag.categorias_datos_tecnicosList = _categoria_dato_tecnicoRepository.GetallCategorias_datos_tecnicosBygrupo(gdtec_id);
                //ViewBag.tipos_datos_tecnicosList = _tipos_datos_tecnicosRepository.GetallTiposByCategoria_datos_tecnicos(cdtec_id);                           

               /* ViewBag.estadosList = _usuarioRepository.FindAll()*/;
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            }



            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Descripcion_dato_tecnico Descripcion_dato_tecnico)
        {
            if (ModelState.IsValid)
            {
                _inventario_descripciones_datos_tecnicosRepository.Add(Descripcion_dato_tecnico);

                return RedirectToAction("Index");
            }
            _inventario_descripciones_datos_tecnicosRepository.Add(Descripcion_dato_tecnico);
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int gdtec_id, int cdtec_id, int tdtec_id, int un_id , int tval_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.grupos_datos_tecnicosList = _grupo_dato_tecnicoRepository.FindAll();
                ViewBag.categorias_datos_tecnicosList = _categoria_dato_tecnicoRepository.Getcmb_categoriabygrupo_dato_tecnico(gdtec_id);
                ViewBag.tipos_datos_tecnicosList = _tipos_datos_tecnicosRepository.Getcmb_tipobycategoria_dato_tecnico(cdtec_id);

        
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosDispobilidadList = _usuarioRepository.FindAll();

                Descripcion_dato_tecnico obj = _inventario_descripciones_datos_tecnicosRepository.FindByID(id.Value);
                if (obj.DDTec_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Descripcion_dato_tecnico obj)
        {
            try
            {

                _inventario_descripciones_datos_tecnicosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _inventario_descripciones_datos_tecnicosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_dato_tecnico(int GDTec_id)
        {
            var categorias_datos_tecnicosList = _categoria_dato_tecnicoRepository.Getcmb_categoriabygrupo_dato_tecnico(GDTec_id);
            return Json(categorias_datos_tecnicosList);
        }


        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_dato_tecnico(int CDTec_id)
        {

            var tipos_datos_tecnicosList = _tipos_datos_tecnicosRepository.Getcmb_tipobycategoria_dato_tecnico(CDTec_id);
            return Json(tipos_datos_tecnicosList);
        }




        //public JsonResult GetallUnidadesBytipos_unidades(int TUn_id)
        //{

        //    var UnidadesList = _unidadesRepository.GetallUnidadesByTipo(TUn_id);
        //    return Json(UnidadesList);
        //}



        //public JsonResult GetSitioByCodEmpresa(string codigo_empresa)
        //{
        //    var sitios = _unidadesRepository.GetUnidadesByCodEmpresa(codigo_empresa);
        //    //return Json(new { success = true, message = "Order updated successfully" });
        //    if (sitios != null)
        //    {
        //        return Json(new { success = true, message = "Successful", sitios });
        //    }
        //    else
        //    {
        //        return Json(new { success = false, message = "Not Successful" });
        //    }

        //}





    }
}