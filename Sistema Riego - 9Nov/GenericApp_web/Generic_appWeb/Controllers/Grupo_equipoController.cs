﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_equipoController : Controller
    {

        private readonly Grupo_equipoRepository _grupo_equipoRepository;
        //private readonly UsuarioRepository _usuarioRepository;


        public Grupo_equipoController(IConfiguration configuration)
        {
            _grupo_equipoRepository = new Grupo_equipoRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_grupo_equipoRepository.FindAll());
        }

       

        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.estadosList = _usuarioRepository.FindAll();
           // ViewBag.tipos_activosList = _grupo_equipoRepository.FindAll();
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Grupo_equipo obj)
        {
            
            _grupo_equipoRepository.Add(obj);

            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

                Grupo_equipo obj = _grupo_equipoRepository.FindByID(id.Value);
                if (obj.GE_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Grupo_equipo obj)
        {
            try
            {
                _grupo_equipoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _grupo_equipoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}