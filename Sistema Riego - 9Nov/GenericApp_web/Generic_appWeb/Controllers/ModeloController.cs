﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class ModeloController : Controller
    {

        private readonly ModeloRepository _modelosRepository;
        private readonly MarcaRepository _marcasRepository;
        //private readonly UsuarioRepository _usuarioRepository;


        public ModeloController(IConfiguration configuration)
        {
            _modelosRepository = new ModeloRepository(configuration);
            _marcasRepository = new MarcaRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_modelosRepository.FindAll());
        }

       

        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.marcas_activosList = _marcasRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();
           
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Modelo obj)
        {
          
            _modelosRepository.Add(obj);
          
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.marcas_activosList = _marcasRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
               
                Modelo obj = _modelosRepository.FindByID(id.Value);
                if (obj.Mo_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Modelo obj)
        {
            try
            {
                _modelosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _modelosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}