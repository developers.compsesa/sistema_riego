﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_riegoController : Controller
    {

        private readonly Grupo_riegoRepository _grupos_riegoRepository;
        //private readonly UsuarioRepository _usuarioRepository;
       

        public Grupo_riegoController(IConfiguration configuration)
        {


            _grupos_riegoRepository = new Grupo_riegoRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_grupos_riegoRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Grupo_riego Grupo_riego)
        {
            if (ModelState.IsValid)
            {
                _grupos_riegoRepository.Add(Grupo_riego);

                return RedirectToAction("Index");
            }
            return View(Grupo_riego);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Grupo_riego obj = _grupos_riegoRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Grupo_riego obj)
        {
            try
            {
                _grupos_riegoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _grupos_riegoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        
    }
}