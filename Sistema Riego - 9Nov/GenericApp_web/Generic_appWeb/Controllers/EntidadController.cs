﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class EntidadController : Controller
    {

        private readonly EntidadRepository _entidadRepository;
        private readonly Grupo_entidadRepository _grupos_entidadRepository;
        private readonly Categoria_entidadRepository _categorias_entidadRepository;
        private readonly Tipo_entidadRepository _tipos_entidadRepository;

        public EntidadController(IConfiguration configuration)
        {


            _entidadRepository = new EntidadRepository(configuration);
            _grupos_entidadRepository = new Grupo_entidadRepository(configuration);
            _categorias_entidadRepository = new Categoria_entidadRepository(configuration);
            _tipos_entidadRepository = new Tipo_entidadRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_entidadRepository.FindAll());
        }


        // modal pára buscar entidades
        public IActionResult PartialIndex(int? grupo_entidad_id, int? categoria_entidad_id, int? tipo_entidad_id)
        {
            return PartialView(_entidadRepository.FindAllFiltered(grupo_entidad_id, categoria_entidad_id, tipo_entidad_id));
        }



        // GET: Estados/Create
        public ActionResult Create()
        {

            ViewBag.gruposentidadesList = _grupos_entidadRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();

            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Entidad Entidades)
        {
            if (ModelState.IsValid)
            {
                _entidadRepository.Add(Entidades);

                return RedirectToAction("Index");
            }
            return View(Entidades);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id, int gen_id, int cen_id)
        {

            try
            {
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.grupos_entidadesList = _grupos_entidadRepository.FindAll();
                ViewBag.categorias_entidadesList = _categorias_entidadRepository.Getcmb_categoriabygrupo_entidad(gen_id);
                ViewBag.tipos_entidadesList = _tipos_entidadRepository.Getcmb_tipobycategoria_entidad(cen_id);
                //ViewBag.estadosDispobilidadList = _usuarioRepository.FindAll(); // 1 y 0
                Entidad obj = _entidadRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;

            }


        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Entidad obj)
        {
            try
            {
                _entidadRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _entidadRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }






        // comobo box obtener entidades by tipo
        public JsonResult GetallEntidadesByTipo(int id)
        {
            var result = _entidadRepository.GetallEntidadByTipo(id);
            return Json(result);
        }



        public JsonResult GetEntidadesByCodEmpresa(string codigo_empresa)
        {
            var entidades = _entidadRepository.GetEntidadByCodEmpresa(codigo_empresa);
            //return Json(new { success = true, message = "Order updated successfully" });
            if (entidades != null)
            {
                return Json(new { success = true, message = "Successful", entidades });
            }
            else
            {
                return Json(new { success = false, message = "Not Successful" });
            }

        }






    }
}