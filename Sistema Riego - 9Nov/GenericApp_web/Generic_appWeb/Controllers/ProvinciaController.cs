﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class ProvinciaController : Controller
    {

        private readonly ProvinciaRepository _provinciasRepository;
        private readonly PaisRepository _paisRepository;
        private readonly UsuarioRepository _usuarioRepository;


        public ProvinciaController(IConfiguration configuration)
        {
            _provinciasRepository = new ProvinciaRepository(configuration);
            _paisRepository = new PaisRepository(configuration);
            _usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_provinciasRepository.FindAll());
        }

       

        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.paisList = _paisRepository.FindAll();
            ViewBag.estadosList = _usuarioRepository.FindAll();
            // ViewBag.tipos_activosList = _provinciasRepository.FindAll();
            return View();
        }

        [HttpPost]
        public IActionResult Create(Provincia obj)
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            _provinciasRepository.Add(obj);

            return RedirectToAction("Index");

        }



        // POST: Provincia/Create
        [HttpPost]
        public IActionResult PartialCreate(Provincia obj)
        {
            ViewBag.estadosList = _usuarioRepository.FindAll();
            _provinciasRepository.Add(obj);

            return View();

        }

        public IActionResult PartialCreate()
        {

            ViewBag.estadosList = _usuarioRepository.FindAll();
            // ViewBag.tipos_activosList = _provinciasRepository.FindAll();
            return PartialView();
        }







        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.paisList = _paisRepository.FindAll();
                ViewBag.estadosList = _usuarioRepository.FindAll();
                //ViewBag.tipos_activosList = _provinciasRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Provincia obj = _provinciasRepository.FindByID(id.Value);
                if (obj.Pr_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Provincia obj)
        {
            try
            {
                _provinciasRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _provinciasRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}