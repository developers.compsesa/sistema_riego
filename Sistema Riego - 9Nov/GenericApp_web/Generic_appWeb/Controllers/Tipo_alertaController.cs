﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_alertaController : Controller
    {

        private readonly Categoria_alertaRepository _categorias_alertas;
        //private readonly EstadosRepository _estadosRepository;
        private readonly Tipo_alertaRepository _tipos_alertasRepository;
        public readonly IconoRepository _iconoRepository;
        public readonly ColorRepository _colorRepository;
        public Tipo_alertaController(IConfiguration configuration)
        {
            _categorias_alertas = new Categoria_alertaRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
            _tipos_alertasRepository = new Tipo_alertaRepository(configuration);
            _iconoRepository = new IconoRepository(configuration);
            _colorRepository = new ColorRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_tipos_alertasRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.categorias_alertasList = _categorias_alertas.FindAll();
            ViewBag.iconoList = _iconoRepository.FindAll();
            ViewBag.colorList = _colorRepository.FindAll();
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.iconoList = _iconoRepository.FindAll2();

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Tipo_alerta obj)
        {

            _tipos_alertasRepository.Add(obj);

            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                ViewBag.categorias_alertasList = _categorias_alertas.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.iconoList = _iconoRepository.FindAll();
                ViewBag.colorList = _colorRepository.FindAll();
                Tipo_alerta obj = _tipos_alertasRepository.FindByID(id.Value);

                if (obj.TAl_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Tipo_alerta obj)
        {
            try
            {
                _tipos_alertasRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _tipos_alertasRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }


        // comobo box obtener tipos por categoria
        public JsonResult Getcmb_tipobycategoria_alerta(int CAl_id)
        {

            var list = _tipos_alertasRepository.Getcmb_tipobycategoria_alerta(CAl_id);
            return Json(list);
        }


    }
}