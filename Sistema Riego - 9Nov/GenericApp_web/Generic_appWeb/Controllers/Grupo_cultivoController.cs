﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_cultivoController : Controller
    {

        private readonly Grupo_cultivoRepository _grupos_cultivoRepository;
        //private readonly UsuarioRepository _usuarioRepository;
       

        public Grupo_cultivoController(IConfiguration configuration)
        {


            _grupos_cultivoRepository = new Grupo_cultivoRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_grupos_cultivoRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Grupo_cultivo Grupo_cultivo)
        {
           // if (ModelState.IsValid)
           // {
                _grupos_cultivoRepository.Add(Grupo_cultivo);

                return RedirectToAction("Index");
           // }
           // return View(Grupo_cultivo);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

           // try
            //{
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Grupo_cultivo obj = _grupos_cultivoRepository.FindByID(id.Value);

                return View(obj);
           // }
           // catch (Exception)
           // {
            //    throw;
               
         //   }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Grupo_cultivo obj)
        {
            //try
            //{
                _grupos_cultivoRepository.Update(obj);
                return RedirectToAction("Index");
            //}
            //catch (Exception e)
            //{
            //    return View(obj);
           // }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _grupos_cultivoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        
    }
}