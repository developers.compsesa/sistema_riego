﻿using CsvHelper;
using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Generic_appWeb.Controllers
{
    public class PuntoController : Controller
    {
        private readonly Grupo_puntoRepository _grupos_puntosRepository;
        private readonly Categoria_puntoRepository _categorias_puntosRepository;
        private readonly Tipo_puntoRepository _tipos_puntosRepository;
        private readonly PuntoRepository _puntosRepository;

        //private readonly ProvinciasRepository _provinciasRepository;
        //private readonly CiudadesRepository _ciudadesRepository;
        //private readonly LugaresRepository _lugaresRepository;

        //private readonly Grupos_sitiosRepository _grupos_sitiosRepository;
        //private readonly Categorias_sitiosRepository _categorias_sitiosRepository;
        //private readonly Tipos_sitiosRepository _tipos_sitiosRepository;
        //private readonly SitiosRepository _sitiosRepository;

        //private readonly Grupos_areasRepository _grupos_areasRepository;
        //private readonly Categorias_areasRepository _categorias_areasRepository;
        //private readonly Tipos_areasRepository _tipos_areasRepository;
        //private readonly AreasRepository _areasRepository;

        private readonly ILogger _logger;



        //private readonly EstadosRepository _estadosRepository;

        public PuntoController(IConfiguration configuration, IHostingEnvironment hostingEnvironment, ILogger<EquipoController> logger)
        {
            _grupos_puntosRepository = new Grupo_puntoRepository(configuration);
            _categorias_puntosRepository = new Categoria_puntoRepository(configuration);
            _tipos_puntosRepository = new Tipo_puntoRepository(configuration);
            _puntosRepository = new PuntoRepository(configuration);

            //_provinciasRepository = new ProvinciasRepository(configuration);
            //_ciudadesRepository = new CiudadesRepository(configuration);
            //_lugaresRepository = new LugaresRepository(configuration);

            //_grupos_sitiosRepository = new Grupos_sitiosRepository(configuration);
            //_categorias_sitiosRepository = new Categorias_sitiosRepository(configuration);
            //_tipos_sitiosRepository = new Tipos_sitiosRepository(configuration);
            //_sitiosRepository = new SitiosRepository(configuration);


            //_grupos_areasRepository = new Grupos_areasRepository(configuration);
            //_categorias_areasRepository = new Categorias_areasRepository(configuration);
            //_tipos_areasRepository = new Tipos_areasRepository(configuration);
            //_areasRepository = new AreasRepository(configuration);

            _hostingEnvironment = hostingEnvironment;

            _logger = logger;
            //_estadosRepository = new EstadosRepository(configuration);
        }


        public IActionResult Index()
        {

            try
            {
                return View(_puntosRepository.FindAll());
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
           
        }

        private readonly IHostingEnvironment _hostingEnvironment;

        public async Task<IActionResult> Export(CancellationToken cancellationToken)
        {

            try
            {
                // query data from database  
                await Task.Yield();

                var stream = new MemoryStream();

                using (var package = new ExcelPackage(stream))
                {
                    var workSheet = package.Workbook.Worksheets.Add("JEARQUIA DE POSICIONES");
                    //workSheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Justify;



                    //workSheet.Cells.LoadFromCollection(_puntosRepository.FindAllToExport(), true);
                    package.Save();
                }

                stream.Position = 0;
                string excelName = $"POSICIONES-{DateTime.Now.ToString("yyyyMMddHHmmss")}.xlsx";

                //return File(stream, "application/octet-stream", excelName);  
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
           
        }



      
        public IActionResult Create()
        {
            try
            {
                ViewBag.grupos_puntosList = _grupos_puntosRepository.FindAll();
                //ViewBag.grupos_sitiosList = _grupos_sitiosRepository.FindAll();
                //ViewBag.grupos_areasList = _grupos_areasRepository.FindAll();
                //ViewBag.provinciasList = _provinciasRepository.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");


                return View();
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
           
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Punto Punto)
        {
            try
            {
                _puntosRepository.Add(Punto);

              
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
           

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int gpu_id, int cpu_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.grupos_puntosList = _grupos_puntosRepository.FindAll();
                ViewBag.categorias_puntosList = _categorias_puntosRepository.Getcmb_categoriabygrupo_punto(gpu_id);
                ViewBag.tipos_puntosList = _tipos_puntosRepository.Getcmb_tipobycategoria_punto(cpu_id);

                //ViewBag.provinciasList = _provinciasRepository.FindAll();
                //ViewBag.ciudadesList = _ciudadesRepository.GetallCiudadesbyProvincias(pr_id);
                //ViewBag.lugaresList = _lugaresRepository.GetallLugaresByCiudades(ci_id);

                //ViewBag.grupos_sitiosList = _grupos_sitiosRepository.FindAll();
                //ViewBag.categorias_sitiosList = _categorias_sitiosRepository.GetallCategoriasBygrupo_sitios(gsit_id);
                //ViewBag.tipos_sitiosList = _tipos_sitiosRepository.GetallTiposBycategoria_sitios(csit_id);
                // ViewBag.sitiosList = _sitiosRepository.GetallSitiosBytipos_sitios(tsit_id);


                //ViewBag.grupos_areasList = _grupos_areasRepository.FindAll();
                //ViewBag.categorias_areasList = _categorias_areasRepository.GetallCategoriasBygrupo_areas(gare_id);
                //ViewBag.tipos_areasList = _tipos_areasRepository.GetallTiposBycategoria_areas(care_id);
                //ViewBag.areasList = _areasRepository.GetallAreasBytipos_areas(tare_id);

                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _estadosRepository.FindAll();

                Punto obj = _puntosRepository.FindByID(id.Value);
                if (obj.Pu_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Punto obj)
        {
            try
            {

                _puntosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                _puntosRepository.Remove(id.Value);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
            
        }



        public JsonResult GetallPuntoBySitios(int Lu_id)
        {
            try
            {
                var PuntoList = _puntosRepository.GetallPuntoBySitios(Lu_id);
                return Json(PuntoList);
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
            
        }




        // comobo box obtener categorias por grupo
        public JsonResult GetallCategoriasBygrupo_puntos(int GPu_id)
        {
            try
            {
                var categorias_puntosList = _categorias_puntosRepository.Getcmb_categoriabygrupo_punto(GPu_id);
                return Json(categorias_puntosList);
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
           
        }


        // comobo box obtener tipos por categoria
        public JsonResult GetallTiposBycategoria_puntos(int CPu_id)
        {
            try
            {
                var categorias_puntos_activosList = _tipos_puntosRepository.Getcmb_tipobycategoria_punto(CPu_id);
                return Json(categorias_puntos_activosList);
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
           
        }





        public JsonResult GetallPuntoBytipos_puntos(int TPu_id)
        {
            try
            {
                var tipos_puntosList = _puntosRepository.GetallPuntoBytipos_puntos(TPu_id);
                return Json(tipos_puntosList);
            }
            catch (Exception e)
            {
                _logger.LogCritical(

                      DateTime.Now + "-CUSTOM_ERROR-" + e.ToString()

                    );
                throw;
            }
            
        }




        public JsonResult GetPuntoByCodEmpresa(string codigo_empresa)
        {
            var puntos = _puntosRepository.GetPuntoByCodEmpresa(codigo_empresa);
            //return Json(new { success = true, message = "Order updated successfully" });
            if (puntos != null)
            {
                return Json(new { success = true, message = "Successful", puntos });
            }
            else
            {
                return Json(new { success = false, message = "Not Successful" });
            }

        }



    }
}