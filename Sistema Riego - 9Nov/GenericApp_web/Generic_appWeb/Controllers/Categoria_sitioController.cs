﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Categoria_sitioController : Controller
    {

       
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_sitioRepository _Grupos_sitiosRepository;
        public Categoria_sitioRepository _Categoria_sitioRepository;
        public EntidadRepository _entidadesRepository;

        public Categoria_sitioController(IConfiguration configuration)
        {
            _Grupos_sitiosRepository = new Grupo_sitioRepository(configuration);
            _Categoria_sitioRepository = new Categoria_sitioRepository(configuration);
            _entidadesRepository = new EntidadRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Categoria_sitioRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.grupos_sitiosList = _Grupos_sitiosRepository.FindAll();
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.entidadesList = _entidadesRepository.FindAll();
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Categoria_sitio obj)
        {
           
            _Categoria_sitioRepository.Add(obj);
            
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.grupos_sitiosList = _Grupos_sitiosRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                ViewBag.entidadesList = _entidadesRepository.FindAll();
                Categoria_sitio obj = _Categoria_sitioRepository.FindByID(id.Value);

                if (obj.CSit_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Categoria_sitio obj)
        {
            try
            {
                _Categoria_sitioRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Categoria_sitioRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }







        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_categoriabygrupo_sitio(int GEn_id)
        {

            var categorias_sitios_sitiosList = _Categoria_sitioRepository.Getcmb_categoriabygrupo_sitio(GEn_id);
            return Json(categorias_sitios_sitiosList);
        }













    }
}