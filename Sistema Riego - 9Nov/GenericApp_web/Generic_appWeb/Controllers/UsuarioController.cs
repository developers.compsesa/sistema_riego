﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class UsuarioController : Controller
    {

        private readonly UsuarioRepository _usuariosRepository;
        //private readonly UsuarioRepository _usuarioRepository;
       

        public UsuarioController(IConfiguration configuration)
        {


            _usuariosRepository = new UsuarioRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_usuariosRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Usuario Usuario)
        {
            if (ModelState.IsValid)
            {
                _usuariosRepository.Add(Usuario);

                return RedirectToAction("Index");
            }
            return View(Usuario);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Usuario obj = _usuariosRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Usuario obj)
        {
            try
            {
                _usuariosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _usuariosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        
    }
}