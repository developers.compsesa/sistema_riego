﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class PendienteController : Controller
    {

        private readonly PendienteRepository _pendienteRepository;
        //private readonly EstadosRepository _estadosRepository;
       

        public PendienteController(IConfiguration configuration)
        {


            _pendienteRepository = new PendienteRepository(configuration);
            //_estadosRepository = new EstadosRepository(configuration);
        }

        // GET: Estados
        public IActionResult Index()
        {
            return View(_pendienteRepository.FindAll());
        }

        // GET: Estados/Create
        public ActionResult Create()
        {
            //ViewBag.estadosList = _estadosRepository.FindAll();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            return View();
        }

        // POST: Estados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Pendiente Pendiente)
        {
            if (ModelState.IsValid)
            {
                _pendienteRepository.Add(Pendiente);

                return RedirectToAction("Index");
            }
            return View(Pendiente);
        }

        // GET: Estados/Edit/5
        public ActionResult Edit(int? id)
        {

            try
            {
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                Pendiente obj = _pendienteRepository.FindByID(id.Value);
                return View(obj);
            }
            catch (Exception e)
            {
                throw;
               
            }
           

        }

        // POST: Estados/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Pendiente obj)
        {
            try
            {
                _pendienteRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET: Estados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _pendienteRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }

        
    }
}