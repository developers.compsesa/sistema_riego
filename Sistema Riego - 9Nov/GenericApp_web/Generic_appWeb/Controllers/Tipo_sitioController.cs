﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Tipo_sitioController : Controller
    {

        private readonly Categoria_sitioRepository _categorias_sitios;
        //private readonly EstadosRepository _estadosRepository;
        private readonly Tipo_sitioRepository _tipos_sitiosRepository;
        public readonly EntidadRepository _entidadRepository;

        public Tipo_sitioController(IConfiguration configuration)
        {
            _categorias_sitios = new Categoria_sitioRepository(configuration);
             //_estadosRepository = new EstadosRepository(configuration);
            _tipos_sitiosRepository = new Tipo_sitioRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_tipos_sitiosRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.categorias_sitiosList = _categorias_sitios.FindAll();
            //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            ViewBag.entidadesList = _entidadRepository.FindAll();


            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Tipo_sitio obj)
        {
            
            _tipos_sitiosRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                ViewBag.categorias_sitiosList = _categorias_sitios.FindAll();
                //ViewBag.estadosList = _estadosRepository.FindAllDisponibilidad();
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.entidadesList = _entidadesRepository.FindAll2();

                Tipo_sitio obj = _tipos_sitiosRepository.FindByID(id.Value);

                if (obj.TSit_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Tipo_sitio obj)
        {
            try
            {
                _tipos_sitiosRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _tipos_sitiosRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult Getcmb_tipobycategoria_sitio(int GEn_id)
        {

            var categorias_sitios_sitiosList = _tipos_sitiosRepository.Getcmb_tipobycategoria_sitio(GEn_id);
            return Json(categorias_sitios_sitiosList);
        }


    }
}