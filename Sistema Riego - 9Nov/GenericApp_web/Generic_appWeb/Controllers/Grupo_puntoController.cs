﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Grupo_puntoController : Controller
    {

       
        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_puntoRepository _Grupo_puntoRepository;
        public readonly EntidadRepository _entidadRepository;

        public Grupo_puntoController(IConfiguration configuration)
        {
           
            
            _Grupo_puntoRepository = new Grupo_puntoRepository(configuration);
            _entidadRepository = new EntidadRepository(configuration);
            //_usuarioRepository = new UsuarioRepository(configuration);
        }


        public IActionResult Index()
        {

            return View(_Grupo_puntoRepository.FindAll());
        }

        public IActionResult Create()
        {
            ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            //ViewBag.estadosList = _usuarioRepository.FindAll();
            ViewBag.entidadList = _entidadRepository.FindAll();

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Grupo_punto obj)
        {
            
            _Grupo_puntoRepository.Add(obj);
           
            return RedirectToAction("Index");
        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.entidadList = _entidadRepository.FindAll();

                Grupo_punto obj = _Grupo_puntoRepository.FindByID(id.Value);

                if (obj.GPu_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Grupo_punto obj)
        {
            try
            {
                _Grupo_puntoRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            _Grupo_puntoRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



    }
}