﻿using Generic_appWeb.Models;
using Generic_appWeb.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Generic_appWeb.Controllers
{
    public class Descripcion_valorController : Controller
    {


        //private readonly UsuarioRepository _usuarioRepository;
        private readonly Grupo_valorRepository _grupos_valorRepository;
        private readonly Categoria_valorRepository _categorias_valorRepository;
        private readonly Tipo_valorRepository _tipos_valorRepository;
        private readonly Descripcion_valorRepository _Inventario_valorRepository;

        public Descripcion_valorController(IConfiguration configuration)
        {

            //_provinciasRepository = new ProvinciasRepository(configuration);
            //_ciudadesRepository = new CiudadesRepository(configuration);
           
            //_lugaresRepository = new LugaresRepository(configuration);
            
            //_usuarioRepository = new UsuarioRepository(configuration);

            _grupos_valorRepository = new Grupo_valorRepository(configuration);
            _categorias_valorRepository = new Categoria_valorRepository(configuration);
            _tipos_valorRepository = new Tipo_valorRepository(configuration);
            _Inventario_valorRepository = new Descripcion_valorRepository(configuration);

        }


        public IActionResult Index()
        {


            return View(_Inventario_valorRepository.FindAll());
        }

        public IActionResult Create()
        {
            if (ModelState.IsValid)
            {
                ViewBag.grupos_valorList = _grupos_valorRepository.FindAll();
                //ViewBag.categorias_valorList = _categorias_valorRepository.FindAll();
                //ViewBag.estadosList = _usuarioRepository.FindAll();
                ViewBag.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
            }
          


            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public IActionResult Create(Descripcion_valor inventario_Valores)
        {
            if (ModelState.IsValid)
            {
                _Inventario_valorRepository.Add(inventario_Valores);

                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }

        // GET: /Customer/Edit/1
        public IActionResult Edit(int? id, int gval_id, int cval_id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }


                ViewBag.grupos_valorList = _grupos_valorRepository.FindAll();
                ViewBag.categorias_valorList = _categorias_valorRepository.GetallCategoria_valorBygrupo(gval_id);
                ViewBag.tipos_valorList = _tipos_valorRepository.GetallTipo_valorByCategoria(cval_id);
                ViewBag.fecha_modificacion = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");
                //ViewBag.estadosDispobilidadList = _usuarioRepository.FindAll();

                Descripcion_valor obj = _Inventario_valorRepository.FindByID(id.Value);
                if (obj.Val_id == 0)
                {
                    return NotFound();
                }
                return View(obj);
            }
            catch (Exception e)
            {
                return View();

            }
        }

        // POST: /Customer/Edit   
        [HttpPost]
        public IActionResult Edit(Descripcion_valor obj)
        {
            try
            {

                _Inventario_valorRepository.Update(obj);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(obj);
            }
        }

        // GET:/Customer/Delete/1
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _Inventario_valorRepository.Remove(id.Value);
            return RedirectToAction("Index");
        }



        // comobo box obtener categorias por grupo
        public JsonResult GetallCategoriasBygrupo_valor(int GVal_id)
        {
            var categorias_valorList = _categorias_valorRepository.GetallCategoria_valorBygrupo(GVal_id);
            return Json(categorias_valorList);
        }


        // comobo box obtener tipos por categoria
        public JsonResult GetallTiposBycategoria_valor(int CVal_id)
        {

            var categorias_valorList = _tipos_valorRepository.GetallTipo_valorByCategoria(CVal_id);
            return Json(categorias_valorList);
        }




        //public JsonResult GetallUnidades()
        //{

        //    var unidades2List = _unidadesRepository.FindAllUnidades();
        //    return Json(unidades2List);
        //}



        //public JsonResult GetSitioByCodEmpresa(string codigo_empresa)
        //{
        //    var sitios = _unidadesRepository.GetUnidadesByCodEmpresa(codigo_empresa);
        //    //return Json(new { success = true, message = "Order updated successfully" });
        //    if (sitios != null)
        //    {
        //        return Json(new { success = true, message = "Successful", sitios });
        //    }
        //    else
        //    {
        //        return Json(new { success = false, message = "Not Successful" });
        //    }

        //}





    }
}