﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.Net.Http.Headers;
using Microsoft.Extensions.Logging;

namespace Generic_appWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //compression
            services.AddResponseCompression();


            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.None;
            });

            services.AddResponseCaching(options =>
            { 
                options.UseCaseSensitivePaths = true;
                options.MaximumBodySize = 1024;
            });
            services.AddMvc();
                 
            //services.AddSingleton<IConfiguration>(Configuration);
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
                options.AutomaticAuthentication = false;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // other code remove for clarity 
            loggerFactory.AddFile("Logs/mylog-{Date}.txt");


            //compression
            app.UseResponseCompression();
            if (env.IsDevelopment())
            {
             
                 app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/CustomErrorPage");
            }
            else
            {
                app.UseExceptionHandler("/Home/CustomErrorPage");
                app.UseHsts();

            }

           
            app.UseCookiePolicy();
            app.UseStaticFiles();
            
            app.UseHttpsRedirection();
            app.UseResponseCaching();
          
            app.UseMvc(routes =>
            {
                  routes.MapRoute(
                  name: "default",
                  template: "{controller}/{action}/{id?}",
                  defaults: new { controller = "Home", action = "Login" });
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "Activos",
                  template: "{controller}/{action}",
                  defaults: new { controller = "Activos", action = "Index" });
            });
        }
    }
}
