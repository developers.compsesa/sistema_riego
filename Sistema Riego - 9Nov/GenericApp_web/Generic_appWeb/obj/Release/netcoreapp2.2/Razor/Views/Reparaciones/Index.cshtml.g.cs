#pragma checksum "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cf91abdf3620aa3358f6e420648dba029f1454c8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Reparaciones_Index), @"mvc.1.0.view", @"/Views/Reparaciones/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Reparaciones/Index.cshtml", typeof(AspNetCore.Views_Reparaciones_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb;

#line default
#line hidden
#line 2 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cf91abdf3620aa3358f6e420648dba029f1454c8", @"/Views/Reparaciones/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f06a2c7a514c84e177b74e75c87dcd856c572c53", @"/Views/_ViewImports.cshtml")]
    public class Views_Reparaciones_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Generic_appWeb.Models.Reparaciones>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-info"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onclick", new global::Microsoft.AspNetCore.Html.HtmlString("return confirm(\'Se eliminara el registro\');"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Js/Estados_index.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(56, 279, true);
            WriteLiteral(@"
<div class=""card card-success"">
    <div class=""card-header"">
        <section class=""content-header"">

            <h3>LISTA REPARACIONES</h3>
        </section>
    </div>
    <div class=""card-body"">
        <p>
            <button type=""button"" class=""btn btn-info""");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 335, "\"", 398, 3);
            WriteAttributeValue("", 345, "location.href=\'", 345, 15, true);
#line 12 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
WriteAttributeValue("", 360, Url.Action("Create", "Reparaciones"), 360, 37, false);

#line default
#line hidden
            WriteAttributeValue("", 397, "\'", 397, 1, true);
            EndWriteAttribute();
            BeginContext(399, 2417, true);
            WriteLiteral(@">Registrar</button>
        </p>
        <div class=""col-xl-12"">
            <table id=""Tablas"" class=""table table-bordered table-responsive"">
                <thead>
                    <tr>
                        <th colspan=""3""></th>
                        <th colspan=""3"" style=""text-align:center"">CODIGOS </th>
                        <th colspan=""5"" style=""text-align:center"">DESCRIPCIONES</th>
                        <th colspan=""2"" style=""text-align:center"">FECHAS</th>
                        <th colspan=""2"" style=""text-align:center"">USUARIOS</th>
                        <th colspan=""3""></th>
                    </tr>
                    <tr>
                        <th>GRUPO</th>
                        <th>CATEGORIA</th>
                        <th>TIPO</th>

                        <th> INTERNO</th>
                        <th> EMPRESA</th>
                        <th> ALTERNO</th>

                        <th> LARGA</th>
                        <th> MEDIANA</th>
            ");
            WriteLiteral(@"            <th> CORTA</th>
                        <th>ABREVIACION</th>
                        <th>OBSERVACION</th>



                        <th>CREACION </th>
                        <th>MODIFICACION</th>
                        <th>CREACION</th>
                        <th>MODIFICACION</th>
                        <th>ESTADO</th>
                        <th></th>
                        <th></th>
                    </tr>

                </thead>
                <thead class=""filters"">
                    <tr>
                        <th>GRUPO</th>
                        <th>CATEGORIA</th>
                        <th> TIPO</th>

                        <th> INTERNO</th>
                        <th> EMPRESA</th>
                        <th> ALTERNO</th>

                        <th> LARGA</th>
                        <th> MEDIANA</th>
                        <th> CORTA</th>
                        <th>ABREVIACION</th>
                        <th>OBSERVACION</th>



   ");
            WriteLiteral(@"                     <th>CREACION </th>
                        <th>MODIFICACION</th>
                        <th>CREACION</th>
                        <th>MODIFICACION</th>
                        <th>ESTADO</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

");
            EndContext();
#line 81 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                     foreach (var item in Model)
                    {

#line default
#line hidden
            BeginContext(2889, 96, true);
            WriteLiteral("                        <tr>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(2986, 77, false);
#line 85 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Grupos_reparaciones.GRep_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(3063, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3169, 81, false);
#line 89 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Categorias_reparaciones.CRep_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(3250, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3356, 76, false);
#line 93 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Tipos_reparaciones.TRep_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(3432, 111, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n\r\n\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3544, 41, false);
#line 100 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_id));

#line default
#line hidden
            EndContext();
            BeginContext(3585, 103, true);
            WriteLiteral("\r\n                            </td>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3689, 53, false);
#line 103 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_Codigo_empresa));

#line default
#line hidden
            EndContext();
            BeginContext(3742, 103, true);
            WriteLiteral("\r\n                            </td>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3846, 53, false);
#line 106 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_Codigo_alterno));

#line default
#line hidden
            EndContext();
            BeginContext(3899, 105, true);
            WriteLiteral("\r\n                            </td>\r\n                            <td>\r\n\r\n                                ");
            EndContext();
            BeginContext(4005, 56, false);
#line 110 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_Descripcion_larga));

#line default
#line hidden
            EndContext();
            BeginContext(4061, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4167, 54, false);
#line 114 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_Descripcion_med));

#line default
#line hidden
            EndContext();
            BeginContext(4221, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4327, 56, false);
#line 118 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(4383, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4489, 50, false);
#line 122 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_Abreviacion));

#line default
#line hidden
            EndContext();
            BeginContext(4539, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4645, 50, false);
#line 126 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_Observacion));

#line default
#line hidden
            EndContext();
            BeginContext(4695, 112, true);
            WriteLiteral("\r\n                            </td>\r\n \r\n\r\n\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4808, 53, false);
#line 133 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_fecha_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(4861, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4967, 57, false);
#line 137 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_fecha_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(5024, 107, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(5132, 55, false);
#line 142 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_usuario_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(5187, 107, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(5295, 59, false);
#line 147 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Rep_usuario_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(5354, 107, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(5462, 62, false);
#line 152 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Estados.E_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(5524, 107, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(5631, 245, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "cf91abdf3620aa3358f6e420648dba029f1454c817824", async() => {
                BeginContext(5866, 6, true);
                WriteLiteral("Editar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 157 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                                                       WriteLiteral(item.Rep_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 157 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                                                                                        WriteLiteral(item.Grupos_reparaciones.GRep_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["grep_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-grep_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["grep_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 157 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                                                                                                                                              WriteLiteral(item.Categorias_reparaciones.CRep_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["crep_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-crep_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["crep_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 157 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                                                                                                                                                                                                        WriteLiteral(item.Tipos_reparaciones.TRep_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["trep_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-trep_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["trep_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5876, 103, true);
            WriteLiteral("\r\n                            </td>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(5979, 140, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "cf91abdf3620aa3358f6e420648dba029f1454c822873", async() => {
                BeginContext(6107, 8, true);
                WriteLiteral("Eliminar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 160 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                                                                                WriteLiteral(item.Rep_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6119, 68, true);
            WriteLiteral("\r\n                            </td>\r\n                        </tr>\r\n");
            EndContext();
#line 163 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Reparaciones\Index.cshtml"
                    }

#line default
#line hidden
            BeginContext(6210, 86, true);
            WriteLiteral("                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(6319, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(6325, 45, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "cf91abdf3620aa3358f6e420648dba029f1454c826058", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(6370, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Generic_appWeb.Models.Reparaciones>> Html { get; private set; }
    }
}
#pragma warning restore 1591
