#pragma checksum "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0cfae54d5fbf16837b12fae510286db54dcf35fb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Grupos_actividades_Index), @"mvc.1.0.view", @"/Views/Grupos_actividades/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Grupos_actividades/Index.cshtml", typeof(AspNetCore.Views_Grupos_actividades_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb;

#line default
#line hidden
#line 2 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0cfae54d5fbf16837b12fae510286db54dcf35fb", @"/Views/Grupos_actividades/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f06a2c7a514c84e177b74e75c87dcd856c572c53", @"/Views/_ViewImports.cshtml")]
    public class Views_Grupos_actividades_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Generic_appWeb.Models.Grupos_actividades>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-info"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onclick", new global::Microsoft.AspNetCore.Html.HtmlString("return confirm(\'Se eliminara el registro\');"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Js/Grupos_actividades_index.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(62, 286, true);
            WriteLiteral(@"
<div class=""card card-success"">
    <div class=""card-header"">
        <section class=""content-header"">
            <h3>LISTA GRUPOS ACTIVIDADES </h3>
        </section>

    </div>
    <div class=""card-body"">
        <p>
            <button type=""button"" class=""btn btn-info""");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 348, "\"", 417, 3);
            WriteAttributeValue("", 358, "location.href=\'", 358, 15, true);
#line 12 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
WriteAttributeValue("", 373, Url.Action("Create", "Grupos_actividades"), 373, 43, false);

#line default
#line hidden
            WriteAttributeValue("", 416, "\'", 416, 1, true);
            EndWriteAttribute();
            BeginContext(418, 2133, true);
            WriteLiteral(@">Crear</button>
        </p>
        <div class=""col-xl-12"">
            <table id=""Tablas"" class=""table table-bordered table-responsive"" style=""width:100%"">
                <thead>
                    <tr>

                        <th colspan=""3"" style=""text-align:center"">CODIGOS </th>
                        <th colspan=""5"" style=""text-align:center"">DESCRIPCIONES</th>
                        <th colspan=""2"" style=""text-align:center"">FECHAS</th>
                        <th colspan=""2"" style=""text-align:center""> USUARIOS</th>
                        <th colspan=""5""></th>

                    </tr>
                    <tr>
                        <th> INTERNO</th>
                        <th> EMPRESA</th>
                        <th> ALTERNO</th>

                        <th> LARGA</th>
                        <th> MEDIANA</th>
                        <th> CORTA</th>
                        <th>ABREVIACION</th>
                        <th>OBSERVACION</th>

                        <th>C");
            WriteLiteral(@"REACION </th>
                        <th>MODIFICACION</th>
                        <th>CREACION</th>
                        <th>MODIFICACION</th>

                        <th>ESTADO</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <thead class=""filters"">
                    <tr>
                        <th> INTERNO</th>
                        <th> EMPRESA</th>
                        <th> ALTERNO</th>

                        <th> LARGA</th>
                        <th> MEDIANA</th>
                        <th> CORTA</th>
                        <th>ABREVIACION</th>
                        <th>OBSERVACION</th>

                        <th>CREACION </th>
                        <th>MODIFICACION</th>
                        <th>CREACION</th>
                        <th>MODIFICACION</th>

                        <th>ESTADO</th>
                        <th></th>
                        <th></");
            WriteLiteral("th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody>\r\n\r\n");
            EndContext();
#line 71 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                     foreach (var item in Model)
                    {

#line default
#line hidden
            BeginContext(2624, 96, true);
            WriteLiteral("                        <tr>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(2721, 41, false);
#line 75 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_id));

#line default
#line hidden
            EndContext();
            BeginContext(2762, 103, true);
            WriteLiteral("\r\n                            </td>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(2866, 53, false);
#line 78 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_Codigo_empresa));

#line default
#line hidden
            EndContext();
            BeginContext(2919, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3025, 53, false);
#line 82 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_Codigo_alterno));

#line default
#line hidden
            EndContext();
            BeginContext(3078, 107, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3186, 56, false);
#line 87 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_Descripcion_larga));

#line default
#line hidden
            EndContext();
            BeginContext(3242, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3348, 54, false);
#line 91 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_Descripcion_med));

#line default
#line hidden
            EndContext();
            BeginContext(3402, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3508, 56, false);
#line 95 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(3564, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3670, 50, false);
#line 99 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_Abreviacion));

#line default
#line hidden
            EndContext();
            BeginContext(3720, 105, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3826, 50, false);
#line 103 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_Observacion));

#line default
#line hidden
            EndContext();
            BeginContext(3876, 107, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(3984, 53, false);
#line 108 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_fecha_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(4037, 103, true);
            WriteLiteral("\r\n                            </td>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4141, 57, false);
#line 111 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_fecha_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(4198, 103, true);
            WriteLiteral("\r\n                            </td>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4302, 55, false);
#line 114 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_usuario_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(4357, 103, true);
            WriteLiteral("\r\n                            </td>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4461, 59, false);
#line 117 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.GAc_usuario_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(4520, 109, true);
            WriteLiteral("\r\n                            </td>\r\n\r\n\r\n\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4630, 62, false);
#line 123 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Estados.E_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(4692, 103, true);
            WriteLiteral("\r\n                            </td>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4795, 80, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0cfae54d5fbf16837b12fae510286db54dcf35fb16025", async() => {
                BeginContext(4865, 6, true);
                WriteLiteral("Editar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 126 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                                                       WriteLiteral(item.GAc_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4875, 103, true);
            WriteLiteral("\r\n                            </td>\r\n                            <td>\r\n                                ");
            EndContext();
            BeginContext(4978, 140, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0cfae54d5fbf16837b12fae510286db54dcf35fb18595", async() => {
                BeginContext(5106, 8, true);
                WriteLiteral("Eliminar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 129 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                                                                                WriteLiteral(item.GAc_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5118, 68, true);
            WriteLiteral("\r\n                            </td>\r\n                        </tr>\r\n");
            EndContext();
#line 132 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_actividades\Index.cshtml"
                    }

#line default
#line hidden
            BeginContext(5209, 94, true);
            WriteLiteral("                </tbody>\r\n\r\n            </table>\r\n\r\n        </div>\r\n\r\n    </div>\r\n\r\n</div>\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(5326, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(5332, 56, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0cfae54d5fbf16837b12fae510286db54dcf35fb21808", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(5388, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
            BeginContext(5393, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Generic_appWeb.Models.Grupos_actividades>> Html { get; private set; }
    }
}
#pragma warning restore 1591
