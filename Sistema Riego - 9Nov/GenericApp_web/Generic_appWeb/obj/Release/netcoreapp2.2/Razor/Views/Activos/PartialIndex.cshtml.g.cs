#pragma checksum "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "94cb200db4cda72ada4e02c3c2cbba94613a7bcf"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Activos_PartialIndex), @"mvc.1.0.view", @"/Views/Activos/PartialIndex.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Activos/PartialIndex.cshtml", typeof(AspNetCore.Views_Activos_PartialIndex))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb;

#line default
#line hidden
#line 2 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"94cb200db4cda72ada4e02c3c2cbba94613a7bcf", @"/Views/Activos/PartialIndex.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f06a2c7a514c84e177b74e75c87dcd856c572c53", @"/Views/_ViewImports.cshtml")]
    public class Views_Activos_PartialIndex : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Generic_appWeb.Models.Activos>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
  
    Layout = null;

#line default
#line hidden
            BeginContext(78, 1903, true);
            WriteLiteral(@"<div class=""card card-success"">
    <div class=""card-header"">
        <section class=""content-header"">

            <h3>LISTA ACTIVOS</h3>
        </section>
    </div>
    <div class=""card-body"">

        <table id=""PartialIndexactivo"" class=""table table-bordered table-responsive"">
            <thead>

                <tr>
                    <th>PROPIO</th>
                    <th>GRUPO</th>
                    <th>CATEGORIA</th>
                    <th> TIPO</th>
                    <th>SUB TIPO</th>
                    <th> INTERNO</th>
                    <th> EMPRESA</th>
                    <th> ALTERNO</th>

                    <th> LARGA</th>
                    <th> MEDIANA</th>
                    <th> CORTA</th>
                    <th>ABREVIACION</th>
                    <th>OBSERVACION</th>

                    <th>MARCA</th>
                    <th>MODELO</th>
                    <th>SERIAL</th>
                    <th>STATUS</th>


                    <th>ESTA");
            WriteLiteral(@"DO</th>
                </tr>

            </thead>
            <thead class=""filters"">
                <tr>
                    <th>PROPIO</th>
                    <th>GRUPO</th>
                    <th>CATEGORIA</th>
                    <th> TIPO</th>
                    <th>SUB TIPO</th>
                    <th> INTERNO</th>
                    <th> EMPRESA</th>
                    <th> ALTERNO</th>

                    <th> LARGA</th>
                    <th> MEDIANA</th>
                    <th> CORTA</th>
                    <th>ABREVIACION</th>
                    <th>OBSERVACION</th>

                    <th>MARCA</th>
                    <th>MODELO</th>
                    <th>SERIAL</th>
                    <th>STATUS</th>


                    <th>ESTADO</th>

                </tr>
            </thead>
            <tbody>

");
            EndContext();
#line 72 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                 foreach (var item in Model)
                {

#line default
#line hidden
            BeginContext(2046, 74, true);
            WriteLiteral("                <tr>\r\n                    <td>\r\n\r\n                        ");
            EndContext();
            BeginContext(2121, 13, false);
#line 77 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(item.A_propio);

#line default
#line hidden
            EndContext();
            BeginContext(2134, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2214, 70, false);
#line 80 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Grupos_activos.GA_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(2284, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2366, 74, false);
#line 84 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Categorias_activos.CA_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(2440, 65, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n\r\n\r\n                    <td>\r\n\r\n");
            EndContext();
#line 92 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                         if (String.IsNullOrEmpty(item.Tipos_activos.TA_Padre_Descripcion_corta))
                        {
                            

#line default
#line hidden
            BeginContext(2660, 69, false);
#line 94 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                       Write(Html.DisplayFor(modelItem => item.Tipos_activos.TA_Descripcion_corta));

#line default
#line hidden
            EndContext();
#line 94 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                                                                                                  
                        }
                        else
                        {
                            

#line default
#line hidden
            BeginContext(2844, 75, false);
#line 98 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                       Write(Html.DisplayFor(modelItem => item.Tipos_activos.TA_Padre_Descripcion_corta));

#line default
#line hidden
            EndContext();
#line 98 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                                                                                                        
                        }

#line default
#line hidden
            BeginContext(2948, 59, true);
            WriteLiteral("\r\n\r\n                    </td>\r\n                    <td>\r\n\r\n");
            EndContext();
#line 105 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                         if (String.IsNullOrEmpty(item.Tipos_activos.TA_Padre_Descripcion_corta))
                        {

                        }
                        else
                        {
                            

#line default
#line hidden
            BeginContext(3248, 69, false);
#line 111 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                       Write(Html.DisplayFor(modelItem => item.Tipos_activos.TA_Descripcion_corta));

#line default
#line hidden
            EndContext();
#line 111 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                                                                                                  
                        }

#line default
#line hidden
            BeginContext(3346, 85, true);
            WriteLiteral("\r\n\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3432, 39, false);
#line 119 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.A_id));

#line default
#line hidden
            EndContext();
            BeginContext(3471, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3551, 51, false);
#line 122 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.A_Codigo_empresa));

#line default
#line hidden
            EndContext();
            BeginContext(3602, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3682, 51, false);
#line 125 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.A_Codigo_alterno));

#line default
#line hidden
            EndContext();
            BeginContext(3733, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3813, 54, false);
#line 128 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.A_Descripcion_larga));

#line default
#line hidden
            EndContext();
            BeginContext(3867, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3949, 52, false);
#line 132 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.A_Descripcion_med));

#line default
#line hidden
            EndContext();
            BeginContext(4001, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4083, 54, false);
#line 136 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.A_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(4137, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4219, 48, false);
#line 140 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.A_Abreviacion));

#line default
#line hidden
            EndContext();
            BeginContext(4267, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4349, 48, false);
#line 144 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.A_Observacion));

#line default
#line hidden
            EndContext();
            BeginContext(4397, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4477, 62, false);
#line 147 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Marcas.Ma_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(4539, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4621, 63, false);
#line 151 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Modelos.Mo_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(4684, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4764, 43, false);
#line 154 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.A_Serial));

#line default
#line hidden
            EndContext();
            BeginContext(4807, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4887, 61, false);
#line 157 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.A_status_descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(4948, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(5028, 62, false);
#line 160 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Estados.E_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(5090, 54, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                </tr>\r\n");
            EndContext();
#line 164 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Activos\PartialIndex.cshtml"
                }

#line default
#line hidden
            BeginContext(5163, 1391, true);
            WriteLiteral(@"            </tbody>
        </table>
    </div>
</div>


<script type=""text/javascript"">
    var table_activo;
    $(document).ready(function () {
        // DataTable
        table_activo = $('#PartialIndexactivo').removeAttr('width').DataTable(optTable);
        $('#PartialIndexactivo .filters th').each(function () {

            var title = $('#PartialIndexactivo thead th').eq($(this).index()).text();
            $(this).html('<input type=""text"" placeholder=""Buscar"" />');
        });

        // Apply the search
        table_activo.columns().eq(0).each(function (colIdx) {
            $('input', $('.filters th')[colIdx]).on('keyup change', function () {
                table_activo
                    .column(colIdx)
                    .search(this.value)
                    .draw();
            });
        });


        // seleccionar 1
        $('#PartialIndexactivo tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(t");
            WriteLiteral(@"his).removeClass('selected');
            }
            else {
                table_activo.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                console.log(table_activo.row(this).data());
                var rowData = table_activo.row(this).data();
            }

        });

     
    });
</script>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Generic_appWeb.Models.Activos>> Html { get; private set; }
    }
}
#pragma warning restore 1591
