#pragma checksum "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b9a5c136157be4c7e1c3c585e87ea6f11ba40081"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Categorias_puntos_Index), @"mvc.1.0.view", @"/Views/Categorias_puntos/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Categorias_puntos/Index.cshtml", typeof(AspNetCore.Views_Categorias_puntos_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb;

#line default
#line hidden
#line 2 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b9a5c136157be4c7e1c3c585e87ea6f11ba40081", @"/Views/Categorias_puntos/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f06a2c7a514c84e177b74e75c87dcd856c572c53", @"/Views/_ViewImports.cshtml")]
    public class Views_Categorias_puntos_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Generic_appWeb.Models.Categorias_puntos>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-info"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onclick", new global::Microsoft.AspNetCore.Html.HtmlString("return confirm(\'Se eliminara el registro\');"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Js/Tipos_estados_index.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(61, 284, true);
            WriteLiteral(@"
<div class=""card card-success"">
    <div class=""card-header"">
        <section class=""content-header"">
            <h3>LISTA CATEGORIAS PUNTOS</h3>
        </section>

    </div>
    <div class=""card-body"">
        <p>
            <button type=""button"" class=""btn btn-info""");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 345, "\"", 413, 3);
            WriteAttributeValue("", 355, "location.href=\'", 355, 15, true);
#line 12 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
WriteAttributeValue("", 370, Url.Action("Create", "Categorias_puntos"), 370, 42, false);

#line default
#line hidden
            WriteAttributeValue("", 412, "\'", 412, 1, true);
            EndWriteAttribute();
            BeginContext(414, 2064, true);
            WriteLiteral(@">CREAR</button>
        </p>
        <table id=""Tablas"" class=""table table-bordered table-responsive"">
            <thead>
                <tr>
                    <th colspan=""1""></th>
                    <th colspan=""3"" style=""text-align:center"">CODIGOS </th>
                    <th colspan=""5"" style=""text-align:center"">DESCRIPCIONES</th>
                   
                    <th colspan=""2"" style=""text-align:center"">FECHAS</th>
                    <th colspan=""2"" style=""text-align:center"">USUARIOS</th>
                    <th colspan=""3""></th>
                </tr>
                <tr>
                    <th>GRUPO </th>
                    <th> INTERNO</th>
                    <th> EMPRESA</th>
                    <th> ALTERNO</th>

                    <th> LARGA</th>
                    <th> MEDIANA</th>
                    <th> CORTA</th>

                    <th>ABREVIACION</th>
                    <th>OBSERVACION</th>

                    <th>CREACION </th>
               ");
            WriteLiteral(@"     <th>MODIFICACION</th>
                    <th>CREACION</th>
                    <th>MODIFICACION</th>



                    <th>ESTADO</th>


                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <thead class=""filters"">
                <tr>
                    <th>GRUPO </th>
                    <th> INTERNO</th>
                    <th> EMPRESA</th>
                    <th> ALTERNO</th>

                    <th> LARGA</th>
                    <th> MEDIANA</th>
                    <th> CORTA</th>
                    
                    <th>ABREVIACION</th>
                    <th>OBSERVACION</th>

                    <th>CREACION </th>
                    <th>MODIFICACION</th>
                    <th>CREACION</th>
                    <th>MODIFICACION</th>



                    <th>ESTADO</th>

                    <th></th>
                    <th></th>
                </tr>
            </thead>
       ");
            WriteLiteral("     <tbody>\r\n\r\n");
            EndContext();
#line 81 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                 foreach (var item in Model)
                {

#line default
#line hidden
            BeginContext(2543, 72, true);
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2616, 70, false);
#line 85 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Grupos_puntos.GPu_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(2686, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2766, 41, false);
#line 88 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_id));

#line default
#line hidden
            EndContext();
            BeginContext(2807, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2889, 53, false);
#line 92 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_Codigo_empresa));

#line default
#line hidden
            EndContext();
            BeginContext(2942, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3022, 53, false);
#line 95 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_Codigo_alterno));

#line default
#line hidden
            EndContext();
            BeginContext(3075, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3155, 56, false);
#line 98 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_Descripcion_larga));

#line default
#line hidden
            EndContext();
            BeginContext(3211, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3293, 54, false);
#line 102 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_Descripcion_med));

#line default
#line hidden
            EndContext();
            BeginContext(3347, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3429, 56, false);
#line 106 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(3485, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3567, 50, false);
#line 110 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_Abreviacion));

#line default
#line hidden
            EndContext();
            BeginContext(3617, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3699, 50, false);
#line 114 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_Observacion));

#line default
#line hidden
            EndContext();
            BeginContext(3749, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3833, 53, false);
#line 119 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_fecha_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(3886, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3966, 57, false);
#line 122 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_fecha_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(4023, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4103, 55, false);
#line 125 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_usuario_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(4158, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4238, 59, false);
#line 128 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CPu_usuario_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(4297, 85, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4383, 62, false);
#line 134 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Estados.E_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(4445, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4524, 80, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b9a5c136157be4c7e1c3c585e87ea6f11ba4008115958", async() => {
                BeginContext(4594, 6, true);
                WriteLiteral("Editar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 137 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                                               WriteLiteral(item.CPu_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4604, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4683, 140, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b9a5c136157be4c7e1c3c585e87ea6f11ba4008118494", async() => {
                BeginContext(4811, 8, true);
                WriteLiteral("Eliminar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 140 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                                                                        WriteLiteral(item.CPu_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4823, 52, true);
            WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
            EndContext();
#line 143 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Categorias_puntos\Index.cshtml"
                }

#line default
#line hidden
            BeginContext(4894, 82, true);
            WriteLiteral("            </tbody>\r\n\r\n        </table>\r\n\r\n    </div>\r\n\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(4999, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(5005, 51, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b9a5c136157be4c7e1c3c585e87ea6f11ba4008121675", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(5056, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
            BeginContext(5061, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Generic_appWeb.Models.Categorias_puntos>> Html { get; private set; }
    }
}
#pragma warning restore 1591
