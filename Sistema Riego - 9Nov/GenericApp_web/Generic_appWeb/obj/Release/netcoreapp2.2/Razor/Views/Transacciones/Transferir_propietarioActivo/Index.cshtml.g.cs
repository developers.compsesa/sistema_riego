#pragma checksum "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9740179eee343c5ec372af2534a28531e8fbc3e3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Transacciones_Transferir_propietarioActivo_Index), @"mvc.1.0.view", @"/Views/Transacciones/Transferir_propietarioActivo/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Transacciones/Transferir_propietarioActivo/Index.cshtml", typeof(AspNetCore.Views_Transacciones_Transferir_propietarioActivo_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb;

#line default
#line hidden
#line 2 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9740179eee343c5ec372af2534a28531e8fbc3e3", @"/Views/Transacciones/Transferir_propietarioActivo/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f06a2c7a514c84e177b74e75c87dcd856c572c53", @"/Views/_ViewImports.cshtml")]
    public class Views_Transacciones_Transferir_propietarioActivo_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Generic_appWeb.ViewModels.Transferir_propietarioActivoViewModel>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-info"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onclick", new global::Microsoft.AspNetCore.Html.HtmlString("return confirm(\'Se eliminara el registro\');"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Js/CambiarStatustatus_index.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(85, 290, true);
            WriteLiteral(@"
<div class=""card card-success"">
    <div class=""card-header"">
        <section class=""content-header"">
            <h3>LISTA PROPIETARIOS DE ACTIVOS</h3>
        </section>

    </div>
    <div class=""card-body"">
        <p>
            <button type=""button"" class=""btn btn-info""");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 375, "\"", 454, 3);
            WriteAttributeValue("", 385, "location.href=\'", 385, 15, true);
#line 12 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
WriteAttributeValue("", 400, Url.Action("Create", "Transferir_propietarioActivo"), 400, 53, false);

#line default
#line hidden
            WriteAttributeValue("", 453, "\'", 453, 1, true);
            EndWriteAttribute();
            BeginContext(455, 4864, true);
            WriteLiteral(@">Crear</button>
        </p>
        <table id=""Tablas"" class=""table table-bordered table-responsive display"">
            <thead>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ");
            WriteLiteral(@"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ");
            WriteLiteral(@"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                <tr>
                    <th>CODIGO INTERNO</th>
                    <th>CODIGO EMPRESA</th>
                    <th>CODIGO ALTERNO</th>
                    <th>DESCRIPCION LARGA</th>
                    <th>DESCRIPCION MEDIANA</th>
                    <th>DESCRIPCION CORTA</th>
                    <th>ABREVIACION</th>
                    <th>OBSERVACION</th>

                    <th>GRUPO ACTIVO</th>
    ");
            WriteLiteral(@"                <th>CATEGORIA ACTIVO</th>
                    <th>TIPO ACTIVO</th>
                    <th>ACTIVO</th>

                    <th>TIPO ENTIDAD ANTERIOR</th>
                    <th>ENTIDAD ANTERIOR</th>

                    <th>TIPO ENTIDAD</th>
                    <th>ENTIDAD</th>

                    <th>FECHA CREACION</th>
                    <th>FECHA MODIFICACION</th>
                    <th>USUARIO CREACION</th>
                    <th>USUARIO MODIFICACION</th>

                    <th>ESTADO</th>

                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <thead class=""filters"">
                <tr>
                    <th>CODIGO INTERNO</th>
                    <th>CODIGO EMPRESA</th>
                    <th>CODIGO ALTERNO</th>
                    <th>DESCRIPCION LARGA</th>
                    <th>DESCRIPCION MEDIANA</th>
                    <th>DESCRIPCION CORTA</th>
                    <th>ABREVIACION<");
            WriteLiteral(@"/th>
                    <th>OBSERVACION</th>

                    <th>GRUPO ACTIVO</th>
                    <th>CATEGORIA ACTIVO</th>
                    <th>TIPO ACTIVO</th>
                    <th>ACTIVO</th>

                    <th>TIPO ENTIDAD ANTERIOR</th>
                    <th>ENTIDAD ANTERIOR</th>

                    <th>TIPO ENTIDAD</th>
                    <th>ENTIDAD</th>

                    <th>FECHA CREACION</th>
                    <th>FECHA MODIFICACION</th>
                    <th>USUARIO CREACION</th>
                    <th>USUARIO MODIFICACION</th>

                    <th>ESTADO</th>

                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

");
            EndContext();
#line 83 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                 foreach (var item in Model)
                {

#line default
#line hidden
            BeginContext(5384, 72, true);
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(5457, 41, false);
#line 87 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_id));

#line default
#line hidden
            EndContext();
            BeginContext(5498, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(5580, 53, false);
#line 91 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Codigo_empresa));

#line default
#line hidden
            EndContext();
            BeginContext(5633, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(5713, 53, false);
#line 94 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Codigo_alterno));

#line default
#line hidden
            EndContext();
            BeginContext(5766, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(5846, 56, false);
#line 97 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(5902, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(5984, 54, false);
#line 101 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Descripcion_med));

#line default
#line hidden
            EndContext();
            BeginContext(6038, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(6120, 56, false);
#line 105 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(6176, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(6258, 50, false);
#line 109 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Abreviacion));

#line default
#line hidden
            EndContext();
            BeginContext(6308, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(6388, 50, false);
#line 112 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Observacion));

#line default
#line hidden
            EndContext();
            BeginContext(6438, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(6522, 55, false);
#line 117 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GA_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(6577, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(6661, 55, false);
#line 122 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.CA_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(6716, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(6798, 55, false);
#line 126 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TA_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(6853, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(6937, 55, false);
#line 131 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TA_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(6992, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(7076, 65, false);
#line 136 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TEn_Descripcion_corta_anterior));

#line default
#line hidden
            EndContext();
            BeginContext(7141, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(7223, 64, false);
#line 140 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.En_Descripcion_corta_anterior));

#line default
#line hidden
            EndContext();
            BeginContext(7287, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(7369, 56, false);
#line 144 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TEn_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(7425, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(7507, 55, false);
#line 148 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.En_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(7562, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(7646, 53, false);
#line 153 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Fecha_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(7699, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(7781, 57, false);
#line 157 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Fecha_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(7838, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(7922, 55, false);
#line 162 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Usuario_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(7977, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(8059, 59, false);
#line 166 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TPA_Usuario_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(8118, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(8200, 54, false);
#line 170 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.E_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(8254, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(8333, 345, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9740179eee343c5ec372af2534a28531e8fbc3e323097", async() => {
                BeginContext(8668, 6, true);
                WriteLiteral("Editar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 173 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                                               WriteLiteral(item.TPA_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 173 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                                                                              WriteLiteral(item.GA_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["ga_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-ga_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["ga_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 173 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                                                                                                            WriteLiteral(item.CA_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["ca_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-ca_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["ca_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 173 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                                                                                                                                          WriteLiteral(item.TA_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["ta_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-ta_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["ta_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 173 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                                                                                                                                                                        WriteLiteral(item.A_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["a_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-a_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["a_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 173 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                                                                                                                                                                                                         WriteLiteral(item.TEn_id_anterior);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["te_id_ant"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-te_id_ant", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["te_id_ant"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 173 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                                                                                                                                                                                                                                                    WriteLiteral(item.En_id_anterior);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["e_id_ant"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-e_id_ant", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["e_id_ant"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 173 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                                                                                                                                                                                                                                                                                           WriteLiteral(item.TEn_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["te_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-te_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["te_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 173 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                                                                                                                                                                                                                                                                                                                         WriteLiteral(item.En_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["e_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-e_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["e_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(8678, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(8757, 140, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9740179eee343c5ec372af2534a28531e8fbc3e332713", async() => {
                BeginContext(8885, 8, true);
                WriteLiteral("Eliminar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 176 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                                                                        WriteLiteral(item.TPA_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(8897, 52, true);
            WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
            EndContext();
#line 179 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Transacciones\Transferir_propietarioActivo\Index.cshtml"
                }

#line default
#line hidden
            BeginContext(8968, 74, true);
            WriteLiteral("            </tbody>\r\n\r\n        </table>\r\n\r\n    </div>\r\n\r\n</div>\r\n\r\n\r\n\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(9065, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(9071, 56, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9740179eee343c5ec372af2534a28531e8fbc3e335928", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(9127, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Generic_appWeb.ViewModels.Transferir_propietarioActivoViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
