#pragma checksum "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "afe33cfaaeedd5d8fba3f07af5e7a8117fe3c52c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Tipos_ordenes_trabajo_Index), @"mvc.1.0.view", @"/Views/Tipos_ordenes_trabajo/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Tipos_ordenes_trabajo/Index.cshtml", typeof(AspNetCore.Views_Tipos_ordenes_trabajo_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb;

#line default
#line hidden
#line 2 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"afe33cfaaeedd5d8fba3f07af5e7a8117fe3c52c", @"/Views/Tipos_ordenes_trabajo/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f06a2c7a514c84e177b74e75c87dcd856c572c53", @"/Views/_ViewImports.cshtml")]
    public class Views_Tipos_ordenes_trabajo_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Generic_appWeb.Models.Tipos_ordenes_trabajo>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-info"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onclick", new global::Microsoft.AspNetCore.Html.HtmlString("return confirm(\'Se eliminara el registro\');"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Js/Activos_index.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(65, 288, true);
            WriteLiteral(@"
<div class=""card card-success"">
    <div class=""card-header"">
        <section class=""content-header"">

            <h3>LISTA TIPOS ORDENES TRABAJO</h3>
        </section>
    </div>
    <div class=""card-body"">
        <p>
            <button type=""button"" class=""btn btn-info""");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 353, "\"", 425, 3);
            WriteAttributeValue("", 363, "location.href=\'", 363, 15, true);
#line 12 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
WriteAttributeValue("", 378, Url.Action("Create", "Tipos_ordenes_trabajo"), 378, 46, false);

#line default
#line hidden
            WriteAttributeValue("", 424, "\'", 424, 1, true);
            EndWriteAttribute();
            BeginContext(426, 1600, true);
            WriteLiteral(@">CREAR</button>
        </p>
        <table id=""Tablas"" class=""table table-bordered table-responsive"">
            <thead>
                <tr>
                    <th> CATEGORIA</th>

                    <th> INTERNO</th>
                    <th> EMPRESA</th>
                    <th> ALTERNO</th>

                    <th> LARGA</th>
                    <th> MEDIANA</th>
                    <th> CORTA</th>
                    <th>ABREVIACION</th>
                    <th>OBSERVACION</th>

                    <th> CREACION </th>
                    <th> MODIFICACION</th>

                    <th> CREACION</th>
                    <th> MODIFICACION</th>

                    <th>ESTADO</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <thead class=""filters"">
                <tr>
                    <th> CATEGORIA</th>

                    <th> INTERNO</th>
                    <th> EMPRESA</th>
                 ");
            WriteLiteral(@"   <th> ALTERNO</th>

                    <th> LARGA</th>
                    <th> MEDIANA</th>
                    <th> CORTA</th>
                    <th>ABREVIACION</th>
                    <th>OBSERVACION</th>

                    <th> CREACION </th>
                    <th> MODIFICACION</th>

                    <th> CREACION</th>
                    <th> MODIFICACION</th>

                    <th>ESTADO</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
           
            <tbody>

");
            EndContext();
#line 68 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                 foreach (var item in Model)
                {

#line default
#line hidden
            BeginContext(2091, 74, true);
            WriteLiteral("                <tr>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2166, 86, false);
#line 73 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Categorias_ordenes_trabajo.COTrab_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(2252, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2334, 44, false);
#line 77 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_id));

#line default
#line hidden
            EndContext();
            BeginContext(2378, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2460, 56, false);
#line 81 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_Codigo_empresa));

#line default
#line hidden
            EndContext();
            BeginContext(2516, 85, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2602, 56, false);
#line 87 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_Codigo_alterno));

#line default
#line hidden
            EndContext();
            BeginContext(2658, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2740, 59, false);
#line 91 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_Descripcion_larga));

#line default
#line hidden
            EndContext();
            BeginContext(2799, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2879, 57, false);
#line 94 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_Descripcion_med));

#line default
#line hidden
            EndContext();
            BeginContext(2936, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3016, 59, false);
#line 97 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(3075, 81, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n\r\n                        ");
            EndContext();
            BeginContext(3157, 53, false);
#line 101 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_Abreviacion));

#line default
#line hidden
            EndContext();
            BeginContext(3210, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3292, 53, false);
#line 105 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_Observacion));

#line default
#line hidden
            EndContext();
            BeginContext(3345, 91, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n\r\n\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3437, 56, false);
#line 114 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_fecha_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(3493, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3575, 60, false);
#line 118 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_fecha_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(3635, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3719, 58, false);
#line 123 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_usuario_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(3777, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3861, 62, false);
#line 128 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TOTrab_usuario_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(3923, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4007, 62, false);
#line 133 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Estados.E_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(4069, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4152, 124, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "afe33cfaaeedd5d8fba3f07af5e7a8117fe3c52c15644", async() => {
                BeginContext(4266, 6, true);
                WriteLiteral("Editar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 138 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                                               WriteLiteral(item.TOTrab_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 138 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                                                                                     WriteLiteral(item.Estados.E_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["estado_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-estado_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["estado_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4276, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4355, 143, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "afe33cfaaeedd5d8fba3f07af5e7a8117fe3c52c18956", async() => {
                BeginContext(4486, 8, true);
                WriteLiteral("Eliminar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 141 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                                                                        WriteLiteral(item.TOTrab_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4498, 52, true);
            WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
            EndContext();
#line 144 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Tipos_ordenes_trabajo\Index.cshtml"
                }

#line default
#line hidden
            BeginContext(4569, 62, true);
            WriteLiteral("            </tbody>\r\n        </table>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(4654, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(4660, 45, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "afe33cfaaeedd5d8fba3f07af5e7a8117fe3c52c22108", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4705, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Generic_appWeb.Models.Tipos_ordenes_trabajo>> Html { get; private set; }
    }
}
#pragma warning restore 1591
