#pragma checksum "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3e185185877d58bbfed5f9a8f5e67aec4a40a1d8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Grupos_transacciones_Index), @"mvc.1.0.view", @"/Views/Grupos_transacciones/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Grupos_transacciones/Index.cshtml", typeof(AspNetCore.Views_Grupos_transacciones_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb;

#line default
#line hidden
#line 2 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3e185185877d58bbfed5f9a8f5e67aec4a40a1d8", @"/Views/Grupos_transacciones/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f06a2c7a514c84e177b74e75c87dcd856c572c53", @"/Views/_ViewImports.cshtml")]
    public class Views_Grupos_transacciones_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Generic_appWeb.Models.Grupos_transacciones>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-info"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onclick", new global::Microsoft.AspNetCore.Html.HtmlString("return confirm(\'Se eliminara el registro\');"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Js/Grupos_transacciones_index.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(64, 287, true);
            WriteLiteral(@"
<div class=""card card-success"">
    <div class=""card-header"">
        <section class=""content-header"">

            <h3>LISTA GRUPOS transacciones</h3>
        </section>
    </div>
    <div class=""card-body"">
        <p>
            <button type=""button"" class=""btn btn-info""");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 351, "\"", 422, 3);
            WriteAttributeValue("", 361, "location.href=\'", 361, 15, true);
#line 12 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
WriteAttributeValue("", 376, Url.Action("Create", "Grupos_transacciones"), 376, 45, false);

#line default
#line hidden
            WriteAttributeValue("", 421, "\'", 421, 1, true);
            EndWriteAttribute();
            BeginContext(423, 1935, true);
            WriteLiteral(@">CREAR</button>
        </p>
        <table id=""Tablas"" class=""table table-bordered table-responsive"">
            <thead>
                <tr>

                    <th colspan=""3"" style=""text-align:center"">CODIGOS </th>

                    <th colspan=""5"" style=""text-align:center"">DESCRIPCIONES</th>
                   
                    <th colspan=""2"" style=""text-align:center"">FECHAS</th>
                    <th colspan=""2"" style=""text-align:center""> USUARIOS</th>
                    <th colspan=""3""></th>


                </tr>
                <tr>
                    <th> INTERNO</th>
                    <th> EMPRESA</th>
                    <th> ALTERNO</th>
                    <th> LARGA</th>
                    <th> MEDIANA</th>
                    <th> CORTA</th>
                    <th>ABREVIACION</th>
                    <th>OBSERVACION</th>


                    <th> CREACION </th>
                    <th> MODIFICACION</th>
                    <th> CREACION</th>
   ");
            WriteLiteral(@"                 <th> MODIFICACION</th>
                    <th>ESTADO</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <thead class=""filters"">
                <tr>
                    <th> INTERNO</th>
                    <th> EMPRESA</th>
                    <th> ALTERNO</th>
                    <th> LARGA</th>
                    <th> MEDIANA</th>
                    <th> CORTA</th>
                    <th>ABREVIACION</th>
                    <th>OBSERVACION</th>


                    <th> CREACION </th>
                    <th> MODIFICACION</th>
                    <th> CREACION</th>
                    <th> MODIFICACION</th>
                    <th>ESTADO</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
           
            <tbody>

");
            EndContext();
#line 72 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                 foreach (var item in Model)
                {

#line default
#line hidden
            BeginContext(2423, 72, true);
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2496, 40, false);
#line 76 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_id));

#line default
#line hidden
            EndContext();
            BeginContext(2536, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2618, 52, false);
#line 80 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_Codigo_empresa));

#line default
#line hidden
            EndContext();
            BeginContext(2670, 85, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2756, 52, false);
#line 86 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_Codigo_alterno));

#line default
#line hidden
            EndContext();
            BeginContext(2808, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2890, 55, false);
#line 90 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_Descripcion_larga));

#line default
#line hidden
            EndContext();
            BeginContext(2945, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3025, 53, false);
#line 93 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_Descripcion_med));

#line default
#line hidden
            EndContext();
            BeginContext(3078, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3158, 55, false);
#line 96 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_Descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(3213, 81, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n\r\n                        ");
            EndContext();
            BeginContext(3295, 49, false);
#line 100 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_Abreviacion));

#line default
#line hidden
            EndContext();
            BeginContext(3344, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3426, 49, false);
#line 104 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_Observacion));

#line default
#line hidden
            EndContext();
            BeginContext(3475, 109, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                  \r\n\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3585, 52, false);
#line 113 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_fecha_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(3637, 91, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n\r\n\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3729, 56, false);
#line 122 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_fecha_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(3785, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3869, 54, false);
#line 127 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_usuario_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(3923, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4007, 58, false);
#line 132 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.GT_usuario_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(4065, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4149, 62, false);
#line 137 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Estados.E_Descripcion_larga));

#line default
#line hidden
            EndContext();
            BeginContext(4211, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4294, 120, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3e185185877d58bbfed5f9a8f5e67aec4a40a1d815441", async() => {
                BeginContext(4404, 6, true);
                WriteLiteral("Editar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 142 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                                               WriteLiteral(item.GT_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#line 142 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                                                                                 WriteLiteral(item.Estados.E_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["estaGT_id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-estaGT_id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["estaGT_id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4414, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(4493, 139, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3e185185877d58bbfed5f9a8f5e67aec4a40a1d818743", async() => {
                BeginContext(4620, 8, true);
                WriteLiteral("Eliminar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 145 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                                                                        WriteLiteral(item.GT_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4632, 52, true);
            WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
            EndContext();
#line 148 "C:\Users\vicente.pinargote\Desktop\repos\GenericApp_web\Generic_appWeb\Views\Grupos_transacciones\Index.cshtml"
                }

#line default
#line hidden
            BeginContext(4703, 62, true);
            WriteLiteral("            </tbody>\r\n        </table>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(4788, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(4794, 58, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3e185185877d58bbfed5f9a8f5e67aec4a40a1d821889", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4852, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Generic_appWeb.Models.Grupos_transacciones>> Html { get; private set; }
    }
}
#pragma warning restore 1591
