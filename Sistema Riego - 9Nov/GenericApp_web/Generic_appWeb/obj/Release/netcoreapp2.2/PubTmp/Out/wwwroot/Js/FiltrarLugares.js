﻿//create by israel pinargote 2018


$('#Activos_Grupos_activos_GA_id').change(function () {
    //var url = '@Url.Content("~/")' + "Asignar_activos/GetallCategoriasBygrupo";
    var ddlsource = "#Activos_Grupos_activos_GA_id";
    var Ga_id = $(ddlsource).val();
    if (Ga_id != "Seleccione") {
        $.getJSON("/Asignar_activos/GetallCategoriasBygrupo", { GA_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>"
            $("#CA_id").empty();
            $("#Tipos_activos_TA_Padre_id").empty();
            $('#Tipos_activos_TA_Padre_id').html(items);
            $("#Tipos_activos_TA_id").empty();
            $('#Tipos_activos_TA_id').html(items);

            $("#select2-CA_id-container").html("<option value= " + 0 + ">" + "Seleccione</option>");
            $("#select2-Tipos_activos_TA_Padre_id-container").html("<option value= " + 0 + ">" + "Seleccione</option>");
            $("#select2-Tipos_activos_TA_id-container").html("<option value= " + 0 + ">" + "Seleccione</option>");


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cA_id + "'>" + modelo.cA_Descripcion_larga + "</option>";
            });
            $('#CA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#CA_id').html(items);
    }
});


$('#CA_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallTiposBycategoria";
    var ddlsource = "#CA_id";
    var Ca_id = $(ddlsource).val();
    if (Ca_id != "Seleccione") {
        $.getJSON("/Activos/GetallTiposBycategoria", { CA_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_activos_TA_Padre_id").empty();
            $('#Tipos_activos_TA_Padre_id').html(items);
            $("#Tipos_activos_TA_id").empty();
            $('#Tipos_activos_TA_id').html(items);
            $("#Tipos_activos_TA_Padre_id-container").html("<option value= " + 0 + ">" + "Seleccione</option>");
            $("#Tipos_activos_TA_id-container").html("<option value= " + 0 + ">" + "Seleccione</option>");

            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tA_id + "'>" + modelo.tA_Descripcion_larga + "</option>";
            });
            $('#Tipos_activos_TA_Padre_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#Tipos_activos_TA_Padre_id').html(items);
    }
});


$('#Tipos_activos_TA_Padre_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetSubTipos";
    var ddlsource = "#Tipos_activos_TA_Padre_id";
    var ta_id = $(ddlsource).val();
    if (ta_id != "Seleccione") {
        $.getJSON("/Activos/GetSubTipos", { ta_padre_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_activos_TA_id").empty();
            $('#Tipos_activos_TA_id').html(items);
            $("#select2-Tipos_activos_TA_id-container").html("<option value= " + 0 + ">" + "Seleccione</option>");

            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tA_id + "'>" + modelo.tA_Descripcion_larga + "</option>";
            });
            $('#Tipos_activos_TA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#Tipos_activos_TA_id').html(items);
    }

});






$('#Lugares_Grupos_lugares_GL_id').change(function () {
        var url = '@Url.Content("~/")' + "Lugares/GetallCategoriasBygrupo_lugares";
    var ddlsource = "#Lugares_Grupos_lugares_GL_id";
   
        $.getJSON("/Lugares/GetallCategoriasBygrupo_lugares", { GL_id: $(ddlsource).val() }, function (data) {
            var items = '';

            items = "<option value= " + 0 + ">" + "Seleccione</option>"
            $("#Lugares_Categorias_lugares_CL_id").empty();

            $("#Lugares_Tipos_lugares_TL_id").empty();
            $('#Lugares_Tipos_lugares_TL_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cL_id + "'>" + modelo.cL_Descripcion_larga + "</option>";
            });
            $('#Lugares_Categorias_lugares_CL_id').html(items);
        });
    });


$('#Lugares_Categorias_lugares_CL_id').change('input', function () {
        // var url = '@Url.Content("~/")' + "Activos/GetallTiposBycategoria";
    var ddlsource = "#Lugares_Categorias_lugares_CL_id";
        $.getJSON("/Lugares/GetallTiposBycategoria_lugares", { CL_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Lugares_Tipos_lugares_TL_id").empty();

            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tL_id + "'>" + modelo.tL_Descripcion_larga + "</option>";
            });
            $('#Lugares_Tipos_lugares_TL_id').html(items);
        });
    });


    //$('#Provincias_Pr_id').change('input', function () {
    //    // var url = '@Url.Content("~/")' + "Activos/GetallTiposBycategoria";
    //    var ddlsource = "#Provincias_Pr_id";
    //    $.getJSON("/Lugares/GetallCiudadesbyProvincias", { Pr_id: $(ddlsource).val() }, function (data) {
    //        var items = '';
    //        items = "<option value= " + 0 + ">" + "Seleccione</option>";


    //        $("#Ciudades_Ci_id").empty();
    //        $('#Ciudades_Ci_id').html(items);

    //        $("#Zonas_Zo_id").empty();
    //        $('#Zonas_Zo_id').html(items);
            

    //        $.each(data, function (i, modelo) {
    //            items += "<option value='" + modelo.ci_id + "'>" + modelo.ci_Descripcion_larga + "</option>";
    //        });
    //        $('#Ciudades_Ci_id').html(items);
    //    });
    //});



    //$('#Ciudades_Ci_id').change('input', function () {
    //    // var url = '@Url.Content("~/")' + "Activos/GetallTiposBycategoria";
    //    var ddlsource = "#Ciudades_Ci_id";
    //    $.getJSON("/Lugares/GetallZonasByCiudades", { Ci_id: $(ddlsource).val() }, function (data) {
    //        var items = '';
    //        items = "<option value= " + 0 + ">" + "Seleccione</option>";

    //        $("#Zonas_Zo_id").empty();
    //        $('#Zonas_Zo_id').html(items);


    //        $.each(data, function (i, modelo) {
    //            items += "<option value='" + modelo.zo_id + "'>" + modelo.zo_Descripcion_larga + "</option>";
    //        });
    //        $('#Zonas_Zo_id').html(items);
    //    });
    //});