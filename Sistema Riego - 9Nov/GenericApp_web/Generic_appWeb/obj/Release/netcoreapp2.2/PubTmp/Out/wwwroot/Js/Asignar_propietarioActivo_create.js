﻿//////////////////////////////////////////////////////////////////////created by israel pinargote 2018 ////////////////////////////////////////////////////////////////////////
$('#GA_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#GA_id";
    var Ga_id = $(ddlsource).val();
    if (Ga_id !== "Seleccione") {
        $.getJSON("/Activos/GetallCategoriasBygrupo", { GA_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#CA_id").empty();
            $('#CA_id').html(items);
            $("#TA_id").empty();
            $('#TA_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cA_id + "'>" + modelo.cA_Descripcion_corta + "</option>";
            });
            $('#CA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#CA_id').html(items);
    }
});


$('#CA_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallTiposBycategoria";
    var ddlsource = "#CA_id";
    var Ca_id = $(ddlsource).val();
    if (Ca_id !== "Seleccione") {
        $.getJSON("/Activos/GetallTiposBycategoria", { CA_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

           
            $("#TA_id").empty();
            $('#TA_id').html(items);
         
           
            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tA_id + "'>" + modelo.tA_Descripcion_corta + "</option>";
            });
            $('#TA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#TA_id').html(items);
    }
});



$('#GEn_id').change(function () {

    var ddlsource = "#GEn_id";
    var GEn_id = $(ddlsource).val();
    if (GEn_id !== "Seleccione") {
        $.getJSON("/Categorias_entidades/GetallCategorias_entidadesBygrupo", { GEn_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#CEn_id").empty();
            $('#CEn_id').html(items);
            $("#TEn_id").empty();
            $('#TEn_id').html(items);
            $("#En_id").empty();
            $('#En_id').html(items);

            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cEn_id + "'>" + modelo.cEn_Descripcion_corta + "</option>";
            });
            $('#CEn_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#CEn_id').html(items);
    }
});


$('#CEn_id').change('input', function () {

    var ddlsource = "#CEn_id";
    var CEn_id = $(ddlsource).val();
    if (CEn_id !== "Seleccione") {
        $.getJSON("/Tipos_entidades/GetallTipos_entidadesByCategoria", { CEn_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#TEn_id").empty();
            $('#TEn_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tEn_id + "'>" + modelo.tEn_Descripcion_corta + "</option>";
            });
            $('#TEn_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#TEn_id').html(items);
    }
});



//////////////////////////////////////////////////////////////////////created by israel pinargote 2018 ////////////////////////////////////////////////////////////////////////


    $("#buscar_activos").click(function () {
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };

        var grupo_activo_id = $('#GA_id').val();
        var categoria_activo_id = $('#CA_id').val();
        var tipo_activo_id = $('#TA_id').val();


        $.ajax({
            type: "GET",
            url: '/Activos/PartialIndex_multiSelect',
            data: {
                "grupo_activo_id": grupo_activo_id,
                "categoria_activo_id": categoria_activo_id,
                "tipo_activo_id": tipo_activo_id

            },
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {

                $('#myModalContent_activo').html(data);
                $('#myModal_activo').modal(options);
                $('#myModal_activo').modal('show');
            },
            error: function () {
                alert("Content load failed.");
            }
        });
    });
    //var jsonString = [];


$('#ActivosIndexTable').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "pagingType": "full_numbers",
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
            "sInfoEmpty": "No existen registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "paginate": {
                "previous": "Antes",
                "next": "Despues",
                "first": "Primero",
                "last": "Ultimo"
            }
        }
     
    });

  

    $('#elegir_activo').on('click', function () {
   
            jsonString = [];
            var arreglo = [];
            oData = table_activo.rows('.selected').data();
            for (var i = 0; i < oData.length; i++) {
                jsonString[i] = oData[i];

                arreglo[i] = jsonString[i][3];
            }




        $('#Arreglo').val(arreglo);
            


            var datatable = $('#ActivosIndexTable').DataTable();

            $('#myModal_activo').modal('hide');

            datatable.clear();
            datatable.rows.add(jsonString);
            datatable.draw();
   
      
});


    $('.modal-content').resizable({
        //alsoResize: ".modal-dialog",
        minHeight: 800,
        minWidth: 800
    });
    $('.modal-dialog').draggable();

    $('#myModal_activo').on('show.bs.modal', function () {
        $(this).find('.modal-body').css({
            'max-height': '100%'
        });
    });




//////////////////////////////////////////////////////////////////////created by israel pinargote 2018 - entidad ////////////////////////////////////////////////////////////////////////


$("#buscar_entidades").click(function () {
    var $buttonClicked = $(this);
    var id = $buttonClicked.attr('data-id');
    var options = { "backdrop": "static", keyboard: true };

    var grupo_entidad_id = $('#GEn_id').val();
    var categoria_entidad_id = $('#CEn_id').val();
    var tipo_entidad_id = $('#TEn_id').val();


    $.ajax({
        type: "GET",
        url: '/Entidades/PartialIndex',
        data: {
            "grupo_entidad_id": grupo_entidad_id,
            "categoria_entidad_id": categoria_entidad_id,
            "tipo_entidad_id": tipo_entidad_id

        },
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {

            $('#myModalContent_entidad').html(data);
            $('#myModal_entidad').modal(options);
            $('#myModal_entidad').modal('show');
        },
        error: function () {
            alert("Content load failed.");
        }
    });
});
//var jsonString = [];


$('#EntidadesIndexTable').DataTable({
    "paging": false,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "pagingType": "full_numbers",
    "language": {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
        "sInfoEmpty": "No existen registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "paginate": {
            "previous": "Antes",
            "next": "Despues",
            "first": "Primero",
            "last": "Ultimo"
        }
    }

});



$('#elegir_entidad').on('click', function () {

    jsonString = [];
    
    oData = table_entidad.rows('.selected').data();
    for (var i = 0; i < oData.length; i++) {
        jsonString[i] = oData[i];

        $('#GEn_Descripcion_corta').val(jsonString[i][0]);
        $('#CEn_Descripcion_corta').val(jsonString[i][1]);
        $('#TEn_Descripcion_corta').val(jsonString[i][2]);
        $('#En_Descripcion_corta').val(jsonString[i][6]);
        $('#En_Codigo_empresa').val(jsonString[i][4]);
        $('#En_Codigo_alterno').val(jsonString[i][5]);
       
        $('#En_id').val(jsonString[i][3]);
    }

    $('#myModal_entidad').modal('hide');

    

});

$('#elegir_entidad_cod_empresa').click(function () { 
    var codigo_empresa = $('#En_Codigo_empresa1').val();

    if (codigo_empresa !== "") {

        $.ajax({
            url: "/Entidades/GetEntidadesByCodEmpresa",
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8', //define a contentType of your request
            cache: false,
            data: { codigo_empresa: codigo_empresa },
            success: function (data) {

                if (data.success) {
                    $('#GEn_Descripcion_corta').val(data.entidades.grupos_entidades.gEn_Descripcion_corta);
                    $('#CEn_Descripcion_corta').val(data.entidades.categorias_entidades.cEn_Descripcion_corta);
                    $('#TEn_Descripcion_corta').val(data.entidades.tipos_entidades.tEn_Descripcion_corta);
                    $('#En_Descripcion_corta').val(data.entidades.en_Descripcion_corta);
                    $('#En_Codigo_empresa').val(data.entidades.en_Codigo_empresa);
                    $('#En_Codigo_alterno').val(data.entidades.en_Codigo_alterno);
                   
                    $("#En_id").val(data.entidades.en_id);

                } else {

                    alert("PROPIETARIO NO ENCONTRADO");
                }
            },
            error: function (xhr, error) {
                alert(xhr);
                alert(error);
            }
        });
    } else {

        alert("INGRESE EL CODIGO/EMPRESA DEL PROPIETARIO");

    }
});

$('.modal-content').resizable({
    //alsoResize: ".modal-dialog",
    minHeight: 800,
    minWidth: 800
});
$('.modal-dialog').draggable();

$('#myModal_entidad').on('show.bs.modal', function () {
    $(this).find('.modal-body').css({
        'max-height': '100%'
    });
});
