//created by israel Pinargote 2018
$("#Provincias_Pr_id").change("input", function () {
    $.getJSON("/Lugares/GetallCiudadesbyProvincias", {
        Pr_id: $("#Provincias_Pr_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Ciudades_Ci_id").empty(), $("#Ciudades_Ci_id").html(o), $("#Zonas_Zo_id").empty(), $("#Zonas_Zo_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.ci_id + "'>" + a.ci_Descripcion_corta + "</option>"
        }), $("#Ciudades_Ci_id").html(o)
    })
}), $("#Ciudades_Ci_id").change("input", function () {
    $.getJSON("/Lugares/GetallZonasByCiudades", {
        Ci_id: $("#Ciudades_Ci_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Zonas_Zo_id").empty(), $("#Zonas_Zo_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.zo_id + "'>" + a.zo_Descripcion_corta + "</option>"
        }), $("#Zonas_Zo_id").html(o)
    })
});