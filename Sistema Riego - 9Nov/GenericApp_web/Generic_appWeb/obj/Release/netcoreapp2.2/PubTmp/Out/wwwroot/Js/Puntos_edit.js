﻿// created by israel pinargote 10/3/2019

$("#Grupos_puntos_GPu_id").change(function () {
    $.getJSON("/Puntos/GetallCategoriasBygrupo_puntos", {
        GPu_id: $("#Grupos_puntos_GPu_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Categorias_puntos_CPu_id").empty(), $("#Tipos_puntos_TPu_id").empty(), $("#Tipos_puntos_TPu_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.cPu_id + "'>" + a.cPu_Descripcion_corta + "</option>"
        }), $("#Categorias_puntos_CPu_id").html(o)
    })
})
$("#Categorias_puntos_CPu_id").change("input", function () {
    $.getJSON("/Puntos/GetallTiposBycategoria_puntos", {
        CPu_id: $("#Categorias_puntos_CPu_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Tipos_puntos_TPu_id").empty(), $("#Tipos_puntos_TPu_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.tPu_id + "'>" + a.tPu_Descripcion_corta + "</option>"
        }), $("#Tipos_puntos_TPu_id").html(o)
    })
})



$("#Provincias_Pr_id").change("input", function () {
    $.getJSON("/Lugares/GetallCiudadesbyProvincias", {
        Pr_id: $("#Provincias_Pr_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Ciudades_Ci_id").empty(),
            $("#Ciudades_Ci_id").html(o),

            $("#Lugares_Lu_id").empty(),
            $("#Lugares_Lu_id").html(o),

            $.each(i, function (i, a) {
                o += "<option value='" + a.ci_id + "'>" + a.ci_Descripcion_corta + "</option>"
            }), $("#Ciudades_Ci_id").html(o)
    })
})
$("#Ciudades_Ci_id").change("input", function () {
    $.getJSON("/Lugares/GetallLugaresByCiudades", {
        Ci_id: $("#Ciudades_Ci_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",

            $("#Lugares_Lu_id").empty(),
            $("#Lugares_Lu_id").html(o),

            $.each(i, function (i, a) {

                o += "<option value='" + a.lu_id + "'>" + a.lu_Descripcion_corta + "</option>"
            }), $("#Lugares_Lu_id").html(o)
    })
})





$("#Grupos_sitios_GSit_id").change(function () {
    $.getJSON("/Sitios/GetallCategoriasBygrupo_sitios", {
        GSit_id: $("#Grupos_sitios_GSit_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Categorias_sitios_CSit_id").empty(),
            $("#Tipos_sitios_TSit_id").empty(),
            $("#Sitios_Si_id").empty(),

            $.each(i, function (i, a) {
                o += "<option value='" + a.cSit_id + "'>" + a.cSit_Descripcion_corta + "</option>"
            }), $("#Categorias_sitios_CSit_id").html(o),
            $("#Tipos_sitios_TSit_id").html(o),
            $("#Sitios_Si_id").html(o)
    })
})
$("#Categorias_sitios_CSit_id").change("input", function () {
    $.getJSON("/Sitios/GetallTiposBycategoria_sitios", {
        CSit_id: $("#Categorias_sitios_CSit_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Tipos_sitios_TSit_id").empty(),
            $("#Tipos_sitios_TSit_id").html(o),
            $("#Sitios_Si_id").empty(),
            $("#Sitios_Si_id").html(o),
            $.each(i, function (i, a) {
                o += "<option value='" + a.tSit_id + "'>" + a.tSit_Descripcion_corta + "</option>"
            }), $("#Tipos_sitios_TSit_id").html(o)
    })
})

$("#Tipos_sitios_TSit_id").change("input", function () {
    $.getJSON("/Sitios/GetallSitiosBytipos_sitios", {
        TSit_id: $("#Tipos_sitios_TSit_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Sitios_Si_id").empty(),
            $("#Sitios_Si_id").html(o),
            $.each(i, function (i, a) {
                o += "<option value='" + a.si_id + "'>" + a.si_Descripcion_corta + "</option>"
            }), $("#Sitios_Si_id").html(o)
    })
})



$("#Grupos_areas_GAre_id").change(function () {
    $.getJSON("/Areas/GetallCategoriasBygrupo_areas", {
        GAre_id: $("#Grupos_areas_GAre_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Categorias_areas_CAre_id").empty(),
            $("#Tipos_areas_TAre_id").empty(),
            $("#Tipos_areas_TAre_id").html(o),
            $("#Areas_Are_id").empty(),
            $("#Areas_Are_id").html(o),
            $.each(i, function (i, a) {
                o += "<option value='" + a.cAre_id + "'>" + a.cAre_Descripcion_corta + "</option>"
            }), $("#Categorias_areas_CAre_id").html(o)
    })
})
$("#Categorias_areas_CAre_id").change("input", function () {
    $.getJSON("/Areas/GetallTiposBycategoria_areas", {
        CAre_id: $("#Categorias_areas_CAre_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Tipos_areas_TAre_id").empty(),
            $("#Tipos_areas_TAre_id").html(o),
            $("#Areas_Are_id").empty(),
            $("#Areas_Are_id").html(o),
            $.each(i, function (i, a) {
                o += "<option value='" + a.tAre_id + "'>" + a.tAre_Descripcion_corta + "</option>"
            }), $("#Tipos_areas_TAre_id").html(o)
    })
})
$("#Tipos_areas_TAre_id").change("input", function () {
    $.getJSON("/Areas/GetallAreasBytipos_areas", {
        TAre_id: $("#Tipos_areas_TAre_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Areas_Are_id").empty(),
            $("#Areas_Are_id").html(o),
            $.each(i, function (i, a) {
                o += "<option value='" + a.are_id + "'>" + a.are_Descripcion_corta + "</option>"
            }), $("#Areas_Are_id").html(o)
    })
})




//$("#Lugares_Lu_id").change("input", function () {

//    $.getJSON("/Lugares/GetallSitiosByLugares", {
//        Lu_id: $("#Lugares_Lu_id").val()
//    }, function (i) {
//        var o = "";
//        o = "<option value= 0>Seleccione</option>",
//            $("#Sitios_Si_id").empty(),
//            $("#Sitios_Si_id").html(o),
//            $("#Areas_Are_id").empty(),
//            $("#Areas_Are_id").html(o),
//            $.each(i, function (i, a) {

//                o += "<option value='" + a.si_id + "'>" + a.si_Descripcion_corta + "</option>"
//            }), $("#Sitios_Si_id").html(o)
//    })
//})


//$("#Sitios_Si_id").change("input", function () {

//    $.getJSON("/Lugares/GetallAreasBySitios", {
//        Si_id: $("#Sitios_Si_id").val()
//    }, function (i) {
//        var o = "";
//        o = "<option value= 0>Seleccione</option>",
//            $("#Areas_Are_id").empty(),
//            $("#Areas_Are_id").html(o),
//            $.each(i, function (i, a) {

//                o += "<option value='" + a.are_id + "'>" + a.are_Descripcion_corta + "</option>"
//            }), $("#Areas_Are_id").html(o)
//    })
//})