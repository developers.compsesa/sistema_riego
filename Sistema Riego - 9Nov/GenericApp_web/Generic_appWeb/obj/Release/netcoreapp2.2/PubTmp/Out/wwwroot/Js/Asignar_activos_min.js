//created by israel Pinargote 2018

$("#Grupos_lugares_GL_id").change(function () {
    $.getJSON("/Lugares/GetallCategoriasBygrupo_lugares", {
        GL_id: $("#Grupos_lugares_GL_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Categorias_lugares_CL_id").empty(), $("#Tipos_lugares_TL_id").empty(), $("#Tipos_lugares_TL_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.cL_id + "'>" + a.cL_Descripcion_larga + "</option>"
        }), $("#Categorias_lugares_CL_id").html(o)
    })
})
$("#Categorias_lugares_CL_id").change("input", function () {
    $.getJSON("/Lugares/GetallTiposBycategoria_lugares", {
        CL_id: $("#Categorias_lugares_CL_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Tipos_lugares_TL_id").empty(), $("#Tipos_lugares_TL_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.tL_id + "'>" + a.tL_Descripcion_larga + "</option>"
        }), $("#Tipos_lugares_TL_id").html(o)
    })
})
$("#Provincias_Pr_id").change("input", function () {
    $.getJSON("/Lugares/GetallCiudadesbyProvincias", {
        Pr_id: $("#Provincias_Pr_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Ciudades_Ci_id").empty(), $("#Ciudades_Ci_id").html(o), $("#Zonas_Zo_id").empty(), $("#Zonas_Zo_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.ci_id + "'>" + a.ci_Descripcion_larga + "</option>"
        }), $("#Ciudades_Ci_id").html(o)
    })
})
$("#Ciudades_Ci_id").change("input", function () {
    $.getJSON("/Lugares/GetallZonasByCiudades", {
        Ci_id: $("#Ciudades_Ci_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Zonas_Zo_id").empty(), $("#Zonas_Zo_id").html(o), $.each(i, function (i, a) {
            
            o += "<option value='" + a.zo_id + "'>" + a.zo_Descripcion_larga + "</option>"
        }), $("#Zonas_Zo_id").html(o)
    })
})

$("#Zonas_Zo_id").change("input", function () {
   
    $.getJSON("/Lugares/GetallLugaresByZonas", {
        Zo_id: $("#Zonas_Zo_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Lugares_Lu_id").empty(), $("#Lugares_Lu_id").html(o), $.each(i, function (i, a) {
            
            o += "<option value='" + a.lu_id + "'>" + a.lu_Descripcion_larga + "</option>"
        }), $("#Lugares_Lu_id").html(o)
    })
})


$("#Lugares_Lu_id").change("input", function () {

    $.getJSON("/Sitios/GetallSitiosByLugares", {
        Lu_id: $("#Lugares_Lu_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Sitios_Si_id").empty(), $("#Sitios_Si_id").html(o), $.each(i, function (i, a) {

            o += "<option value='" + a.si_id + "'>" + a.si_Descripcion_larga + "</option>"
        }), $("#Sitios_Si_id").html(o)
    })
})



$("#Sitios_Si_id").change("input", function () {
   
    $.getJSON("/Puntos/GetallPuntosBySitios", {
        Pu_id: $("#Sitios_Si_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Puntos_Pu_id").empty(), $("#Puntos_Pu_id").html(o), $.each(i, function (i, a) {

            o += "<option value='" + a.pu_id + "'>" + a.pu_Descripcion_larga + "</option>"
        }), $("#Puntos_Pu_id").html(o)
    })
})


