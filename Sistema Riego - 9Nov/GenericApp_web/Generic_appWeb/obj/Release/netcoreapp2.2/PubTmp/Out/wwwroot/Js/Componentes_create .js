﻿// created by israel pinargote 10/3/2019

$("#Grupos_componentes_GComp_id").change(function () {
    $.getJSON("/Componentes/GetallCategoriasBygrupo_componentes", {
        GComp_id: $("#Grupos_componentes_GComp_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Categorias_componentes_CComp_id").empty(), $("#Tipos_componentes_TComp_id").empty(), $("#Tipos_componentes_TComp_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.cComp_id + "'>" + a.cComp_Descripcion_corta + "</option>"
        }), $("#Categorias_componentes_CComp_id").html(o)
    })
})
$("#Categorias_componentes_CComp_id").change("input", function () {
    $.getJSON("/Componentes/GetallTiposBycategoria_componentes", {
        CComp_id: $("#Categorias_componentes_CComp_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Tipos_componentes_TComp_id").empty(), $("#Tipos_componentes_TComp_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.tComp_id + "'>" + a.tComp_Descripcion_corta + "</option>"
        }), $("#Tipos_componentes_TComp_id").html(o)
    })
})

