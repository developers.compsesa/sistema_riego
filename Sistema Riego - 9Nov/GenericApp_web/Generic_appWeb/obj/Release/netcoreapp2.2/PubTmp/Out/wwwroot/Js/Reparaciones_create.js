$('#Grupos_reparaciones_GRep_id').change(function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_reparaciones_GRep_id";
    var GRep_id = $(ddlsource).val();
    if (GRep_id !== "Seleccione") {
        $.getJSON("/Reparaciones/GetallCategoria_reparacionesBygrupo", { GRep_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_reparaciones.CRep_id").empty();
            $("#Categorias_reparaciones.CRep_id").html(items);
            $("#Tipos_reparaciones_TRep_id").empty();
            $("#Tipos_reparaciones_TRep_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cRep_id + "'>" + modelo.cRep_Descripcion_corta + "</option>";
            });
            $('#Categorias_reparaciones_CRep_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_reparaciones_CRep_id').html(items);
        $('#Tipos_reparaciones_TRep_id').html(items);
       
    }
});


$('#Categorias_reparaciones_CRep_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallTiposBycategoria";
    var ddlsource = "#Categorias_reparaciones_CRep_id";
    var CRep_id = $(ddlsource).val();
    if (CRep_id !== "Seleccione") {
        $.getJSON("/Reparaciones/GetallTipos_reparacionesBycategoria", { CRep_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_reparaciones_TRep_id").empty();
            $("#Tipos_reparaciones_TRep_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tRep_id + "'>" + modelo.tRep_Descripcion_corta + "</option>";
            });
            $('#Tipos_reparaciones_TRep_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_reparaciones_TRep_id').html(items);
    }
});



