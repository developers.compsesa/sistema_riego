﻿//created by israel pinargote 2018
$(function () {


    $("#modalx").click(function () {
      
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };

        var grupo_activo_id = $('#Activos_Grupos_activos_GA_id').val();
        var categoria_activo_id = $('#CA_id').val();
        var tipo_activo_id = $('#Tipos_activos_TA_Padre_id').val();
        var subtipo_activo_id = $('#Tipos_activos_TA_id').val();

        $.ajax({
            type: "GET",
            url: '/Activos/PartialIndex',
            data: {
                "grupo_activo_id": grupo_activo_id,
                "categoria_activo_id": categoria_activo_id,
                "tipo_activo_id": tipo_activo_id,
                "subtipo_activo_id": subtipo_activo_id
            },
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {

                $('#myModalContent').html(data);
                $('#myModal_activo').modal(options);
                $('#myModal_activo').modal('show');
            },
            error: function () {
                alert("Content load failed.");
            }
        });
        $('#Codigo_empresa').val('');
        $('#Activos_A_id').val('');
        $('#descripcion_larga').val('');

    });

   

    $("#buscar_lugar").click(function () {
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };
        
        var grupo_lugar_id = $('#Lugares_Grupos_lugares_GL_id').val();
        var categoria_lugar_id = $('#Lugares_Categorias_lugares_CL_id').val();
        var tipo_lugar_id = $('#Lugares_Tipos_lugares_TL_id').val();

      
        $.ajax({
            type: "GET",
            url: '/Lugares/PartialIndex',
            data: {
                "grupo_lugar_id": grupo_lugar_id,
                "categoria_lugar_id": categoria_lugar_id,
                "tipo_lugar_id": tipo_lugar_id
                
            },
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {

                $('#myModalContent_lugar').html(data);
                $('#myModal_lugar').modal(options);
                $('#myModal_lugar').modal('show');
            },
            error: function () {
                alert("Content load failed.");
            }
        });
    });
    var jsonString = []; 
   

    $('#LugaresIndexTable').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "pagingType": "full_numbers",
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
            "sInfoEmpty": "No existen registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "paginate": {
                "previous": "Antes",
                "next": "Despues",
                "first": "Primero",
                "last": "Ultimo"
            }
        }
        //columns: [
        //    { title: "GRUPO" },
        //    { title: "CATEGORIA" },
        //    { title: "TIPO" },
        //    { title: "CODIGO INTERNO" },
        //    { title: "CODIGO EMPRESA" },
        //    { title: "CODIGO ALTERNO" },
        //    { title: "DESCRIPCION LARGA" },
        //    { title: "DESCRIPCION MEDIANA." },
        //    { title: "DESCRIPCION CORTA" },
        //    { title: "ABREVIACION" },
        //    { title: "OBSERVACION" },
        //    { title: "DIRECCION" },
        //    { title: "CALLE PRINCIPAL" },
            
        //    { title: "PROVINCIA" },
        //    { title: "CIUDAD" },
        //    { title: "ZONA" },
        //    { title: "ESTADO" }
        //]
    });

    $('#elegir_activo').on('click', function () {

       
            var codigo_empresa = document.getElementById('activo_codigo_empresa');
            var id = document.getElementById('activo_id');
            var descripcion = document.getElementById('activo_descripcion');


            $('#Codigo_empresa').val($(codigo_empresa).val());
            $('#Activos_A_id').val($(id).val());
            $('#descripcion_larga').val($(descripcion).val());
            $('#myModal_activo').modal('hide');

            //console.log($(descripcion).val());
    
    });

    $('#clean_lugares').on('click', function () {
      
        var datatable = $('#LugaresIndexTable').DataTable();
        datatable.clear();
    });



    $('#elegir_lugar').on('click', function () {
        
            
            //var table = $('#LugaresIndexTable').DataTable();

            //if (table.data().any()) {
            //    //var tableCustomized = LugaresIndexTable.rows().data();

            //    var cells = [];
            //    var rows = $("#LugaresIndexTable").dataTable().fnGetNodes();
            //    for (var i = 0; i < rows.length; i++) {
            //        // Get HTML of 3rd column (for example)
            //        cells.push($(rows[i]).find("td:eq(2)").html());
            //    }
            //    console.log(cells);
            //} else {
                oData = table_lugar.rows('.selected').data();
                for (var i = 0; i < oData.length; i++) {
                    jsonString[i] = oData[i];

            }
            
            var datatable = $('#LugaresIndexTable').DataTable();

                datatable.clear();
                datatable.rows.add(jsonString);
            datatable.draw();
            jsonString = [];
            //}
            $('#myModal_lugar').modal('hide');

     
    });
    $('.modal-content').resizable({
        //alsoResize: ".modal-dialog",
        minHeight: 800,
        minWidth: 800
    });
    $('.modal-dialog').draggable();

    $('#myModal_lugar').on('show.bs.modal', function () {
        $(this).find('.modal-body').css({
            'max-height': '100%'
        });
    });

});