﻿//////////////////////////////////////////////////////////////////////created by israel pinargote 2018 ////////////////////////////////////////////////////////////////////////
$('#Grupos_activos_GA_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_activos_GA_id";
    var Ga_id = $(ddlsource).val();
    if (Ga_id !== "Seleccione") {
        $.getJSON("/Activos/GetallCategoriasBygrupo", { Ga_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_activos_CA_id").empty();
            $('#Categorias_activos_CA_id').html(items);
            $("#Tipos_activos_TA_id").empty();
            $('#Tipos_activos_TA_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cA_id + "'>" + modelo.cA_Descripcion_corta + "</option>";
            });
            $('#Categorias_activos_CA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_activos_CA_id').html(items);
    }
});


$('#Categorias_activos_CA_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallTiposBycategoria";
    var ddlsource = "#Categorias_activos_CA_id";
    var Ca_id = $(ddlsource).val();
    if (Ca_id !== "Seleccione") {
        $.getJSON("/Activos/GetallTiposBycategoria", { CA_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";


            $("#Tipos_activos_TA_id").empty();
            $('#Tipos_activos_TA_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tA_id + "'>" + modelo.tA_Descripcion_corta + "</option>";
            });
            $('#Tipos_activos_TA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_activos_TA_id').html(items);
    }
});

$("#buscar_activos").click(function () {
    var $buttonClicked = $(this);
    var id = $buttonClicked.attr('data-id');
    var options = { "backdrop": "static", keyboard: true };

    var grupo_activo_id = $('#Grupos_activos_GA_id').val();
    var categoria_activo_id = $('#Categorias_activos_CA_id').val();
    var tipo_activo_id = $('#Tipos_activos_TA_id').val();


    $.ajax({
        type: "GET",
        url: '/Activos/PartialIndex_multiSelect',
        data: {
            "grupo_activo_id": grupo_activo_id,
            "categoria_activo_id": categoria_activo_id,
            "tipo_activo_id": tipo_activo_id

        },
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {

            $('#myModalContent_activo').html(data);
            $('#myModal_activo').modal(options);
            $('#myModal_activo').modal('show');
        },
        error: function () {
            alert("Content load failed.");
        }
    });
});



$('#ActivosIndexTable').DataTable({
    "paging": false,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "pagingType": "full_numbers",
    "language": {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
        "sInfoEmpty": "No existen registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "paginate": {
            "previous": "Antes",
            "next": "Despues",
            "first": "Primero",
            "last": "Ultimo"
        }
    }

});

$('#elegir_activo').on('click', function () {

    jsonString = [];
    var arreglo = [];
    oData = table_activo.rows('.selected').data();
    for (var i = 0; i < oData.length; i++) {
        jsonString[i] = oData[i];

        arreglo[i] = jsonString[i][3];
    }




    $('#Arreglo').val(arreglo);



    var datatable = $('#ActivosIndexTable').DataTable();

    $('#myModal_activo').modal('hide');

    datatable.clear();
    datatable.rows.add(jsonString);
    datatable.draw();


});


$('.modal-content').resizable({
    //alsoResize: ".modal-dialog",
    minHeight: 800,
    minWidth: 800
});
$('.modal-dialog').draggable();

$('#myModal_activo').on('show.bs.modal', function () {
    $(this).find('.modal-body').css({
        'max-height': '100%'
    });
});



//////////////////////////////////////////////////////////////////////  LUGARES  /////////////////////////////////////////////////////////////////////////////

//$("#Grupos_lugares_GL_id").change(function () {
//    $.getJSON("/Lugares/GetallCategoriasBygrupo_lugares", {
//        GL_id: $("#Grupos_lugares_GL_id").val()
//    }, function (i) {
//        var o = "";
//        o = "<option value= 0>Seleccione</option>", $("#Categorias_lugares_CL_id").empty(), $("#Tipos_lugares_TL_id").empty(), $("#Tipos_lugares_TL_id").html(o), $.each(i, function (i, a) {
//            o += "<option value='" + a.cL_id + "'>" + a.cL_Descripcion_corta + "</option>"
//        }), $("#Categorias_lugares_CL_id").html(o)
//    })
//}), $("#Categorias_lugares_CL_id").change("input", function () {
//    $.getJSON("/Lugares/GetallTiposBycategoria_lugares", {
//        CL_id: $("#Categorias_lugares_CL_id").val()
//    }, function (i) {
//        var o = "";
//        o = "<option value= 0>Seleccione</option>", $("#Tipos_lugares_TL_id").empty(), $("#Tipos_lugares_TL_id").html(o), $.each(i, function (i, a) {
//            o += "<option value='" + a.tL_id + "'>" + a.tL_Descripcion_corta + "</option>"
//        }), $("#Tipos_lugares_TL_id").html(o)
//    })
//    });

$("#Provincias_Pr_id").change("input", function () {
    $.getJSON("/Lugares/GetallCiudadesbyProvincias", {
        Pr_id: $("#Provincias_Pr_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Ciudades_Ci_id").empty(),
            $("#Ciudades_Ci_id").html(o),
          
           
            $.each(i, function (i, a) {
                o += "<option value='" + a.ci_id + "'>" + a.ci_Descripcion_corta + "</option>"
            }), $("#Ciudades_Ci_id").html(o)
    })
})
$("#Ciudades_Ci_id").change("input", function () {
    $.getJSON("/Lugares/GetallLugaresByCiudades", {
        Ci_id: $("#Ciudades_Ci_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",

            $("#Lugares_Lu_id").empty(),
            $("#Lugares_Lu_id").html(o),

            $.each(i, function (i, a) {

                o += "<option value='" + a.lu_id + "'>" + a.lu_Descripcion_corta + "</option>"
            }), $("#Lugares_Lu_id").html(o)
    })
})




$("#Grupos_sitios_GSit_id").change(function () {
    $.getJSON("/Sitios/GetallCategoriasBygrupo_sitios", {
        GSit_id: $("#Grupos_sitios_GSit_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Categorias_sitios_CSit_id").empty(),
            $("#Tipos_sitios_TSit_id").empty(),
            $("#Sitios_Si_id").empty(),

            $.each(i, function (i, a) {
                o += "<option value='" + a.cSit_id + "'>" + a.cSit_Descripcion_corta + "</option>"
            }), $("#Categorias_sitios_CSit_id").html(o),
            $("#Tipos_sitios_TSit_id").html(o),
            $("#Sitios_Si_id").html(o)
    })
})
$("#Categorias_sitios_CSit_id").change("input", function () {
    $.getJSON("/Sitios/GetallTiposBycategoria_sitios", {
        CSit_id: $("#Categorias_sitios_CSit_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Tipos_sitios_TSit_id").empty(),
            $("#Tipos_sitios_TSit_id").html(o),
            $("#Sitios_Si_id").empty(),
            $("#Sitios_Si_id").html(o),
            $.each(i, function (i, a) {
                o += "<option value='" + a.tSit_id + "'>" + a.tSit_Descripcion_corta + "</option>"
            }), $("#Tipos_sitios_TSit_id").html(o)
    })
})

$("#Tipos_sitios_TSit_id").change("input", function () {
    $.getJSON("/Sitios/GetallSitiosBytipos_sitios", {
        TSit_id: $("#Tipos_sitios_TSit_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Sitios_Si_id").empty(),
            $("#Sitios_Si_id").html(o),
            $.each(i, function (i, a) {
                o += "<option value='" + a.si_id + "'>" + a.si_Descripcion_corta + "</option>"
            }), $("#Sitios_Si_id").html(o)
    })
})



$("#Grupos_areas_GAre_id").change(function () {
    $.getJSON("/Areas/GetallCategoriasBygrupo_areas", {
        GAre_id: $("#Grupos_areas_GAre_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Categorias_areas_CAre_id").empty(),
            $("#Tipos_areas_TAre_id").empty(),
            $("#Tipos_areas_TAre_id").html(o),
            $("#Areas_Are_id").empty(),
            $("#Areas_Are_id").html(o),
            $.each(i, function (i, a) {
                o += "<option value='" + a.cAre_id + "'>" + a.cAre_Descripcion_corta + "</option>"
            }), $("#Categorias_areas_CAre_id").html(o)
    })
})
$("#Categorias_areas_CAre_id").change("input", function () {
    $.getJSON("/Areas/GetallTiposBycategoria_areas", {
        CAre_id: $("#Categorias_areas_CAre_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Tipos_areas_TAre_id").empty(),
            $("#Tipos_areas_TAre_id").html(o),
            $("#Areas_Are_id").empty(),
            $("#Areas_Are_id").html(o),
            $.each(i, function (i, a) {
                o += "<option value='" + a.tAre_id + "'>" + a.tAre_Descripcion_corta + "</option>"
            }), $("#Tipos_areas_TAre_id").html(o)
    })
})
$("#Tipos_areas_TAre_id").change("input", function () {
    $.getJSON("/Areas/GetallAreasBytipos_areas", {
        TAre_id: $("#Tipos_areas_TAre_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Areas_Are_id").empty(),
            $("#Areas_Are_id").html(o),
            $.each(i, function (i, a) {
                o += "<option value='" + a.are_id + "'>" + a.are_Descripcion_corta + "</option>"
            }), $("#Areas_Are_id").html(o)
    })
})


$("#Grupos_puntos_GPu_id").change(function () {
    $.getJSON("/Puntos/GetallCategoriasBygrupo_puntos", {
        GPu_id: $("#Grupos_puntos_GPu_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Categorias_puntos_CPu_id").empty(), $("#Tipos_puntos_TPu_id").empty(), $("#Tipos_puntos_TPu_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.cPu_id + "'>" + a.cPu_Descripcion_corta + "</option>"
        }), $("#Categorias_puntos_CPu_id").html(o)
    })
})
$("#Categorias_puntos_CPu_id").change("input", function () {
    $.getJSON("/Puntos/GetallTiposBycategoria_puntos", {
        CPu_id: $("#Categorias_puntos_CPu_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Tipos_puntos_TPu_id").empty(), $("#Tipos_puntos_TPu_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.tPu_id + "'>" + a.tPu_Descripcion_corta + "</option>"
        }), $("#Tipos_puntos_TPu_id").html(o)
    })
})


$("#Tipos_puntos_TPu_id").change("input", function () {
    $.getJSON("/Puntos/GetallPuntosBytipos_puntos", {
        TPu_id: $("#Tipos_puntos_TPu_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>",
            $("#Puntos_Pu_id").empty(),
            $("#Puntos_Pu_id").html(o),
            $.each(i, function (i, a) {
            o += "<option value='" + a.pu_id + "'>" + a.pu_Descripcion_corta + "</option>"
        }), $("#Puntos_Pu_id").html(o)
    })
})
