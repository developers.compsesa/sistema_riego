//created by israel Pinargote 2019 - april 08
$("#Grupos_actividades_GAc_id").change(function () {
    $.getJSON("/Actividades/GetallCategoriasBygrupo_actividades", {
        GAc_id: $("#Grupos_actividades_GAc_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Categorias_actividades_CAc_id").empty(), $("#Tipos_actividades_TAc_id").empty(), $("#Tipos_actividades_TAc_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.cAc_id + "'>" + a.cAc_Descripcion_corta + "</option>"
        }), $("#Categorias_actividades_CAc_id").html(o)
    })
}), $("#Categorias_actividades_CAc_id").change("input", function () {
    $.getJSON("/Actividades/GetallTiposBycategoria_actividades", {
        CAc_id: $("#Categorias_actividades_CAc_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Tipos_actividades_TAc_id").empty(), $("#Tipos_actividades_TAc_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.tAc_id + "'>" + a.tAc_Descripcion_corta + "</option>"
        }), $("#Tipos_actividades_TAc_id").html(o)
    })
});