﻿//---------------------------------------------  TRANSACCIONES ---------------------------------

$('#GT_Descripcion_larga').change(function () {
   
    var ddlsource = "#GT_Descripcion_larga";
    var GT_id = $(ddlsource).val();
    if (GT_id !== "Seleccione") {
        $.getJSON("/Categorias_transacciones/GetallCategoriasTransaccionesBygrupo", { GT_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#CT_Descripcion_larga").empty();
            $('#CT_Descripcion_larga').html(items);
            $("#TT_Descripcion_larga").empty();
            $('#TT_Descripcion_larga').html(items);

            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cT_id + "'>" + modelo.cT_Descripcion_larga + "</option>";
            });
            $('#CT_Descripcion_larga').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#CT_Descripcion_larga').html(items);
    }
});


$('#CT_Descripcion_larga').change('input', function () {
   
    var ddlsource = "#CT_Descripcion_larga";
    var CT_id = $(ddlsource).val();
    if (CT_id !== "Seleccione") {
        $.getJSON("/Tipos_transacciones/GetallTiposTransaccionesByCategoria", { CT_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";


            $("#TT_Descripcion_larga").empty();
            $('#TT_Descripcion_larga').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tT_id + "'>" + modelo.tT_Descripcion_larga + "</option>";
            });
            $('#TT_Descripcion_larga').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#TT_Descripcion_larga').html(items);
    }
});


//------------------------------------------   status ----------------------------------------------------



$('#GE_id').change(function () {

    var ddlsource = "#GE_id";
    var GE_id = $(ddlsource).val();
    if (GE_id !== "Seleccione") {
        $.getJSON("/Categorias_estados/GetallCategorias_estadosBygrupo", { GE_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#CE_id").empty();
            $('#CE_id').html(items);
            $("#TE_id").empty();
            $('#TE_id').html(items);

            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cE_id + "'>" + modelo.cE_Descripcion_larga + "</option>";
            });
            $('#CE_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#CE_id').html(items);
    }
});


$('#CE_id').change('input', function () {

    var ddlsource = "#CE_id";
    var CE_id = $(ddlsource).val();
    if (CE_id !== "Seleccione") {
        $.getJSON("/Tipos_estados/GetallTipos_estadosByCategoria", { CE_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#TE_id").empty();
            $('#TE_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tE_id + "'>" + modelo.tE_Descripcion_larga + "</option>";
            });
            $('#TE_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#TE_id').html(items);
    }
});



$("#buscar_estados").click(function () {
    var $buttonClicked = $(this);
    var id = $buttonClicked.attr('data-id');
    var options = { "backdrop": "static", keyboard: true };

    var grupo_estado_id = $('#GE_id').val();
    var categoria_estado_id = $('#CE_id').val();
    var tipo_estado_id = $('#TE_id').val();


    $.ajax({
        type: "GET",
        url: '/Estados/PartialIndex',
        data: {
            "grupo_estado_id": grupo_estado_id,
            "categoria_estado_id": categoria_estado_id,
            "tipo_estado_id": tipo_estado_id

        },
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {

            $('#myModalContent_estado').html(data);
            $('#myModal_estado').modal(options);
            $('#myModal_estado').modal('show');
        },
        error: function () {
            alert("Content load failed.");
        }
    });
});



$('#elegir_estado').on('click', function () {

    jsonString = [];
    //var arreglo = [];
    oData = table_estado.rows('.selected').data();
    for (var i = 0; i < oData.length; i++) {
        jsonString[i] = oData[i];

        $('#GE_Descripcion_larga').val(jsonString[i][0]);
        $('#CE_Descripcion_larga').val(jsonString[i][1]);
        $('#TE_Descripcion_larga').val(jsonString[i][2]);
        $('#E_Descripcion_larga').val(jsonString[i][6]);
        $('#E_Codigo_empresa').val(jsonString[i][4]);
        $('#E_id').val(jsonString[i][3]);
    }




    $('#myModal_estado').modal('hide');

});
$('#modal_status').click(function () {
    var codigo_empresa = $('#E_Codigo_empresa1').val();

    if (codigo_empresa !== "") {

        $.ajax({
            url: "/Estados/GetEstadosByCodEmpresa",
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8', //define a contentType of your request
            cache: false,
            data: { codigo_empresa: codigo_empresa },
            success: function (data) {

                if (data.success) {
                    $('#GE_Descripcion_larga').val(data.status.grupos_estados.gE_Descripcion_larga);
                    $('#CE_Descripcion_larga').val(data.status.categorias_estados.cE_Descripcion_larga);
                    $('#TE_Descripcion_larga').val(data.status.tipos_estados.tE_Descripcion_larga);
                    $('#E_Descripcion_larga').val(data.status.e_Descripcion_larga);
                    $('#E_Codigo_empresa').val(data.status.e_Codigo_empresa);
                    $("#E_id").val(data.status.e_id);

                } else {

                    alert("STATUS NO ENCONTRADO");
                }
            },
            error: function (xhr, error) {
                alert(xhr);
                alert(error);
            }
        });
    } else {

        alert("INGRESE EL CODIGO DE EMPRESA DEL STATUS");

    }
});


/*------------------------------------------- MOTIVOS-----------------------------------------------------*/

$('#modal_motivo').click(function () {
    var codigo_empresa = $('#Mot_Codigo_empresa1').val();

    if (codigo_empresa !== "") {

        $.ajax({
            url: "/Motivos/GetMotivosByCodEmpresa",
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8', //define a contentType of your request
            cache: false,
            data: { codigo_empresa: codigo_empresa },
            success: function (data) {

                if (data.success) {
                    $('#GM_Descripcion_larga').val(data.motivos.grupos_motivos.gM_Descripcion_larga);
                    $('#CM_Descripcion_larga').val(data.motivos.categorias_motivos.cM_Descripcion_larga);
                    $('#TMot_Descripcion_larga').val(data.motivos.tipos_motivos.tMot_Descripcion_larga);
                    $('#Mot_Descripcion_larga').val(data.motivos.mot_Descripcion_larga);
                    $('#Mot_Codigo_empresa').val(data.motivos.mot_Codigo_empresa);
                    $("#Mot_id").val(data.motivos.mot_id);

                } else {

                    alert("MOTIVO NO ENCONTRADO");
                }
            },
            error: function (xhr, error) {
                alert(xhr);
                alert(error);
            }
        });
    } else {

        alert("INGRESE EL CODIGO DE EMPRESA DEL MOTIVO");

    }
});




$('#GM_id').change(function () {

    var ddlsource = "#GM_id";
    var GM_id = $(ddlsource).val();
    if (GM_id !== "Seleccione") {
        $.getJSON("/Categorias_motivos/GetallCategoriasMotivosBygrupo", { GM_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>"
            $("#CM_id").empty();
            $('#CM_id').html(items);
            $("#TMot_id").empty();
            $('#TMot_id').html(items);

            $("#Mot_id").empty();
            $('#Mot_id').html(items);

            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cM_id + "'>" + modelo.cM_Descripcion_larga + "</option>";
            });
            $('#CM_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#CM_id').html(items);
        $('#TMot_id').html(items);
        $('#Mot_id').html(items);
    }
});


$('#CM_id').change('input', function () {

    var ddlsource = "#CM_id";
    var CM_id = $(ddlsource).val();
    if (CM_id !== "Seleccione") {
        $.getJSON("/Tipos_motivos/GetallTiposMotivosByCategorias", { CM_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#TMot_id").empty();
            $('#TMot_id').html(items);

            $("#Mot_id").empty();
            $('#Mot_id').html(items);

            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tMot_id + "'>" + modelo.tMot_Descripcion_larga + "</option>";
            });
            $('#TMot_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#TMot_id').html(items);
        $('#Mot_id').html(items);
    }
});

$("#buscar_motivos").click(function () {
    var $buttonClicked = $(this);
    var id = $buttonClicked.attr('data-id');
    var options = { "backdrop": "static", keyboard: true };

    var grupo_motivo_id = $('#GM_id').val();
    var categoria_motivo_id = $('#CM_id').val();
    var tipo_motivo_id = $('#TMot_id').val();


    $.ajax({
        type: "GET",
        url: '/Motivos/PartialIndex',
        data: {
            "grupo_motivo_id": grupo_motivo_id,
            "categoria_motivo_id": categoria_motivo_id,
            "tipo_motivo_id": tipo_motivo_id

        },
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {

            $('#myModalContent_motivo').html(data);
            $('#myModal_motivo').modal(options);
            $('#myModal_motivo').modal('show');
        },
        error: function () {
            alert("Content load failed.");
        }
    });
});


$('#elegir_motivo').on('click', function () {

    jsonString = [];
    //var arreglo = [];
    oData = table_motivo.rows('.selected').data();
    for (var i = 0; i < oData.length; i++) {
        jsonString[i] = oData[i];

        //  arreglo[i] = jsonString[i][1]
        $('#GM_Descripcion_larga').val(jsonString[i][0]);
        $('#CM_Descripcion_larga').val(jsonString[i][1]);
        $('#TMot_Descripcion_larga').val(jsonString[i][2]);
        $('#Mot_Descripcion_larga').val(jsonString[i][6]);
        $('#Mot_Codigo_empresa').val(jsonString[i][4]);

        $('#TranCE_activo_descripcion_status_ant').val(jsonString[i][14]);
        $("#Mot_id").val(jsonString[i][3]);
    }


    $('#myModal_motivo').modal('hide');


});



 /*---------------------------------------------------------------- ACTIVOS -------------------------------------------------*/
$('#GA_id').change(function () {
        //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#GA_id";
        var Ga_id = $(ddlsource).val();
        if (Ga_id !== "Seleccione") {
            $.getJSON("/Activos/GetallCategoriasBygrupo", { GA_id: $(ddlsource).val() }, function (data) {
                var items = '';
                items = "<option value= " + 0 + ">" + "Seleccione</option>";

                $("#CA_id").empty();
                $("#TA_id").empty();
                $('#CA_id').html(items);
                $('#TA_id').html(items);

              
                $.each(data, function (i, modelo) {
                    items += "<option value='" + modelo.cA_id + "'>" + modelo.cA_Descripcion_larga + "</option>";
                });
                $('#CA_id').html(items);
            });
        } else {
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $('#CA_id').html(items);
        }
    });


$('#CA_id').change('input', function () {
 
    var ddlsource = "#CA_id";
    var Ca_id = $(ddlsource).val();
    if (Ca_id !== "Seleccione") {
        $.getJSON("/Activos/GetallTiposBycategoria", { CA_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

          
            $("#TA_id").empty();
            $('#TA_id').html(items);
          

            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tA_id + "'>" + modelo.tA_Descripcion_larga + "</option>";
            });
            $('#TA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#TA_id').html(items);
    }
});

$("#buscar_activos").click(function () {
    var $buttonClicked = $(this);
    var id = $buttonClicked.attr('data-id');
    var options = { "backdrop": "static", keyboard: true };

    var grupo_activo_id = $('#GA_id').val();
    var categoria_activo_id = $('#CA_id').val();
    var tipo_activo_id = $('#TA_id').val();


    $.ajax({
        type: "GET",
        url: '/Activos/PartialIndex',
        data: {
            "grupo_activo_id": grupo_activo_id,
            "categoria_activo_id": categoria_activo_id,
            "tipo_activo_id": tipo_activo_id

        },
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {

            $('#myModalContent_activo').html(data);
            $('#myModal_activo').modal(options);
            $('#myModal_activo').modal('show');
        },
        error: function () {
            alert("Content load failed.");
        }
    });
});




$('#elegir_activo').on('click', function () {

    jsonString = [];
    //var arreglo = [];
    oData = table_activo.rows('.selected').data();
    for (var i = 0; i < oData.length; i++) {
        jsonString[i] = oData[i];

        //  arreglo[i] = jsonString[i][1]
        $('#GA_Descripcion_larga').val(jsonString[i][0]);
        $('#CA_Descripcion_larga').val(jsonString[i][1]);
        $('#TA_Descripcion_larga').val(jsonString[i][2]);
        $('#A_Descripcion_larga').val(jsonString[i][6]);
        $('#A_Codigo_empresa').val(jsonString[i][4]);
        $('#A_Serial').val(jsonString[i][13]);

        $('#A_status_descripcion_larga').val(jsonString[i][14]);
        $('#A_id').val(jsonString[i][3]);
    }

 
    $('#myModal_activo').modal('hide');

});
$('#modal_activo').click(function () {
    var codigo_empresa = $('#A_Codigo_empresa1').val();

    if (codigo_empresa !== "") {

        $.ajax({
            url: "/CambiarStatus/GetActivoByCodEmpresa",
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8', //define a contentType of your request
            cache: false,
            data: { codigo_empresa: codigo_empresa },
            success: function (data) {

                if (data.success) {
                    $('#GA_Descripcion_larga').val(data.activo.grupos_activos.gA_Descripcion_larga);
                    $('#CA_Descripcion_larga').val(data.activo.categorias_activos.cA_Descripcion_larga);
                    $('#TA_Descripcion_larga').val(data.activo.tipos_activos.tA_Descripcion_larga);
                    $('#A_Descripcion_larga').val(data.activo.a_Descripcion_larga);
                    $('#A_Codigo_empresa').val(data.activo.a_Codigo_empresa);
                    $('#A_Serial').val(data.activo.a_Serial);
                    $('#TranCE_activo_descripcion_status_ant').val(data.activo.a_status_descripcion_larga);
                    $("#A_id").val(data.activo.a_id);

                } else {

                    alert("ACTIVO NO ENCONTRADO");
                }
            },
            error: function (xhr, error) {
                alert(xhr);
                alert(error);
            }
        });
    } else {

        alert("INGRESE EL CODIGO DE EMPRESA DEL ACTIVO");

    }
});

