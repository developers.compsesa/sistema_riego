$('#Grupos_fallas_reportadas_GFRep_id').change(function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_fallas_reportadas_GFRep_id";
    var GFRep_id = $(ddlsource).val();
    if (GFRep_id !== "Seleccione") {
        $.getJSON("/Fallas_reportadas/GetallCategoria_fallas_reportadasBygrupo", { GFRep_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_fallas_reportadas.CFRep_id").empty();
            $("#Categorias_fallas_reportadas.CFRep_id").html(items);
            $("#Tipos_fallas_reportadas_TFRep_id").empty();
            $("#Tipos_fallas_reportadas_TFRep_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cfRep_id + "'>" + modelo.cfRep_Descripcion_corta + "</option>";
            });
            $('#Categorias_fallas_reportadas_CFRep_id').html(items);
            
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_fallas_reportadas_CFRep_id').html(items);
        $('#Tipos_fallas_reportadas_TFRep_id').html(items);
    }
});


$('#Categorias_fallas_reportadas_CFRep_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallTiposBycategoria";
    var ddlsource = "#Categorias_fallas_reportadas_CFRep_id";
    var CFRep_id = $(ddlsource).val();
    if (CFRep_id !== "Seleccione") {
        $.getJSON("/Fallas_reportadas/GetallTipos_fallas_reportadasBycategoria", { CFRep_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_fallas_reportadas_TFRep_id").empty();
            $("#Tipos_fallas_reportadas_TFRep_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tfRep_id + "'>" + modelo.tfRep_Descripcion_corta + "</option>";
            });
            $('#Tipos_fallas_reportadas_TFRep_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_fallas_reportadas_TFRep_id').html(items);
    }
});



