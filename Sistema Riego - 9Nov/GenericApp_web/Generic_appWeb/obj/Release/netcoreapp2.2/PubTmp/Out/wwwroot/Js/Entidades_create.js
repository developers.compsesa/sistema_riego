﻿$('#Grupos_entidades_GEn_id').change(function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_entidades_GEn_id";
    var GEn_id = $(ddlsource).val();
    if (GEn_id !== "Seleccione") {
        $.getJSON("/Categorias_entidades/GetallCategorias_entidadesBygrupo", { GEn_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_entidades_CEn_id").empty();
            $("#Categorias_entidades_CEn_id").html("<option value= " + 0 + ">" + "Seleccione</option>");
            $("#Tipos_entidades_TEn_id").empty();
            $("#Tipos_entidades_TEn_id").html("<option value= " + 0 + ">" + "Seleccione</option>");
         


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cEn_id + "'>" + modelo.cEn_Descripcion_corta + "</option>";
            });
            $('#Categorias_entidades_CEn_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_entidades_CEn_id').html(items);
        $('#Tipos_entidades_TEn_id').html(items);
    }
});


$('#Categorias_entidades_CEn_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallTiposBycategoria";
    var ddlsource = "#Categorias_entidades_CEn_id";
    var CEn_id = $(ddlsource).val();
    if (CEn_id !== "Seleccione") {
        $.getJSON("/Tipos_entidades/GetallTipos_entidadesByCategoria", { CEn_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_entidades_TEn_id").empty();
            $("#Tipos_entidades_TEn_id").html("<option value= " + 0 + ">" + "Seleccione</option>");
         
          
           
            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tEn_id + "'>" + modelo.tEn_Descripcion_corta + "</option>";
            });
            $('#Tipos_entidades_TEn_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_entidades_TEn_id').html(items);
    }
});

