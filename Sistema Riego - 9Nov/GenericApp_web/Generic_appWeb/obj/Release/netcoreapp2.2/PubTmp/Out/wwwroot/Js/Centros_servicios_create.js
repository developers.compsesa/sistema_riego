//created by israel Pinargote 2018
$("#Grupos_centros_servicios_GCSer_id").change(function () {
    $.getJSON("/Centros_servicios/GetallCategoriasBygrupo_centros_servicios", {
        GCSer_id: $("#Grupos_centros_servicios_GCSer_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Categorias_centros_servicios_CCSer_id").empty(), $("#Tipos_centros_servicios_TCSer_id").empty(), $("#Tipos_centros_servicios_TCSer_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.ccSer_id + "'>" + a.ccSer_Descripcion_corta + "</option>"
        }), $("#Categorias_centros_servicios_CCSer_id").html(o)
    })
}), $("#Categorias_centros_servicios_CCSer_id").change("input", function () {
    $.getJSON("/Centros_servicios/GetallTiposBycategoria_centros_servicios", {
        CCSer_id: $("#Categorias_centros_servicios_CCSer_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Tipos_centros_servicios_TCSer_id").empty(), $("#Tipos_centros_servicios_TCSer_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.tcSer_id + "'>" + a.tcSer_Descripcion_corta + "</option>"
        }), $("#Tipos_centros_servicios_TCSer_id").html(o)
    })
}), $("#Provincias_Pr_id").change("input", function () {
    $.getJSON("/Centros_servicios/GetallCiudadesbyProvincias", {
        Pr_id: $("#Provincias_Pr_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Ciudades_Ci_id").empty(), $("#Ciudades_Ci_id").html(o), $("#Zonas_Zo_id").empty(), $("#Zonas_Zo_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.ci_id + "'>" + a.ci_Descripcion_corta + "</option>"
        }), $("#Ciudades_Ci_id").html(o)
    })
}), $("#Ciudades_Ci_id").change("input", function () {
    $.getJSON("/Centros_servicios/GetallZonasByCiudades", {
        Ci_id: $("#Ciudades_Ci_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Zonas_Zo_id").empty(), $("#Zonas_Zo_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.zo_id + "'>" + a.zo_Descripcion_corta + "</option>"
        }), $("#Zonas_Zo_id").html(o)
    })
});