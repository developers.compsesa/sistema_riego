#pragma checksum "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ee35159240df7fe758d14a5e3558272132cbb83e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Pendiente_Index), @"mvc.1.0.view", @"/Views/Pendiente/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Pendiente/Index.cshtml", typeof(AspNetCore.Views_Pendiente_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb;

#line default
#line hidden
#line 2 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ee35159240df7fe758d14a5e3558272132cbb83e", @"/Views/Pendiente/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f06a2c7a514c84e177b74e75c87dcd856c572c53", @"/Views/_ViewImports.cshtml")]
    public class Views_Pendiente_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Generic_appWeb.Models.Pendiente>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-route-usu", " item.Pen_usuario_creacion", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-info"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onclick", new global::Microsoft.AspNetCore.Html.HtmlString("return confirm(\'Se eliminara el registro\');"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.5/plugins/datatables/jquery.dataTables.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.5/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.5/plugins/datatables-responsive/js/dataTables.responsive.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.5/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_10 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Js/Tipos_estados_index.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
  
    ViewData["Nombre_tabla"] = "Pendiente";
    ViewData["lugar"] = "Pendiente";

    Layout = "_index"; 

#line default
#line hidden
            BeginContext(115, 12, true);
            WriteLiteral("\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
            BeginContext(180, 100, true);
            WriteLiteral("\r\n<div class=\"card-body\">\r\n    <table id=\"example1\" class=\"table table-bordered table-striped \">\r\n\r\n");
            EndContext();
            BeginContext(801, 643, true);
            WriteLiteral(@"
        <thead class=""filters"">
            <tr>
                <th>Interno</th>
                <th>Empresa</th>
                <th>Alterno</th>

                <th>Larga</th>
                <th>Mediana</th>
                <th>Corta</th>

                <th>Abreviacion</th>
                <th>Observacion</th>

                <th>Creacion</th>
                <th>Modificacion</th>
                <th>Creacion</th>
                <th>Modificacion</th>
                <th>Secuencia</th>


                <th></th>
                <th></th>
            </tr>

        </thead>



        <tbody>


");
            EndContext();
#line 63 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
            BeginContext(1501, 72, true);
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1574, 41, false);
#line 67 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_id));

#line default
#line hidden
            EndContext();
            BeginContext(1615, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1697, 53, false);
#line 71 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_codigo_empresa));

#line default
#line hidden
            EndContext();
            BeginContext(1750, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1830, 53, false);
#line 74 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_codigo_alterno));

#line default
#line hidden
            EndContext();
            BeginContext(1883, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1963, 56, false);
#line 77 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_descripcion_larga));

#line default
#line hidden
            EndContext();
            BeginContext(2019, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2101, 54, false);
#line 81 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_descripcion_med));

#line default
#line hidden
            EndContext();
            BeginContext(2155, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2237, 56, false);
#line 85 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(2293, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2375, 50, false);
#line 89 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_abreviacion));

#line default
#line hidden
            EndContext();
            BeginContext(2425, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2507, 50, false);
#line 93 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_observacion));

#line default
#line hidden
            EndContext();
            BeginContext(2557, 85, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2643, 53, false);
#line 99 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_fecha_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(2696, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2776, 57, false);
#line 102 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_fecha_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(2833, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2913, 78, false);
#line 105 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_usuario_creacion.Users_usuario_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(2991, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3071, 86, false);
#line 108 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_usuario_modificacion.Users_usuario_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(3157, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3241, 48, false);
#line 113 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Pen_secuencia));

#line default
#line hidden
            EndContext();
            BeginContext(3289, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3372, 171, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ee35159240df7fe758d14a5e3558272132cbb83e16037", async() => {
                BeginContext(3533, 6, true);
                WriteLiteral("Editar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 118 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                                               WriteLiteral(item.Pen_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-usu", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["usu"] = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            BeginWriteTagHelperAttribute();
#line 118 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                                                                                                                        WriteLiteral(item.Pen_usuario_modificacion);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["usu1"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-usu1", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["usu1"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3543, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3622, 140, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ee35159240df7fe758d14a5e3558272132cbb83e19930", async() => {
                BeginContext(3750, 8, true);
                WriteLiteral("Eliminar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 121 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
                                                                        WriteLiteral(item.Pen_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3762, 52, true);
            WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
            EndContext();
#line 124 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Pendiente\Index.cshtml"
            }

#line default
#line hidden
            BeginContext(3829, 62, true);
            WriteLiteral("\r\n        </tbody>\r\n\r\n\r\n\r\n    </table>\r\n\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(3914, 51, true);
                WriteLiteral("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n    <!-- DataTables -->\r\n    ");
                EndContext();
                BeginContext(3965, 84, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ee35159240df7fe758d14a5e3558272132cbb83e23201", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4049, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(4055, 95, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ee35159240df7fe758d14a5e3558272132cbb83e24457", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4150, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(4156, 102, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ee35159240df7fe758d14a5e3558272132cbb83e25714", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4258, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(4264, 102, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ee35159240df7fe758d14a5e3558272132cbb83e26971", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4366, 8, true);
                WriteLiteral("\r\n\r\n    ");
                EndContext();
                BeginContext(4374, 51, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ee35159240df7fe758d14a5e3558272132cbb83e28231", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_10);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4425, 527, true);
                WriteLiteral(@"






    <script>
        $(function () {
            $(""#example1"").DataTable({
                ""responsive"": true,
                ""autoWidth"": false,
            });
            $('#example2').DataTable({
                ""paging"": true,
                ""lengthChange"": false,
                ""searching"": false,
                ""ordering"": true,
                ""info"": true,
                ""autoWidth"": false,
                ""responsive"": true,
            });
        });
    </script>




");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Generic_appWeb.Models.Pendiente>> Html { get; private set; }
    }
}
#pragma warning restore 1591
