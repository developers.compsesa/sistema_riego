#pragma checksum "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Shared\_input.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "523c13bcefa293a65f553603f3371a412549229a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__input), @"mvc.1.0.view", @"/Views/Shared/_input.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/_input.cshtml", typeof(AspNetCore.Views_Shared__input))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb;

#line default
#line hidden
#line 2 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"523c13bcefa293a65f553603f3371a412549229a", @"/Views/Shared/_input.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f06a2c7a514c84e177b74e75c87dcd856c572c53", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__input : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("hold-transition sidebar-mini layout-fixed"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 25, true);
            WriteLiteral("<!DOCTYPE html>\r\n<html>\r\n");
            EndContext();
            BeginContext(25, 1574, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "523c13bcefa293a65f553603f3371a412549229a3898", async() => {
                BeginContext(31, 100, true);
                WriteLiteral("\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <title>");
                EndContext();
                BeginContext(132, 17, false);
#line 6 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Shared\_input.cshtml"
      Write(ViewData["Title"]);

#line default
#line hidden
                EndContext();
                BeginContext(149, 1443, true);
                WriteLiteral(@" - app_riego</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name=""viewport"" content=""width=device-width, initial-scale=1"">
    <!-- Font Awesome -->
    <link rel=""stylesheet"" href=""AdminLTE-3.0.5/plugins/fontawesome-free/css/all.min.css"">
    <!-- Ionicons -->
    <link rel=""stylesheet"" href=""AdminLTE-3.0.5/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel=""stylesheet"" href=""AdminLTE-3.0.5/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"">
    <!-- iCheck -->
    <link rel=""stylesheet"" href=""AdminLTE-3.0.5/plugins/icheck-bootstrap/icheck-bootstrap.min.css"">
    <!-- JQVMap -->
    <link rel=""stylesheet"" href=""AdminLTE-3.0.5/plugins/jqvmap/jqvmap.min.css"">
    <!-- Theme style -->
    <link rel=""stylesheet"" href=""AdminLTE-3.0.5/dist/css/adminlte.min.css"">
    <!-- overlayScrollbars -->
    <link rel=""stylesheet"" href=""AdminLTE-3.0.5/plugins/overlayScrollbars/css/Ov");
                WriteLiteral(@"erlayScrollbars.min.css"">
    <!-- Daterange picker -->
    <link rel=""stylesheet"" href=""AdminLTE-3.0.5/plugins/daterangepicker/daterangepicker.css"">
    <!-- summernote -->
    <link rel=""stylesheet"" href=""AdminLTE-3.0.5/plugins/summernote/summernote-bs4.css"">
    <!-- Google Font: Source Sans Pro -->
    <link href=""https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700"" rel=""stylesheet"">
");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1599, 12, true);
            WriteLiteral("\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
            BeginContext(1611, 18173, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "523c13bcefa293a65f553603f3371a412549229a7134", async() => {
                BeginContext(1667, 14630, true);
                WriteLiteral(@"

    <div class=""wrapper"">

        <!-- Navbar -->
        <nav class=""main-header navbar navbar-expand navbar-white navbar-light"">
            <!-- Left navbar links -->
            <ul class=""navbar-nav"">
                <li class=""nav-item"">
                    <a class=""nav-link"" data-widget=""pushmenu"" href=""#"" role=""button""><i class=""fas fa-bars""></i></a>
                </li>
                <li class=""nav-item d-none d-sm-inline-block"">
                    <a href=""index3.html"" class=""nav-link"">Inicio</a>
                </li>

            </ul>

            <!-- SEARCH FORM -->
            <!--
            <form class=""form-inline ml-3"">
              <div class=""input-group input-group-sm"">
                <input class=""form-control form-control-navbar"" type=""search"" placeholder=""Search"" aria-label=""Search"">
                <div class=""input-group-append"">
                  <button class=""btn btn-navbar"" type=""submit"">
                    <i class=""fas fa-search""></i>
     ");
                WriteLiteral(@"             </button>
                </div>
              </div>
            </form>
            -->
            <!-- Right navbar links -->
            <ul class=""navbar-nav ml-auto"">
                <!-- Messages Dropdown Menu -->
                <li class=""nav-item dropdown"">
                    <a class=""nav-link"" data-toggle=""dropdown"" href=""#"">
                        <i class=""fas fa-redo""></i>

                    </a>
                    <div class=""dropdown-menu dropdown-menu-lg dropdown-menu-right"">
                        <a href=""#"" class=""dropdown-item"">
                            <!-- Message Start -->
                            <div class=""media"">
                                <img src=""dist/img/user1-128x128.jpg"" alt=""User Avatar"" class=""img-size-50 mr-3 img-circle"">
                                <div class=""media-body"">
                                    <h3 class=""dropdown-item-title"">
                                        Brad Diesel
                        ");
                WriteLiteral(@"                <span class=""float-right text-sm text-danger""><i class=""fas fa-star""></i></span>
                                    </h3>
                                    <p class=""text-sm"">Call me whenever you can...</p>
                                    <p class=""text-sm text-muted""><i class=""far fa-clock mr-1""></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class=""dropdown-divider""></div>
                        <a href=""#"" class=""dropdown-item"">
                            <!-- Message Start -->
                            <div class=""media"">
                                <img src=""dist/img/user8-128x128.jpg"" alt=""User Avatar"" class=""img-size-50 img-circle mr-3"">
                                <div class=""media-body"">
                                    <h3 class=""dropdown-item-title"">
                                        John ");
                WriteLiteral(@"Pierce
                                        <span class=""float-right text-sm text-muted""><i class=""fas fa-star""></i></span>
                                    </h3>
                                    <p class=""text-sm"">I got your message bro</p>
                                    <p class=""text-sm text-muted""><i class=""far fa-clock mr-1""></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class=""dropdown-divider""></div>
                        <a href=""#"" class=""dropdown-item"">
                            <!-- Message Start -->
                            <div class=""media"">
                                <img src=""dist/img/user3-128x128.jpg"" alt=""User Avatar"" class=""img-size-50 img-circle mr-3"">
                                <div class=""media-body"">
                                    <h3 class=""dropdown-item-title"">
                   ");
                WriteLiteral(@"                     Nora Silvester
                                        <span class=""float-right text-sm text-warning""><i class=""fas fa-star""></i></span>
                                    </h3>
                                    <p class=""text-sm"">The subject goes here</p>
                                    <p class=""text-sm text-muted""><i class=""far fa-clock mr-1""></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class=""dropdown-divider""></div>
                        <a href=""#"" class=""dropdown-item dropdown-footer"">See All Messages</a>
                    </div>


                </li>


                <!-- Notifications Dropdown Menu -->
                <li class=""nav-item dropdown"">
                    <a class=""nav-link"" data-toggle=""dropdown"" href=""#"">
                        <i class=""far fa-bell""></i>
                    ");
                WriteLiteral(@"    <span class=""badge badge-warning navbar-badge"">15</span>
                    </a>
                    <div class=""dropdown-menu dropdown-menu-lg dropdown-menu-right"">
                        <span class=""dropdown-item dropdown-header"">15 Notifications</span>
                        <div class=""dropdown-divider""></div>
                        <a href=""#"" class=""dropdown-item"">
                            <i class=""fas fa-envelope mr-2""></i> 4 new messages
                            <span class=""float-right text-muted text-sm"">3 mins</span>
                        </a>
                        <div class=""dropdown-divider""></div>
                        <a href=""#"" class=""dropdown-item"">
                            <i class=""fas fa-users mr-2""></i> 8 friend requests
                            <span class=""float-right text-muted text-sm"">12 hours</span>
                        </a>
                        <div class=""dropdown-divider""></div>
                        <a href=""#"" class=""dropdown");
                WriteLiteral(@"-item"">
                            <i class=""fas fa-file mr-2""></i> 3 new reports
                            <span class=""float-right text-muted text-sm"">2 days</span>
                        </a>
                        <div class=""dropdown-divider""></div>
                        <a href=""#"" class=""dropdown-item dropdown-footer"">See All Notifications</a>
                    </div>
                </li>
                <li class=""nav-item"">
                    <a class=""nav-link"" data-widget=""control-sidebar"" data-slide=""true"" href=""#"" role=""button"">
                        <i class=""fas fa-th-large""></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class=""main-sidebar sidebar-dark-primary elevation-4"">
            <!-- Brand Logo -->
            <a href=""index3.html"" class=""brand-link"">


                <!--
                      <img src=""dist/img/AdminLTELogo.png"" al");
                WriteLiteral(@"t=""AdminLTE Logo"" class=""brand-image img-circle elevation-3""
                           style=""opacity: .8"">

                -->


                <span class=""brand-text font-weight-light"">Compsesa</span>
            </a>

            <!-- Sidebar -->
            <div class=""sidebar"">
                <!-- Sidebar user panel (optional) -->
                <div class=""user-panel mt-3 pb-3 mb-3 d-flex"">
                    <div class=""image"">

                        <!--
                                  <img src=""dist/img/user2-160x160.jpg"" class=""img-circle elevation-2"" alt=""User Image"">

                        -->


                    </div>
                    <div class=""info"">
                        <a href=""#"" class=""d-block"">Stephany</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class=""mt-2"">
                    <ul class=""nav nav-pills nav-sidebar flex-column"" data-widget=""treeview"" role=""menu"" data-ac");
                WriteLiteral(@"cordion=""false"">
                        <!-- Add icons to the links using the .nav-icon class
                             with font-awesome or any other icon font library -->
                        <li class=""nav-item has-treeview"">
                            <a href=""#"" class=""nav-link"">
                                <i class=""nav-icon fas fa-tachometer-alt""></i>
                                <p>
                                    Dashboard
                                    <i class=""right fas fa-angle-left""></i>
                                </p>
                            </a>


                            <ul class=""nav nav-treeview ml-2"">
                                <li class=""nav-item"">
                                    <a href=""#"" class=""nav-link active"">
                                        <i class=""far fa-circle nav-icon""></i>
                                        <p>Dashboard 1</p>
                                    </a>
                                <");
                WriteLiteral(@"/li>
                                <li class=""nav-item"">
                                    <a href=""#"" class=""nav-link"">
                                        <i class=""far fa-circle nav-icon""></i>
                                        <p>Dashboard 2</p>
                                    </a>
                                </li>
                                <li class=""nav-item"">
                                    <a href=""#"" class=""nav-link"">
                                        <i class=""far fa-circle nav-icon""></i>
                                        <p>Dashboard 3</p>
                                    </a>
                                </li>
                            </ul>


                        </li>

                        <li class=""nav-item has-treeview"">
                            <a href=""#"" class=""nav-link"">
                                <i class=""nav-icon fas fa-copy""></i>
                                <p>
                                  ");
                WriteLiteral(@"  Programacion
                                    <i class=""fas fa-angle-left right""></i>
                                </p>
                            </a>
                            <ul class=""nav nav-treeview ml-2"">
                                <li class=""nav-item"">
                                    <a href=""#"" class=""nav-link"">
                                        <i class=""nav-icon fas fa-th""></i>
                                        <p>
                                            Invent. programa diario
                                        </p>
                                    </a>
                                </li>
                                <li class=""nav-item"">
                                    <a href=""#"" class=""nav-link"">
                                        <i class=""nav-icon fas fa-edit""></i>
                                        <p>
                                            Programa diario

                                        </p>
  ");
                WriteLiteral(@"                                  </a>
                                </li>

                                <li class=""nav-item"">
                                    <a href=""#"" class=""nav-link"">
                                        <i class=""nav-icon far fa-calendar-alt""></i>
                                        <p>
                                            Agendar riego
                                        </p>
                                    </a>
                                </li>

                            </ul>
                        </li>


                        <li class=""nav-item has-treeview"">
                            <a href=""#"" class=""nav-link"">
                                <i class=""nav-icon fas fa-chart-pie""></i>
                                <p>
                                    Charts

                                </p>
                            </a>

                        </li>

                        <li class=""nav-item"">
 ");
                WriteLiteral(@"                           <a href=""#"" class=""nav-link"">
                                <i class=""nav-icon fas fa-th""></i>
                                <p>
                                    Equipos
                                </p>
                            </a>
                        </li>



                        <li class=""nav-item has-treeview"">
                            <a href=""#"" class=""nav-link"">
                                <i class=""nav-icon fas fa-table""></i>
                                <p>
                                    Tablas

                                </p>
                            </a>
                        </li>



                        <li class=""nav-item"">
                            <a href=""#"" class=""nav-link"">
                                <i class=""nav-icon fas fa-file""></i>
                                <p>Documentation</p>
                            </a>
                        </li>

                        <li ");
                WriteLiteral(@"class=""nav-item has-treeview"">
                            <a href=""#"" class=""nav-link"">
                                <i class=""nav-icon far fa-plus-square""></i>
                                <p>
                                    Extras

                                </p>
                            </a>
                        </li>



                        <!-- ICONOS VAROS QUE SE PUEDEN USAR EN ALGUN MOMENTO
                        <i class=""nav-icon fas fa-book""></i>
                        <i class=""nav-icon far fa-image""></i>
                        <i class=""nav-icon far fa-envelope""></i>

                        -->






                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class=""content-wrapper"">



            <div class=""content-header"">
                <div class=""container-fluid"">
                    <di");
                WriteLiteral(@"v class=""row mb-2"">

                        <div class=""col-sm-6"">
                            <ol class=""breadcrumb float-sm-left"">
                                <li class=""breadcrumb-item""><a href=""#"">Inicio</a></li>
                                <li class=""breadcrumb-item active"">");
                EndContext();
                BeginContext(16298, 17, false);
#line 360 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Shared\_input.cshtml"
                                                              Write(ViewData["Lugar"]);

#line default
#line hidden
                EndContext();
                BeginContext(16315, 400, true);
                WriteLiteral(@"</li><!--AQUI VA EL LUGAR DONDE ESTAS -->
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
















            <div class=""container-fluid"">
                <div class=""card card-primary"">
                    <div class=""card-header"">
                        <h3 class=""card-title"">");
                EndContext();
                BeginContext(16716, 24, false);
#line 385 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Shared\_input.cshtml"
                                          Write(ViewData["Titulo_lugar"]);

#line default
#line hidden
                EndContext();
                BeginContext(16740, 216, true);
                WriteLiteral("</h3>\r\n                    </div>\r\n\r\n                    <div class=\"card-body\">\r\n                        <div class=\"row\">\r\n\r\n                            <div class=\"col-md-12\">\r\n\r\n\r\n                                ");
                EndContext();
                BeginContext(16957, 12, false);
#line 394 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Shared\_input.cshtml"
                           Write(RenderBody());

#line default
#line hidden
                EndContext();
                BeginContext(16969, 2808, true);
                WriteLiteral(@"

                                







                            </div>

                        </div>
                    </div>
                </div>

            </div>



























            
            <!-- /.content -->
        </div>













        <!-- /.content-wrapper -->
        <footer class=""main-footer"">
            <strong>Copyright &copy; 2020 <a href=""#"">Sistema de riego</a>.</strong>
            All rights reserved.
            <div class=""float-right d-none d-sm-inline-block"">
                <b>Version</b> 1.0.0
            </div>
        </footer>

        <!-- Control Sidebar -->
        <aside class=""control-sidebar control-sidebar-dark"">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    <!-- jQuery -->
    <script src=""AdminLTE-3.0.5/plugins/jquery/jquery.min.js""></script>
    <!-- jQue");
                WriteLiteral(@"ry UI 1.11.4 -->
    <script src=""AdminLTE-3.0.5/plugins/jquery-ui/jquery-ui.min.js""></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src=""AdminLTE-3.0.5/plugins/bootstrap/js/bootstrap.bundle.min.js""></script>
    <!-- ChartJS -->
    <script src=""AdminLTE-3.0.5/plugins/chart.js/Chart.min.js""></script>
    <!-- Sparkline -->
    <script src=""AdminLTE-3.0.5/plugins/sparklines/sparkline.js""></script>
    <!-- JQVMap -->
    <script src=""AdminLTE-3.0.5/plugins/jqvmap/jquery.vmap.min.js""></script>
    <script src=""AdminLTE-3.0.5/plugins/jqvmap/maps/jquery.vmap.usa.js""></script>
    <!-- jQuery Knob Chart -->
    <script src=""AdminLTE-3.0.5/plugins/jquery-knob/jquery.knob.min.js""></script>
    <!-- daterangepicker -->
    <script src=""AdminLTE-3.0.5/plugins/moment/moment.min.js""></script>
    <script src=""AdminLTE-3.0.5/plugins/daterangepicker/datera");
                WriteLiteral(@"ngepicker.js""></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src=""AdminLTE-3.0.5/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js""></script>
    <!-- Summernote -->
    <script src=""AdminLTE-3.0.5/plugins/summernote/summernote-bs4.min.js""></script>
    <!-- overlayScrollbars -->
    <script src=""AdminLTE-3.0.5/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js""></script>
    <!-- AdminLTE App -->
    <script src=""AdminLTE-3.0.5/dist/js/adminlte.js""></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src=""AdminLTE-3.0.5/dist/js/pages/dashboard.js""></script>
    <!-- AdminLTE for demo purposes -->
    <script src=""AdminLTE-3.0.5/dist/js/demo.js""></script>
");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(19784, 23, true);
            WriteLiteral("\r\n</html>\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
