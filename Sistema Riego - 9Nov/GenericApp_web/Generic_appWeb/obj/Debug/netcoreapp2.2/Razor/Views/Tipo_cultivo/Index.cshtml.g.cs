#pragma checksum "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b015f753890accb56426fb155553d6bcd3fcf74e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Tipo_cultivo_Index), @"mvc.1.0.view", @"/Views/Tipo_cultivo/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Tipo_cultivo/Index.cshtml", typeof(AspNetCore.Views_Tipo_cultivo_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb;

#line default
#line hidden
#line 2 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\_ViewImports.cshtml"
using Generic_appWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b015f753890accb56426fb155553d6bcd3fcf74e", @"/Views/Tipo_cultivo/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f06a2c7a514c84e177b74e75c87dcd856c572c53", @"/Views/_ViewImports.cshtml")]
    public class Views_Tipo_cultivo_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Generic_appWeb.Models.Tipo_cultivo>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-route-usu", " item.TCul_usuario_creacion", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-info"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onclick", new global::Microsoft.AspNetCore.Html.HtmlString("return confirm(\'Se eliminara el registro\');"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.5/plugins/datatables/jquery.dataTables.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.5/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.5/plugins/datatables-responsive/js/dataTables.responsive.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.5/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_10 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Js/Tipos_estados_index.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
  
    ViewData["Nombre_tabla"] = "Tipo cultivo";
    ViewData["sitio"] = "Tipo cultivo";

    Layout = "_index"; 

#line default
#line hidden
            BeginContext(121, 12, true);
            WriteLiteral("\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
            BeginContext(189, 100, true);
            WriteLiteral("\r\n<div class=\"card-body\">\r\n    <table id=\"example1\" class=\"table table-bordered table-striped \">\r\n\r\n");
            EndContext();
            BeginContext(810, 707, true);
            WriteLiteral(@"
        <thead class=""filters"">
            <tr>
                
                <th>Categoria cultivo</th>

                <th>Interno</th>
                <th>Empresa</th>
                <th>Alterno</th>

                <th>Larga</th>
                <th>Mediana</th>
                <th>Corta</th>

                <th>Abreviacion</th>
                <th>Observacion</th>

                <th>Creacion</th>
                <th>Modificacion</th>
                <th>Creacion</th>
                <th>Modificacion</th>
                <th>Secuencia</th>


                <th></th>
                <th></th>
            </tr>

        </thead>



        <tbody>


");
            EndContext();
#line 66 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
            BeginContext(1574, 96, true);
            WriteLiteral("                <tr>\r\n                    \r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1671, 73, false);
#line 72 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Categoria_cultivo.CCul_descripcion_med));

#line default
#line hidden
            EndContext();
            BeginContext(1744, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1826, 42, false);
#line 76 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_id));

#line default
#line hidden
            EndContext();
            BeginContext(1868, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1950, 54, false);
#line 80 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_codigo_empresa));

#line default
#line hidden
            EndContext();
            BeginContext(2004, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2084, 54, false);
#line 83 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_codigo_alterno));

#line default
#line hidden
            EndContext();
            BeginContext(2138, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2218, 57, false);
#line 86 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_descripcion_larga));

#line default
#line hidden
            EndContext();
            BeginContext(2275, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2357, 55, false);
#line 90 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_descripcion_med));

#line default
#line hidden
            EndContext();
            BeginContext(2412, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2494, 57, false);
#line 94 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_descripcion_corta));

#line default
#line hidden
            EndContext();
            BeginContext(2551, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2633, 51, false);
#line 98 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_abreviacion));

#line default
#line hidden
            EndContext();
            BeginContext(2684, 81, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2766, 51, false);
#line 102 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_observacion));

#line default
#line hidden
            EndContext();
            BeginContext(2817, 85, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2903, 54, false);
#line 108 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_fecha_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(2957, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3037, 58, false);
#line 111 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_fecha_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(3095, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3175, 79, false);
#line 114 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_usuario_creacion.Users_usuario_creacion));

#line default
#line hidden
            EndContext();
            BeginContext(3254, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3334, 87, false);
#line 117 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_usuario_modificacion.Users_usuario_modificacion));

#line default
#line hidden
            EndContext();
            BeginContext(3421, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3505, 49, false);
#line 122 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TCul_secuencia));

#line default
#line hidden
            EndContext();
            BeginContext(3554, 83, true);
            WriteLiteral("\r\n                    </td>\r\n\r\n\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3637, 174, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b015f753890accb56426fb155553d6bcd3fcf74e16761", async() => {
                BeginContext(3801, 6, true);
                WriteLiteral("Editar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 127 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                                               WriteLiteral(item.TCul_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-usu", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["usu"] = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            BeginWriteTagHelperAttribute();
#line 127 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                                                                                                                          WriteLiteral(item.TCul_usuario_modificacion);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["usu1"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-usu1", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["usu1"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3811, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(3890, 141, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b015f753890accb56426fb155553d6bcd3fcf74e20664", async() => {
                BeginContext(4019, 8, true);
                WriteLiteral("Eliminar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 130 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
                                                                        WriteLiteral(item.TCul_id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4031, 52, true);
            WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
            EndContext();
#line 133 "C:\Users\User\Documents\sistema_riego\sistema_riego\Sistema Riego - 9Nov\GenericApp_web\Generic_appWeb\Views\Tipo_cultivo\Index.cshtml"
            }

#line default
#line hidden
            BeginContext(4098, 62, true);
            WriteLiteral("\r\n        </tbody>\r\n\r\n\r\n\r\n    </table>\r\n\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(4183, 51, true);
                WriteLiteral("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n    <!-- DataTables -->\r\n    ");
                EndContext();
                BeginContext(4234, 84, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b015f753890accb56426fb155553d6bcd3fcf74e23942", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4318, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(4324, 95, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b015f753890accb56426fb155553d6bcd3fcf74e25198", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4419, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(4425, 102, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b015f753890accb56426fb155553d6bcd3fcf74e26455", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4527, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(4533, 102, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b015f753890accb56426fb155553d6bcd3fcf74e27712", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4635, 8, true);
                WriteLiteral("\r\n\r\n    ");
                EndContext();
                BeginContext(4643, 51, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b015f753890accb56426fb155553d6bcd3fcf74e28972", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_10);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4694, 527, true);
                WriteLiteral(@"






    <script>
        $(function () {
            $(""#example1"").DataTable({
                ""responsive"": true,
                ""autoWidth"": false,
            });
            $('#example2').DataTable({
                ""paging"": true,
                ""lengthChange"": false,
                ""searching"": false,
                ""ordering"": true,
                ""info"": true,
                ""autoWidth"": false,
                ""responsive"": true,
            });
        });
    </script>




");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Generic_appWeb.Models.Tipo_cultivo>> Html { get; private set; }
    }
}
#pragma warning restore 1591
