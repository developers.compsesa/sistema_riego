﻿//////////////////////////////////////////////////////////////////////created by cindy zeballos 2020 ////////////////////////////////////////////////////////////////////////
$('#Grupos_datos_tecnicos_GDTec_id').change(function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_datos_tecnicos_GDTec_id";
    var GDTec_id = $(ddlsource).val();
    if (GDTec_id !== "Seleccione") {
        $.getJSON("/Inventario_descripciones_datos_tecnicos/GetallCategoriasBygrupo_datos_tecnicos", { GDTec_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_datos_tecnicos_CDTec_id").empty();
            $("#Categorias_datos_tecnicos_CDTec_id").html(items);
            $("#Tipos_datos_tecnicos_TDTec_id").empty();
            $("#Tipos_datos_tecnicos_TDTec_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cdTec_id + "'>" + modelo.cdTec_Descripcion_corta + "</option>";
            });
            $('#Categorias_datos_tecnicos_CDTec_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_datos_tecnicos_CDTec_id').html(items);
        $('#Tipos_datos_tecnicos_TDTec_id').html(items);

    }
});


$('#Categorias_datos_tecnicos_CDTec_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallTiposBycategoria";
    var ddlsource = "#Categorias_datos_tecnicos_CDTec_id";
    var CDTec_id = $(ddlsource).val();
    if (CDTec_id !== "Seleccione") {
        $.getJSON("/Inventario_descripciones_datos_tecnicos/GetallTiposBycategoria_datos_tecnicos", { CDTec_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_datos_tecnicos_TDTec_id").empty();
            $("#Tipos_datos_tecnicos_TDTec_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tdtec_id + "'>" + modelo.tdTec_Descripcion_corta + "</option>";
            });
            $('#Tipos_datos_tecnicos_TDTec_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_datos_tecnicos_TDTec_id').html(items);
    }
});



$("#buscar_datos_tecnicos").click(function () {
    var $buttonClicked = $(this);
    var id = $buttonClicked.attr('data-id');
    var options = { "backdrop": "static", keyboard: true };

    var grupo_datos_tecnicos_id = $('#Grupos_datos_tecnicos_GDTec_id').val();
    var categoria_datos_tecnicos_id = $('#Categorias_datos_tecnicos_CDTec_id').val();
    var tipos_datos_tecnicos_id = $('#Grupos_datos_tecnicos_GDTec_id').val();


    $.ajax({
        type: "GET",
        url: '/Inv_datos_tecnicos/PartialIndex_multiSelect',
        data: {
            "grupo_datos_tecnicos_id": grupo_datos_tecnicos_id,
            "categoria_datos_tecnicos_id": categoria_datos_tecnicos_id,
            "tipos_datos_tecnicos_id": tipos_datos_tecnicos_id

        },
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {

            $('#myModalContent_activo').html(data);
            $('#myModal_activo').modal(options);
            $('#myModal_activo').modal('show');
        },
        error: function () {
            alert("No se puedo cargar el contenido.");
        }
    });
});



$('#ActivosIndexTable').DataTable({
    "paging": false,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "pagingType": "full_numbers",
    "language": {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
        "sInfoEmpty": "No existen registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "paginate": {
            "previous": "Antes",
            "next": "Despues",
            "first": "Primero",
            "last": "Ultimo"
        }
    }

});

$('#elegir_dato_tecnico').on('click', function () {

    jsonString = [];
    var arreglo = [];
    oData = table_datos_tecnicos.rows('.selected').data();
    for (var i = 0; i < oData.length; i++) {
        jsonString[i] = oData[i];

        arreglo[i] = jsonString[i][3];
    }




    $('#Arreglo').val(arreglo);

    var datatable = $('#ActivosIndexTable').DataTable();

    $('#myModal_activo').modal('hide');

    datatable.clear();
    datatable.rows.add(jsonString);
    datatable.draw();


});


$('.modal-content').resizable({
    //alsoResize: ".modal-dialog",
    minHeight: 800,
    minWidth: 800
});
$('.modal-dialog').draggable();

$('#myModal_activo').on('show.bs.modal', function () {
    $(this).find('.modal-body').css({
        'max-height': '100%'
    });
});






