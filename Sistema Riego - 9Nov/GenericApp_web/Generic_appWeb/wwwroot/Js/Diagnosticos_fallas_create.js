$('#Grupos_diagnosticos_fallas_GDF_id').change(function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_diagnosticos_fallas_GDF_id";
    var GDF_id = $(ddlsource).val();
    if (GDF_id !== "Seleccione") {
        $.getJSON("/Diagnosticos_fallas/GetallCategoria_diagnosticos_fallasBygrupo", { GDF_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_diagnosticos_fallas.CDF_id").empty();
            $("#Categorias_diagnosticos_fallas.CDF_id").html(items);
            $("#Tipos_diagnosticos_fallas_CDF_id").empty();
            $("#Tipos_diagnosticos_fallas_CDF_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cdF_id + "'>" + modelo.cdF_Descripcion_corta + "</option>";
            });
            $('#Categorias_diagnosticos_fallas_CDF_id').html(items);
          
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_diagnosticos_fallas_CDF_id').html(items);
        $('#Tipos_fallas_reportadas_TFRep_id').html(items);
    }
});


$('#Categorias_diagnosticos_fallas_CDF_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallTiposBycategoria";
    var ddlsource = "#Categorias_diagnosticos_fallas_CDF_id";
    var CDF_id = $(ddlsource).val();
    if (CDF_id !== "Seleccione") {
        $.getJSON("/Diagnosticos_fallas/GetallTipos_diagnosticos_fallasBycategoria", { CDF_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_diagnosticos_fallas_TDF_id").empty();
            $("#Tipos_diagnosticos_fallas_TDF_id").html(items);

            
            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tdF_id + "'>" + modelo.tdF_Descripcion_corta + "</option>";
            });
            $('#Tipos_diagnosticos_fallas_TDF_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_diagnosticos_fallas_TDF_id').html(items);
    }
});



