﻿// DataTable
var table = $('.table').removeAttr('width').DataTable(optTable);


$('.table .filters th').each(function () {

    var title = $('.table thead th').eq($(this).index()).text();
    $(this).html('<input type="text" placeholder="Buscar" />');

});





// Apply the search
table.columns().eq(0).each(function (colIdx) {
    $('input', $('.filters th')[colIdx]).on('keyup change', function () {
        table
            .column(colIdx)
            .search(this.value)
            .draw();
    });
});