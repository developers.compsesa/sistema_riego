$('#Grupos_valor_GVal_id').change(function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_valor_GVal_id";
    var GVal_id = $(ddlsource).val();
    if (GVal_id !== "Seleccione") {
        $.getJSON("/Inventario_valor/GetallCategoriasBygrupo_valor", { GVal_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_valor_CVal_id").empty();
            $("#Categorias_valor_CVal_id").html(items);
            $("#Tipos_valor_TVal_id").empty();
            $("#Tipos_valor_TVal_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cVal_id + "'>" + modelo.cVal_Descripcion_larga + "</option>";
            });
            $('#Categorias_valor_CVal_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_valor_CVal_id').html(items);
        $('#Tipos_valor_TVal_id').html(items);
       
    }
});


$('#Categorias_valor_CVal_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallTiposBycategoria";
    var ddlsource = "#Categorias_valor_CVal_id";
    var CVal_id = $(ddlsource).val();
    if (CVal_id !== "Seleccione") {
        $.getJSON("/Inventario_valor/GetallTiposBycategoria_valor", { CVal_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_valor_TVal_id").empty();
            $("#Tipos_valor_TVal_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tVal_id + "'>" + modelo.tVal_Descripcion_larga + "</option>";
            });
            $('#Tipos_valor_TVal_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_valor_TVal_id').html(items);
    }
});



