// created by Israel pinargote 2018
$("#Grupos_lugares_GL_id").change(function () {
    $.getJSON("/Lugares/GetallCategoriasBygrupo_lugares", {
        GL_id: $("#Grupos_lugares_GL_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Categorias_lugares_CL_id").empty(), $("#Tipos_lugares_TL_id").empty(), $("#Tipos_lugares_TL_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.cL_id + "'>" + a.cL_Descripcion_corta + "</option>"
        }), $("#Categorias_lugares_CL_id").html(o)
    })
}), $("#Categorias_lugares_CL_id").change("input", function () {
    $.getJSON("/Lugares/GetallTiposBycategoria_lugares", {
        CL_id: $("#Categorias_lugares_CL_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Tipos_lugares_TL_id").empty(), $("#Tipos_lugares_TL_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.tL_id + "'>" + a.tL_Descripcion_corta + "</option>"
        }), $("#Tipos_lugares_TL_id").html(o)
    })
}), $("#Provincias_Pr_id").change("input", function () {
    $.getJSON("/Lugares/GetallCiudadesbyProvincias", {
        Pr_id: $("#Provincias_Pr_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Ciudades_Ci_id").empty(), $("#Ciudades_Ci_id").html(o), $("#Zonas_Zo_id").empty(), $("#Zonas_Zo_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.ci_id + "'>" + a.ci_Descripcion_corta + "</option>"
        }), $("#Ciudades_Ci_id").html(o)
    })
}), $("#Ciudades_Ci_id").change("input", function () {
    $.getJSON("/Lugares/GetallZonasByCiudades", {
        Ci_id: $("#Ciudades_Ci_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Zonas_Zo_id").empty(), $("#Zonas_Zo_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.zo_id + "'>" + a.zo_Descripcion_corta + "</option>"
        }), $("#Zonas_Zo_id").html(o)
    })
});