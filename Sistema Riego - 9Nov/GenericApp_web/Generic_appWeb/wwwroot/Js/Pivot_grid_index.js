﻿
// DataTable
var table = $('.table').removeAttr('width').DataTable(optTable);


$('.table .filters th').each(function () {

    var title = $('.table thead th').eq($(this).index()).text();
    $(this).html('<input type="text" placeholder="Buscar" />');

});





// Apply the search
table.columns().eq(0).each(function (colIdx) {
    $('input', $('.filters th')[colIdx]).on('keyup change', function () {
        table
            .column(colIdx)
            .search(this.value)
            .draw();
    });
});


function load1() {
    $('#pg').pivotgrid({
        url: 'pivotgrid_data1.json',
        method: 'get',
        pivot: {
            rows: ['Country', 'Category'],
            columns: ['Color'],
            values: [
                { field: 'Price', op: 'sum' },
                { field: 'Discount', op: 'sum' }
            ],
           
            }
        },
       valuePrecision: 0,
        valueStyler: function (value, row, index) {
            if (/Discount$/.test(this.field) && value > 100 && value < 500) {
                return 'background:#D8FFD8'
            }
        }
    })
}