﻿$('#Grupos_causas_GCau_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_causas_GCau_id";
    var GCau_id = $(ddlsource).val();
    if (GCau_id !== "Seleccione") {
        $.getJSON("/Causas/GetallCategoria_causasBygrupo", { GCau_id: $(ddlsource).val() }, function (data) {

            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#CCau_id").empty();
            $("#Tipos_causas_TCau_id").empty();
            $('#Tipos_causas_TCau_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cCau_id + "'>" + modelo.cCau_Descripcion_corta + "</option>";
            });
            $('#Categorias_causas_CCau_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_causas_CCau_id').html(items);
        $('#Tipos_causas_TCau_id').html(items);
        
    }
});


$('#Categorias_causas_CCau_id').change('input', function () {
    var ddlsource = "#Categorias_causas_CCau_id";
    var CCau_id = $(ddlsource).val();
    if (CCau_id !== "Seleccione") {
        $.getJSON("/Causas/GetallTipos_causasBycategoria", { CCau_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_causas_TCau_id").empty();
            $("#Tipos_causas_TCau_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tCau_id + "'>" + modelo.tCau_Descripcion_corta + "</option>";
            });
            $('#Tipos_causas_TCau_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_causas_TCau_id').html(items);
    }
});

