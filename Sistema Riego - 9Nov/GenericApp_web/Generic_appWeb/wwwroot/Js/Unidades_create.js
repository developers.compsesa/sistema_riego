$('#Grupos_unidades_GUn_id').change(function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_unidades_GUn_id";
    var GUn_id = $(ddlsource).val();
    if (GUn_id !== "Seleccione") {
        $.getJSON("/Unidades/GetallCategoriasBygrupo_unidades", { GUn_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_unidades.CUn_id").empty();
            $("#Categorias_unidades.CUn_id").html(items);
            $("#Tipos_unidades_TUn_id").empty();
            $("#Tipos_unidades_TUn_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cUn_id + "'>" + modelo.cUn_Descripcion_larga + "</option>";
            });
            $('#Categorias_unidades_CUn_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_unidades_CUn_id').html(items);
        $('#Tipos_unidades_TUn_id').html(items);
       
    }
});


$('#Categorias_unidades_CUn_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallTiposBycategoria";
    var ddlsource = "#Categorias_unidades_CUn_id";
    var CUn_id = $(ddlsource).val();
    if (CUn_id !== "Seleccione") {
        $.getJSON("/Unidades/GetallTiposBycategoria_unidades", { CUn_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_unidades_TUn_id").empty();
            $("#Tipos_unidades_TUn_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tUn_id + "'>" + modelo.tUn_Descripcion_larga + "</option>";
            });
            $('#Tipos_unidades_TUn_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_unidades_TUn_id').html(items);
    }
});



