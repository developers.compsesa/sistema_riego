﻿$('#Grupos_unidades_GUn_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_Unidades_GUn_id";
    var GUn_id = $(ddlsource).val();
    if (GUn_id !== "Seleccione") {
        $.getJSON("/Unidades/GetallCategoria_unidadesBygrupo", { GUn_id: $(ddlsource).val() }, function (data) {

            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#CUn_id").empty();
            $("#Tipos_unidades_TUn_id").empty();
            $('#Tipos_unidades_TUn_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cUn_id + "'>" + modelo.cUn_Descripcion_corta + "</option>";
            });
            $('#Categorias_unidades_CUn_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_unidades_CUn_id').html(items);
        $('#Tipos_unidades_TUn_id').html(items);
        
    }
});


$('#Categorias_unidades_CUn_id').change('input', function () {
    var ddlsource = "#Categorias_unidades_CUn_id";
    var CUn_id = $(ddlsource).val();
    if (CUn_id !== "Seleccione") {
        $.getJSON("/Unidades/GetallTipos_unidadesBycategoria", { CUn_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_unidades_TUn_id").empty();
            $("#Tipos_unidades_TUn_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tUn_id + "'>" + modelo.tUn_Descripcion_corta + "</option>";
            });
            $('#Tipos_unidades_TUn_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_unidades_TUn_id').html(items);
    }
});

