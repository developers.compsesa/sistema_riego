﻿
$('#Grupos_motivos_GM_id').change(function () {
   
    var ddlsource = "#Grupos_motivos_GM_id";
    var GM_id = $(ddlsource).val();
    if (GM_id != "Seleccione") {
        $.getJSON("/Categorias_motivos/GetallCategoriasMotivosBygrupo", { GM_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>"
            $("#Categorias_motivos_CM_id").empty();
            $('#Categorias_motivos_CM_id').html(items);
            $("#Tipos_motivos_TMot_id").empty();
            $('#Tipos_motivos_TMot_id').html(items);

        

            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cM_id + "'>" + modelo.cM_Descripcion_larga + "</option>";
            });
            $('#Categorias_motivos_CM_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#Categorias_motivos_CM_id').html(items);
        $('#Tipos_motivos_TMot_id').html(items);
    }
});


$('#Categorias_motivos_CM_id').change('input', function () {
   
    var ddlsource = "#Categorias_motivos_CM_id";
    var CM_id = $(ddlsource).val();
    if (CM_id != "Seleccione") {
        $.getJSON("/Tipos_motivos/GetallTiposMotivosByCategorias", { CM_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_motivos_TMot_id").empty();
            $('#Tipos_motivos_TMot_id').html(items);
          
           
           
            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tMot_id + "'>" + modelo.tMot_Descripcion_larga + "</option>";
            });
            $('#Tipos_motivos_TMot_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#Tipos_motivos_TMot_id').html(items);
    }
});

