﻿// DataTable
var table = $('.table').removeAttr('width').DataTable(optTable);


$('.table .filters th').each(function () {

    var title = $('.table thead th').eq($(this).index()).text();
    $(this).html('<input type="text" placeholder="Buscar" />');

});

// Apply the search
table.columns().eq(0).each(function (colIdx) {
    $('input', $('.filters th')[colIdx]).on('keyup change', function () {
        table
            .column(colIdx)
            .search(this.value)
            .draw();
    });
});


$('.table tbody').on('click', 'tr', function () {

    if ($(this).hasClass('selected')) {
        $(this).removeClass('selected');
    }
    else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        //console.log(table.row(this).data());
        var rowData = table.row(this).data();

        var codigo_empresa = document.getElementById('activo_codigo_empresa');
        var id = document.getElementById('activo_id');
        var descripcion = document.getElementById('activo_descripcion');

        $(id).val(rowData[0]);
        $(codigo_empresa).val(rowData[1])
        $(descripcion).val(rowData[3]);

    }


});