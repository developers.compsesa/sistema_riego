﻿$('#Grupos_valor_GVal_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_valor_GVal_id";
    var GVal_id = $(ddlsource).val();
    if (GVal_id !== "Seleccione") {
        $.getJSON("/Inventario_valor/GetallCategorias_valorBygrupo", { GVal_id: $(ddlsource).val() }, function (data) {

            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#CVal_id").empty();
            $("#Tipos_valor_TVal_id").empty();
            $('#Tipos_valor_TVal_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cVal_id + "'>" + modelo.cVal_Descripcion_corta + "</option>";
            });
            $('#Categorias_valor_CVal_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_valor_CVal_id').html(items);
        $('#Tipos_valor_TVal_id').html(items);
        
    }
});


$('#Categorias_valor_CVal_id').change('input', function () {
    var ddlsource = "#Categorias_valor_CVal_id";
    var CVal_id = $(ddlsource).val();
    if (CVal_id !== "Seleccione") {
        $.getJSON("/Inventario_valor/GetallTiposBycategoria_valor", { CVal_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_valor_TVal_id").empty();
            $("#Tipos_valor_TVal_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tVal_id + "'>" + modelo.tVal_Descripcion_corta + "</option>";
            });
            $('#Tipos_valor_TVal_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_valor_TVal_id').html(items);
    }
});

