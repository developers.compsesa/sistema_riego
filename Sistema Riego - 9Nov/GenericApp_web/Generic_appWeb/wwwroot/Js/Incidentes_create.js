$('#Grupos_incidentes_GInc_id').change(function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_incidentes_GInc_id";
    var GInc_id = $(ddlsource).val();
    if (GInc_id !== "Seleccione") {
        $.getJSON("/Incidentes/GetallCategoria_incidentesBygrupo", { GInc_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_incidentes_CInc_id").empty();
            $("#Categorias_incidentes_CInc_id").html(items);
            $("#Tipos_incidentes_TInc_id").empty();
            $("#Tipos_incidentes_TInc_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cInc_id + "'>" + modelo.cInc_Descripcion_corta + "</option>";
            });
            $('#Categorias_incidentes_CInc_id').html(items);
            
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_incidentes_CInc_id').html(items);
        $('#Tipos_incidentes_TInc_id').html(items);
    }
});


$('#Categorias_incidentes_CInc_id').change('input', function () {
   
    var ddlsource = "#Categorias_incidentes_CInc_id";
    var CInc_id = $(ddlsource).val();
    if (CInc_id !== "Seleccione") {
        $.getJSON("/Incidentes/GetallTipos_incidentesBycategoria", { CInc_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_incidentes_TInc_id").empty();
            $("#Tipos_incidentes_TInc_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tInc_id + "'>" + modelo.tInc_Descripcion_corta + "</option>";
            });
            $('#Tipos_incidentes_TInc_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_incidentes_TInc_id').html(items);
    }
});



