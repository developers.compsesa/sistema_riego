//created by israel Pinargote 2019 - april
$("#Grupos_marcas_GMa_id").change(function () {
    $.getJSON("/Marcas/GetallCategoriasBygrupo_marcas", {
        GMa_id: $("#Grupos_marcas_GMa_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Categorias_marcas_CMa_id").empty(), $("#Tipos_marcas_TMa_id").empty(), $("#Tipos_marcas_TMa_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.cmA_id + "'>" + a.cmA_Descripcion_corta + "</option>"
        }), $("#Categorias_marcas_CMa_id").html(o)
    })
}), $("#Categorias_marcas_CMa_id").change("input", function () {
    $.getJSON("/Marcas/GetallTiposBycategoria_marcas", {
        CMa_id: $("#Categorias_marcas_CMa_id").val()
    }, function (i) {
        var o = "";
        o = "<option value= 0>Seleccione</option>", $("#Tipos_marcas_TMa_id").empty(), $("#Tipos_marcas_TMa_id").html(o), $.each(i, function (i, a) {
            o += "<option value='" + a.tmA_id + "'>" + a.tmA_Descripcion_corta + "</option>"
        }), $("#Tipos_marcas_TMa_id").html(o)
    })
});