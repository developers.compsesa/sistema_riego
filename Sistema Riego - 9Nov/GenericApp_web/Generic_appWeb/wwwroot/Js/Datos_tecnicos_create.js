$('#Grupos_datos_tecnicos_GDTec_id').change(function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_datos_tecnicos_GDTec_id";
    var GDTec_id = $(ddlsource).val();
    if (GDTec_id !== "Seleccione") {
        $.getJSON("/Inventario_descripciones_datos_tecnicos/GetallCategoriasBygrupo_datos_tecnicos", { GDTec_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_datos_tecnicos.CDTec_id").empty();
            $("#Categorias_datos_tecnicos.CDTec_id").html(items);
            $("#Tipos_datos_tecnicos_TDTec_id").empty();
            $("#Tipos_datos_tecnicos_TDTec_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cdTec_id + "'>" + modelo.cdTec_Descripcion_corta + "</option>";
            });
            $('#Categorias_datos_tecnicos_CDTec_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_datos_tecnicos_CDTec_id').html(items);
        $('#Tipos_datos_tecnicos_TDTec_id').html(items);
       
    }
});


$('#Categorias_datos_tecnicos_CDTec_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallTiposBycategoria";
    var ddlsource = "#Categorias_datos_tecnicos_CDTec_id";
    var CDTec_id = $(ddlsource).val();
    if (CDTec_id !== "Seleccione") {
        $.getJSON("/Inventario_descripciones_datos_tecnicos/GetallTiposBycategoria_datos_tecnicos", { CDTec_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_datos_tecnicos_TDTec_id").empty();
            $("#Tipos_datos_tecnicos_TDTec_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tdTec_id + "'>" + modelo.tdTec_Descripcion_corta + "</option>";
            });
            $('#Tipos_datos_tecnicos_TDTec_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_datos_tecnicos_TDTec_id').html(items);
    }
});





