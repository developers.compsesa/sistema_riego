﻿$('#Marcas_Ma_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallmodelosBymarca";
    var ddlsource = "#Marcas_Ma_id";
    var ma_id = $(ddlsource).val();
    if (ma_id !== "Seleccione") {
        $.getJSON("/Inventario_Marca_Modelos/GetallmodelosBymarca", { Ma_id: $(ddlsource).val() }, function (data) {
            var items = '';
            $("#Modelos_Mo_id").empty();

            items = "<option value= " + 0 + ">" + "Seleccione</option>"
            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.mo_id + "'>" + modelo.mo_Descripcion_corta + "</option>";
            });
            $('#Modelos_Mo_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#Modelos_GA_id').html(items);
    }
});

$('#Grupos_activos_GA_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_activos_GA_id";
    var Ga_id = $(ddlsource).val();
    if (Ga_id !== "Seleccione") {
        $.getJSON("/Activos/GetallCategoriasBygrupo", { GA_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>"
            $("#CA_id").empty();
            //$("#Tipos_activos_TA_Padre_id").empty();
            //$('#Tipos_activos_TA_Padre_id').html(items);

            $("#Tipos_activos_TA_id").empty();
            $('#Tipos_activos_TA_id').html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cA_id + "'>" + modelo.cA_Descripcion_corta + "</option>";
            });
            $('#CA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#CA_id').html(items);
        $('#Tipos_activos_TA_id').html(items);

        $('#A_tipo_activo1_id').html(items);


        $('#A_tipo_activo2_id').html(items);


        $('#A_tipo_activo3_id').html(items);
    }
});


$('#CA_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallTiposBycategoria";
    var ddlsource = "#CA_id";
    var Ca_id = $(ddlsource).val();
    if (Ca_id !== "Seleccione") {
        $.getJSON("/Activos/GetallTiposBycategoria", { CA_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>"

            // $("#Tipos_activos_TA_Padre_id").empty();
            // $('#Tipos_activos_TA_Padre_id').html(items);
            $("#Tipos_activos_TA_id").empty();
            $('#Tipos_activos_TA_id').html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tA_id + "'>" + modelo.tA_Descripcion_corta + "</option>";
            });
            // $('#Tipos_activos_TA_Padre_id').html(items);
            $('#Tipos_activos_TA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        //$('#Tipos_activos_TA_Padre_id').html(items);
        $('#Tipos_activos_TA_id').html(items);

        $('#A_tipo_activo1_id').html(items);


        $('#A_tipo_activo2_id').html(items);


        $('#A_tipo_activo3_id').html(items);

    }
});


$('#Tipos_activos_TA_id').change(function () {
    // var url = '@Url.Content("~/")' + "Activos/GetSubTipos";
    var ddlsource = "#Tipos_activos_TA_id";
    var ta_id = $(ddlsource).val();
    if (ta_id !== "Seleccione") {
        $.getJSON("/Tipos_Activos/GetSubTipos", { ta_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>"

            $("#A_tipo_activo1_id").empty();
            $('#A_tipo_activo1_id').html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tA_id + "'>" + modelo.tA_Descripcion_corta + "</option>";
            });
            $('#A_tipo_activo1_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#A_tipo_activo1_id').html(items);

        $('#A_tipo_activo2_id').html(items);


        $('#A_tipo_activo3_id').html(items);
    }

});



$('#A_tipo_activo1_id').change(function () {
    // var url = '@Url.Content("~/")' + "Activos/GetSubTipos";
    var ddlsource = "#A_tipo_activo1_id";
    var ta_id = $(ddlsource).val();
    if (ta_id !== "Seleccione") {
        $.getJSON("/Tipos_Activos/GetSubTipos", { ta_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>"

            $("#A_tipo_activo2_id").empty();
            $('#A_tipo_activo2_id').html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tA_id + "'>" + modelo.tA_Descripcion_corta + "</option>";
            });
            $('#A_tipo_activo2_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#A_tipo_activo2_id').html(items);

        $('#A_tipo_activo3_id').html(items);
    }

});



$('#A_tipo_activo2_id').change(function () {
    // var url = '@Url.Content("~/")' + "Activos/GetSubTipos";
    var ddlsource = "#A_tipo_activo2_id";
    var ta_id = $(ddlsource).val();
    if (ta_id !== "Seleccione") {
        $.getJSON("/Tipos_Activos/GetSubTipos", { ta_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>"

            $("#A_tipo_activo3_id").empty();
            $('#A_tipo_activo3_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tA_id + "'>" + modelo.tA_Descripcion_corta + "</option>";
            });
            $('#A_tipo_activo3_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#A_tipo_activo3_id').html(items);
    }

});