﻿

$('#Grupos_documentos_GD_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_documentos_GD_id";
    var GD_id = $(ddlsource).val();
    if (GD_id != "Seleccione") {
        $.getJSON("/Categorias_documentos/GetallCategoriasDocumentosBygrupo", { id: GD_id }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>"
            $("#Categorias_documentos_CD_id").empty();
            $("#Tipos_documentos_TD_id").empty();
            

            $("#Categorias_documentos_CD_id").html("<option value= " + 0 + ">" + "Seleccione</option>");
            $("#Tipos_documentos_TD_id").html("<option value= " + 0 + ">" + "Seleccione</option>");

            $.each(data, function (i, item) {
                items += "<option value='" + item.cD_id + "'>" + item.cD_Descripcion_larga + "</option>";
            });
            $('#Categorias_documentos_CD_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#Categorias_documentos_CD_id').html(items);
        $('#Tipos_documentos_TD_id').html(items);

    }
});


$('#Categorias_documentos_CD_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallTiposBycategoria";
    var ddlsource = "#Categorias_documentos_CD_id";
    var CD_id = $(ddlsource).val();
    if (CD_id != "Seleccione") {
        $.getJSON("/Tipos_documentos/GetallTiposDocumentosByCategorias", { CD_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

          
            $("#Tipos_documentos_TD_id").empty();
            $('#Tipos_documentos_TD_id').html(items);
          
  
            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tD_id + "'>" + modelo.tD_Descripcion_larga + "</option>";
            });
            $('#Tipos_documentos_TD_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#Tipos_documentos_TD_id').html(items);
    }
});

