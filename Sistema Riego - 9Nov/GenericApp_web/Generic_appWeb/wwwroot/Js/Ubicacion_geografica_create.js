$('#Pais_Pa_id').change(function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallCategoriasBygrupo";
    var ddlsource = "#Pais_Pa_id";
    var Pa_id = $(ddlsource).val();
    if (Pa_id !== "Seleccione") {
        $.getJSON("/Ubicacion_geografica/GetallProvinciabyPais", { Pa_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Provincias_Pr_id").empty();
            $("#Provincias_Pr_id").html(items);
            $("#Ciudades_Ci_id").empty();
            $("#Ciudades_Ci_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.pr_id + "'>" + modelo.pr_Descripcion_larga + "</option>";
            });
            $('#Provincias_Pr_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Provincias_Pr_id').html(items);
        $('#Ciudades_Ci_id').html(items);
       
    }
});


$('#Provincias_Pr_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallTiposBycategoria";
    var ddlsource = "#Provincias_Pr_id";
    var Pr_id = $(ddlsource).val();
    if (Pr_id !== "Seleccione") {
        $.getJSON("/Ubicacion_geografica/GetallCiudadesbyProvincias", { Pr_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Ciudades_Ci_id").empty();
            $("#Ciudades_Ci_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.ci_id + "'>" + modelo.ci_Descripcion_larga + "</option>";
            });
            $('#Ciudades_Ci_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Ciudades_Ci_id').html(items);
    }
});



