﻿$('#Grupos_ordenes_trabajo_GOTrab_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_ordenes_trabajo_GOTrab_id";
    var GOTrab_id = $(ddlsource).val();
    if (GOTrab_id !== "Seleccione") {
        $.getJSON("/Ordenes_trabajo/GetallCategoria_ordenes_trabajoBygrupo", { GOtrab_id: $(ddlsource).val() }, function (data) {

            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#COTrab_id").empty();
            $("#Tipos_ordenes_trabajo_TOTrab_id").empty();
            $('#Tipos_ordenes_trabajo_TOTrab_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.coTrab_id + "'>" + modelo.coTrab_Descripcion_corta + "</option>";
            });
            $('#Categorias_ordenes_trabajo_COTrab_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_ordenes_trabajo_COTrab_id').html(items);
        $('#Tipos_ordenes_trabajo_TOTrab_id').html(items);
    }
});


$('#Categorias_ordenes_trabajo_COTrab_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "entidades/GetallTiposBycategoria";
    var ddlsource = "#Categorias_ordenes_trabajo_COTrab_id";
    var COtrab_id = $(ddlsource).val();
    if (COtrab_id !== "Seleccione") {
        $.getJSON("/Ordenes_trabajo/GetallTipos_ordenes_trabajoBycategoria", { COtrab_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_ordenes_trabajo_TOTrab_id").empty();
            $("#Tipos_ordenes_trabajo_TOTrab_id").html("<option value= " + 0 + ">" + "Seleccione</option>");



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.toTrab_id + "'>" + modelo.toTrab_Descripcion_corta + "</option>";
            });
            $('#Tipos_ordenes_trabajo_TOTrab_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_ordenes_trabajo_TOTrab_id').html(items);
    }
});

