﻿$('#Grupos_tecnicos_GTec_id').change(function () {
    //var url = '@Url.Content("~/")' + "tecnicos/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_tecnicos_GTec_id";
    var GTec_id = $(ddlsource).val();
    if (GTec_id !== "Seleccione") {
        $.getJSON("/Categorias_tecnicos/GetallCategorias_tecnicosBygrupo", { GTec_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#Categorias_tecnicos_CTec_id").empty();
            $("#Categorias_tecnicos_CTec_id").html("<option value= " + 0 + ">" + "Seleccione</option>");
            $("#Tipos_tecnicos_TTec_id").empty();
            $("#Tipos_tecnicos_TTec_id").html("<option value= " + 0 + ">" + "Seleccione</option>");



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cTec_id + "'>" + modelo.cTec_Descripcion_corta + "</option>";
            });
            $('#Categorias_tecnicos_CTec_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_tecnicos_CTec_id').html(items);
        $('#Tipos_tecnicos_TTec_id').html(items);
    }
});


$('#Categorias_tecnicos_CTec_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "tecnicos/GetallTiposBycategoria";
    var ddlsource = "#Categorias_tecnicos_CTec_id";
    var CTec_id = $(ddlsource).val();
    if (CTec_id !== "Seleccione") {
        $.getJSON("/Tipos_tecnicos/GetallTipos_tecnicosByCategoria", { CTec_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_tecnicos_TTec_id").empty();
            $("#Tipos_tecnicos_TTec_id").html("<option value= " + 0 + ">" + "Seleccione</option>");



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tTec_id + "'>" + modelo.tTec_Descripcion_corta + "</option>";
            });
            $('#Tipos_tecnicos_TTec_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_tecnicos_TTec_id').html(items);
    }
});

