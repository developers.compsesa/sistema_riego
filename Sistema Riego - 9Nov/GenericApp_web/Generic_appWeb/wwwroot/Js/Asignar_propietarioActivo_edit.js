﻿
//////////////////////////////////////////////////////////////////////created by israel pinargote 2018 ////////////////////////////////////////////////////////////////////////
$('#GA_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#GA_id";
    var Ga_id = $(ddlsource).val();
    if (Ga_id != "Seleccione") {
        $.getJSON("/Activos/GetallCategoriasBygrupo", { GA_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>"
            $("#CA_id").empty();
            $('#CA_id').html(items);
            $("#TA_id").empty();
            $('#TA_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cA_id + "'>" + modelo.cA_Descripcion_larga + "</option>";
            });
            $('#CA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#CA_id').html(items);
    }
});


$('#CA_id').change('input', function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallTiposBycategoria";
    var ddlsource = "#CA_id";
    var Ca_id = $(ddlsource).val();
    if (Ca_id != "Seleccione") {
        $.getJSON("/Activos/GetallTiposBycategoria", { CA_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

           
            $("#TA_id").empty();
            $('#TA_id').html(items);
         
           
            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tA_id + "'>" + modelo.tA_Descripcion_larga + "</option>";
            });
            $('#TA_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#TA_id').html(items);
    }
});


$('#TEn_id').change('input', function () {
   
    var ddlsource = "#TEn_id";
    var Ca_id = $(ddlsource).val();
    if (Ca_id != "Seleccione") {
        $.getJSON("/Entidades/GetallEntidadesByTipo", { id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";


            $("#En_id").empty();
            $('#En_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.en_id + "'>" + modelo.en_Descripcion_larga + "</option>";
            });
            $('#En_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>"
        $('#En_id').html(items);
    }
});
//////////////////////////////////////////////////////////////////////created by israel pinargote 2018 ////////////////////////////////////////////////////////////////////////


    $("#buscar_activos").click(function () {
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };

        var grupo_activo_id = $('#GA_id').val();
        var categoria_activo_id = $('#CA_id').val();
        var tipo_activo_id = $('#TA_id').val();


        $.ajax({
            type: "GET",
            url: '/Activos/PartialIndex_multiSelect',
            data: {
                "grupo_activo_id": grupo_activo_id,
                "categoria_activo_id": categoria_activo_id,
                "tipo_activo_id": tipo_activo_id

            },
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {

                $('#myModalContent_activo').html(data);
                $('#myModal_activo').modal(options);
                $('#myModal_activo').modal('show');
            },
            error: function () {
                alert("Content load failed.");
            }
        });
    });
    var jsonString = [];


$('#ActivosIndexTable').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "pagingType": "full_numbers",
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
            "sInfoEmpty": "No existen registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ líneas)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "paginate": {
                "previous": "Antes",
                "next": "Despues",
                "first": "Primero",
                "last": "Ultimo"
            }
        }
     
    });

  

    $('#elegir_activo').on('click', function () {
        try {
            jsonString = [];
            var arreglo = [];
            oData = table_activo.rows('.selected').data();
            for (var i = 0; i < oData.length; i++) {
                jsonString[i] = oData[i];

                arreglo[i] = jsonString[i][3]
            }




            $('#Arreglo').val(arreglo)
            


            var datatable = $('#ActivosIndexTable').DataTable();

            $('#myModal_activo').modal('hide');

            datatable.clear();
            datatable.rows.add(jsonString);
            datatable.draw();
   
        } catch (e) {

        }

    });
    $('.modal-content').resizable({
        //alsoResize: ".modal-dialog",
        minHeight: 800,
        minWidth: 800
    });
    $('.modal-dialog').draggable();

    $('#myModal_activo').on('show.bs.modal', function () {
        $(this).find('.modal-body').css({
            'max-height': '100%'
        });
    });




