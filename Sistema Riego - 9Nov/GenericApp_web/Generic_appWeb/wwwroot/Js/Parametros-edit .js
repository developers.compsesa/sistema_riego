﻿$('#Grupos_parametros_GPar_id').change(function () {
    //var url = '@Url.Content("~/")' + "Activos/GetallCategoriasBygrupo";
    var ddlsource = "#Grupos_parametros_GPar_id";
    var GPar_id = $(ddlsource).val();
    if (GPar_id !== "Seleccione") {
        $.getJSON("/Inventario_parametros/GetallCategoriasBygrupo_parametros", { GPar_id: $(ddlsource).val() }, function (data) {

            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";
            $("#CPar_id").empty();
            $("#Tipos_parametros_TPar_id").empty();
            $('#Tipos_parametros_TPar_id').html(items);


            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.cPar_id + "'>" + modelo.cPar_Descripcion_larga + "</option>";
            });
            $('#Categorias_parametros_CPar_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Categorias_parametros_CPar_id').html(items);
        $('#Tipos_parametros_TPar_id').html(items);
        
    }
});


$('#Categorias_parametros_CPar_id').change('input', function () {
    var ddlsource = "#Categorias_parametros_CPar_id";
    var CPar_id = $(ddlsource).val();
    if (CPar_id !== "Seleccione") {
        $.getJSON("/Inventario_parametros/GetallTiposBycategoria_parametros", { CPar_id: $(ddlsource).val() }, function (data) {
            var items = '';
            items = "<option value= " + 0 + ">" + "Seleccione</option>";

            $("#Tipos_parametros_TPar_id").empty();
            $("#Tipos_parametros_TPar_id").html(items);



            $.each(data, function (i, modelo) {
                items += "<option value='" + modelo.tPar_id + "'>" + modelo.tPar_Descripcion_larga + "</option>";
            });
            $('#Tipos_parametros_TPar_id').html(items);
        });
    } else {
        items = "<option value= " + 0 + ">" + "Seleccione</option>";
        $('#Tipos_parametros_TPar_id').html(items);
    }
});

