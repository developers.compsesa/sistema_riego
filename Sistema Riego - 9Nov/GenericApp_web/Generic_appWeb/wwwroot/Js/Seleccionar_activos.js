﻿//created by israel pinargote 2018
$(function () {
    $("#modalx").click(function () {
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };
        var grupo_activo_id = $('#Activos_Grupos_activos_GA_id').val();
        var categoria_activo_id = $('#CA_id').val();
        var tipo_activo_id = $('#Tipos_activos_TA_id').val();
        //var tipo_activo_id = $('#Tipos_activos_TA_Padre_id').val();
        //var subtipo_activo_id = $('#Tipos_activos_TA_id').val();

        $.ajax({
            type: "GET",
            url: '/Activos/PartialIndex',
            data: {
                "grupo_activo_id": grupo_activo_id,
                "categoria_activo_id": categoria_activo_id,
                "tipo_activo_id": tipo_activo_id,
                //"subtipo_activo_id": subtipo_activo_id
            },
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {

                $('#myModalContent').html(data);
                $('#myModal_activo').modal(options);
                $('#myModal_activo').modal('show');
            },
            error: function () {
                alert("Content load failed.");
            }
        });
        $('#Codigo_empresa').val('');
        $('#Activos_A_id').val('');
        $('#descripcion_larga').val('');

    });
    //$("#closebtn").on('click',function(){
    //    $('#myModal').modal('hide');


    $('#elegir_activo').on('click', function () {

        try {

            
            var codigo_empresa = document.getElementById('activo_codigo_empresa');
            var id = document.getElementById('activo_id');
            var descripcion = document.getElementById('activo_descripcion');


            $('#Codigo_empresa').val($(codigo_empresa).val());
            $('#Activos_A_id').val($(id).val());
            $('#descripcion_larga').val($(descripcion).val());
            $('#myModal_activo').modal('hide');

            //console.log($(descripcion).val());
        } catch (e) {

        }

    });

    $('.modal-content').resizable({
        //alsoResize: ".modal-dialog",
        minHeight: 800,
        minWidth: 800
    });
    $('.modal-dialog').draggable();

    $('#myModal_activo').on('show.bs.modal', function () {
        $(this).find('.modal-body').css({
            'max-height': '100%'
        });
    });

});