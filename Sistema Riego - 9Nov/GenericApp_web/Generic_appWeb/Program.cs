﻿using Microsoft.AspNetCore;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;

namespace Generic_appWeb
{
    public class Program
    {
        public static void Main(string[] args)
        {
           // BuildWebHost(args).Run();
            CreateWebHostBuilder(args).Build().Run(); // TO DEPLOY IN LINUX SERVER NGINEX 
        }

        //public static IWebHost BuildWebHost(string[] args) =>
        //    WebHost.CreateDefaultBuilder(args)
        //        .UseStartup<Startup>()
        //        .UseUrls("http://0.0.0.0:8000")


        //        .Build();

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
         .ConfigureLogging(logBuilder =>
         {
             logBuilder.ClearProviders(); // removes all providers from LoggerFactory
             logBuilder.AddConsole();
             logBuilder.AddTraceSource("Information, ActivityTracing"); // Add Trace listener provider
         })
        .UseStartup<Startup>()
        .UseUrls("http://0.0.0.0:8000");

        //.UseKestrel(options =>
        //{
        //    options.Listen(IPAddress.Any, 8000);

        //});
    }
}
